package com.eshraqgroup.mint.api.models.tag;

import javax.validation.constraints.NotNull;

/** Created by ahmad on 5/9/17. */
public class TagCreateModel {

  @NotNull(message = "error.tag.name.null")
  private String name;

  private String group;

  public TagCreateModel() {}

  public TagCreateModel(String name, String group) {
    this.name = name;
    this.group = group;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TagCreateModel that = (TagCreateModel) o;

    if (!name.equals(that.name)) {
      return false;
    }
    return group != null ? group.equals(that.group) : that.group == null;
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + (group != null ? group.hashCode() : 0);
    return result;
  }
}
