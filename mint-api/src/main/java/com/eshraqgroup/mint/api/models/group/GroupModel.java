package com.eshraqgroup.mint.api.models.group;

import com.eshraqgroup.mint.api.models.organization.SimpleOrganizationModel;
import com.eshraqgroup.mint.models.SimpleModel;

/** Created by ahmad on 3/7/16. */
public class GroupModel extends GroupCreateModel {

  private Long id;
  private Integer userCount;
  private SimpleOrganizationModel organization;
  private SimpleModel foundation;
  private Integer spaceCount;

  public GroupModel() {}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getUserCount() {
    return userCount;
  }

  public void setUserCount(Integer userCount) {
    this.userCount = userCount;
  }

  public SimpleOrganizationModel getOrganization() {
    return organization;
  }

  public void setOrganization(SimpleOrganizationModel organization) {
    this.organization = organization;
  }

  public Integer getSpaceCount() {
    return spaceCount;
  }

  public void setSpaceCount(Integer spaceCount) {
    this.spaceCount = spaceCount;
  }

  public SimpleModel getFoundation() {
    return foundation;
  }

  public void setFoundation(SimpleModel foundation) {
    this.foundation = foundation;
  }

  @Override
  public String toString() {
    return "GroupModel{"
        + "id="
        + id
        + ", userCount="
        + userCount
        + ", organization="
        + organization
        + ", spaceCount="
        + spaceCount
        + "} "
        + super.toString();
  }
}
