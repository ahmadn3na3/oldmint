package com.eshraqgroup.mint.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = {"com.eshraqgroup.mint", "com.eshraqgroup.mint.api"})
@EnableJpaRepositories(basePackages = {"com.eshraqgroup.mint.repos.jpa"})
@EntityScan(basePackages = {"com.eshraqgroup.mint.domain"})
@EnableMongoRepositories(basePackages = {"com.eshraqgroup.mint.repos.mongo"})
public class MintApplication {

  public static void main(String[] args) {
    SpringApplication.run(MintApplication.class, args);
  }
}
