package com.eshraqgroup.mint.api.configuration;

import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.HashMap;
import java.util.Map;
import org.dozer.CustomConverter;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.eshraqgroup.mint.api.models.assessment.AssessmentCreateModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentQuestionCreateModel;
import com.eshraqgroup.mint.api.models.foundation.FoundationModel;
import com.eshraqgroup.mint.api.models.group.GroupModel;
import com.eshraqgroup.mint.api.models.organization.OrganizationModel;
import com.eshraqgroup.mint.api.models.role.RoleModel;
import com.eshraqgroup.mint.api.models.space.response.SpaceListingModel;
import com.eshraqgroup.mint.domain.jpa.Assessment;
import com.eshraqgroup.mint.domain.jpa.AssessmentQuestion;
import com.eshraqgroup.mint.domain.jpa.Content;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Groups;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Role;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.models.content.ContentModel;

/** Created by ahmad on 2/18/16. */
@Configuration
public class AppConfiguration {

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean(destroyMethod = "destroy")
  public Mapper getMapper() {
    DozerBeanMapper beanMapper = new DozerBeanMapper();
    BeanMappingBuilder builder =
        new BeanMappingBuilder() {
          @Override
          protected void configure() {
            mapping(Organization.class, OrganizationModel.class)
                .exclude("endDate")
                .exclude("startDate")
                .exclude("deletedDate")
                .exclude("creationDate")
                .exclude("lastModifiedDate");
            mapping(Foundation.class, FoundationModel.class)
                .exclude("creationDate")
                .exclude("lastModifiedDate");
            mapping(Space.class, SpaceListingModel.class)
                .exclude("creationDate")
                .exclude("lastModified");
            mapping(Role.class, RoleModel.class)
                .exclude("permission")
                .exclude("creationDate")
                .exclude("lastModifiedDate")
                .exclude("lastModifiedDate")
                .exclude("choicesList");
            mapping(Assessment.class, AssessmentModel.class)
                .exclude("creationDate")
                .exclude("lastModifiedDate")
                .exclude("dueDate");
            mapping(Assessment.class, AssessmentCreateModel.class)
                .exclude("creationDate")
                .exclude("lastModifiedDate")
                .exclude("dueDate");
            mapping(Groups.class, GroupModel.class).exclude("tags").exclude("canAccess");
            mapping(Content.class, ContentModel.class)
                .exclude("tags")
                .exclude("lastModifiedDate")
                .exclude("creationDate");
            mapping(AssessmentQuestionCreateModel.class, AssessmentQuestion.class)
                .exclude("assessment")
                .exclude("question")
                .exclude("creationDate")
                .exclude("lastModifiedDate")
                .exclude("dueDate");
            // mapping(SpaceCreateModel.class,
            // SpaceCreateModel.class).exclude("creationDate").exclude("lastModified");
            // mapping(Role.class, RoleModel.class)
            // .fields("permission", "permission",
            // FieldsMappingOptions.customConverter(PermissionConverter.class));
            mapping(
                ZonedDateTime.class,
                ZonedDateTime.class,
                TypeMappingOptions.mapId("zonedDateTime"));
            // mapping(User.class,UserInfoModel.class).exclude("creationDate").exclude("lastModifiedDate").exclude("birthDate").exclude("activationDate");

          }
        };

    beanMapper.addMapping(builder);
    Map<String, CustomConverter> customConverterMap = new HashMap<>();
    customConverterMap.put("zonedDateTime", new ZonedDateTimeConverter());
    beanMapper.setCustomConvertersWithId(customConverterMap);
    return beanMapper;
  }

  private static class ZonedDateTimeConverter implements CustomConverter {

    @Override
    public Object convert(Object o, Object o1, Class<?> aClass, Class<?> aClass1) {
      if (o == null) {
        return null;
      }
      return ZonedDateTime.from((TemporalAccessor) o);
    }
  }
}
