package com.eshraqgroup.mint.api.services;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eshraqgroup.mint.api.models.report.ReportModel;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.domain.mongo.Report;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.SpaceRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.mongo.ReportRepository;
import com.eshraqgroup.mint.security.SecurityUtils;

/** Created by ahmad on 5/26/16. */
@Service
public class ReportService {
  private final Logger log = LoggerFactory.getLogger(ReportService.class);

  private final ReportRepository reportRepository;

  private final UserRepository userRepository;

  private final SpaceRepository spaceRepository;

  private final JoinedRepository joinedRepository;

  private final Mapper mapper;

  @Autowired
  public ReportService(
      ReportRepository reportRepository,
      UserRepository userRepository,
      SpaceRepository spaceRepository,
      JoinedRepository joinedRepository,
      Mapper mapper) {
    this.reportRepository = reportRepository;
    this.userRepository = userRepository;
    this.spaceRepository = spaceRepository;
    this.joinedRepository = joinedRepository;
    this.mapper = mapper;
  }

  @Auditable(EntityAction.REPORT_POST)
  public ResponseModel postReport(ReportModel reportModel) {

    log.debug("post a report: ", reportModel.getReportDate());

    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              if (reportModel.getSpaceId() != null) {
                Space space = spaceRepository.findOne(reportModel.getSpaceId());
                if (space != null) {
                  joinedRepository
                      .findOneByUserIdAndSpaceIdAndDeletedFalse(user.getId(), space.getId())
                      .map(
                          joined -> {
                            if (!(joined.getSpaceRole().equals(SpaceRole.OWNER)
                                || joined.getSpaceRole().equals(SpaceRole.CO_OWNER))) {
                              return save(reportModel);
                            } else {
                              throw new NotPermittedException();
                            }
                          })
                      .orElseThrow(NotPermittedException::new);
                } else {
                  throw new NotFoundException();
                }
              }
              return save(reportModel);
            })
        .orElseThrow(NotPermittedException::new);
  }

  private ResponseModel save(ReportModel reportModel) {
    Report report = mapper.map(reportModel, Report.class);
    reportRepository.save(report);
    log.debug("Report posted");
    return ResponseModel.done();
  }
}
