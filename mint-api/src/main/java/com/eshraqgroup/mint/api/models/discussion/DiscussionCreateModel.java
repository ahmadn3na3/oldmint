package com.eshraqgroup.mint.api.models.discussion;

/** Created by ayman on 25/08/16. */
public class DiscussionCreateModel {

  private String title;
  private String body;
  private String resourceUrl;
  private Long spaceId;
  private Long contentId;

  public DiscussionCreateModel() {}

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getResourceUrl() {
    return resourceUrl;
  }

  public void setResourceUrl(String resourceUrl) {
    this.resourceUrl = resourceUrl;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  public Long getContentId() {
    return contentId;
  }

  public void setContentId(Long contentId) {
    this.contentId = contentId;
  }

  @Override
  public String toString() {
    return "DiscussionCreateModel{"
        + "title='"
        + title
        + '\''
        + ", body='"
        + body
        + '\''
        + ", resourceUrl='"
        + resourceUrl
        + '\''
        + ", spaceId="
        + spaceId
        + '}';
  }
}
