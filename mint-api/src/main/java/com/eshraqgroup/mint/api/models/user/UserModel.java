package com.eshraqgroup.mint.api.models.user;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.map.HashedMap;
import com.eshraqgroup.mint.api.models.organization.SimpleOrganizationModel;
import com.eshraqgroup.mint.api.models.timelock.TimeModel;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.models.SimpleModel;

/** Created by ahmad on 2/17/16. */
public class UserModel extends UserInfoModel {
  private SimpleOrganizationModel organization;
  private SimpleModel foundation;
  private List<SimpleModel> roles;
  private List<SimpleModel> groups;
  private Long spacesCount = 0L;
  private Map<String, List<TimeModel>> dayModels = new HashedMap();

  public UserModel(User user) {
    super(user);
    if (user.getOrganization() != null) {
      this.organization =
          new SimpleOrganizationModel(
              user.getOrganization().getId(),
              user.getOrganization().getName(),
              user.getOrganization().getOrgId());
    }
    if (user.getFoundation() != null) {
      this.foundation =
          new SimpleModel(user.getFoundation().getId(), user.getFoundation().getName());
    }

    this.roles =
        user.getRoles()
            .stream()
            .map(role -> new SimpleModel(role.getId(), role.getName()))
            .collect(Collectors.toList());
    this.groups =
        user.getGroups()
            .stream()
            .map(groups -> new SimpleModel(groups.getId(), groups.getName()))
            .collect(Collectors.toList());
    this.spacesCount = user.getSpaces().stream().filter(space -> !space.isDeleted()).count();
  }

  public SimpleOrganizationModel getOrganization() {
    return organization;
  }

  public void setOrganization(SimpleOrganizationModel organization) {
    this.organization = organization;
  }

  public SimpleModel getFoundation() {
    return foundation;
  }

  public void setFoundation(SimpleModel foundation) {
    this.foundation = foundation;
  }

  public List<SimpleModel> getRoles() {
    return roles;
  }

  public void setRoles(List<SimpleModel> roles) {
    this.roles = roles;
  }

  public List<SimpleModel> getGroups() {
    return groups;
  }

  public void setGroups(List<SimpleModel> groups) {
    this.groups = groups;
  }

  public Long getSpacesCount() {
    return spacesCount;
  }

  public void setSpacesCount(Long spacesCount) {
    this.spacesCount = spacesCount;
  }

  public Map<String, List<TimeModel>> getDayModels() {
    return dayModels;
  }

  public void setDayModels(Map<String, List<TimeModel>> dayModels) {
    this.dayModels = dayModels;
  }

  @Override
  public String toString() {
    return "UserModel{"
        + "organization="
        + organization
        + ", foundation="
        + foundation
        + ", roles="
        + roles
        + ", groups="
        + groups
        + "} "
        + super.toString();
  }
}
