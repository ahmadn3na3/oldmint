package com.eshraqgroup.mint.api.services;

import static org.springframework.data.jpa.domain.Specifications.where;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.api.models.account.ChangePasswordModel;
import com.eshraqgroup.mint.api.models.account.RegesiterAccountModel;
import com.eshraqgroup.mint.api.models.timelock.TimeModel;
import com.eshraqgroup.mint.api.models.user.UserCreateModel;
import com.eshraqgroup.mint.api.models.user.UserInfoModel;
import com.eshraqgroup.mint.api.models.user.UserModel;
import com.eshraqgroup.mint.api.models.user.UserSearchModel;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.configuration.notifications.Message;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.LockStatus;
import com.eshraqgroup.mint.constants.PackageType;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.constants.WeekDay;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.CloudPackage;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Permission;
import com.eshraqgroup.mint.domain.jpa.Role;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.exception.ExistException;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.repos.jpa.CloudPackageRepository;
import com.eshraqgroup.mint.repos.jpa.FoundationRepository;
import com.eshraqgroup.mint.repos.jpa.OrganizationRepository;
import com.eshraqgroup.mint.repos.jpa.PermissionRepository;
import com.eshraqgroup.mint.repos.jpa.RoleRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.specifications.UserSpecifications;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.DateConverter;
import com.eshraqgroup.mint.util.RandomUtils;

/**
 * Created by ahmad on 2/17/16.
 *
 * @author ahmad neanaa
 */
@Service
public class AccountService {

  private final Logger log = LoggerFactory.getLogger(AccountService.class);
  private final UserRepository userRepository;

  private final PasswordEncoder passwordEncoder;

  private final RoleRepository roleRepository;

  private final FoundationRepository foundationRepository;

  private final OrganizationRepository organizationRepository;

  private final CloudPackageRepository cloudPackageRepository;

  private final PermissionRepository permissionRepository;

  @Autowired
  public AccountService(
      PasswordEncoder passwordEncoder,
      FoundationRepository foundationRepository,
      RoleRepository roleRepository,
      OrganizationRepository organizationRepository,
      UserRepository userRepository,
      CloudPackageRepository cloudPackageRepository,
      PermissionRepository permissionRepository) {
    this.passwordEncoder = passwordEncoder;
    this.foundationRepository = foundationRepository;
    this.roleRepository = roleRepository;
    this.organizationRepository = organizationRepository;
    this.userRepository = userRepository;
    this.cloudPackageRepository = cloudPackageRepository;
    this.permissionRepository = permissionRepository;
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  @Message(entityAction = EntityAction.USER_REACTIVATE, services = Services.NOTIFICATIONS)
  public ResponseModel resendActivationCode(String mail, String lang) {
    return userRepository
        .findOneByEmailAndDeletedFalse(mail)
        .map(
            user -> {
              if (user.getStatus()) {
                log.warn("invalid , user {} not activated", user.getId());
                throw new MintException(Code.INVALID, "error.email.activated");
              }
              if (!user.getFirstLogin()) {
                throw new NotPermittedException("error.user.disabled");
              }

              user.setActivationKey(RandomUtils.generateActivationKey());
              user.setActivationDate(new Date());
              userRepository.save(user);
              return ResponseModel.done(null, new UserInfoMessage(user, null, lang));
            })
        .orElseThrow(() -> new NotFoundException("error.email.account"));
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  @Message(entityAction = EntityAction.USER_ACTIVATE, services = Services.CHAT)
  public ResponseModel activateRegistration(String key) {
    log.debug("Activating user for activation key {}", key);
    return userRepository
        .findOneByActivationKeyAndDeletedFalse(key)
        .map(
            user -> {
              if (!user.getFirstLogin()) {
                throw new NotPermittedException("error.user.disabled");
              }
              Calendar calendar = Calendar.getInstance();
              calendar.setTime(user.getActivationDate());
              calendar.add(Calendar.HOUR, 24);
              if (new Date().after(calendar.getTime())) {
                throw new MintException(Code.INVALID, "error.activation.code");
              }

              user.setActivationKey(null);
              user.setActivationDate(null);
              user.setStatus(Boolean.TRUE);
              userRepository.save(user);
              log.debug("Activated user: {}", user);
              return ResponseModel.done(null, new UserInfoMessage(user, null, user.getLangKey()));
            })
        .orElseThrow(() -> new MintException(Code.INVALID, "error.activation.code"));
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  public ResponseModel completePasswordReset(String newPassword, String key) {
    log.debug("Reset user password for reset key {}", key);

    return userRepository
        .findOneByResetKeyAndDeletedFalse(key)
        .map(
            user -> {
              Calendar calendar = Calendar.getInstance();
              calendar.setTime(user.getResetDate());
              calendar.add(Calendar.HOUR, 24);
              if (new Date().after(calendar.getTime())) {
                log.warn("reset key {} expired", key);
                throw new MintException(Code.INVALID, "error.reset.code");
              }
              user.setResetDate(null);
              user.setResetKey(null);
              user.setPassword(passwordEncoder.encode(newPassword));
              if (user.getForceChangePassword()) {
                user.setForceChangePassword(Boolean.FALSE);
              }
              userRepository.save(user);
              return ResponseModel.done();
            })
        .orElseThrow(() -> new MintException(Code.INVALID, "error.reset.code"));
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  @Message(entityAction = EntityAction.USER_FORGETPASSOWORD, services = Services.NOTIFICATIONS)
  public ResponseModel requestPasswordReset(String mail, String baseUrl, String lang) {
    log.debug("{} request password reset", mail);
    return userRepository
        .findOneByEmailAndDeletedFalse(mail)
        .map(
            user -> {
              if (!user.getStatus()) {
                log.warn("user {} not activated", user.getUserName());
                throw new MintException(Code.NOT_ACTIVATED);
              }
              user.setResetKey(RandomUtils.generateResetKey());
              user.setResetDate(Date.from(ZonedDateTime.now(ZoneOffset.UTC).toInstant()));
              userRepository.save(user);
              log.debug("password reset , and sent to {}", mail);
              return ResponseModel.done(null, new UserInfoMessage(user, null, lang));
            })
        .orElseThrow(() -> new NotFoundException("email"));
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  public ResponseModel checkResetCode(String code) {
    log.debug("check reset code {} ", code);
    return userRepository
        .findOneByResetKeyAndDeletedFalse(code)
        .map(
            user -> {
              if (ZonedDateTime.now(ZoneOffset.UTC)
                  .isAfter(
                      ZonedDateTime.ofInstant(user.getResetDate().toInstant(), ZoneOffset.UTC)
                          .plusHours(24))) {
                log.warn("code {} is invalid ", code);
                throw new MintException(Code.INVALID, "error.reset.code");
              }
              return ResponseModel.done();
            })
        .orElseThrow(() -> new NotFoundException("error.reset.code"));
  }

  @Transactional
  @Auditable(EntityAction.USER_CREATE)
  @Message(entityAction = EntityAction.USER_REGISTER, services = Services.NOTIFICATIONS)
  public synchronized ResponseModel createUserInformation(
      RegesiterAccountModel userCreateModel, String baseUrl, String lang) {
    log.debug("create user information:{}", userCreateModel);
    if (userRepository
        .findOneByUserNameAndDeletedFalse(userCreateModel.getUsername())
        .isPresent()) {
    throw new ExistException(userCreateModel.getUsername());
    }
    if (userRepository.findOneByEmailAndDeletedFalse(userCreateModel.getEmail()).isPresent()) {
    	throw new ExistException(userCreateModel.getEmail());
    }
    
    User newUser = new User();

    String encryptedPassword = passwordEncoder.encode(userCreateModel.getPassword());
    // new user gets initially a generated password
    newUser.setPassword(encryptedPassword);
    newUser.setFullName(userCreateModel.getFullName());
    newUser.setUserName(userCreateModel.getUsername());
    newUser.setEmail(userCreateModel.getEmail());
    newUser.setLangKey(userCreateModel.getLang());
    // new user is not active
    newUser.setStatus(false);
    newUser.setFirstLogin(Boolean.TRUE);
    // new user gets registration key
    newUser.setActivationKey(RandomUtils.generateActivationKey());
    newUser.setActivationDate(new Date());
    newUser.setType(UserType.USER);
    newUser.setBirthDate(DateConverter.convertZonedDateTimeToDate(userCreateModel.getBirthDate()));
    newUser.setCountry(userCreateModel.getCountry());
    newUser.setMobile(userCreateModel.getMobile());
    newUser.setGender(userCreateModel.getGender().getValue());

    CloudPackage cloudPackage =
        cloudPackageRepository.findByPackageTypeAndNameAndDeletedFalse(
            PackageType.STANDARD, PackageType.STANDARD.name());
    if (userCreateModel.getPackageId() != null && !userCreateModel.getPackageId().equals(0L)) {
      cloudPackage = cloudPackageRepository.findOne(userCreateModel.getPackageId());
    }
    if (cloudPackage == null) {
      throw new MintException(Code.INVALID, "error.cloud.package");
    }
    newUser.setCloudPackage(cloudPackage);
    userRepository.save(newUser);
    log.debug("Created Information for User: {}", newUser);

    return ResponseModel.done(
        "Please check your mail to complete activation process",
        new UserInfoMessage(newUser, null, lang));
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  @Message(entityAction = EntityAction.USER_UPDATE, services = Services.NOTIFICATIONS)
  public ResponseModel updateUserInformation(UserCreateModel userUpdateModel) {
    log.debug("update user information: {}", userUpdateModel);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(u -> getUserUpdateResponseModel(userUpdateModel, u))
        .orElseThrow(() -> new NotFoundException("user"));
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  public ResponseModel changePassword(ChangePasswordModel password) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            u -> {
              String oldPass = passwordEncoder.encode(password.getOldPassword());

              if (!passwordEncoder.matches(password.getOldPassword(), u.getPassword())) {
                throw new MintException(Code.INVALID, "error.password.old.invalid");
              }

              String encryptedPassword = passwordEncoder.encode(password.getPassword());
              u.setPassword(encryptedPassword);
              u.setFirstLogin(Boolean.FALSE);
              u.setForceChangePassword(Boolean.FALSE);
              userRepository.save(u);
              log.debug("Changed password for User: {}", u);
              return ResponseModel.done();
            })
        .orElseThrow(() -> new NotFoundException("user"));
  }

  @Transactional(readOnly = true)
  // TODO: Recreate Permission For
  // @PreAuthorize("hasAuthority('USER_READ')")
  public ResponseModel getUserWithAuthorities(Long id) {
    log.debug("get user {} with authorities", id);
    User user = userRepository.findOne(id);
    if (user == null) {
      log.warn("user {} not found", id);
      throw new NotFoundException("user");
    }
    UserInfoModel userInfoModel = getUserInfoWithPermission(user, false);
    return ResponseModel.done(userInfoModel);
  }

  @Transactional(readOnly = true)
  public ResponseModel getUserWithAuthorities() {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(user -> ResponseModel.done(getUserInfoWithPermission(user, true)))
        .orElseThrow(() -> new NotFoundException("user"));
  }

  private UserInfoModel getUserInfoWithPermission(User user, boolean includePermission) {
    log.debug("get user {} information with permission {}", user.getUserName(), includePermission);
    UserModel userInfoModel = new UserModel(user);

    if (includePermission) {
      if (user.getType() == UserType.SYSTEM_ADMIN || user.getType() == UserType.SUPER_ADMIN) {
        permissionRepository
            .findByTypeInAndDeletedFalse(
                Arrays.asList(
                    UserType.SYSTEM_ADMIN,
                    UserType.FOUNDATION_ADMIN,
                    UserType.ADMIN,
                    UserType.USER))
            .forEach(
                permission -> {
                  byte val = permission.getCode().byteValue();
                  if (!userInfoModel.getPermissions().containsKey(permission.getKeyCode())) {
                    userInfoModel.getPermissions().put(permission.getKeyCode(), val);
                  } else if ((Integer.valueOf(
                                  userInfoModel
                                      .getPermissions()
                                      .get(permission.getKeyCode())
                                      .toString())
                              .byteValue()
                          & val)
                      != val) {
                    userInfoModel
                        .getPermissions()
                        .put(
                            permission.getKeyCode(),
                            Integer.valueOf(
                                        userInfoModel
                                            .getPermissions()
                                            .get(permission.getKeyCode())
                                            .toString())
                                    .byteValue()
                                | val);
                  }
                });
      } else if (user.getCloudPackage() != null) {
        user.getCloudPackage()
            .getPermission()
            .forEach(
                (key, value) -> {
                  byte val = value;
                  if (!userInfoModel.getPermissions().containsKey(key)) {
                    userInfoModel.getPermissions().put(key, val);
                  } else if ((Integer.valueOf(userInfoModel.getPermissions().get(key).toString())
                              .byteValue()
                          & val)
                      != val) {
                    userInfoModel
                        .getPermissions()
                        .put(
                            key,
                            Integer.valueOf(userInfoModel.getPermissions().get(key).toString())
                                    .byteValue()
                                | val);
                  }
                });

      } else {
        Supplier<Stream<Permission>> streamSupplier =
            () ->
                permissionRepository.findByModuleInAndDeletedFalse(
                    user.getFoundation().getFoundationPackage().getModules());
        user.getRoles()
            .forEach(
                role ->
                    role.getPermission()
                        .forEach(
                            (key, value) ->
                                streamSupplier
                                    .get()
                                    .filter(
                                        permission -> permission.getKeyCode().equalsIgnoreCase(key))
                                    .findFirst()
                                    .ifPresent(
                                        permission -> {
                                          byte val = value;
                                          if (!userInfoModel.getPermissions().containsKey(key)) {
                                            userInfoModel.getPermissions().put(key, val);
                                          } else if (((byte) userInfoModel.getPermissions().get(key)
                                                  & val)
                                              != val) {
                                            userInfoModel
                                                .getPermissions()
                                                .put(
                                                    key,
                                                    (byte) userInfoModel.getPermissions().get(key)
                                                        | val);
                                          }
                                        })));
      }
      Arrays.stream(SpaceRole.values())
          .forEach(
              spaceRole -> {
                Map<String, Object> objectMap =
                    userInfoModel.getSpaceRolePermission().getOrDefault(spaceRole, new HashMap<>());
                spaceRole
                    .getPermissions()
                    .forEach(
                        (k, v) -> {
                          byte val =
                              (byte)
                                  (userInfoModel.getPermissions().containsKey(k)
                                      ? Byte.valueOf(String.valueOf(v))
                                          & Byte.valueOf(
                                              userInfoModel.getPermissions().get(k).toString())
                                      : 0);
                          objectMap.put(k, val);
                        });
                userInfoModel.getSpaceRolePermission().put(spaceRole, objectMap);
              });
    }

    if (user.getTimeLock() != null && !user.getTimeLock().isDeleted() && includePermission) {
      Calendar date = Calendar.getInstance();
      WeekDay weekDay = WeekDay.valueOf(date.get(Calendar.DAY_OF_WEEK));
      getTimeLockWeek(user, userInfoModel, date.getTime(), weekDay, false);
    }
    return userInfoModel;
  }

  private void getTimeLockWeek(
      User user, UserModel userInfoModel, Date date, WeekDay currentDay, boolean next) {
    final boolean[] isException = new boolean[] {false};
    log.debug("time lock date to validate {}", date);
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(date.getTime());
    calendar.set(Calendar.HOUR, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    if (next) {
      calendar.add(Calendar.DAY_OF_MONTH, 1);
      if (calendar.get(Calendar.DAY_OF_WEEK) == currentDay.getDay()) {
        return;
      }
    }
    String weekDay = String.format("%1$td-%1$tm-%1$tY", calendar);
    if ((calendar.getTimeInMillis() >= user.getTimeLock().getFromDate().getTime())
        && (calendar.getTimeInMillis() <= user.getTimeLock().getToDate().getTime())) {

      user.getTimeLock()
          .getTimeLockExceptions()
          .stream()
          .filter(
              e ->
                  (calendar.getTimeInMillis() >= e.getFromDate().getTime())
                      && (calendar.getTimeInMillis() <= e.getToDate().getTime()))
          .forEach(
              e -> {
                if (e.getLockStatus() == LockStatus.LOCK) {
                  if (weekDay != null) {
                    List<TimeModel> timeModels =
                        userInfoModel.getDayModels().getOrDefault(weekDay, new ArrayList<>());
                    timeModels.add(new TimeModel(e.getFromTime(), e.getToTime()));
                  }
                } else {
                  userInfoModel.getDayModels().put(weekDay, Collections.emptyList());
                }
                isException[0] = true;
              });
      if (!isException[0]) {
        user.getTimeLock()
            .getDays()
            .entrySet()
            .stream()
            .filter(entry -> entry.getKey() == WeekDay.valueOf(calendar.get(Calendar.DAY_OF_WEEK)))
            .findFirst()
            .ifPresent(
                weekDayStringEntry ->
                    userInfoModel
                        .getDayModels()
                        .put(
                            weekDay,
                            Arrays.stream(weekDayStringEntry.getValue().split(","))
                                .map(
                                    s -> {
                                      String[] time = s.split(">");
                                      return new TimeModel(time[0], time[1]);
                                    })
                                .collect(Collectors.toList())));
      }
    }
    getTimeLockWeek(user, userInfoModel, calendar.getTime(), currentDay, true);
  }

  private ResponseModel getUserUpdateResponseModel(UserCreateModel userUpdateModel, User user) {
    log.debug("map user from user create model");
    user.setBirthDate(DateConverter.convertZonedDateTimeToDate(userUpdateModel.getBirthDate()));
    user.setFullName(userUpdateModel.getFullName());
    user.setCountry(userUpdateModel.getCountry());
    user.setProfession(userUpdateModel.getProfession());
    user.setMobile(userUpdateModel.getMobile());
    user.setInterests(userUpdateModel.getInterests());
    user.setUserStatus(userUpdateModel.getUserStatus());
    user.setThumbnail(userUpdateModel.getImage());
    user.setLangKey(userUpdateModel.getLang());
    user.setGender(userUpdateModel.getGender().getValue());
    user.setMailNotification(userUpdateModel.getEmailNotification());
    user.setNotification(userUpdateModel.getNotification());
    userRepository.save(user);
    log.debug("Changed Information for User: {}", user);
    return ResponseModel.done(null, new UserInfoMessage(user));
  }

  @Transactional
  @PreAuthorize("hasAuthority('USER_READ') AND hasAuthority('ADMIN')")
  public ResponseModel searchUser(UserSearchModel userSearchModel) {
    log.debug("search for user by specification", userSearchModel);

    Specification<User> byUserType = null;
    Specification<User> byRoleId = null;
    Specification<User> byOrganization = null;
    Specification<User> byFoundation = null;

    if (userSearchModel.getUserType() == null
        && userSearchModel.getFoundationId() == null
        && userSearchModel.getRoleId() == null
        && userSearchModel.getOrganizationId() == null) {
      return ResponseModel.done(
          userRepository.findAll().stream().map(UserModel::new).collect(Collectors.toList()));
    } else {
      if (userSearchModel.getUserType() != null) {
        byUserType = UserSpecifications.hasUserType(userSearchModel.getUserType());
      }

      if (userSearchModel.getRoleId() != null) {
        Optional<Role> rol = roleRepository.findOneByIdAndDeletedFalse(userSearchModel.getRoleId());
        if (rol.isPresent()) {
          byRoleId = UserSpecifications.hasRole(rol.get());
        } else {
          log.warn("role {} not found", userSearchModel.getRoleId());
          throw new NotFoundException("role");
        }
      }

      if (userSearchModel.getOrganizationId() != null) {
        Optional<Organization> org =
            organizationRepository.findOneByIdAndDeletedFalse(userSearchModel.getOrganizationId());
        if (org.isPresent()) {
          byOrganization = UserSpecifications.inOrganization(org.get());
        } else {
          log.warn("organization {} not found", userSearchModel.getOrganizationId());
          throw new NotFoundException("organization");
        }
      }

      if (userSearchModel.getFoundationId() != null) {
        Optional<Foundation> foundationIns =
            foundationRepository.findOneByIdAndDeletedFalse(userSearchModel.getFoundationId());
        if (foundationIns.isPresent()) {
          byFoundation = UserSpecifications.inFoundation(foundationIns.get());
        } else {
          log.warn("foundation {} not found", userSearchModel.getFoundationId());
          throw new NotFoundException("role");
        }
      }

      return ResponseModel.done(
          userRepository
              .findAll(
                  where(byUserType)
                      .and(byRoleId)
                      .and(byOrganization)
                      .and(byFoundation)
                      .and(UserSpecifications.notDeleted()))
              .stream()
              .map(UserModel::new)
              .collect(Collectors.toList()));
    }
  }
}
