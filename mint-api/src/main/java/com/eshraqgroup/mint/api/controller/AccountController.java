package com.eshraqgroup.mint.api.controller;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.api.models.account.ChangePasswordModel;
import com.eshraqgroup.mint.api.models.account.KeyAndPasswordDTO;
import com.eshraqgroup.mint.api.models.account.RegesiterAccountModel;
import com.eshraqgroup.mint.api.models.account.ResetPasswordModel;
import com.eshraqgroup.mint.api.models.user.UserCreateModel;
import com.eshraqgroup.mint.api.services.AccountService;
import com.eshraqgroup.mint.api.services.UploadService;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.CategoryRepository;
import io.swagger.annotations.ApiOperation;

/** REST controller for managing the current user's account. */
@RestController
@RequestMapping("/api")
public class AccountController {

  private final Logger log = LoggerFactory.getLogger(AccountController.class);

  @Autowired private AccountService accountService;

  @Autowired private UploadService uploadService;

  @Autowired private CategoryRepository categoryRepository;

  @Value("${mint.version}")
  private String version;

  /** POST /register -> register the user. */
  @RequestMapping(
    value = "/register",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(
    value = "Register new account",
    notes = "this method is used to register for a new account",
    response = ResponseModel.class
  )
  public ResponseModel registerAccount(
      @Valid @RequestBody RegesiterAccountModel userDTO, HttpServletRequest request) {
    String baseUrl =
        request.getScheme()
            + // "http"
            "://"
            + // "://"
            request.getServerName()
            + // "myhost"
            ":"
            + // ":"
            request.getServerPort()
            + // "80"
            request.getContextPath();
    String lang =
        request.getHeader("lang") != null ? request.getHeader("lang") : "en"; // "/myContextPath"
    // or "" if
    // deployed
    // in root
    // context

    return accountService.createUserInformation(userDTO, baseUrl, lang);
  }

  @RequestMapping(
    value = "/reactivate",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(
    value = "Resend Activtion Code",
    notes = "This method is used to resend activation code to registered users",
    response = ResponseModel.class
  )
  public ResponseModel resendActivationMail(
      @RequestBody @Validated ResetPasswordModel model,
      @RequestHeader(required = false, defaultValue = "en") String lang) {
    return accountService.resendActivationCode(model.getMail(), lang);
  }

  /** GET /activate -> activate the registered user. */
  @RequestMapping(
    value = "/activate/{key}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(
    value = "Activate Account",
    notes = "This method is used to activate account with activation key"
  )
  public ResponseModel activateAccount(@PathVariable String key) {
    return accountService.activateRegistration(key);
  }

  /** GET /account -> get the current user. */
  @RequestMapping(
    value = "/account",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(value = "Get Account", notes = "this method is used to get user authorities")
  public ResponseModel getAccount(HttpServletRequest request) {
    return accountService.getUserWithAuthorities();
  }

  /** POST /account -> update the current user information. */
  @RequestMapping(
    value = "/account",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(value = "Save Account", notes = "this method is used to save user's information")
  public ResponseModel saveAccount(
      @RequestBody UserCreateModel userDTO, HttpServletRequest request) {
    return accountService.updateUserInformation(userDTO);
  }

  /** POST /change_password -> changes the current user's password */
  @RequestMapping(
    value = "/account/change_password",
    method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(value = "Change password", notes = "this method is used to change user's password")
  public ResponseModel changePassword(
      @RequestBody @Validated ChangePasswordModel model, HttpServletRequest request) {

    return accountService.changePassword(model);
  }

  @RequestMapping(
    value = "/account/forget_password/init",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(
    value = "Request Reset password",
    notes = "this method is used to request resetting user's password"
  )
  public ResponseModel requestPasswordReset(
      @RequestBody @Validated ResetPasswordModel model,
      @RequestHeader(defaultValue = "en", required = false) String lang) {
    return accountService.requestPasswordReset(model.getMail(), "", lang);
  }

  @RequestMapping(
    value = "/account/forget_password/checkcode/{key}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(
    value = "Validate Reset Password key",
    notes = "this method is used to validate reset password code"
  )
  public ResponseModel requestPasswordReset(@PathVariable String key, HttpServletRequest request) {
    return accountService.checkResetCode(key);
  }

  @RequestMapping(
    value = "/account/forget_password/finish",
    method = RequestMethod.POST,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(
    value = "Reset password",
    notes = "this method is used to complete reset password process"
  )
  public ResponseModel finishPasswordReset(
      @Valid @RequestBody KeyAndPasswordDTO keyAndPassword, HttpServletRequest request) {

    return accountService.completePasswordReset(
        keyAndPassword.getNewPassword(), keyAndPassword.getKey());
  }

  @RequestMapping(
    value = "/interests",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @Transactional(readOnly = true)
  @ApiOperation(
    value = "Get interest",
    notes = "this method is used to list categories of interests"
  )
  public ResponseModel getInterests(
      @RequestHeader(name = "lang", required = false, defaultValue = "en") String lang) {
    Set<String> categories =
        categoryRepository
            .findByOrganizationIsNullAndFoundationIsNullAndDeletedFalse()
            .map(category -> lang.equals("ar") ? category.getNameAr() : category.getName())
            .collect(Collectors.toSet());
    return ResponseModel.done(categories);
  }

  @RequestMapping(
    value = "/time",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(value = "Get Time", notes = "this method is used to return server time to caller")
  public ResponseModel getTime() {
    ResponseModel responseModel = ResponseModel.done(new Date());
    responseModel.setMessage("mint version = " + version);
    return responseModel;
  }

  @RequestMapping(
    value = "/images_thumbnail",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  @ApiOperation(
    value = "Get default images",
    notes = "this method is used to get default images and thumbnails"
  )
  public ResponseModel getDefaultImagesAndThumbnails() {
    return uploadService.getDefaultImagesAndThumbnails();
  }
}
