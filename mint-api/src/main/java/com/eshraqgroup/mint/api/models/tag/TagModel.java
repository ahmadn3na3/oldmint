package com.eshraqgroup.mint.api.models.tag;

import com.eshraqgroup.mint.domain.mongo.Tag;

/** Created by ahmad on 5/9/17. */
public class TagModel extends TagCreateModel {
  private String id;

  public TagModel() {}

  public TagModel(Tag tag) {
    this(tag.getName(), tag.getGroup(), tag.getId());
  }

  public TagModel(String name, String group, String id) {
    super(name, group);
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}
