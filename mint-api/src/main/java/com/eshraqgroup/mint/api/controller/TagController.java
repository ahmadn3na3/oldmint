package com.eshraqgroup.mint.api.controller;

import java.util.Locale;
import java.util.Set;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.api.models.tag.TagCreateModel;
import com.eshraqgroup.mint.api.services.TagService;
import com.eshraqgroup.mint.controller.abstractcontroller.AbstractController;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

/** Created by ahmad on 5/9/17. */
@RestController()
@RequestMapping("/api/tags")
public class TagController extends AbstractController<TagCreateModel, String> {

  private final TagService tagService;

  public TagController(TagService tagService) {
    this.tagService = tagService;
  }

  @RequestMapping(method = RequestMethod.POST)
  @ApiOperation(value = "Create Tag", notes = "This method is used to create new tags")
  public ResponseModel create(@RequestBody Set<TagCreateModel> tagCreateModels) {
    return tagService.create(tagCreateModels);
  }

  @RequestMapping(method = RequestMethod.DELETE, path = "/{id}")
  @Override
  @ApiOperation(value = "Delete tag", notes = "This method is used to delete tag by id")
  public ResponseModel delete(@PathVariable String id) {
    return tagService.delete(id);
  }

  @RequestMapping(method = RequestMethod.GET)
  @Override
  @ApiOperation(value = "Get Tag", notes = "This method is used to get all tags by current local")
  public ResponseModel get(
      @RequestHeader(name = "lang", required = false, defaultValue = "en") String lang) {
    Locale currentLocale = Locale.forLanguageTag(lang);
    return tagService.getAll(currentLocale);
  }
}
