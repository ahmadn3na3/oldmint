package com.eshraqgroup.mint.api.models.Annotation;

import java.util.LinkedList;
import com.eshraqgroup.mint.constants.AnnotationType;
import com.eshraqgroup.mint.models.annotation.AnnotationPoint;

/** Created by ayman on 03/08/16. */
public class FreehandAnnotationModel extends AnnotationBaseModel {
  private String color;
  private LinkedList<AnnotationPoint> points;
  private Float pageScale = 0f;

  public FreehandAnnotationModel() {
    setAnnotationType(AnnotationType.FREEHAND);
  }

  public LinkedList<AnnotationPoint> getPoints() {
    return points;
  }

  public void setPoints(LinkedList<AnnotationPoint> points) {
    this.points = points;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public Float getPageScale() {
    return pageScale;
  }

  public void setPageScale(Float pageScale) {
    this.pageScale = pageScale;
  }

  @Override
  public String toString() {
    return "FreehandAnnotationModel{"
        + "color='"
        + color
        + '\''
        + ", points="
        + points
        + "} "
        + super.toString();
  }
}
