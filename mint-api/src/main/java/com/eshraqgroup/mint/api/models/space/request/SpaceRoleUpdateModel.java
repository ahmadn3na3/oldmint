package com.eshraqgroup.mint.api.models.space.request;

import javax.validation.constraints.NotNull;
import com.eshraqgroup.mint.constants.SpaceRole;

/** Created by ahmad on 6/12/16. */
public class SpaceRoleUpdateModel {

  @NotNull private Long userId;
  @NotNull private Long spaceId;
  @NotNull private SpaceRole spaceRole;

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public SpaceRole getSpaceRole() {
    return spaceRole;
  }

  public void setSpaceRole(SpaceRole spaceRole) {
    this.spaceRole = spaceRole;
  }

  @Override
  public String toString() {
    return String.format(
        "SpaceRoleUpdateModel{userId=%d, spaceId=%d, spaceRole=%s}", userId, spaceId, spaceRole);
  }
}
