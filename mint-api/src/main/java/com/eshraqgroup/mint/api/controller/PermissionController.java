package com.eshraqgroup.mint.api.controller;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.api.services.ModuleService;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

/** Created by ahmad on 5/15/16. */
@RestController
public class PermissionController {
  private final Logger log = LoggerFactory.getLogger(PermissionController.class);

  @Autowired ModuleService moduleService;

  @RequestMapping(path = "/api/module", method = RequestMethod.GET)
  @ApiOperation(value = "Get Modules", notes = "this method is used to list all modules")
  public ResponseModel get(
      @RequestHeader(required = false) Integer page,
      @RequestHeader(required = false) Integer size,
      HttpServletRequest request) {
    return moduleService.getModules();
  }

  @RequestMapping(path = "/api/module/{id}", method = RequestMethod.GET)
  @ApiOperation(value = "Get Module", notes = "this method is used to get module by id")
  public ResponseModel get(@PathVariable Long id) {
    return moduleService.getModule(id);
  }

  @RequestMapping("/api/permission")
  @ApiOperation(
    value = "Get Permissions",
    notes = "this method is used to list all user permissions"
  )
  public ResponseModel getAll(HttpServletRequest request) {
    return moduleService.getPermissions();
  }
}
