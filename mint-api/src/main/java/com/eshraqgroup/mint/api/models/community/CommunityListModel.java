package com.eshraqgroup.mint.api.models.community;

import java.util.HashSet;
import java.util.Set;
import com.eshraqgroup.mint.api.models.group.GroupModel;
import com.eshraqgroup.mint.api.models.user.UserInfoModel;

/** Created by ahmad on 7/25/16. */
public class CommunityListModel {
  Set<UserInfoModel> userInfoModels = new HashSet<>();
  Set<GroupModel> groupModels = new HashSet<>();

  public Set<UserInfoModel> getUserInfoModels() {
    return userInfoModels;
  }

  public void setUserInfoModels(Set<UserInfoModel> userInfoModels) {
    this.userInfoModels = userInfoModels;
  }

  public Set<GroupModel> getGroupModels() {
    return groupModels;
  }

  public void setGroupModels(Set<GroupModel> groupModels) {
    this.groupModels = groupModels;
  }

  @Override
  public String toString() {
    return "CommunityListModel{"
        + "userInfoModels="
        + userInfoModels
        + ", groupModels="
        + groupModels
        + '}';
  }
}
