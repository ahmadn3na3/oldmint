package com.eshraqgroup.mint.api.models.assessment;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import com.eshraqgroup.mint.constants.QuestionType;

/** Created by ayman on 29/06/16. */
public class AssessmentQuestionCreateModel {

  private Long Id;

  @NotNull(message = "error.question.body.null")
  private String body;

  @NotNull(message = "error.question.questionType.null")
  private QuestionType questionType;

  private String correctAnswer;

  private String bodyResourceUrl;

  private List<ChoicesModel> choicesList = new ArrayList<>();

  private Integer questionWeight = 0;

  public AssessmentQuestionCreateModel() {}

  public AssessmentQuestionCreateModel(MongoQuestionModel questionModel) {
    this.body = questionModel.getBody();
    this.questionType = questionModel.getQuestionType();
    this.correctAnswer = questionModel.getCorrectAnswer();
    this.bodyResourceUrl = questionModel.getBodyResourceUrl();
    this.choicesList = questionModel.getChoicesList();
  }

  public Integer getQuestionWeight() {
    return questionWeight;
  }

  public void setQuestionWeight(Integer questionWeight) {
    this.questionWeight = questionWeight;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public QuestionType getQuestionType() {
    return questionType;
  }

  public void setQuestionType(QuestionType questionType) {
    this.questionType = questionType;
  }

  public String getCorrectAnswer() {
    return correctAnswer;
  }

  public void setCorrectAnswer(String correctAnswer) {
    this.correctAnswer = correctAnswer;
  }

  public String getBodyResourceUrl() {
    return bodyResourceUrl;
  }

  public void setBodyResourceUrl(String bodyResourceUrl) {
    this.bodyResourceUrl = bodyResourceUrl;
  }

  public List<ChoicesModel> getChoicesList() {
    return choicesList;
  }

  public void setChoicesList(List<ChoicesModel> choicesList) {
    this.choicesList = choicesList;
  }

  public Long getId() {
    return Id;
  }

  public void setId(Long id) {
    Id = id;
  }

  @Override
  public String toString() {
    return "AssessmentQuestionCreateModel{"
        + "questionWeight="
        + questionWeight
        + "} "
        + super.toString();
  }
}
