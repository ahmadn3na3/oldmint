package com.eshraqgroup.mint.api.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.eshraqgroup.mint.api.models.Annotation.AnnotationModel;
import com.eshraqgroup.mint.api.models.comment.CommentViewModel;
import com.eshraqgroup.mint.api.models.comment.LikeViewModel;
import com.eshraqgroup.mint.domain.mongo.Annotation;
import com.eshraqgroup.mint.domain.mongo.Comment;
import com.eshraqgroup.mint.domain.mongo.Like;
import com.eshraqgroup.mint.util.DateConverter;

/** Created by ayman on 26/09/16. */
@Service
public class MapperService {
  private final Logger log = LoggerFactory.getLogger(MapperService.class);

  private List<LikeViewModel> mapLikes(List<Like> likeList) {

    List<LikeViewModel> likeViewModels = new ArrayList<>();
    if (null != likeList) {
      for (Like like : likeList) {
        LikeViewModel likeViewModel = new LikeViewModel();
        likeViewModel.setId(like.getId());
        likeViewModel.setUserId(like.getUserId());
        likeViewModel.setUserName(like.getUserName());
        likeViewModels.add(likeViewModel);
      }
    }
    return likeViewModels;
  }

  public List<CommentViewModel> mapComments(List<Comment> commentList) {
    List<CommentViewModel> commentViewList = new ArrayList<>();
    if (commentList != null) {
      for (Comment comment : commentList) {
        if (comment == null || comment.isDeleted()) {
          continue;
        }
        CommentViewModel commentViewModel = mapComment(comment);
        commentViewList.add(commentViewModel);
      }
    }
    return commentViewList;
  }

  public CommentViewModel mapComment(Comment comment) {
    CommentViewModel commentViewModel = new CommentViewModel();
    commentViewModel.setId(comment.getId());
    commentViewModel.setCommentBody(comment.getBody());
    commentViewModel.setUserId(comment.getUserId());
    commentViewModel.setUserName(comment.getUserFullName());
    commentViewModel.setUserImage(comment.getUserThumbnail());
    commentViewModel.setUserFullname(comment.getUserFullName());
    commentViewModel.setLikes(mapLikes(comment.getLikes()));
    commentViewModel.setCreationDate(
        DateConverter.convertDateToZonedDateTime(comment.getCreationDate()));
    commentViewModel.setLastModifiedDate(
        DateConverter.convertDateToZonedDateTime(comment.getLastModifiedDate()));

    return commentViewModel;
  }

  public AnnotationModel mapAnnotation(Annotation annotation) {

    AnnotationModel annotationModel = new AnnotationModel(annotation);
    List<Comment> comments =
        annotation
            .getComments()
            .stream()
            .filter(comment -> !comment.isDeleted())
            .collect(Collectors.toList());

    List<CommentViewModel> commentViewModels = mapComments(comments);

    List<Like> likeList = annotation.getLikes();
    List<LikeViewModel> likeViewModels = mapLikes(likeList);

    if (null != likeViewModels) {
      annotationModel.setLikes(likeViewModels);
    }

    if (null != commentViewModels) {
      annotationModel.setComments(commentViewModels);
    }

    log.debug("Annotation {} Found ", annotationModel);
    return annotationModel;
  }
}
