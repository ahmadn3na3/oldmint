package com.eshraqgroup.mint.api.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.api.models.Annotation.FreehandAnnotationModel;
import com.eshraqgroup.mint.api.models.Annotation.StyleAnnotationModel;
import com.eshraqgroup.mint.api.models.Annotation.TextNoteAnnotationModel;
import com.eshraqgroup.mint.api.models.Annotation.VoiceNoteAnnotationModel;
import com.eshraqgroup.mint.api.models.discussion.CommentCreateModel;
import com.eshraqgroup.mint.api.services.AnnotationService;
import com.eshraqgroup.mint.constants.AnnotationType;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

/** Created by ayman on 02/08/16. */
@RestController
@RequestMapping("/api/annotation")
public class AnnotationController {

  @Autowired AnnotationService annotationService;

  @RequestMapping(path = "/textnote", method = RequestMethod.POST)
  @ApiOperation(value = "Create Text Note", notes = "this method is used to create text annotation")
  public ResponseModel createTextNote(@RequestBody @Validated TextNoteAnnotationModel createModel) {
    return annotationService.createTextNote(createModel);
  }

  @RequestMapping(path = "/voicenote", method = RequestMethod.POST)
  @ApiOperation(
    value = "Create voice Note",
    notes = "this method is used to create voice annotation"
  )
  public ResponseModel createVoicetNote(
      @RequestBody @Validated VoiceNoteAnnotationModel createModel, HttpServletRequest request) {
    return annotationService.createVoiceNote(createModel);
  }

  @RequestMapping(path = "/style", method = RequestMethod.POST)
  @ApiOperation(
    value = "Create Style",
    notes = "this method is used to create highlight , underline or bookmark"
  )
  public ResponseModel createStyle(
      @RequestBody @Validated StyleAnnotationModel createModel, HttpServletRequest request) {
    return annotationService.createStyleAnnotation(createModel);
  }

  @RequestMapping(path = "/freehand", method = RequestMethod.POST)
  @ApiOperation(
    value = "Create freehand",
    notes = "this method is used to create freehand annotation"
  )
  public ResponseModel createFreehand(
      @RequestBody @Validated FreehandAnnotationModel createModel, HttpServletRequest request) {
    return annotationService.createFreeHandAnnotation(createModel);
  }

  @RequestMapping(path = "/textnote/{id}", method = RequestMethod.PUT)
  @ApiOperation(
    value = "update text annotation",
    notes = "this method is used to update text annotation"
  )
  public ResponseModel update(
      @PathVariable String id,
      @RequestBody @Validated TextNoteAnnotationModel updateModel,
      HttpServletRequest request) {
    return annotationService.update(id, updateModel);
  }

  @RequestMapping(path = "/voicenote/{id}", method = RequestMethod.PUT)
  @ApiOperation(
    value = "update voice annotation",
    notes = "this method is used to update voice annotation"
  )
  public ResponseModel update(
      @PathVariable String id,
      @RequestBody @Validated VoiceNoteAnnotationModel updateModel,
      HttpServletRequest request) {
    return annotationService.update(id, updateModel);
  }

  @RequestMapping(path = "/style/{id}", method = RequestMethod.PUT)
  @ApiOperation(
    value = "update style annotation",
    notes = "this method is used to update style annotation"
  )
  public ResponseModel update(
      @PathVariable String id,
      @RequestBody @Validated StyleAnnotationModel updateModel,
      HttpServletRequest request) {
    return annotationService.update(id, updateModel);
  }

  @RequestMapping(path = "/freehand/{id}", method = RequestMethod.PUT)
  @ApiOperation(
    value = "update freehand annotation",
    notes = "this method is used to update freehand annotation"
  )
  public ResponseModel update(
      @PathVariable String id,
      @RequestBody @Validated FreehandAnnotationModel createModel,
      HttpServletRequest request) {
    return annotationService.createFreeHandAnnotation(createModel);
  }

  @RequestMapping(path = "/{id}", method = RequestMethod.GET)
  @ApiOperation(
    value = "get annotation",
    notes = "this method is used to get user's specific annotation"
  )
  public ResponseModel get(@PathVariable String id, HttpServletRequest request) {
    return annotationService.getOne(id);
  }

  // paging
  @RequestMapping(path = "/get/{id}", method = RequestMethod.GET)
  @ApiOperation(
    value = "get content annotation",
    notes = "this method is used to get content's annotations"
  )
  public ResponseModel get(@PathVariable Long id, HttpServletRequest request) {
    return annotationService.get(id);
  }

  @RequestMapping(path = "/get/{id}/{page}", method = RequestMethod.GET)
  @ApiOperation(
    value = "get page annotation",
    notes = "this method is used to get content's annotations in specific"
  )
  public ResponseModel get(
      @PathVariable Long id, @PathVariable Integer page, HttpServletRequest request) {
    return annotationService.get(id, page);
  }

  @RequestMapping(path = "/getAnnotation/{annotationType}/{id}", method = RequestMethod.GET)
  @ApiOperation(
    value = "get page annotation",
    notes = "this method is used to get content's annotations in specific page"
  )
  public ResponseModel get(
      @PathVariable AnnotationType annotationType,
      @PathVariable Long id,
      HttpServletRequest request) {
    return annotationService.get(id, annotationType);
  }

  @RequestMapping(path = "/get/{annotationType}/{id}/{page}", method = RequestMethod.GET)
  @ApiOperation(
    value = "get annotation by  page and type",
    notes = "this method is used to get content's annotations in specific page and type"
  )
  public ResponseModel get(
      @PathVariable AnnotationType annotationType,
      @PathVariable Long id,
      @PathVariable Integer page,
      HttpServletRequest request) {
    return annotationService.get(id, page, annotationType);
  }

  @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
  @ApiOperation(
    value = "delete annotation",
    notes = "this method is used to delete annotation by id"
  )
  public ResponseModel delete(@PathVariable String id, HttpServletRequest request) {
    return annotationService.delete(id);
  }

  @RequestMapping(path = "/comment/{id}", method = RequestMethod.POST)
  @ApiOperation(value = "Comment", notes = "this method is used to get add comment on annotation")
  public ResponseModel comment(
      @PathVariable("id") String id,
      @RequestBody @Validated CommentCreateModel commentCreateModel,
      HttpServletRequest request) {
    return annotationService.addComment(id, commentCreateModel);
  }

  @RequestMapping(path = "/comment/delete/{id}", method = RequestMethod.DELETE)
  @ApiOperation(
    value = "Delete comment",
    notes = "this method is used to get delete comment on annotation"
  )
  public ResponseModel deleteComment(@PathVariable("id") String id, HttpServletRequest request) {
    return annotationService.deleteComment(id);
  }

  @RequestMapping(path = "/comment/edit/{id}", method = RequestMethod.PUT)
  @ApiOperation(
    value = "Edit comment",
    notes = "this method is used to get edit comment on annotation"
  )
  public ResponseModel editComment(
      @PathVariable("id") String id,
      @RequestBody CommentCreateModel body,
      HttpServletRequest request) {
    return annotationService.editComment(id, body.getCommentBody());
  }

  @RequestMapping(path = "/comment/like/{id}", method = RequestMethod.POST)
  @ApiOperation(
    value = "Like comment",
    notes = "this method is used to like a comment on annotation"
  )
  public ResponseModel likeComment(@PathVariable("id") String id, HttpServletRequest request) {
    return annotationService.likeComment(id);
  }

  @RequestMapping(path = "/like/{id}", method = RequestMethod.POST)
  @ApiOperation(value = "Like annotation", notes = "this method is used to like an annotation")
  public ResponseModel like(@PathVariable("id") String id, HttpServletRequest request) {
    return annotationService.likeAnnotation(id);
  }
}
