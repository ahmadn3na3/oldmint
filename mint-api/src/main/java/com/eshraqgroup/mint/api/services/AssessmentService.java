package com.eshraqgroup.mint.api.services;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.eshraqgroup.mint.api.models.PracticeGenerateModel;
import com.eshraqgroup.mint.api.models.QuestionSearchModel;
import com.eshraqgroup.mint.api.models.UserPracticeModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentCreateModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentGetAllModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentListModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentQuestionCreateModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentUserModel;
import com.eshraqgroup.mint.api.models.assessment.AssessmentsUpdatesModel;
import com.eshraqgroup.mint.api.models.assessment.ChoicesModel;
import com.eshraqgroup.mint.api.models.assessment.QuestionAnswerGetModel;
import com.eshraqgroup.mint.api.models.assessment.QuestionAnswerModel;
import com.eshraqgroup.mint.api.models.assessment.QuestionBankResponseModel;
import com.eshraqgroup.mint.api.models.assessment.UserAssessmentModel;
import com.eshraqgroup.mint.configuration.MintProperties;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.configuration.notifications.Message;
import com.eshraqgroup.mint.constants.AssessmentStatus;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.ContentStatus;
import com.eshraqgroup.mint.constants.ContentType;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.constants.SortField;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.Assessment;
import com.eshraqgroup.mint.domain.jpa.AssessmentQuestion;
import com.eshraqgroup.mint.domain.jpa.AssessmentQuestionChoice;
import com.eshraqgroup.mint.domain.jpa.Content;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.domain.mongo.QuestionAnswer;
import com.eshraqgroup.mint.domain.mongo.UserAssessment;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.PageRequestModel;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.models.assessment.WorkSheetAnswerModel;
import com.eshraqgroup.mint.models.content.ContentUserModel;
import com.eshraqgroup.mint.models.messages.From;
import com.eshraqgroup.mint.models.messages.assessment.AssessementsInfoMessage;
import com.eshraqgroup.mint.models.messages.assessment.AssessmentSubmitMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.repos.jpa.AssessmentQuestionChoicesRepository;
import com.eshraqgroup.mint.repos.jpa.AssessmentQuestionRepository;
import com.eshraqgroup.mint.repos.jpa.AssessmentRepository;
import com.eshraqgroup.mint.repos.jpa.ContentRepository;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.SpaceRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.mongo.QuestionAnswerRepository;
import com.eshraqgroup.mint.repos.mongo.UserAssessmentRepository;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.DateConverter;

/** Created by ayman on 13/06/16. */
@Service
public class AssessmentService {
	private final Logger log = LoggerFactory.getLogger(AssessmentService.class);
	private final QuestionAnswerRepository questionAnswerRepository;
	private final AssessmentRepository assessmentRepository;
	private final AssessmentQuestionRepository assessmentQuestionRepository;
	private final AssessmentQuestionChoicesRepository assessmentQuestionChoicesRepository;
	private final RestTemplate restTemplate = new RestTemplate();
	private final MintProperties mintProperties;

	private static final int MAX_USER_IN_COMMUNITY = 4;

	private final SpaceRepository spaceRepository;
	private final ContentRepository contentRepository;

	private final UserRepository userRepository;

	private final JoinedRepository joinedRepository;

	private final Mapper mapper;
	private final UserAssessmentRepository userAssessmentRepository;
	private final MessageSource messageSource;

	@Autowired
	public AssessmentService(AssessmentQuestionChoicesRepository assessmentQuestionChoicesRepository,
			AssessmentQuestionRepository assessmentQuestionRepository, Mapper mapper,
			UserAssessmentRepository userAssessmentRepository, UserRepository userRepository,
			AssessmentRepository assessmentRepository, QuestionAnswerRepository questionAnswerRepository,
			MintProperties mintProperties, SpaceRepository spaceRepository, JoinedRepository joinedRepository,
			ContentRepository contentRepository, MessageSource messageSource) {
		this.assessmentQuestionChoicesRepository = assessmentQuestionChoicesRepository;
		this.assessmentQuestionRepository = assessmentQuestionRepository;
		this.mapper = mapper;
		this.userAssessmentRepository = userAssessmentRepository;
		this.userRepository = userRepository;
		this.assessmentRepository = assessmentRepository;
		this.mintProperties = mintProperties;
		this.spaceRepository = spaceRepository;
		this.joinedRepository = joinedRepository;
		this.questionAnswerRepository = questionAnswerRepository;
		this.contentRepository = contentRepository;
		this.messageSource = messageSource;
	}

	@Transactional
	@Auditable(EntityAction.ASSESSMENT_CREATE)
	@PreAuthorize("hasAuthority('ASSESSMENT_CREATE')")
	public ResponseModel generatePractice(PracticeGenerateModel practiceGenerateModel, HttpServletRequest request) {
		if (practiceGenerateModel.getQuestionSearchModel().getSpaceId() == null) {
			practiceGenerateModel.getQuestionSearchModel().setSpaceId(practiceGenerateModel.getSpaceId());
		}
		QuestionBankResponseModel responseModel = searchQuestionBank(practiceGenerateModel.getQuestionSearchModel(),
				request);
		if (responseModel.getCode() == 10) {
			if (responseModel.getData() != null && !responseModel.getData().isEmpty()
					&& responseModel.getData().size() >= practiceGenerateModel.getQuestionSearchModel().getLimit()) {
				int maximum = practiceGenerateModel.getQuestionSearchModel().getQuestionType().length
						* practiceGenerateModel.getMinimum();
				log.debug("maximum == > {}", maximum);
				List<AssessmentQuestionCreateModel> assessmentQuestionCreateModels = responseModel.getData().stream()
						.map(questionModel -> {
							AssessmentQuestionCreateModel assessmentQuestionCreateModel = new AssessmentQuestionCreateModel(
									questionModel);
							assessmentQuestionCreateModel.setQuestionWeight(1);
							return assessmentQuestionCreateModel;
						}).collect(Collectors.toList());
				Collections.shuffle(assessmentQuestionCreateModels);
				log.debug("assessmentQuestionCreateModels == > {}", assessmentQuestionCreateModels.size());

				AssessmentCreateModel assessmentCreateModel = new AssessmentCreateModel(
						practiceGenerateModel.getPracticeName(), AssessmentType.PRACTICE, true,
						practiceGenerateModel.getSpaceId(),
						assessmentQuestionCreateModels.stream().limit(maximum).collect(Collectors.toList()));

				assessmentCreateModel.setLockMint(false);

				return create(assessmentCreateModel);
			} else {
				throw new NotFoundException("error.assessment.noenoughquestion");
			}
		} else {
			throw new MintException(responseModel.getCodeType(), responseModel.getMessage());
		}
	}

	@Transactional
	@Auditable(EntityAction.ASSESSMENT_CREATE)
	@PreAuthorize("hasAuthority('ASSESSMENT_CREATE')")
	@Message(entityAction = EntityAction.ASSESSMENT_CREATE, services = Services.NOTIFICATIONS)
	public ResponseModel create(AssessmentCreateModel assesmentCreateModel) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).map(user -> {
			if (assesmentCreateModel.getSpaceId() == null) {
				throw new MintException(Code.INVALID_KEY, "spaceId");
			}
			Space space = spaceRepository.findOneByIdAndDeletedFalse(assesmentCreateModel.getSpaceId())
					.orElseThrow(NotFoundException::new);

			Joined joined = joinedRepository
					.findOneBySpaceIdAndUserIdAndSpaceRoleInAndDeletedFalse(space.getId(), user.getId(),
							SpaceRole.COLLABORATOR, SpaceRole.CO_OWNER, SpaceRole.EDITOR, SpaceRole.OWNER)
					.orElseThrow(NotPermittedException::new);

			if (assesmentCreateModel.isPublish() && assesmentCreateModel.getAssessmentQuestionCreateModels().isEmpty()
					&& (assesmentCreateModel.getAssessmentType() == AssessmentType.ASSIGNMENT
							|| assesmentCreateModel.getAssessmentType() == AssessmentType.QUIZ)) {
				throw new MintException(Code.INVALID, "error.assessment.question.empty");
			}
			Assessment assessment = new Assessment();
			mapper.map(assesmentCreateModel, assessment);
			assessment.setOwner(user);
			assessment.setSpace(space);
			if (assesmentCreateModel.getStartDate() != null) {
				if (!validateDate((assesmentCreateModel.getStartDate()))) {
					throw new InvalidException("error.invalid.date");
				}
				assessment.setStartDateTime(
						DateConverter.convertZonedDateTimeToDate(assesmentCreateModel.getStartDate()));
			}
			if (assesmentCreateModel.getDueDate() != null) {
				if (!validateDate(assesmentCreateModel.getDueDate())) {
					throw new InvalidException("error.invalid.date");
				}
				assessment.setDueDate(DateConverter.convertZonedDateTimeToDate(assesmentCreateModel.getDueDate()));
			}

			if (assessment.getPublish()) {
				assessment.setPublishDate(new Date());
			}

			assessmentRepository.save(assessment);

			switch (assesmentCreateModel.getAssessmentType()) {
			case ASSIGNMENT:
			case QUIZ:
			case PRACTICE:
				if (assesmentCreateModel.getAssessmentType() == AssessmentType.PRACTICE) {
					assessment.setPublishDate(assessment.getCreationDate());
				}
				mapAndSaveQuestion(assesmentCreateModel, assessment);
				break;

			case WORKSHEET:
				if (assesmentCreateModel.getWorkSheetContentId() != null
						&& assesmentCreateModel.getWorkSheetContentId() != 0) {
					Content content = this.contentRepository.findOne(assesmentCreateModel.getWorkSheetContentId());
					if (content == null || content.getType() != ContentType.WORKSHEET
							|| (content.getType() == ContentType.WORKSHEET
									&& content.getStatus() != ContentStatus.READY)) {

						return ResponseModel.error(Code.INVALID, "error.worksheet.content.invalid");
					}
					assessment.setContent(content);
					if (assesmentCreateModel.getTotalAssessmentPoints() == null
							|| assesmentCreateModel.getTotalAssessmentPoints() == 0) {
						return ResponseModel.error(Code.INVALID, "error.assessment.totalpoints");
					}
					assessment.setTotalPoints(assesmentCreateModel.getTotalAssessmentPoints());
				}
				break;

			default:
				break;
			}

			assessmentRepository.save(assessment);
			joined.setAssessmentCount(
					assessmentRepository.countByOwnerIdAndSpaceIdAndDeletedFalse(user.getId(), space.getId()));
			joinedRepository.save(joined);
			return ResponseModel.done(assessment.getId(),
					new AssessementsInfoMessage(assessment.getId(), assessment.getTitle(),
							assessment.getAssessmentType(),
							new From(user.getId(), user.getFullName(), user.getThumbnail(), null),
							assessment.getSpace().getId(), assessment.getStartDateTime(), assessment.getDueDate(),
							space.getName(), space.getCategory().getName()));
		}).orElseThrow(NotPermittedException::new);
	}

	private void extractChoices(AssessmentQuestionCreateModel assessmentQuestionCreateModel,
			AssessmentQuestion assessmentQuestion) {
		if (assessmentQuestionCreateModel.getChoicesList() != null) {
			Set<AssessmentQuestionChoice> temp = new HashSet<>();
			for (ChoicesModel choicesModel : assessmentQuestionCreateModel.getChoicesList()) {
				AssessmentQuestionChoice choice = new AssessmentQuestionChoice();
				choice.setCorrectAnswer(choicesModel.getCorrectAnswer());
				choice.setCorrectOrder(choicesModel.getCorrectOrder());
				choice.setPairCol(choicesModel.getPairColumn());
				choice.setLabel(choicesModel.getLabel());
				choice.setCorrectAnswerResourceUrl(choicesModel.getCorrectAnswerResourceUrl());
				choice.setAssessmentQuestion(assessmentQuestion);
				temp.add(choice);
			}
			assessmentQuestion.setAssessmentQuestionChoices(temp);
		}
	}

	@Transactional
	@Auditable(EntityAction.ASSESSMENT_UPDATE)
	@PreAuthorize("hasAuthority('ASSESSMENT_UPDATE')")
	@Message(entityAction = EntityAction.ASSESSMENT_UPDATE, services = Services.NOTIFICATIONS)
	public ResponseModel update(Long id, AssessmentCreateModel assessmentModel) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
				.map(user -> Optional.ofNullable(assessmentRepository.findOne(id)).map(assessment -> {
					if (assessment.getPublish() == Boolean.TRUE) {
						throw new MintException(Code.INVALID, "error.assessment.status");
					}

					mapper.map(assessmentModel, assessment);
					assessment
							.setStartDateTime(DateConverter.convertZonedDateTimeToDate(assessmentModel.getStartDate()));
					assessment.setDueDate(DateConverter.convertZonedDateTimeToDate(assessmentModel.getDueDate()));
					assessmentRepository.save(assessment);

					List<AssessmentQuestion> assessmentQuestions = assessmentQuestionRepository
							.findByAssessmentAndDeletedFalse(assessment);

					if (assessmentQuestions != null && !assessmentQuestions.isEmpty()) {
						assessmentQuestions.stream()
								.filter(assessmentQuestion -> assessmentQuestion.getAssessmentQuestionChoices() != null
										&& !assessmentQuestion.getAssessmentQuestionChoices().isEmpty())
								.forEach(assessmentQuestion -> assessmentQuestionChoicesRepository
										.delete(assessmentQuestion.getAssessmentQuestionChoices()));
						assessmentQuestionRepository.delete(assessmentQuestions);
					}
					assessment.setTotalPoints(0);
					mapAndSaveQuestion(assessmentModel, assessment);
					assessmentRepository.save(assessment);
					return ResponseModel.done();
				}).orElseThrow(NotFoundException::new)).orElseThrow(NotPermittedException::new);
	}

	private void mapAndSaveQuestion(AssessmentCreateModel assessmentModel, Assessment assessment) {
		if (assessmentModel.getAssessmentQuestionCreateModels() != null
				&& !assessmentModel.getAssessmentQuestionCreateModels().isEmpty()) {
			assessmentModel.getAssessmentQuestionCreateModels().forEach(assessmentQuestionCreateModel -> {
				AssessmentQuestion assessmentQuestion = new AssessmentQuestion();
				mapper.map(assessmentQuestionCreateModel, assessmentQuestion);
				assessmentQuestion.setId(null);
				// TODO: REMOVE Next release
				// if (assessmentQuestionCreateModel.getQuestionType() != QuestionType.ESSAY &&
				// assessmentQuestionCreateModel.getQuestionType() != QuestionType.TRUE_FALSE) {
				extractChoices(assessmentQuestionCreateModel, assessmentQuestion);

				// }
				assessmentQuestion.setAssessment(assessment);
				assessmentQuestionRepository.save(assessmentQuestion);
				assessment.setTotalPoints(assessment.getTotalPoints() + assessmentQuestion.getQuestionWeight());
				log.debug("assessmentQuestionCreateModel == > {}", assessmentQuestionCreateModel.getBody());
			});
		}
	}

	@Transactional
	@PreAuthorize("hasAuthority('ASSESSMENT_READ')")
	public ResponseModel get(Long id) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
				.map(user -> assessmentRepository.findOneByIdAndDeletedFalse(id).map(assesment -> {
					AssessmentModel assessmentModel = assessmentMapping(assesment, user.getId());
					if (assesment.getLimitDuration() != null) {
						assessmentModel.setLimitedByTime(true);
					}

					return ResponseModel.done(assessmentModel);
				}).orElseThrow(NotFoundException::new)).orElseThrow(NotPermittedException::new);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ASSESSMENT_READ')")
	public ResponseModel get(AssessmentGetAllModel assessmentGetAllModel, PageRequest pageRequest) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).map(user -> {
			if (spaceRepository.findOne(assessmentGetAllModel.getSpaceId()) != null) {
				PageRequest pageRequestWithDefaultSort = pageRequest;
				if (pageRequest.getSort() == null) {
					PageRequestModel.getPageRequestModel(pageRequest.getPageNumber(), pageRequest.getPageSize(),
							new Sort(Sort.Direction.DESC, SortField.PUBLISH_DATE.getFieldName()));
				}
				if (assessmentGetAllModel.getAssessmentType() != null) {

					Page<Assessment> allAssessmentsPage = assessmentRepository.findAllByAssessmentType(
							assessmentGetAllModel.getAssessmentType(), assessmentGetAllModel.getSpaceId(), user.getId(),
							pageRequestWithDefaultSort);
					return PageResponseModel.done(
							allAssessmentsPage.getContent().stream()
									.map(assessment -> assessmentListMapping(assessment, user.getId()))
									.collect(Collectors.toList()),
							allAssessmentsPage.getTotalPages(), pageRequestWithDefaultSort.getPageNumber(),
							allAssessmentsPage.getTotalElements());
				}
				Page<Assessment> ownedPage = assessmentRepository.findBySpaceIdAndDeletedFalseAndOwnerOrPublishTrue(
						assessmentGetAllModel.getSpaceId(), user, pageRequest);

				Set<AssessmentListModel> assessmentModels = new HashSet<>();
				assessmentModels.addAll(ownedPage.getContent().stream()
						.map(assessment -> assessmentListMapping(assessment, user.getId()))
						.filter(assessmentListModel -> !((assessmentListModel
								.getAssessmentType() == AssessmentType.PRACTICE
								&& !Objects.equals(assessmentListModel.getOwner(), user.getId()))))
						.collect(Collectors.toSet()));
				return PageResponseModel.done(assessmentModels, ownedPage.getTotalPages(), pageRequest.getPageNumber(),
						assessmentModels.size());

			} else {
				throw new NotFoundException("space");
			}
		}).orElseThrow(NotPermittedException::new);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ASSESSMENT_READ')")
	public ResponseModel getAssessmentOverview(Long spaceId) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).map(user -> {
			Map<AssessmentType, List<AssessmentListModel>> assessmentGroups = assessmentRepository
					.findBySpaceIdAndPublishTrueAndPublishDateNotNullAndDeletedFalseOrderByPublishDateDesc(spaceId)
					.collect(Collectors.groupingBy(Assessment::getAssessmentType, HashMap::new,
							Collectors.collectingAndThen(Collectors.toSet(), list -> mapList(list, user.getId()))));
			return ResponseModel.done(assessmentGroups);
		}).orElseThrow(NotPermittedException::new);
	}

	private List<AssessmentListModel> mapList(Set<Assessment> assessmentList, Long currentUserId) {
		return assessmentList.stream().limit(3).map(assessment -> {
			AssessmentListModel assessmentListModel = new AssessmentListModel(assessment, userAssessmentRepository
					.findByAssessmentIdAndDeletedFalse(assessment.getId()).collect(Collectors.toList()), currentUserId);
			assessmentListModel.setNumberOfQuestions(
					assessmentQuestionRepository.countByAssessmentIdAndDeletedFalse(assessmentListModel.getId()));
			return assessmentListModel;
		}).collect(Collectors.toList());
	}

	@Transactional
	@PreAuthorize("hasAuthority('ASSESSMENT_READ') and hasAuthority('ADMIN')")
	public ResponseModel getAll(PageRequest pageRequest, Long spaceId, AssessmentType assessmentType) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).map(user -> {
			Specification<Assessment> spaceIdSpec = null;
			Specification<Assessment> typeSpec = null;
			if (spaceId != null) {
				Space space = spaceRepository.findOneByIdAndDeletedFalse(spaceId).orElseThrow(NotFoundException::new);
				spaceIdSpec = (root, query, cb) -> cb.equal(root.get("space"), space);
			}

			if (assessmentType != null) {
				typeSpec = (root, query, cb) -> cb.equal(root.get("assessmentType"), assessmentType);
			}

			Page<AssessmentListModel> allAssessmentsPage = assessmentRepository
					.findAll(Specifications.where(spaceIdSpec).and(typeSpec)
							.and((root, query, cb) -> cb.isFalse(root.get("deleted"))), pageRequest)
					.map(assessment -> assessmentListMapping(assessment, user.getId()));

			return PageResponseModel.done(allAssessmentsPage.getContent(), allAssessmentsPage.getTotalPages(),
					pageRequest.getPageNumber(), allAssessmentsPage.getTotalElements());
		}).orElseThrow(NotPermittedException::new);
	}

	@Transactional
	@Auditable(EntityAction.ASSESSMENT_DELETE)
	@PreAuthorize("hasAuthority('ASSESSMENT_DELETE')")
	public ResponseModel delete(Long id) {
		if (null != id) {
			return assessmentRepository.findOneByIdAndDeletedFalse(id).map(assessment -> {
				if (assessment.getAssessmentType() != AssessmentType.PRACTICE
						&& userAssessmentRepository.findByAssessmentIdAndDeletedFalse(id).count() >= 1) {
					throw new MintException(Code.INVALID, "error.assessment.status");
				}

				Joined joined = joinedRepository.findOneBySpaceIdAndUserIdAndSpaceRoleInAndDeletedFalse(
						assessment.getSpace().getId(), SecurityUtils.getCurrentUser().getId(), SpaceRole.COLLABORATOR,
						SpaceRole.CO_OWNER, SpaceRole.EDITOR, SpaceRole.OWNER).orElseThrow(NotPermittedException::new);

				List<AssessmentQuestion> assessmentQuestions = assessmentQuestionRepository
						.findByAssessmentAndDeletedFalse(assessment);

				if (assessmentQuestions != null && !assessmentQuestions.isEmpty()) {
					assessmentQuestions.stream()
							.filter(assessmentQuestion -> assessmentQuestion.getAssessmentQuestionChoices() != null
									&& !assessmentQuestion.getAssessmentQuestionChoices().isEmpty())
							.forEach(assessmentQuestion -> assessmentQuestionChoicesRepository
									.delete(assessmentQuestion.getAssessmentQuestionChoices()));
					assessmentQuestionRepository.delete(assessmentQuestions);
				}
				assessmentRepository.delete(assessment);
				joined.setAssessmentCount(assessmentRepository.countByOwnerIdAndSpaceIdAndDeletedFalse(
						SecurityUtils.getCurrentUser().getId(), assessment.getSpace().getId()));
				joinedRepository.save(joined);
				return ResponseModel.done();
			}).orElseThrow(NotFoundException::new);
		}
		throw new MintException(Code.INVALID);
	}

	@Transactional
	@Auditable(EntityAction.ASSESSMENT_DELETE)
	@PreAuthorize("hasAuthority('ASSESSMENT_SOLVE_CREATE')")
	@Message(entityAction = EntityAction.ASSESSMENT_SUBMIT, services = Services.NOTIFICATIONS)
	public ResponseModel submit(UserAssessmentModel userAssessmentModel) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
				.map(user -> assessmentRepository.findOneByIdAndDeletedFalse(userAssessmentModel.getAssessmentId())
						.map(assessment -> {
							UserAssessment userAssessment = userAssessmentRepository
									.findOneByUserIdAndAssessmentIdAndDeletedFalse(user.getId(),
											userAssessmentModel.getAssessmentId())
									.orElseGet(() -> {
										UserAssessment userAssessment2 = new UserAssessment();
										userAssessment2.setUserId(user.getId());
										userAssessment2.setAssessmentId(assessment.getId());
										userAssessment2.setFullGrade(assessment.getTotalPoints());
										return userAssessment2;
									});

							if (Arrays
									.asList(AssessmentStatus.FINISHED, AssessmentStatus.NOT_EVALUATED,
											AssessmentStatus.EVALUATED)
									.contains(userAssessment.getAssessmentStatus())) {
								throw new MintException(Code.INVALID, "error.assessment.taken");
							}
							if (assessment.getAssessmentType() != AssessmentType.PRACTICE
									&& userAssessmentModel.getUserId() != null
									&& !userAssessmentModel.getUserId().equals(user.getId())
									&& user.equals(assessment.getOwner())) {
								userAssessment = userAssessmentRepository
										.findOneByUserIdAndAssessmentIdAndDeletedFalse(userAssessmentModel.getUserId(),
												assessment.getId())
										.orElseThrow(
												() -> new MintException(Code.INVALID, "error.assessment.nottaken"));
							}

							userAssessment.setAssessmentStatus(userAssessmentModel.getAssessmentStatus());

							if (userAssessmentModel.getDuration() != null) {
								userAssessment.setDuration(userAssessmentModel.getDuration());
							}

							switch (assessment.getAssessmentType()) {
							case ASSIGNMENT:
							case QUIZ:
							case PRACTICE:
								List<AssessmentQuestion> assessmentQuestions = assessmentQuestionRepository
										.findByAssessmentAndDeletedFalse(assessment);
								if (assessment.getAssessmentType().equals(AssessmentType.PRACTICE)) {
									assessmentQuestions
											.forEach(assessmentQuestion -> assessmentQuestion.setQuestionWeight(1));
									assessment.setAssessmentStatus(userAssessmentModel.getAssessmentStatus());
									assessmentQuestionRepository.save(assessmentQuestions);
									assessmentRepository.save(assessment);
								}

								grade(userAssessmentModel, userAssessment, assessmentQuestions, assessment, user);

								break;
							case WORKSHEET:
								if (!assessment.getOwner().getId().equals(user.getId())
										&& userAssessmentModel.getUserWorkSheetAnswerModel() != null) {
									userAssessment.setWorkSheetAnswerModel(
											mapper.map(userAssessmentModel.getUserWorkSheetAnswerModel(),
													WorkSheetAnswerModel.class));
									userAssessmentModel.setFullGrade(assessment.getTotalPoints());
									userAssessment.setAssessmentStatus(AssessmentStatus.NOT_EVALUATED);
								} else if (assessment.getOwner().getId().equals(user.getId())
										&& userAssessmentModel.getOwnerWorkSheetAnswerModel() != null) {
									userAssessment.setOwnerWorkSheetAnswerModel(
											mapper.map(userAssessmentModel.getOwnerWorkSheetAnswerModel(),
													WorkSheetAnswerModel.class));
									userAssessment.setTotalGrade(userAssessmentModel.getTotalGrade());
									if (assessment.getTotalPoints() > 0 && null != assessment.getTotalPoints()) {
										userAssessment.setPercentage((userAssessment.getTotalGrade()
												/ assessment.getTotalPoints().floatValue()) * 100);
									}
									userAssessment.setAssessmentStatus(AssessmentStatus.EVALUATED);
								} else {
									throw new MintException(Code.INVALID);
								}
								break;
							default:
								throw new InvalidException("error.assessment.type");
							}
							userAssessmentRepository.save(userAssessment);
							final AssessmentUserModel contentUserModel = new AssessmentUserModel();
							User user1 = user;
							if (!user.getId().equals(userAssessment.getUserId())) {
								user1 = userRepository.findOne(userAssessment.getUserId());
							}
							contentUserModel.setId(user1.getId());
							contentUserModel.setName(user1.getFullName());
							contentUserModel.setUserName(user1.getUserName());
							contentUserModel.setImage(user1.getThumbnail());
							contentUserModel.setFullGrade(userAssessment.getFullGrade());
							contentUserModel.setTotalGrade(userAssessment.getTotalGrade());
							contentUserModel.setAssessmentStatus(userAssessment.getAssessmentStatus());
							contentUserModel.setPercentage(userAssessment.getPercentage());
							contentUserModel.setMessage(getReportMessage(userAssessment.getPercentage(),
									Locale.forLanguageTag(user.getLangKey() == null ? "en" : user.getLangKey())));
							return ResponseModel.done(contentUserModel,
									new AssessmentSubmitMessage(assessment.getId(), assessment.getTitle(),
											assessment.getAssessmentType(), new From(SecurityUtils.getCurrentUser()),
											assessment.getSpace().getId(), assessment.getStartDateTime(),
											assessment.getDueDate(), assessment.getSpace().getName(),
											assessment.getSpace().getCategory().getName(),
											userAssessment.getAssessmentStatus(), user.equals(assessment.getOwner()),
											new UserInfoMessage(user1), new UserInfoMessage(assessment.getOwner())));
						}).orElseThrow(NotFoundException::new))
				.orElseThrow(NotPermittedException::new);
	}

	@Transactional
	@Auditable(EntityAction.ASSESSMENT_SUBMIT)
	public ResponseModel submitQuestion(UserPracticeModel userPracticeModel) {
		return userRepository
				.findOneByUserNameAndDeletedFalse(
						SecurityUtils.getCurrentUserLogin())
				.map(user -> assessmentRepository
						.findOneByIdAndDeletedFalseAndOwnerAndAssessmentType(userPracticeModel.getAssessmentId(), user,
								AssessmentType.PRACTICE)
						.map(assessment -> userAssessmentRepository
								.findOneByUserIdAndAssessmentIdAndDeletedFalse(user.getId(),
										userPracticeModel.getAssessmentId())
								.map(userAssessment -> {
									QuestionAnswer questionAnswer = questionAnswerRepository
											.findOneByUserIdAndQuestionIdAndDeletedFalse(user.getId(),
													userPracticeModel.getQuestionAnswerModels().getQuestionId())
											.map(questionAnswer1 -> {
												questionAnswer1.setUserAnswer(
														userPracticeModel.getQuestionAnswerModels().getUserAnswer());
												return questionAnswer1;
											}).orElseGet(() -> {
												QuestionAnswer questionAnswer1 = new QuestionAnswer();
												questionAnswer1.setUserAnswer(
														userPracticeModel.getQuestionAnswerModels().getUserAnswer());
												questionAnswer1.setQuestionId(
														userPracticeModel.getQuestionAnswerModels().getQuestionId());
												questionAnswer1.setUserId(user.getId());
												return questionAnswer1;
											});
									questionAnswerRepository.save(questionAnswer);
									if (!userAssessment.getQuestionAnswerList().contains(questionAnswer)) {
										userAssessment.getQuestionAnswerList().add(questionAnswer);
									}
									userAssessmentRepository.save(userAssessment);
									return ResponseModel.done((Object) userAssessment.getId());
								}).orElseGet(() -> {
									UserAssessment userAssessment = new UserAssessment();
									userAssessment.setAssessmentId(userPracticeModel.getAssessmentId());
									userAssessment.setUserId(user.getId());
									userAssessment.setAssessmentStatus(AssessmentStatus.STARTED);
									userAssessment.setQuestionAnswerList(new ArrayList<>());
									QuestionAnswer questionAnswer = new QuestionAnswer();
									questionAnswer
											.setUserAnswer(userPracticeModel.getQuestionAnswerModels().getUserAnswer());
									questionAnswer
											.setQuestionId(userPracticeModel.getQuestionAnswerModels().getQuestionId());
									questionAnswer.setUserId(user.getId());
									questionAnswerRepository.save(questionAnswer);
									userAssessment.getQuestionAnswerList().add(questionAnswer);
									userAssessmentRepository.save(userAssessment);
									return ResponseModel.done((Object) userAssessment.getId());
								}))
						.orElseThrow(NotFoundException::new))
				.orElseThrow(NotPermittedException::new);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ASSESSMENT_READ')")
	public ResponseModel getUpdates(AssessmentGetAllModel assessmentGetAllModel) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).map(user -> {
			AssessmentsUpdatesModel assessmentsUpdatesModel = new AssessmentsUpdatesModel();
			if (spaceRepository.getOne(assessmentGetAllModel.getSpaceId()) != null) {
				assessmentsUpdatesModel.setDeletedAssessments(assessmentRepository
						.findBySpaceIdAndDeletedDateAfterAndDeletedTrue(assessmentGetAllModel.getSpaceId(),
								DateConverter.convertZonedDateTimeToDate(assessmentGetAllModel.getDate()))
						.map(assessment -> assessmentMapping(assessment, user.getId())).collect(Collectors.toList()));
				assessmentsUpdatesModel.setUpdatedAssessments(assessmentRepository
						.findBySpaceIdAndLastModifiedDateAfterAndDeletedFalse(assessmentGetAllModel.getSpaceId(),
								DateConverter.convertZonedDateTimeToDate(assessmentGetAllModel.getDate()))
						.map(assessment -> assessmentMapping(assessment, user.getId())).collect(Collectors.toList()));
				assessmentsUpdatesModel
						.setNewAssessments(
								assessmentRepository
										.findBySpaceIdAndLastModifiedDateIsNullAndCreationDateAfterAndDeletedFalse(
												assessmentGetAllModel.getSpaceId(),
												DateConverter
														.convertZonedDateTimeToDate(assessmentGetAllModel.getDate()))
										.map(assessment -> assessmentMapping(assessment, user.getId()))
										.collect(Collectors.toList()));

				return ResponseModel.done(assessmentsUpdatesModel);
			} else {
				throw new NotFoundException("space");
			}
		}).orElseThrow(NotPermittedException::new);
	}

	@Transactional
	@Auditable(EntityAction.ASSESSMENT_PUBLISH)
	@PreAuthorize("hasAuthority('ASSESSMENT_UPDATE')")
	public ResponseModel togglePublish(Long id) {
		return assessmentRepository.findOneByIdAndDeletedFalse(id).map(assessment -> {
			if (assessment.getPublish().equals(Boolean.FALSE)) {
				if ((assessment.getAssessmentType() == AssessmentType.QUIZ
						|| assessment.getAssessmentType() == AssessmentType.ASSIGNMENT)
						&& assessmentQuestionRepository.findByAssessmentAndDeletedFalse(assessment).isEmpty()) {
					return ResponseModel.error(Code.INVALID, "error.assessment.question.empty");
				}
				assessment.setPublish(Boolean.TRUE);
				assessment.setPublishDate(new Date());
			} else {
				assessment.setPublish(Boolean.FALSE);
			}
			assessmentRepository.save(assessment);
			return ResponseModel.done();
		}).orElseThrow(NotFoundException::new);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ASSESSMENT_READ')")
	public ResponseModel getCommunityList(Long id) {
		return assessmentRepository.findOneByIdAndDeletedFalse(id).map(assessment -> {
			Set<AssessmentUserModel> communityList = new HashSet<>();
			Supplier<Stream<UserAssessment>> userAssessmentSupplier = () -> userAssessmentRepository
					.findByAssessmentIdAndDeletedFalse(id);
			Set<Long> userIdList = joinedRepository.findBySpaceIdAndDeletedFalse(assessment.getSpace().getId())
					.filter(joined -> !joined.getUser().equals(assessment.getOwner()))
					.map(joined -> joined.getUser().getId()).collect(Collectors.toSet());
			if (userIdList != null && !userIdList.isEmpty()) {
				List<User> userList = userRepository.findAll(userIdList);
				for (User user : userList) {
					final AssessmentUserModel contentUserModel = new AssessmentUserModel();
					contentUserModel.setId(user.getId());
					contentUserModel.setName(user.getFullName());
					contentUserModel.setUserName(user.getUserName());
					contentUserModel.setImage(user.getThumbnail());
					userAssessmentSupplier.get()
							.filter(userAssessment -> userAssessment.getUserId().equals(user.getId())).findFirst()
							.ifPresent(userAssessment -> {
								contentUserModel.setFullGrade(userAssessment.getFullGrade());
								contentUserModel.setTotalGrade(userAssessment.getTotalGrade());
								contentUserModel.setAssessmentStatus(userAssessment.getAssessmentStatus());
							});

					communityList.add(contentUserModel);
				}
			}
			return ResponseModel.done(communityList);
		}).orElseThrow(NotFoundException::new);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ASSESSMENT_READ')")
	public ResponseModel getUserAssessment(Long assessmentId, Long userId) {
		User user = userRepository.findOne(userId);
		Assessment assessment = assessmentRepository.findOne(assessmentId);

		if (user == null) {
			throw new NotFoundException("user");
		}

		if (assessment == null) {
			throw new NotFoundException("assessment");
		}
		return userAssessmentRepository.findOneByUserIdAndAssessmentIdAndDeletedFalse(user.getId(), assessmentId)
				.map(userAssessment -> {
					AssessmentModel userAssessmentGetModel = new AssessmentModel(assessment,
							Collections.singletonList(userAssessment), user.getId());
					userAssessmentGetModel.getAssessmentQuestionCreateModels().clear();
					switch (assessment.getAssessmentType()) {
					case ASSIGNMENT:
					case QUIZ:
						assessment.getAssessmentQuestions().stream()
								.filter(assessmentQuestion -> !assessmentQuestion.isDeleted())
								.forEach(assessmentQuestion -> {
									QuestionAnswerGetModel questionAnswerGetModel = mapQuestionAnswerGetModel(
											assessmentQuestion);
									userAssessment.getQuestionAnswerList().stream()
											.filter(questionAnswer -> questionAnswer.getQuestionId()
													.equals(questionAnswerGetModel.getId()))
											.findFirst().ifPresent(questionAnswer -> {
												questionAnswerGetModel.setUserAnswer(questionAnswer.getUserAnswer());
												questionAnswerGetModel.setGrade(questionAnswer.getGrade());
											});
									userAssessmentGetModel.getAssessmentQuestionCreateModels()
											.add(questionAnswerGetModel);
								});
						break;
					case WORKSHEET:
						if (userAssessment.getWorkSheetAnswerModel() != null) {
							userAssessmentGetModel.setUserWorkSheetAnswerModel(
									mapper.map(userAssessment.getWorkSheetAnswerModel(), WorkSheetAnswerModel.class));
						}
						if (userAssessment.getOwnerWorkSheetAnswerModel() != null) {
							userAssessmentGetModel.setOwnerWorkSheetAnswerModel(mapper
									.map(userAssessment.getOwnerWorkSheetAnswerModel(), WorkSheetAnswerModel.class));
						}
						userAssessmentGetModel.setTotalGrade(userAssessment.getTotalGrade());
						break;
					case PRACTICE:
						if (assessment.getOwner().getId().equals(SecurityUtils.getCurrentUser().getId())) {
							assessment.getAssessmentQuestions().stream()
									.filter(assessmentQuestion -> !assessmentQuestion.isDeleted())
									.forEach(assessmentQuestion -> {
										QuestionAnswerGetModel questionAnswerGetModel = mapQuestionAnswerGetModel(
												assessmentQuestion);
										userAssessment.getQuestionAnswerList().stream()
												.filter(questionAnswer -> questionAnswer.getQuestionId()
														.equals(questionAnswerGetModel.getId()))
												.findFirst().ifPresent(questionAnswer -> {
													questionAnswerGetModel
															.setUserAnswer(questionAnswer.getUserAnswer());
													questionAnswerGetModel.setGrade(0.0f);
												});
										userAssessmentGetModel.getAssessmentQuestionCreateModels()
												.add(questionAnswerGetModel);
										userAssessmentGetModel.setLimitDuration(userAssessment.getDuration());
									});
						} else {
							throw new NotPermittedException();
						}
						break;
					}
					return ResponseModel.done(userAssessmentGetModel);
				}).orElseThrow(() -> new MintException(Code.INVALID_CODE, "error.assessment.nottaken"));
	}

	// TODO: Review
	// Status are STARTED , FINISHED OR PAUSED
	@Transactional
	@Auditable(EntityAction.ASSESSMENT_UPDATE)
	public ResponseModel updateAssessmentStatus(Long assessmentId, AssessmentStatus assessmentStatus) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
				.map(user -> assessmentRepository.findOneByIdAndDeletedFalseAndOwnerAndAssessmentType(assessmentId,
						user, AssessmentType.PRACTICE).map(assessment -> {
							assessment.setAssessmentStatus(assessmentStatus);
							if (assessmentStatus.equals(AssessmentStatus.STARTED)) {
								assessment.setStartDateTime(new Date());
							}
							assessmentRepository.save(assessment);
							return ResponseModel.done();
						}).orElseThrow(NotFoundException::new))
				.orElseThrow(NotPermittedException::new);
	}

	// TODO: Review
	@Transactional
	@Auditable(EntityAction.ASSESSMENT_UPDATE)
	public ResponseModel reset(Long id) {
		return userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
				.map(user -> assessmentRepository
						.findOneByIdAndDeletedFalseAndOwnerAndAssessmentType(id, user, AssessmentType.PRACTICE)
						.map(assessment -> userAssessmentRepository
								.findOneByUserIdAndAssessmentIdAndDeletedFalse(user.getId(), assessment.getId())
								.map(userAssessment -> {
									questionAnswerRepository.delete(userAssessment.getQuestionAnswerList());
									userAssessmentRepository.delete(userAssessment);
									assessment.setStartDateTime(null);
									assessment.setAssessmentStatus(AssessmentStatus.NEW);
									assessment.setLimitDuration(0l);
									assessmentRepository.save(assessment);
									return ResponseModel.done(assessment.getId());
								}).orElseThrow(NotFoundException::new))
						.orElseThrow(NotFoundException::new))
				.orElseThrow(NotPermittedException::new);
	}

	private AssessmentListModel assessmentListMapping(Assessment assessment, Long currentUserId) {

		List<UserAssessment> userAssessmentsList = userAssessmentRepository
				.findByAssessmentIdAndDeletedFalseOrderByTotalGradeAsc(assessment.getId()).collect(Collectors.toList());
		AssessmentListModel assessmentListModel = new AssessmentListModel(assessment, userAssessmentsList,
				currentUserId);

		List<Long> userIdList = userAssessmentsList.stream()
				.filter(userAssessment1 -> userAssessment1.getAssessmentStatus() == AssessmentStatus.FINISHED
						|| userAssessment1.getAssessmentStatus() == AssessmentStatus.NOT_EVALUATED)
				.limit(MAX_USER_IN_COMMUNITY).map(UserAssessment::getUserId).collect(Collectors.toList());
		assessmentListModel.setUserCommunity(getCommunityList(userIdList));
		assessmentListModel.setNumberOfQuestions(
				assessmentQuestionRepository.countByAssessmentIdAndDeletedFalse(assessment.getId()));
		return assessmentListModel;
	}

	private AssessmentModel assessmentMapping(Assessment assessment, Long currentUserId) {

		List<UserAssessment> userAssessmentsList = userAssessmentRepository
				.findByAssessmentIdAndDeletedFalseOrderByTotalGradeAsc(assessment.getId()).collect(Collectors.toList());

		AssessmentModel assessmentModel = new AssessmentModel(assessment, userAssessmentsList, currentUserId);

		List<Long> userIdList = userAssessmentsList.stream()
				.filter(userAssessment1 -> userAssessment1.getAssessmentStatus() == AssessmentStatus.FINISHED
						|| userAssessment1.getAssessmentStatus() == AssessmentStatus.NOT_EVALUATED)
				.limit(MAX_USER_IN_COMMUNITY).map(UserAssessment::getUserId).collect(Collectors.toList());
		if (!userIdList.isEmpty()) {
			assessmentModel.setUserCommunity(getCommunityList(userIdList));
		}

		return assessmentModel;
	}

	private List<ContentUserModel> getCommunityList(List<Long> userIdList) {
		List<ContentUserModel> communityList = new ArrayList<>();
		if (!userIdList.isEmpty()) {
			List<User> userList = userRepository.findAll(userIdList);
			userList.forEach(user -> communityList.add(new ContentUserModel(user)));
		}
		return communityList;
	}

	private void grade(UserAssessmentModel userAssessmentModel, UserAssessment userAssessment,
			List<AssessmentQuestion> assessmentQuestions, Assessment assessment, User user) {
		float totalGrade = 0f;
		List<QuestionAnswer> questionAnswerList = userAssessment.getQuestionAnswerList();
		if (userAssessment.getAssessmentStatus().equals(AssessmentStatus.FINISHED)) {
			userAssessment.setAssessmentStatus(AssessmentStatus.EVALUATED);
		}
		for (AssessmentQuestion assessmentQuestion : assessmentQuestions) {
			Optional<QuestionAnswerModel> questionAnswerModelOptional = userAssessmentModel.getQuestionAnswerModels()
					.stream().filter(questionAnswerModel1 -> questionAnswerModel1.getQuestionId()
							.equals(assessmentQuestion.getId()))
					.findFirst();

			if (!questionAnswerModelOptional.isPresent()) {
				continue;
			}
			QuestionAnswerModel questionAnswerModel = questionAnswerModelOptional.get();

			if (questionAnswerModel.getUserAnswer() != null && !questionAnswerModel.getUserAnswer().trim().isEmpty()) {
				if (questionAnswerModel.getGrade() != null && questionAnswerModel.getGrade() != 0.0f
						&& user.equals(assessment.getOwner())
						&& assessment.getAssessmentType() != AssessmentType.PRACTICE) {
					QuestionAnswer questionAnswer = new QuestionAnswer();
					mapper.map(questionAnswerModel, questionAnswer);
					questionAnswer.setUserId(userAssessment.getUserId());
					int index = questionAnswerList.indexOf(questionAnswer);
					if (index == -1) {
						questionAnswerList.add(questionAnswer);
					} else {
						questionAnswerList.get(index).setGrade(questionAnswerModel.getGrade());
					}

					totalGrade += questionAnswerModel.getGrade();
					continue;
				}
				questionAnswerModel.setGrade(0f);
				switch (assessmentQuestion.getQuestionType()) {
				case TRUE_FALSE:
					gradeTrueFalseQuestion(questionAnswerModel, assessmentQuestion);
					break;
				case SINGLE_CHOICE:
					gradeSingleChoiceQuestion(questionAnswerModel, assessmentQuestion);
					break;
				case MATCHING:
					gradeMatchingQuestion(questionAnswerModel, assessmentQuestion);
					break;
				case MULTIPLE_CHOICES:
					gradeMultipleChoicesQuestion(questionAnswerModel, assessmentQuestion);
					break;
				case SEQUENCE:
					gradeSequenceQuestion(questionAnswerModel, assessmentQuestion);
					break;
				case COMPLETE:
					gradeCompleteQuestion(questionAnswerModel, assessmentQuestion);
					break;

				case ESSAY:
					if (!user.getId().equals(assessment.getOwner().getId())) {
						userAssessment.setAssessmentStatus(AssessmentStatus.NOT_EVALUATED);
					}

					break;
				}

				totalGrade += questionAnswerModel.getGrade();
			}

			QuestionAnswer questionAnswer = new QuestionAnswer();
			mapper.map(questionAnswerModel, questionAnswer);
			questionAnswer.setUserId(userAssessment.getUserId());
			questionAnswerList.add(questionAnswer);
		}
		userAssessment.setTotalGrade(totalGrade);

		if (assessment.getTotalPoints() > 0 && null != assessment.getTotalPoints()) {
			Double temp = Double.valueOf(totalGrade) / Double.valueOf(assessment.getTotalPoints().floatValue()) * 100;
			userAssessment.setPercentage(temp.floatValue());
		}

		questionAnswerRepository.save(questionAnswerList);
		userAssessment.setQuestionAnswerList(questionAnswerList);
	}

	private void gradeTrueFalseQuestion(QuestionAnswerModel questionAnswerModel,
			AssessmentQuestion assessmentQuestion) {

		if ((assessmentQuestion.getCorrectAnswer() == null
				&& questionAnswerModel.getUserAnswer().equalsIgnoreCase("false"))) {
			questionAnswerModel.setGrade(assessmentQuestion.getQuestionWeight().floatValue());
		} else if (assessmentQuestion.getCorrectAnswer() != null && assessmentQuestion.getCorrectAnswer().trim()
				.equalsIgnoreCase(questionAnswerModel.getUserAnswer().trim())) {
			questionAnswerModel.setGrade(assessmentQuestion.getQuestionWeight().floatValue());
		}
	}

	private void gradeSingleChoiceQuestion(QuestionAnswerModel questionAnswerModel,
			AssessmentQuestion assessmentQuestion) {
		ArrayList<String> userAnswers = new ArrayList<>(Arrays.asList(questionAnswerModel.getUserAnswer().split(",")));
		if (userAnswers.size() == 1) {
			Set<AssessmentQuestionChoice> assessmentQuestionChoices = assessmentQuestion.getAssessmentQuestionChoices();
			for (AssessmentQuestionChoice assessmentQuestionChoice : assessmentQuestionChoices) {
				if (assessmentQuestionChoice.getCorrectAnswer()) {
					if (assessmentQuestionChoice.getId() == Long.parseLong(questionAnswerModel.getUserAnswer())) {
						questionAnswerModel.setGrade(assessmentQuestion.getQuestionWeight().floatValue());
					}
				}
			}
		}
	}

	private void gradeMultipleChoicesQuestion(QuestionAnswerModel questionAnswerModel,
			AssessmentQuestion assessmentQuestion) {
		Set<AssessmentQuestionChoice> assessmentQuestionChoices = assessmentQuestion.getAssessmentQuestionChoices();
		ArrayList<String> userAnswers = new ArrayList<>(Arrays.asList(questionAnswerModel.getUserAnswer().split(",")));
		ArrayList<String> correctAnswers = new ArrayList<>();
		for (AssessmentQuestionChoice assessmentQuestionChoice : assessmentQuestionChoices) {
			if (Optional.ofNullable(assessmentQuestionChoice.getCorrectAnswer()).orElse(Boolean.FALSE)) {
				correctAnswers.add(String.valueOf(assessmentQuestionChoice.getId()));
			}
		}
		Collections.sort(userAnswers);
		Collections.sort(correctAnswers);
		if (correctAnswers.equals(userAnswers)) {
			questionAnswerModel.setGrade(assessmentQuestion.getQuestionWeight().floatValue());
		}
	}

	private void gradeSequenceQuestion(QuestionAnswerModel questionAnswerModel, AssessmentQuestion assessmentQuestion) {

		List<String> correctAnswers = assessmentQuestion.getAssessmentQuestionChoices().stream()
				.sorted(Comparator.comparingInt(AssessmentQuestionChoice::getCorrectOrder))
				.map(assessmentQuestionChoice -> assessmentQuestionChoice.getId().toString())
				.collect(Collectors.toList());
		ArrayList<String> userAnswers = new ArrayList<>(Arrays.asList(questionAnswerModel.getUserAnswer().split(",")));

		if (correctAnswers.equals(userAnswers)) {
			questionAnswerModel.setGrade(assessmentQuestion.getQuestionWeight().floatValue());
		}
	}

	// TODO: Need more review
	private void gradeMatchingQuestion(QuestionAnswerModel questionAnswerModel, AssessmentQuestion assessmentQuestion) {
		int point = 0;

		ArrayList<Set<String>> userAnswers = new ArrayList<>();
		ArrayList<Set<String>> correctAnswers = new ArrayList<>();

		List<AssessmentQuestionChoice> assessmentQuestionChoices = assessmentQuestion.getAssessmentQuestionChoices()
				.stream().sorted(Comparator.comparingInt(AssessmentQuestionChoice::getCorrectOrder)
						.thenComparing(AssessmentQuestionChoice::getPairCol))
				.collect(Collectors.toList());

		Arrays.stream(questionAnswerModel.getUserAnswer().split(",")).forEach(s -> {
			Set<String> pair = new HashSet<>();
			pair.addAll(Arrays.asList(s.split("\\|")));
			userAnswers.add(pair);
		});

		for (int i = 0; i < assessmentQuestionChoices.size() - 1; i += 2) {
			Set<String> cpair = new HashSet<>();
			cpair.add(String.valueOf(assessmentQuestionChoices.get(i).getId()));
			cpair.add(String.valueOf(assessmentQuestionChoices.get(i + 1).getId()));
			correctAnswers.add(cpair);
		}

		for (int j = 0; j < userAnswers.size(); j++) {
			if (correctAnswers.contains(userAnswers.get(j))) {
				point++;
			}
		}

		questionAnswerModel.setGrade((point / (float) correctAnswers.size()) * assessmentQuestion.getQuestionWeight());
	}

	private void gradeCompleteQuestion(QuestionAnswerModel questionAnswerModel, AssessmentQuestion assessmentQuestion) {

		ArrayList<String> correctAnswers = new ArrayList<>();
		assessmentQuestion.getAssessmentQuestionChoices().stream()
				.sorted(Comparator.comparingInt(AssessmentQuestionChoice::getCorrectOrder))
				.forEach(assessmentQuestionChoice -> correctAnswers.add(assessmentQuestionChoice.getLabel()));
		ArrayList<String> userAnswers = new ArrayList<>(Arrays.asList(questionAnswerModel.getUserAnswer().split(",")));
		if (correctAnswers.equals(userAnswers)) {
			questionAnswerModel.setGrade(assessmentQuestion.getQuestionWeight().floatValue());
		}
	}

	// TODO: Review
	private QuestionAnswerGetModel mapQuestionAnswerGetModel(AssessmentQuestion assessmentQuestion) {
		QuestionAnswerGetModel questionAnswerGetModel = new QuestionAnswerGetModel();
		questionAnswerGetModel.setBody(assessmentQuestion.getBody());
		questionAnswerGetModel.setBodyResourceUrl(assessmentQuestion.getBodyResourceUrl());
		questionAnswerGetModel.setCorrectAnswer(assessmentQuestion.getCorrectAnswer());
		questionAnswerGetModel.setId(assessmentQuestion.getId());
		questionAnswerGetModel.setQuestionType(assessmentQuestion.getQuestionType());
		questionAnswerGetModel.setQuestionWeight(assessmentQuestion.getQuestionWeight());

		assessmentQuestion.getAssessmentQuestionChoices().forEach(assessmentQuestionChoice -> {
			ChoicesModel assessmentQuestionChoiceModel = new ChoicesModel();
			assessmentQuestionChoiceModel.setId(assessmentQuestionChoice.getId());
			assessmentQuestionChoiceModel.setCorrectAnswer(assessmentQuestionChoice.getCorrectAnswer());
			assessmentQuestionChoiceModel.setCorrectOrder(assessmentQuestionChoice.getCorrectOrder());
			assessmentQuestionChoiceModel.setLabel(assessmentQuestionChoice.getLabel());
			assessmentQuestionChoiceModel.setPairColumn(assessmentQuestionChoice.getPairCol());
			assessmentQuestionChoiceModel
					.setCorrectAnswerResourceUrl(assessmentQuestionChoice.getCorrectAnswerResourceUrl());
			assessmentQuestionChoiceModel
					.setCorrectAnswerDescription(assessmentQuestionChoice.getCorrectAnswerDescription());
			questionAnswerGetModel.getChoicesList().add(assessmentQuestionChoiceModel);
		});

		return questionAnswerGetModel;
	}

	private QuestionBankResponseModel searchQuestionBank(QuestionSearchModel questionSearchModel,
			HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		String questionBankUrl = "http://" + mintProperties.getApiDomain();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", request.getHeader("Authorization"));
		HttpEntity<QuestionSearchModel> httpRequest = new HttpEntity<>(questionSearchModel, headers);
		try {
			return restTemplate.postForObject(questionBankUrl + "/api/question/search", httpRequest,
					QuestionBankResponseModel.class);
		} catch (RestClientException e) {
			return (QuestionBankResponseModel) ResponseModel.error(Code.UNKNOWN, e.getMessage());
		}
	}

	private String getReportMessage(Float percentage, Locale locale) {
		String msg;

		if (percentage <= 49) {
			msg = "mint.practice.morepractice";
		} else if (percentage <= 69) {
			msg = "mint.practice.redo";
		} else if (percentage <= 85) {
			msg = "mint.practice.good";
		} else if (percentage <= 95) {
			msg = "mint.pracice.vgood";
		} else {
			msg = "mint.practce.excellent";
		}
		return messageSource.getMessage(msg, null, "", locale);
	}

	private boolean validateDate(ZonedDateTime date) {
		return date.isAfter(ZonedDateTime.now());
	}
}
