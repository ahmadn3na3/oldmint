package com.eshraqgroup.mint.api.services;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.api.models.Annotation.AnnotationModel;
import com.eshraqgroup.mint.api.models.Annotation.FreehandAnnotationModel;
import com.eshraqgroup.mint.api.models.Annotation.StyleAnnotationModel;
import com.eshraqgroup.mint.api.models.Annotation.TextNoteAnnotationModel;
import com.eshraqgroup.mint.api.models.Annotation.VoiceNoteAnnotationModel;
import com.eshraqgroup.mint.api.models.discussion.CommentCreateModel;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.configuration.notifications.Message;
import com.eshraqgroup.mint.constants.AnnotationType;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.CommentType;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.Content;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.domain.mongo.Annotation;
import com.eshraqgroup.mint.domain.mongo.Comment;
import com.eshraqgroup.mint.domain.mongo.Like;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.models.messages.From;
import com.eshraqgroup.mint.models.messages.annotation.AnnotationMessage;
import com.eshraqgroup.mint.repos.jpa.ContentRepository;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.mongo.AnnotationRepository;
import com.eshraqgroup.mint.repos.mongo.CommentRepository;
import com.eshraqgroup.mint.repos.mongo.LikeRepository;
import com.eshraqgroup.mint.security.CurrentUserDetail;
import com.eshraqgroup.mint.security.SecurityUtils;

/** Created by ayman on 02/08/16. */
@Service
public class AnnotationService {
  private final Logger log = LoggerFactory.getLogger(AnnotationService.class);

  private final AnnotationRepository annotationRepository;

  private final UserRepository userRepository;

  private final ContentRepository contentRepository;

  private final LikeRepository likeRepository;

  private final CommentService commentService;

  private final CommentRepository commentRepository;

  private final MapperService mapperService;

  private final JoinedRepository joinedRepository;

  @Autowired
  public AnnotationService(
      AnnotationRepository annotationRepository,
      UserRepository userRepository,
      ContentRepository contentRepository,
      LikeRepository likeRepository,
      CommentService commentService,
      MapperService mapperService,
      CommentRepository commentRepository,
      JoinedRepository joinedRepository) {
    this.annotationRepository = annotationRepository;
    this.userRepository = userRepository;
    this.contentRepository = contentRepository;
    this.likeRepository = likeRepository;
    this.commentService = commentService;
    this.mapperService = mapperService;
    this.commentRepository = commentRepository;
    this.joinedRepository = joinedRepository;
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_CREATE)
  @PreAuthorize("hasAuthority('ANNOTATION_CREATE')")
  @Message(entityAction = EntityAction.ANNOTATION_CREATE, services = Services.NOTIFICATIONS)
  public ResponseModel createTextNote(TextNoteAnnotationModel annotationModel) {
    log.debug("create create text note with annotation{}", annotationModel);
    CurrentUserDetail user = SecurityUtils.getCurrentUser();
    return contentRepository
        .findOneByIdAndDeletedFalse(annotationModel.getContentId())
        .map(
            content -> {
              Joined joined =
                  joinedRepository
                      .findOneByUserIdAndSpaceIdAndDeletedFalse(
                          user.getId(), content.getSpace().getId())
                      .orElseThrow(NotPermittedException::new);
              log.debug("Role ==> {}", joined.getSpaceRole().name());
              //              if (joined.getSpaceRole() == SpaceRole.VIEWER) {
              //                throw new NotPermittedException();
              //              }
              Annotation annotation = new Annotation();
              annotation.setUserId(user.getId());
              annotation.setUserName(user.getUsername());
              annotation.setUserFullName(user.getFullName());
              annotation.setUserImage(user.getImage());
              annotation.setSpaceId(content.getSpace().getId());
              annotation.setAnnotationType(AnnotationType.TEXT_NOTE);
              annotation.setContentId(annotationModel.getContentId());
              annotation.setTextBody(annotationModel.getTextBody());
              annotation.setPageNumber(annotationModel.getPageNumber());

              if (isValidPoint(annotationModel.getStartX())) {
                annotation.setStartX(annotationModel.getStartX());
              } else {
                throw new MintException(Code.INVALID, "point");
              }

              if (isValidPoint(annotationModel.getStartY())) {
                annotation.setStartY(annotationModel.getStartY());
              }

              if (isValidPoint(annotationModel.getEndX())) {
                annotation.setEndX(annotationModel.getEndX());
              }

              if (isValidPoint(annotationModel.getEndY())) {
                annotation.setEndY(annotationModel.getEndY());
              }

              if (null != annotationModel.getStartIndex()) {
                annotation.setStartIndex(annotationModel.getStartIndex());
              }

              if (null != annotationModel.getEndIndex()) {
                annotation.setEndIndex(annotationModel.getEndIndex());
              }

              annotation.setIsPublic(annotationModel.getIsPublic());
              annotationRepository.save(annotation);
              joined.setAnnotationsCount(
                  annotationRepository.countBySpaceIdAndUserId(
                      annotation.getSpaceId(), annotation.getUserId()));
              joinedRepository.save(joined);
              content.setNumberOfAnnotation(
                  annotationRepository.countByContentIdAndDeletedFalse(content.getId()));
              contentRepository.save(content);
              log.debug("annotation Saved");
              return ResponseModel.done(
                  (Object) annotation.getId(),
                  new AnnotationMessage(
                      annotation.getId(),
                      annotation.getContentId(),
                      content.getName(),
                      annotation.getSpaceId(),
                      content.getSpace().getName(),
                      content.getSpace().getCategory().getName(),
                      new From(user)));
            })
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_CREATE)
  @PreAuthorize("hasAuthority('ANNOTATION_CREATE')")
  @Message(entityAction = EntityAction.ANNOTATION_CREATE, services = Services.NOTIFICATIONS)
  public ResponseModel createVoiceNote(VoiceNoteAnnotationModel annotationModel) {
    CurrentUserDetail user = SecurityUtils.getCurrentUser();
    return contentRepository
        .findOneByIdAndDeletedFalse(annotationModel.getContentId())
        .map(
            content -> {
              Joined joined =
                  joinedRepository
                      .findOneByUserIdAndSpaceIdAndDeletedFalse(
                          user.getId(), content.getSpace().getId())
                      .orElseThrow(NotPermittedException::new);

              //              if (joined.getSpaceRole() == SpaceRole.VIEWER) {
              //                throw new NotPermittedException();
              //              }
              Annotation annotation = new Annotation();
              annotation.setUserId(user.getId());
              annotation.setUserName(user.getUsername());
              annotation.setUserFullName(user.getFullName());
              annotation.setUserImage(user.getImage());
              annotation.setSpaceId(content.getSpace().getId());
              annotation.setAnnotationType(AnnotationType.VOICE_NOTE);
              annotation.setContentId(annotationModel.getContentId());
              annotation.setAudioUrl(annotationModel.getAudioUrl());
              annotation.setPageNumber(annotationModel.getPageNumber());

              if (isValidPoint(annotationModel.getStartX())) {
                annotation.setStartX(annotationModel.getStartX());
              } else {
                throw new MintException(Code.INVALID, "point");
              }

              if (isValidPoint(annotationModel.getStartY())) {
                annotation.setStartY(annotationModel.getStartY());
              }

              if (isValidPoint(annotationModel.getEndX())) {
                annotation.setEndX(annotationModel.getEndX());
              }

              if (isValidPoint(annotationModel.getEndY())) {
                annotation.setEndY(annotationModel.getEndY());
              }

              if (null != annotationModel.getStartIndex()) {
                annotation.setStartIndex(annotationModel.getStartIndex());
              }

              if (null != annotationModel.getEndIndex()) {
                annotation.setEndIndex(annotationModel.getEndIndex());
              }

              annotation.setIsPublic(annotationModel.getIsPublic());
              annotationRepository.save(annotation);
              joined.setAnnotationsCount(
                  annotationRepository.countBySpaceIdAndUserId(
                      annotation.getSpaceId(), annotation.getUserId()));
              joinedRepository.save(joined);
              content.setNumberOfAnnotation(
                  annotationRepository.countByContentIdAndDeletedFalse(content.getId()));
              contentRepository.save(content);
              log.debug("annotation Saved");
              return ResponseModel.done(
                  (Object) annotation.getId(),
                  new AnnotationMessage(
                      annotation.getId(),
                      annotation.getContentId(),
                      content.getName(),
                      annotation.getSpaceId(),
                      content.getSpace().getName(),
                      content.getSpace().getCategory().getName(),
                      new From(SecurityUtils.getCurrentUser())));
            })
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_CREATE)
  @PreAuthorize("hasAuthority('ANNOTATION_CREATE')")
  @Message(entityAction = EntityAction.ANNOTATION_CREATE, services = Services.NOTIFICATIONS)
  public ResponseModel createFreeHandAnnotation(FreehandAnnotationModel annotationModel) {
    log.debug("create Freehand annotation{}", annotationModel);
    CurrentUserDetail user = SecurityUtils.getCurrentUser();
    return contentRepository
        .findOneByIdAndDeletedFalse(annotationModel.getContentId())
        .map(
            content -> {
              Joined joined =
                  joinedRepository
                      .findOneByUserIdAndSpaceIdAndDeletedFalse(
                          user.getId(), content.getSpace().getId())
                      .orElseThrow(NotPermittedException::new);
              //              if (joined.getSpaceRole() == SpaceRole.VIEWER) {
              //                throw new NotPermittedException();
              //              }

              Annotation annotation = new Annotation();
              annotation.setUserId(user.getId());
              annotation.setUserName(user.getUsername());
              annotation.setUserFullName(user.getFullName());
              annotation.setUserImage(user.getImage());
              annotation.setSpaceId(content.getSpace().getId());
              annotation.setAnnotationType(AnnotationType.FREEHAND);
              annotation.setContentId(annotationModel.getContentId());
              annotation.setFreeHandPointList(annotationModel.getPoints());
              annotation.setIsPublic(annotationModel.getIsPublic());
              annotation.setColor(annotationModel.getColor());
              annotation.setPageNumber(annotationModel.getPageNumber());
              annotation.setPageScale(annotationModel.getPageScale());
              annotationRepository.save(annotation);
              joined.setAnnotationsCount(
                  annotationRepository.countBySpaceIdAndUserId(
                      annotation.getSpaceId(), annotation.getUserId()));
              joinedRepository.save(joined);
              content.setNumberOfAnnotation(
                  annotationRepository.countByContentIdAndDeletedFalse(content.getId()));
              contentRepository.save(content);
              log.debug("annotation Saved");
              return ResponseModel.done(
                  (Object) annotation.getId(),
                  new AnnotationMessage(
                      annotation.getId(),
                      annotation.getContentId(),
                      content.getName(),
                      annotation.getSpaceId(),
                      content.getSpace().getName(),
                      content.getSpace().getCategory().getName(),
                      new From(SecurityUtils.getCurrentUser())));
            })
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_CREATE)
  @PreAuthorize("hasAuthority('ANNOTATION_CREATE')")
  public ResponseModel createStyleAnnotation(StyleAnnotationModel annotationModel) {
    log.debug("create Style annotation{}", annotationModel);
    CurrentUserDetail user = SecurityUtils.getCurrentUser();
    return contentRepository
        .findOneByIdAndDeletedFalse(annotationModel.getContentId())
        .map(
            content -> {
              Joined joined =
                  joinedRepository
                      .findOneByUserIdAndSpaceIdAndDeletedFalse(
                          user.getId(), content.getSpace().getId())
                      .orElseThrow(NotPermittedException::new);

              //              if (joined.getSpaceRole() == SpaceRole.VIEWER) {
              //                throw new NotPermittedException();
              //              }

              Annotation annotation = new Annotation();
              annotation.setUserId(user.getId());
              annotation.setUserName(user.getUsername());
              annotation.setUserFullName(user.getFullName());
              annotation.setUserImage(user.getImage());
              annotation.setSpaceId(content.getSpace().getId());
              annotation.setAnnotationType(AnnotationType.valueOf(annotationModel.getStyleType()));
              annotation.setContentId(annotationModel.getContentId());
              annotation.setColor(annotationModel.getColor());
              annotation.setPageNumber(annotationModel.getPageNumber());

              if (!annotationModel.getStyleType().equals("BOOKMARK")) {
                if (isValidPoint(annotationModel.getStartX())) {
                  annotation.setStartX(annotationModel.getStartX());
                } else {
                  return ResponseModel.error(Code.INVALID, "point");
                }

                if (isValidPoint(annotationModel.getStartY())) {
                  annotation.setStartY(annotationModel.getStartY());
                }

                if (isValidPoint(annotationModel.getEndX())) {
                  annotation.setEndX(annotationModel.getEndX());
                }

                if (isValidPoint(annotationModel.getEndY())) {
                  annotation.setEndY(annotationModel.getEndY());
                }
              }

              if (annotationModel.getStyleType().equals(AnnotationType.UNDERLINE.getName())
                  || annotationModel.getStyleType().equals(AnnotationType.HIGHLIGHT.getName())) {

                if (null != annotationModel.getStartIndex()) {
                  annotation.setStartIndex(annotationModel.getStartIndex());
                } else {
                  return ResponseModel.error(Code.MISSING, "startIndex");
                }
                if (null != annotationModel.getEndIndex()) {
                  annotation.setEndIndex(annotationModel.getEndIndex());
                } else {
                  return ResponseModel.error(Code.MISSING, "endIndex");
                }
              }

              annotation.setIsPublic(annotationModel.getIsPublic());

              annotationRepository.save(annotation);
              joined.setAnnotationsCount(
                  annotationRepository.countBySpaceIdAndUserId(
                      annotation.getSpaceId(), annotation.getUserId()));
              joinedRepository.save(joined);
              content.setNumberOfAnnotation(
                  annotationRepository.countByContentIdAndDeletedFalse(content.getId()));
              contentRepository.save(content);
              log.debug("annotation Saved");
              return ResponseModel.done((Object) annotation.getId());
            })
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_DELETE)
  @PreAuthorize("hasAuthority('ANNOTATION_DELETE')")
  public ResponseModel delete(String id) {
    log.debug("Delete annotation with id{}", id);
    CurrentUserDetail user = SecurityUtils.getCurrentUser();
    return annotationRepository
        .findOneByIdAndDeletedFalse(id)
        .map(
            annotation -> {
              Content content =
                  contentRepository
                      .findOneByIdAndDeletedFalse(annotation.getContentId())
                      .orElseThrow(NotFoundException::new);
              Joined joined =
                  joinedRepository
                      .findOneByUserIdAndSpaceIdAndDeletedFalse(
                          annotation.getUserId(), content.getSpace().getId())
                      .orElseThrow(NotPermittedException::new);

              //              if (joined.getSpaceRole() == SpaceRole.VIEWER) {
              //                throw new NotPermittedException();
              //              }
              if (user.getId().equals(annotation.getUserId())
                  || content.getOwner().equals(user)
                  || content.getSpace().getUser().equals(user)) {
                commentService.deleteCommentbyParent(annotation.getId());
                annotationRepository.delete(annotation);

                joined.setAnnotationsCount(
                    annotationRepository.countBySpaceIdAndUserId(
                        annotation.getSpaceId(), annotation.getUserId()));
                joinedRepository.save(joined);
                content.setNumberOfAnnotation(
                    annotationRepository.countByContentIdAndDeletedFalse(content.getId()));
                contentRepository.save(content);
                return ResponseModel.done();
              }
              throw new NotPermittedException();
            })
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @PreAuthorize("hasAuthority('ANNOTATION_READ')")
  public ResponseModel getOne(String id) {
    log.debug("get annotation with id {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                annotationRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        annotation -> {
                          // ToDo: Check for user is joined space
                          AnnotationModel annotationModel = mapperService.mapAnnotation(annotation);
                          if (user.getId().equals(annotation.getUserId())) {
                            annotationModel.setUserImage(user.getThumbnail());
                          }
                          return ResponseModel.done(annotationModel);
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @PreAuthorize("hasAuthority('ANNOTATION_READ')")
  public ResponseModel get(Long contentId) {
    log.debug("get annotation for content with id {}", contentId);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                ResponseModel.done(
                    annotationRepository
                        .findByContentIdAndDeletedFalse(contentId)
                        .stream()
                        .filter(
                            annotationModel ->
                                !annotationModel.getIsPublic().equals(FALSE)
                                    || (annotationModel.getUserId().equals(user.getId())
                                        && annotationModel.getIsPublic().equals(FALSE)))
                        .map(
                            annotation -> {
                              AnnotationModel annotationModel =
                                  mapperService.mapAnnotation(annotation);
                              if (annotationModel.getUserId().equals(user.getId())) {
                                annotationModel.setUserImage(user.getThumbnail());
                              }
                              return annotationModel;
                            })
                        .collect(Collectors.toList())))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_UPDATE)
  @PreAuthorize("hasAuthority('ANNOTATION_UPDATE')")
  @Message(entityAction = EntityAction.ANNOTATION_UPDATE, services = Services.NOTIFICATIONS)
  public ResponseModel update(String id, TextNoteAnnotationModel annotationModel) {
    log.debug(
        "update text note annotation for annotation with id {} and values {}", id, annotationModel);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                annotationRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        annotation -> {
                          if (!annotation.getUserId().equals(user.getId())) {
                            throw new NotPermittedException();
                          }
                          annotation.setTextBody(annotationModel.getTextBody());
                          annotation.setIsPublic(annotationModel.getIsPublic());
                          annotation.setStartX(annotationModel.getStartX());
                          annotation.setStartY(annotationModel.getStartY());
                          annotation.setEndX(annotationModel.getEndX());
                          annotation.setEndY(annotationModel.getEndY());
                          annotation.setStartIndex(annotationModel.getStartIndex());
                          annotation.setEndIndex(annotationModel.getEndIndex());
                          annotation.setStartIndex(annotationModel.getStartIndex());
                          annotation.setEndIndex(annotationModel.getEndIndex());
                          annotation.setPageNumber(annotationModel.getPageNumber());
                          annotationRepository.save(annotation);
                          log.debug("Annotation updated ");

                          Content content = contentRepository.getOne(annotation.getContentId());

                          return ResponseModel.done(
                              (Object) annotation.getId(),
                              new AnnotationMessage(
                                  annotation.getId(),
                                  annotation.getContentId(),
                                  content.getName(),
                                  annotation.getSpaceId(),
                                  content.getSpace().getName(),
                                  content.getSpace().getCategory().getName(),
                                  new From(SecurityUtils.getCurrentUser())));
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_UPDATE)
  @PreAuthorize("hasAuthority('ANNOTATION_UPDATE')")
  public ResponseModel update(String id, FreehandAnnotationModel annotationModel) {
    log.debug(
        "update voice note annotation for annotation with id {} and values {}",
        id,
        annotationModel);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                annotationRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        annotation -> {
                          if (!user.getId().equals(annotation.getUserId())) {
                            throw new NotPermittedException();
                          }
                          annotation.setStartX(annotationModel.getStartX());
                          annotation.setStartY(annotationModel.getStartY());
                          annotation.setEndX(annotationModel.getEndX());
                          annotation.setEndY(annotationModel.getEndY());
                          annotation.setStartIndex(annotationModel.getStartIndex());
                          annotation.setEndIndex(annotationModel.getEndIndex());
                          annotation.setFreeHandPointList(annotationModel.getPoints());
                          annotation.setColor(annotationModel.getColor());
                          annotation.setPageScale(annotationModel.getPageScale());
                          annotation.setPageNumber(annotationModel.getPageNumber());
                          annotation.setFreeHandPointList(annotationModel.getPoints());
                          annotationRepository.save(annotation);
                          log.debug("Annotation updated ");
                          return ResponseModel.done((Object) id);
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Auditable(EntityAction.ANNOTATION_UPDATE)
  @PreAuthorize("hasAuthority('ANNOTATION_UPDATE')")
  public ResponseModel update(String id, VoiceNoteAnnotationModel annotationModel) {
    log.debug(
        "update voice note annotation for annotation with id {} and values {}",
        id,
        annotationModel);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                annotationRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        annotation -> {
                          if (!user.getId().equals(annotation.getUserId())) {
                            throw new NotPermittedException();
                          }

                          annotation.setAudioUrl(annotationModel.getAudioUrl());
                          annotation.setStartX(annotationModel.getStartX());
                          annotation.setStartY(annotationModel.getStartY());
                          annotation.setEndX(annotationModel.getEndX());
                          annotation.setEndY(annotationModel.getEndY());
                          annotation.setStartIndex(annotationModel.getStartIndex());
                          annotation.setEndIndex(annotationModel.getEndIndex());
                          annotation.setPageNumber(annotationModel.getPageNumber());
                          annotationRepository.save(annotation);
                          log.debug("Annotation updated ");
                          return ResponseModel.done((Object) id);
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_UPDATE)
  @PreAuthorize("hasAuthority('ANNOTATION_UPDATE')")
  public ResponseModel update(String id, StyleAnnotationModel annotationModel) {
    log.debug(
        "update style annotation for annotation with id {} and values {}", id, annotationModel);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                annotationRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        annotation -> {
                          if (!user.getId().equals(annotation.getUserId())) {
                            throw new NotPermittedException();
                          }
                          annotation.setColor(annotationModel.getColor());
                          annotation.setStartX(annotationModel.getStartX());
                          annotation.setStartY(annotationModel.getStartY());
                          annotation.setEndX(annotationModel.getEndX());
                          annotation.setEndY(annotationModel.getEndY());
                          annotation.setStartIndex(annotationModel.getStartIndex());
                          annotation.setEndIndex(annotationModel.getEndIndex());
                          annotation.setIsPublic(annotationModel.getIsPublic());
                          annotation.setPageNumber(annotationModel.getPageNumber());
                          annotationRepository.save(annotation);
                          return ResponseModel.done((Object) id);
                        })
                    .orElse(ResponseModel.error(Code.NOT_FOUND)))
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @PreAuthorize("hasAuthority('ANNOTATION_READ')")
  public ResponseModel get(Long id, Integer page) {
    log.debug("get annotation with id {} and page {}", id, page);
    User user =
        userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).get();
    return ResponseModel.done(
        annotationRepository
            .findByContentIdAndPageNumberAndDeletedFalse(id, page)
            .stream()
            .map(mapperService::mapAnnotation)
            .filter(
                annotationModel ->
                    annotationModel.getPublic().equals(TRUE)
                        || (annotationModel.getUserId().equals(user.getId())
                            && !annotationModel.getPublic().equals(TRUE)))
            .collect(Collectors.toList()));
  }

  @Transactional
  @PreAuthorize("hasAuthority('ANNOTATION_READ')")
  public ResponseModel get(Long id, AnnotationType annotationType) {
    log.debug("get annotation with id {} and annotation type {}", id, annotationType);
    User user =
        userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).get();
    return ResponseModel.done(
        annotationRepository
            .findByContentIdAndAnnotationTypeAndDeletedFalse(id, annotationType)
            .stream()
            .filter(
                annotationModel ->
                    annotationModel.getIsPublic().equals(TRUE)
                        || (annotationModel.getUserId().equals(user.getId())
                            && !annotationModel.getIsPublic().equals(TRUE)))
            .map(
                annotation -> {
                  AnnotationModel annotationModel = mapperService.mapAnnotation(annotation);
                  if (annotationModel.getUserId().equals(user.getId())) {
                    annotationModel.setUserImage(user.getThumbnail());
                  }
                  return annotationModel;
                })
            .collect(Collectors.toList()));
  }

  @Transactional
  @PreAuthorize("hasAuthority('ANNOTATION_READ')")
  public ResponseModel get(Long id, Integer page, AnnotationType annotationType) {
    log.debug(
        "get annotation with id {} and page {} and annotation type {}", id, page, annotationType);
    User user =
        userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).get();
    return ResponseModel.done(
        annotationRepository
            .findByContentIdAndPageNumberAndAnnotationTypeAndDeletedFalse(id, page, annotationType)
            .stream()
            .filter(
                annotationModel ->
                    annotationModel.getPublic().equals(TRUE)
                        || (annotationModel.getUserId().equals(user.getId())
                            && !annotationModel.getPublic().equals(TRUE)))
            .map(
                annotation -> {
                  AnnotationModel annotationModel = mapperService.mapAnnotation(annotation);
                  if (annotationModel.getUserId().equals(user.getId())) {
                    annotationModel.setUserImage(user.getThumbnail());
                  }
                  return annotationModel;
                })
            .collect(Collectors.toList()));
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_COMMENT_CREATE)
  @PreAuthorize("hasAuthority('ANNOTATION_COMMENT_CREATE')")
  public ResponseModel addComment(String id, CommentCreateModel commentCreateModel) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                annotationRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        annotation -> {
                          if (annotation.getIsPublic()
                              || annotation.getUserId().equals(user.getId())) {
                            Comment comment =
                                commentService.createComment(
                                    annotation.getId(),
                                    user,
                                    commentCreateModel,
                                    CommentType.ANNOTATION,
                                    annotation.getSpaceId());
                            annotation.getComments().add(comment);
                            //TODO:Tobe removed after Wihle
                            List<Comment> unUpdateComment =
                                annotation
                                    .getComments()
                                    .stream()
                                    .filter(c -> c != null && c.getSpaceId() == null)
                                    .map(
                                        com -> {
                                          com.setSpaceId(annotation.getSpaceId());
                                          com.setType(CommentType.ANNOTATION);
                                          return com;
                                        })
                                    .collect(Collectors.toList());
                            commentRepository.save(unUpdateComment);
                            /////////////////////////////////////////
                            annotation.setLastModifiedDate(new Date());
                            annotation.setLastModifiedBy(user.getUserName());
                            annotationRepository.save(annotation);

                            return ResponseModel.done(mapperService.mapComment(comment));
                          } else {
                            throw new NotPermittedException();
                          }
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_COMMENT_LIKE)
  public ResponseModel likeComment(String id) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(user -> commentService.toggleCommentLike(id, user))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_LIKE)
  public ResponseModel likeAnnotation(String id) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                annotationRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        annotation -> {
                          if (annotation.getIsPublic()
                              || annotation.getUserId().equals(user.getId())) {
                            likeRepository
                                .findOneByUserIdAndParentIdAndDeletedFalse(user.getId(), annotation.getId())
                                .map(
                                    like -> {
                                      likeRepository.delete(like);
                                      annotation.getLikes().remove(like);
                                      annotation.setNumberOfLikes(
                                          annotation.getNumberOfLikes() - 1);
                                      annotationRepository.save(annotation);
                                      return ResponseModel.done(0);
                                    })
                                .orElseGet(
                                    () -> {
                                      Like like =
                                          commentService.newLike(annotation.getId(), user.getId());
                                      like.setUserName(user.getUserName());
                                      likeRepository.save(like);
                                      annotation.getLikes().add(like);
                                      annotation.setNumberOfLikes(
                                          annotation.getNumberOfLikes() + 1);
                                      annotationRepository.save(annotation);
                                      return ResponseModel.done(1);
                                    });
                          } else {
                            throw new NotPermittedException();
                          }
                          return ResponseModel.done();
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_COMMENT_DELETE)
  @PreAuthorize("hasAuthority('ANNOTATION_COMMENT_DELETE')")
  public ResponseModel deleteComment(String id) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                commentRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        comment -> {
                          annotationRepository
                              .findOneByIdAndDeletedFalse(comment.getParentId())
                              .map(
                                  annotation -> {
                                    Content content =
                                        contentRepository
                                            .findOneByIdAndDeletedFalse(annotation.getSpaceId())
                                            .orElseThrow(NotFoundException::new);
                                    if (comment.getUserId().equals(user.getId())
                                        || annotation.getUserId().equals(user.getId())
                                        || content.getOwner().equals(user)
                                        || content.getSpace().getUser().equals(user)) {
                                      commentService.deleteComment(id);
                                      return ResponseModel.done();
                                    } else {
                                      throw new NotPermittedException();
                                    }
                                  })
                              .orElseThrow(NotFoundException::new);
                          return ResponseModel.done();
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ANNOTATION_COMMENT_UPDATE)
  @PreAuthorize("hasAuthority('ANNOTATION_COMMENT_UPDATE')")
  public ResponseModel editComment(String id, String body) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                commentRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        comment -> {
                          if (comment.getUserId().equals(user.getId())) {
                            return commentService.updateComment(id, body);
                          } else {
                            throw new NotPermittedException();
                          }
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotFoundException::new);
  }

  void deletebyContentId(Long contentId) {
    List<Annotation> annotations = annotationRepository.deleteByContentId(contentId);
    if (annotations != null && !annotations.isEmpty()) {
      commentService.deleteCommentbyParentIn(
          annotations.stream().map(Annotation::getId).collect(Collectors.toSet()));
    }
  }

  private Boolean isValidPoint(Double point) {

    if (null == point) {
      return FALSE;
    } else if (point <= -1) {
      return FALSE;
    } else {
      return TRUE;
    }
  }
}
