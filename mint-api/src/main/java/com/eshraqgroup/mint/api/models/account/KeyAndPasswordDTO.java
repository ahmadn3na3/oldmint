package com.eshraqgroup.mint.api.models.account;

import javax.validation.constraints.Size;
import com.eshraqgroup.mint.api.models.user.UserCreateModel;

public class KeyAndPasswordDTO {

  private String key;

  @Size(
    min = UserCreateModel.PASSWORD_MIN_LENGTH,
    max = UserCreateModel.PASSWORD_MAX_LENGTH,
    message = "error.password.length"
  )
  private String newPassword;

  public KeyAndPasswordDTO() {}

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }

  @Override
  public String toString() {
    return String.format("KeyAndPasswordDTO{key='%s', newPassword='%s'}", key, newPassword);
  }
}
