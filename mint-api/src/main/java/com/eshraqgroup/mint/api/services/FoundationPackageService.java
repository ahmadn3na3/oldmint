package com.eshraqgroup.mint.api.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.api.models.foundationpackage.FoundationPackageCreateModel;
import com.eshraqgroup.mint.api.models.foundationpackage.FoundationPackageModel;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.FoundationPackage;
import com.eshraqgroup.mint.domain.jpa.Module;
import com.eshraqgroup.mint.exception.ExistException;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.FoundationPackageRepository;
import com.eshraqgroup.mint.repos.jpa.FoundationRepository;
import com.eshraqgroup.mint.repos.jpa.ModuleRepository;

/** Created by ahmad on 4/18/17. */
@Service
public class FoundationPackageService {
  private final FoundationPackageRepository foundationPackageRepository;
  private final ModuleRepository moduleRepository;
  private final FoundationRepository foundationRepository;

  @Autowired
  public FoundationPackageService(
      FoundationPackageRepository foundationPackageRepository,
      ModuleRepository moduleRepository,
      FoundationRepository foundationRepository) {
    this.foundationPackageRepository = foundationPackageRepository;
    this.moduleRepository = moduleRepository;
    this.foundationRepository = foundationRepository;
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel createFoundationPackage(
      FoundationPackageCreateModel foundationPackageCreateModel) {

    FoundationPackage foundationPackage =
        foundationPackageRepository.findByNameAndDeletedFalse(
            foundationPackageCreateModel.getName());
    if (foundationPackage != null) {
      throw new ExistException("foundationpackage");
    }
    foundationPackage = new FoundationPackage();
    foundationPackage.setName(foundationPackageCreateModel.getName());
    foundationPackage.setPackageTimeLimit(foundationPackageCreateModel.getPackageTimeLimit());
    foundationPackage.setStorage(foundationPackageCreateModel.getStorage());
    foundationPackage.setBroadcastMessages(foundationPackageCreateModel.getBroadcastMessages());
    foundationPackage.setIntegrationWithSIS(foundationPackageCreateModel.getIntegrationWithSIS());
    foundationPackage.setNumberOfUsers(foundationPackageCreateModel.getNumberOfUsers());
    foundationPackage.setNumberOfOrganizations(
        foundationPackageCreateModel.getNumberOfOrganizations());

    if (!foundationPackageCreateModel.getModules().isEmpty()) {
      List<Module> modules = moduleRepository.findAll(foundationPackageCreateModel.getModules());
      foundationPackage.getModules().addAll(modules);
    }

    foundationPackageRepository.save(foundationPackage);

    return ResponseModel.done(foundationPackage.getId());
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel updateFoundationPackage(
      Long id, FoundationPackageCreateModel foundationPackageCreateModel) {

    FoundationPackage foundationPackage = foundationPackageRepository.findOne(id);
    if (foundationPackage == null) {
      throw new NotFoundException();
    }

    if (!foundationPackageCreateModel.getName().equals(foundationPackage.getName())) {
      FoundationPackage tempFoundationPackage =
          foundationPackageRepository.findByNameAndDeletedFalse(
              foundationPackageCreateModel.getName());
      if (tempFoundationPackage != null && !id.equals(tempFoundationPackage.getId())) {
        throw new ExistException("foundationpackage");
      }
    }

    foundationPackage.setName(foundationPackageCreateModel.getName());
    foundationPackage.setPackageTimeLimit(foundationPackageCreateModel.getPackageTimeLimit());
    foundationPackage.setStorage(foundationPackageCreateModel.getStorage());
    foundationPackage.setBroadcastMessages(foundationPackageCreateModel.getBroadcastMessages());
    foundationPackage.setIntegrationWithSIS(foundationPackageCreateModel.getIntegrationWithSIS());
    foundationPackage.setNumberOfUsers(foundationPackageCreateModel.getNumberOfUsers());
    foundationPackage.setNumberOfOrganizations(
        foundationPackageCreateModel.getNumberOfOrganizations());
    if (!foundationPackageCreateModel.getModules().isEmpty()) {
      if (!foundationPackage.getModules().isEmpty()) {
        foundationPackage.getModules().clear();
        foundationPackageRepository.save(foundationPackage);
      }

      List<Module> modules = moduleRepository.findAll(foundationPackageCreateModel.getModules());
      foundationPackage.getModules().addAll(modules);
    }

    foundationPackageRepository.save(foundationPackage);
    return ResponseModel.done();
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel deleteFoundationPackage(Long id) {

    FoundationPackage foundationPackage = foundationPackageRepository.findOne(id);
    if (foundationPackage == null) {
      throw new NotFoundException();
    }

    if (foundationPackage
            .getFoundation()
            .stream()
            .filter(foundation -> !foundation.isDeleted())
            .count()
        > 0) {
      throw new InvalidException("error.foundationpackage.foundation");
    }
    foundationPackageRepository.delete(id);
    return ResponseModel.done();
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public PageResponseModel getAll(PageRequest pageRequest) {
    Page<FoundationPackageModel> foundationPackagePage =
        foundationPackageRepository.findAll(pageRequest).map(FoundationPackageModel::new);
    return PageResponseModel.done(
        foundationPackagePage.getContent(),
        foundationPackagePage.getTotalPages(),
        foundationPackagePage.getNumber(),
        Long.valueOf(foundationPackagePage.getTotalElements()).intValue());
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel get(Long id) {
    FoundationPackage foundationPackage = foundationPackageRepository.findOne(id);
    if (foundationPackage == null) {
      throw new NotFoundException();
    }
    return ResponseModel.done(new FoundationPackageModel(foundationPackage));
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel assign(Long id, List<Long> foundationIds) {
    FoundationPackage foundationPackage = foundationPackageRepository.findOne(id);
    if (foundationPackage == null) {
      throw new NotFoundException("package");
    }
    if (foundationIds.isEmpty()) {
      throw new InvalidException("error.foundtionpackage.assign.empty");
    }
    List<Foundation> foundations = foundationRepository.findAll(foundationIds);
    if (foundations.isEmpty()) {
      throw new NotFoundException("foundation");
    }
    foundationPackage.getFoundation().addAll(foundations);
    foundationPackageRepository.save(foundationPackage);
    return ResponseModel.done();
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel unassign(Long id, List<Long> foundationIds) {
    FoundationPackage foundationPackage = foundationPackageRepository.findOne(id);
    if (foundationPackage == null) {
      throw new NotFoundException("package");
    }
    if (foundationIds.isEmpty()) {
      throw new InvalidException("error.foundtionpackage.assign.empty");
    }
    List<Foundation> foundations = foundationRepository.findAll(foundationIds);
    if (foundations.isEmpty()) {
      throw new NotFoundException("foundation");
    }
    foundationPackage.getFoundation().removeAll(foundations);
    foundationPackageRepository.save(foundationPackage);
    return ResponseModel.done();
  }
}
