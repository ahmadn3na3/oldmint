package com.eshraqgroup.mint.api.services;

import static org.springframework.data.jpa.domain.Specifications.where;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.api.models.group.GroupCreateModel;
import com.eshraqgroup.mint.api.models.group.GroupModel;
import com.eshraqgroup.mint.api.models.user.UserInfoModel;
import com.eshraqgroup.mint.api.models.user.UserModel;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.Gender;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Groups;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.exception.ExistException;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.IdModel;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.models.ToggleStatusModel;
import com.eshraqgroup.mint.repos.jpa.FoundationRepository;
import com.eshraqgroup.mint.repos.jpa.GroupsRepository;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.OrganizationRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.PermissionCheck;

/** Created by ahmad on 3/7/16. */
@Service
public class GroupService {
  private final Logger log = LoggerFactory.getLogger(GroupService.class);

  private final GroupsRepository groupRepository;

  private final UserRepository userRepository;

  private final FoundationRepository foundationRepository;

  private final OrganizationRepository organizationRepository;

  private final JoinedRepository joinedRepository;

  private final SpaceService spaceService;

  private final Mapper mapper;

  @Autowired
  public GroupService(
      GroupsRepository groupRepository,
      UserRepository userRepository,
      FoundationRepository foundationRepository,
      OrganizationRepository organizationRepository,
      JoinedRepository joinedRepository,
      SpaceService spaceService,
      Mapper mapper) {
    this.groupRepository = groupRepository;
    this.userRepository = userRepository;
    this.foundationRepository = foundationRepository;
    this.organizationRepository = organizationRepository;
    this.joinedRepository = joinedRepository;
    this.spaceService = spaceService;
    this.mapper = mapper;
  }

  @Deprecated
  @Transactional
  @Auditable(EntityAction.GROUP_CREATE)
  @PreAuthorize("hasAuthority('GROUP_CREATE') AND hasAuthority('ADMIN')")
  public ResponseModel create(GroupCreateModel groupCreateModel, Long organizationId) {
    log.debug("create group {} in organization {}", groupCreateModel.getName(), organizationId);

    return organizationRepository
        .findOneByIdAndDeletedFalse(organizationId)
        .map(
            organization -> {
              if (groupRepository
                  .findOneByNameAndDeletedFalse(
                      String.format("%s@%s", groupCreateModel.getName(), organization.getOrgId()))
                  .isPresent()) {
                log.debug(
                    "group {} already exist in organization {}",
                    groupCreateModel.getName(),
                    organizationId);
                throw new ExistException("name");
              }
              Groups groups = new Groups();
              groups.setName(
                  String.format("%s@%s", groupCreateModel.getName(), organization.getOrgId()));
              if (groupCreateModel.getGender() != null) {
                groupCreateModel.getTags().add(0, groupCreateModel.getGender().name());
              }
              if (groupCreateModel.getTags() != null) {
                groups.setTags(String.join(",", groupCreateModel.getTags()));
              }
              if (groupCreateModel.getCanAccess() != null) {
                groups.setCanAccess(String.join(",", groupCreateModel.getCanAccess()));
              }
              groups.setOrganization(organization);
              groups.setFoundation(organization.getFoundation());
              groupRepository.save(groups);
              log.debug("group created");
              return ResponseModel.done(new IdModel(groups.getId()));
            })
        .orElseThrow(() -> new NotFoundException("organization"));
  }

  @Transactional
  @Auditable(EntityAction.GROUP_CREATE)
  @PreAuthorize("hasAuthority('GROUP_CREATE') AND hasAuthority('ADMIN')")
  public ResponseModel create(GroupCreateModel groupCreateModel) {
    log.debug("create group {}", groupCreateModel.getName());
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              PermissionCheck.checkUserForFoundationAndOrgOperation(
                  user, groupCreateModel.getOrganizationId(), groupCreateModel.getFoundationId());
              Foundation foundation =
                  groupCreateModel.getFoundationId() != null
                      ? foundationRepository.findOne(groupCreateModel.getFoundationId())
                      : user.getFoundation();

              Organization organization =
                  groupCreateModel.getOrganizationId() != null
                      ? organizationRepository.findOne(groupCreateModel.getOrganizationId())
                      : user.getOrganization();

              if (foundation == null && organization == null) {
                throw new InvalidException("error.groups.valid.orgfound");
              }

              String groupName =
                  organization != null
                      ? String.format("%s@%s", groupCreateModel.getName(), organization.getOrgId())
                      : String.format("%s@%s", groupCreateModel.getName(), foundation.getCode());

              if (groupRepository.findOneByNameAndDeletedFalse(groupName).isPresent()) {
                log.warn("group {} already exist", groupCreateModel.getName());
                throw new ExistException("error.groups.name.exists");
              }
              Groups groups = new Groups();
              groups.setName(groupName);
              if (groupCreateModel.getGender() != null) {
                groupCreateModel.getTags().add(0, groupCreateModel.getGender().name());
              }
              groups.setTags(String.join(",", groupCreateModel.getTags()));
              groups.setCanAccess(String.join(",", groupCreateModel.getCanAccess()));
              groups.setOrganization(organization);
              groups.setFoundation(foundation == null ? organization.getFoundation() : foundation);
              groupRepository.save(groups);
              return ResponseModel.done(new IdModel(groups.getId()));
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('GROUP_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getGroups(
      PageRequest page, Long foundationId, Long organizationId, String filter, boolean all) {
    log.debug("get groups");
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              log.debug("get Users");
              Specification<Groups> byFoundation = null;
              Specification<Groups> byOrganization = null;
              Specification<Groups> name = null;

              if (foundationId != null) {
                Optional<Foundation> foundationIns =
                    foundationRepository.findOneByIdAndDeletedFalse(foundationId);
                if (foundationIns.isPresent()) {
                  byFoundation =
                      (root, criteriaQuery, criteriaBuilder) ->
                          criteriaBuilder.equal(root.get("foundation"), foundationIns.get());

                } else {
                  log.warn("foundation {} not found", foundationId);
                  throw new NotFoundException("error.foundation.notfound");
                }
              }

              if (organizationId != null) {
                Optional<Organization> org =
                    organizationRepository.findOneByIdAndDeletedFalse(organizationId);
                if (org.isPresent()) {
                  byOrganization =
                      (root, criteriaQuery, criteriaBuilder) ->
                          root.get("organization").in(org.get());
                } else {
                  log.warn("organization {} not found", organizationId);
                  throw new NotFoundException("error.organization.notfound");
                }
              } else {
                byOrganization = all ? null : (root, cq, cb) -> cb.isNull(root.get("organization"));
              }

              if (filter != null) {
                name =
                    (root, criteriaQuery, criteriaBuilder) ->
                        criteriaBuilder.like(
                            criteriaBuilder.lower(root.get("name").as(String.class)),
                            "%" + filter.toLowerCase() + "%");
              }
              switch (user.getType()) {
                case SUPER_ADMIN:
                case SYSTEM_ADMIN:
                  break;
                case ADMIN:
                  byOrganization =
                      (root, criteriaQuery, criteriaBuilder) ->
                          root.get("organization").in(user.getOrganization());
                  byFoundation =
                      (root, criteriaQuery, criteriaBuilder) ->
                          root.get("foundation").in(user.getFoundation());

                  break;
                case FOUNDATION_ADMIN:
                  byFoundation =
                      (root, criteriaQuery, criteriaBuilder) ->
                          root.get("foundation").in(user.getFoundation());
                  log.debug("foundation ADMIN COLLECTED");
                  break;
                default:
                  log.warn("user {} not permitted", SecurityUtils.getCurrentUserLogin());
                  throw new NotPermittedException();
              }

              Page<GroupModel> groupModels =
                  groupRepository
                      .findAll(
                          where(byOrganization)
                              .and(byFoundation)
                              .and(name)
                              .and(
                                  (root, criteriaQuery, criteriaBuilder) ->
                                      criteriaBuilder.equal(root.get("deleted"), false)),
                          page)
                      .map(this::getGroupModel);
              return PageResponseModel.done(
                  groupModels.getContent(),
                  groupModels.getTotalPages(),
                  groupModels.getNumber(),
                  groupModels.getTotalElements());
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('GROUP_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getGroup(Long id) {
    log.debug("get group by id {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Groups groups = groupRepository.findOne(id);
              if (groups == null) {
                log.warn("group {} not found", id);
                throw new NotFoundException();
              }
              return ResponseModel.done(getGroupModel(groups));
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('GROUP_ASSIGN_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getUsers(Long id) {
    log.debug("Get users in group {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Groups groups = groupRepository.findOne(id);
              if (groups == null) {
                log.warn("group {} not found", id);
                throw new NotFoundException();
              }

              List<UserInfoModel> userInfoModels =
                  groups.getUsers().stream().map(UserModel::new).collect(Collectors.toList());

              return ResponseModel.done(userInfoModels);
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.GROUP_DELETE)
  @PreAuthorize("hasAuthority('GROUP_DELETE') AND hasAuthority('ADMIN')")
  public ResponseModel delete(Long id) {
    log.debug("Delete Group {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                Optional.ofNullable(groupRepository.findOne(id))
                    .map(
                        groups -> {
                          if (groups.getUsers().size() > 0) {
                            removeUserFromGroup(
                                groups
                                    .getUsers()
                                    .stream()
                                    .map(User::getId)
                                    .collect(Collectors.toList()),
                                id);
                          }
                          groupRepository.delete(id);
                          log.debug("group {} deleted", id);
                          return ResponseModel.done();
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.GROUP_DELETE)
  @PreAuthorize("hasAuthority('GROUP_DELETE') AND hasAuthority('ADMIN')")
  public ResponseModel delete(List<Long> ids) {
    if (ids != null && !ids.isEmpty()) {
      for (Long id : ids) {
        delete(id);
      }
    }
    return ResponseModel.done();
  }

  @Transactional
  @Auditable(EntityAction.GROUP_UPDATE)
  @PreAuthorize("hasAuthority('GROUP_UPDATE') AND hasAuthority('ADMIN')")
  public ResponseModel update(Long id, GroupCreateModel groupCreateModel) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                Optional.ofNullable(groupRepository.findOne(id))
                    .map(
                        groups -> {
                          groupCreateModel.setName(groupCreateModel.getName().split("@")[0]);
                          String groupName = null;
                          if (groups.getOrganization() != null) {
                            groupName =
                                String.format(
                                    "%s@%s",
                                    groupCreateModel.getName(),
                                    groups.getOrganization().getOrgId());
                          } else {
                            groupName =
                                String.format(
                                    "%s@%s",
                                    groupCreateModel.getName(), groups.getFoundation().getCode());
                          }

                          if (!groups.getName().equalsIgnoreCase(groupName)
                              && groupRepository
                                  .findOneByNameAndDeletedFalse(groupName)
                                  .isPresent()) {
                            throw new ExistException("name");
                          }

                          groups.setName(groupName);
                          if (groupCreateModel.getGender() != null) {
                            groupCreateModel.getTags().add(0, groupCreateModel.getGender().name());
                          }
                          if (groupCreateModel.getTags() != null) {
                            groups.setTags(String.join(",", groupCreateModel.getTags()));
                          }
                          if (groupCreateModel.getCanAccess() != null) {
                            groups.setCanAccess(String.join(",", groupCreateModel.getCanAccess()));
                          }
                          groupRepository.save(groups);
                          return ResponseModel.done(new IdModel(groups.getId()));
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.USER_UPDATE)
  @PreAuthorize("hasAuthority('USER_UPDATE') AND hasAuthority('ADMIN')")
  public ResponseModel toggleGroupStatus(ToggleStatusModel toggleStatusModel) {
    log.debug("Toggle Group Status : {}", toggleStatusModel);

    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                Optional.ofNullable(groupRepository.findOne(toggleStatusModel.getId()))
                    .map(
                        groups -> {
                          Set<User> users = groups.getUsers();
                          if (users != null) {
                            users.forEach(u -> u.setStatus(toggleStatusModel.getStatus()));
                            userRepository.save(users);
                            log.debug("group status updated");
                          }
                          return ResponseModel.done();
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.GROUP_UPDATE)
  @PreAuthorize("hasAuthority('GROUP_ASSIGN_CREATE') AND hasAuthority('ADMIN')")
  public ResponseModel assignUserToGroup(List<Long> usersId, Long groupId) {
    log.debug("assign user {} to group {}", usersId, groupId);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                Optional.ofNullable(groupRepository.findOne(groupId))
                    .map(
                        groups -> {
                          Set<User> users =
                              userRepository.findAll(usersId).stream().collect(Collectors.toSet());

                          groups.getUsers().addAll(users);

                          groupRepository.save(groups);
                          log.debug("user {} assigned to group {}", usersId, groupId);
                          Set<Space> spaces =
                              joinedRepository
                                  .findByGroupNameAndDeletedFalse(String.valueOf(groups.getId()))
                                  .map(Joined::getSpace)
                                  .collect(Collectors.toSet());
                          if (spaces.isEmpty()) {
                            spaces =
                                joinedRepository
                                    .findByGroupNameAndDeletedFalse(
                                        String.valueOf(groups.getName()))
                                    .map(Joined::getSpace)
                                    .collect(Collectors.toSet());
                          }

                          Set<Joined> joineds =
                              spaces
                                  .stream()
                                  .flatMap(
                                      space ->
                                          users
                                              .stream()
                                              .map(
                                                  u -> {
                                                    Joined joined =
                                                        joinedRepository
                                                            .findOneBySpaceIdAndUserIdAndDeletedFalse(
                                                                space.getId(), u.getId())
                                                            .orElseGet(() -> new Joined(u, space));
                                                    joined.setGroupName(groups.getId().toString());
                                                    joined.setSpaceRole(SpaceRole.COLLABORATOR);
                                                    return joined;
                                                  }))
                                  .collect(Collectors.toSet());
                          if (!joineds.isEmpty()) {
                            joinedRepository.save(joineds);
                          }
                          log.trace("reshare group");
                          return ResponseModel.done();
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.GROUP_UPDATE)
  @PreAuthorize("hasAuthority('GROUP_ASSIGN_DELETE') AND hasAuthority('ADMIN')")
  public ResponseModel removeUserFromGroup(List<Long> usersId, Long groupId) {
    log.debug("remove user {} from group {}", usersId, groupId);
    if (usersId.isEmpty()) {
      throw new MintException(Code.INVALID);
    }
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                Optional.ofNullable(groupRepository.findOne(groupId))
                    .map(
                        groups -> {
                          List<User> users = userRepository.findAll(usersId);
                          if (users != null && !users.isEmpty()) {
                            groups.getUsers().removeAll(users);
                            groupRepository.save(groups);
                            Set<Joined> joineds =
                                joinedRepository
                                    .findByGroupNameAndDeletedFalse(groups.getId().toString())
                                    .filter(joined -> users.contains(joined.getUser()))
                                    .collect(Collectors.toSet());
                            if (joineds.isEmpty()) {
                              joineds =
                                  joinedRepository
                                      .findByGroupNameAndDeletedFalse(groups.getName())
                                      .filter(joined -> users.contains(joined.getUser()))
                                      .collect(Collectors.toSet());
                            }
                            if (!joineds.isEmpty()) {
                              joinedRepository.delete(joineds);
                            }
                          }
                          log.debug("user {} removed from group {}", usersId, groupId);
                          return ResponseModel.done();
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.GROUP_UPDATE)
  @PreAuthorize(
      "hasAuthority('GROUP_ASSIGN_CREATE')AND hasAuthority('GROUP_ASSIGN_DELETE') AND hasAuthority('ADMIN')")
  public ResponseModel transferUserToGroup(List<Long> userId, Long groupIdFrom, Long groupIdTo) {
    log.debug("transfer user {} from group {} to group {}", userId, groupIdFrom, groupIdTo);
    assignUserToGroup(userId, groupIdTo);
    return removeUserFromGroup(userId, groupIdFrom);
  }

  @Transactional
  @PreAuthorize("hasAuthority('GROUP_ASSIGN_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getSpacesByGroupId(Long id) {
    log.debug("get space is group {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                Optional.ofNullable(groupRepository.findOne(id))
                    .map(groups -> spaceService.getSpaceByGroupName(groups))
                    .orElseThrow(NotPermittedException::new))
        .orElseThrow(NotPermittedException::new);
  }

  public GroupModel getGroupModel(Groups groups) {
    log.debug("get group model");
    GroupModel groupModel = new GroupModel();
    mapper.map(groups, groupModel);
    if (groups.getTags() != null && !groups.getTags().isEmpty()) {
      groupModel.getTags().addAll(Arrays.asList(groups.getTags().split(",")));
      if (groupModel.getTags().get(0).equalsIgnoreCase(Gender.MALE.name())
          || groupModel.getTags().get(0).equalsIgnoreCase(Gender.FEMALE.name())) {
        groupModel.setGender(Gender.valueOf(groupModel.getTags().remove(0)));
      }
    }
    if (groups.getCanAccess() != null && !groups.getCanAccess().isEmpty()) {
      groupModel.getCanAccess().addAll(Arrays.asList(groups.getCanAccess().split(",")));
    }
    groupModel.setSpaceCount(
        joinedRepository.countByDistinctSpaceIdAndGroupName(groups.getId().toString()));
    groupModel.setUserCount(groups.getUsers().size());
    log.debug("group Model got:", groupModel);
    return groupModel;
  }
}
