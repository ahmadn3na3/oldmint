package com.eshraqgroup.mint.api;

import java.io.IOException;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.transaction.annotation.Transactional;

/** Created by ahmad on 5/5/16. */
@Configuration
public class Initializer {

  private final Logger log = LoggerFactory.getLogger(Initializer.class);
  @Autowired MappingMongoConverter mappingMongoConverter;

  @PostConstruct
  @Transactional
  public void contextInitialized() throws IOException {
    log.info("Context Initialized");
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    mappingMongoConverter.setMapKeyDotReplacement("\\+");
  }

  @PreDestroy
  public void contextDestroyed() {
    System.out.println("Context Destroyed");
  }
}
