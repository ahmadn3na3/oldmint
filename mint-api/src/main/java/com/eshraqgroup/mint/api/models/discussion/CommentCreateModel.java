package com.eshraqgroup.mint.api.models.discussion;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/** Created by ayman on 25/08/16. */
public class CommentCreateModel {

  @NotNull(message = "error.comment.body")
  @NotEmpty(message = "error.comment.body")
  private String commentBody;

  public String getCommentBody() {
    return commentBody;
  }

  public void setCommentBody(String commentBody) {
    this.commentBody = commentBody;
  }
}
