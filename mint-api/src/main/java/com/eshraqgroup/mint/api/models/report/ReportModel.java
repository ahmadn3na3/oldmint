package com.eshraqgroup.mint.api.models.report;

import java.util.Date;
import javax.validation.constraints.NotNull;
import com.eshraqgroup.mint.constants.ReportReason;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.fasterxml.jackson.annotation.JsonProperty;

/** Created by ahmad on 5/26/16. */
public class ReportModel {

  private String reporterUserName = SecurityUtils.getCurrentUserLogin();

  @NotNull
  @JsonProperty(required = true, defaultValue = "SYSTEM")
  private String reportedUserName = "SYSTEM";

  @NotNull private ReportReason reportReason;

  private Long spaceId;

  private Date reportDate = new Date();

  private String reportNotes;

  public String getReporterUserName() {
    return reporterUserName;
  }

  public void setReporterUserName(String reporterUserName) {
    this.reporterUserName = reporterUserName;
  }

  public String getReportedUserName() {
    return reportedUserName;
  }

  public void setReportedUserName(String reportedUserName) {
    this.reportedUserName = reportedUserName;
  }

  public ReportReason getReportReason() {
    return reportReason;
  }

  public void setReportReason(ReportReason reportReason) {
    this.reportReason = reportReason;
  }

  public Date getReportDate() {
    return reportDate;
  }

  public void setReportDate(Date reportDate) {
    this.reportDate = reportDate;
  }

  public String getReportNotes() {
    return reportNotes;
  }

  public void setReportNotes(String reportNotes) {
    this.reportNotes = reportNotes;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  @Override
  public String toString() {
    return "ReportModel{"
        + "reporterUserName='"
        + reporterUserName
        + '\''
        + ", reportedUserName='"
        + reportedUserName
        + '\''
        + ", reportReason="
        + reportReason
        + ", reportDate="
        + reportDate
        + ", reportNotes='"
        + reportNotes
        + '\''
        + '}';
  }
}
