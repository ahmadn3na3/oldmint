package com.eshraqgroup.mint.api.models.Annotation;

import com.eshraqgroup.mint.constants.AnnotationType;

/** Created by ayman on 03/08/16. */
public class TextNoteAnnotationModel extends AnnotationBaseModel {

  private String textBody;
  private String positionOnTime;

  public TextNoteAnnotationModel() {
    setAnnotationType(AnnotationType.TEXT_NOTE);
  }

  public String getTextBody() {
    return textBody;
  }

  public void setTextBody(String textBody) {
    this.textBody = textBody;
  }

  public String getPositionOnTime() {
    return positionOnTime;
  }

  public void setPositionOnTime(String positionOnTime) {
    this.positionOnTime = positionOnTime;
  }

  @Override
  public String toString() {
    return "TextNoteAnnotationModel{"
        + "textBody='"
        + textBody
        + '\''
        + ", positionOnTime='"
        + positionOnTime
        + '\''
        + "} "
        + super.toString();
  }
}
