package com.eshraqgroup.mint.api.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.api.models.report.ReportModel;
import com.eshraqgroup.mint.api.services.ReportService;
import com.eshraqgroup.mint.constants.ReportReason;
import com.eshraqgroup.mint.constants.ReportType;
import com.eshraqgroup.mint.controller.abstractcontroller.AbstractController;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

/** Created by ahmad on 5/29/16. */
@RestController
@RequestMapping(path = "/api/report")
public class ReportController extends AbstractController<ReportModel, String> {
  @Autowired ReportService reportService;

  @Autowired MessageSource messageSource;

  @RequestMapping(method = RequestMethod.POST)
  @ApiOperation(value = "Report", notes = "this method is used to report user or space")
  public ResponseModel postReport(@RequestBody @Validated ReportModel reportModel) {
    return reportService.postReport(reportModel);
  }

  @RequestMapping(method = RequestMethod.GET, path = "/reason")
  @ApiOperation(value = "Get Reasons", notes = "this method is used get reporting reasons")
  public ResponseModel getReasons(
      @RequestHeader(required = false, defaultValue = "en") String lang,
      @RequestHeader(required = false) ReportType reportType) {
    Map<ReportReason, String> map = new HashMap<>();
    Stream.of(ReportReason.values())
        .filter(
            reportReason -> {
              if (reportType == null || reportReason.getType() == null) {
                return true;
              }
              return reportReason.getType() == reportType;
            })
        .forEach(
            reportReason -> {
              String message =
                  messageSource.getMessage(
                      reportReason.getMessage(),
                      new Object[0],
                      reportReason.getMessage(),
                      Locale.forLanguageTag(lang));
              map.put(reportReason, message);
            });

    return ResponseModel.done(map);
  }
}
