package com.eshraqgroup.mint.api.models.Annotation;

import java.time.ZonedDateTime;
import java.util.List;
import com.eshraqgroup.mint.api.models.comment.CommentViewModel;
import com.eshraqgroup.mint.api.models.comment.LikeViewModel;
import com.eshraqgroup.mint.constants.AnnotationType;
import com.eshraqgroup.mint.domain.mongo.Annotation;
import com.eshraqgroup.mint.models.annotation.AnnotationPoint;
import com.eshraqgroup.mint.util.DateConverter;

/** Created by ayman on 17/08/16. */
public class AnnotationModel {
  private String id;
  private Long contentId;
  private Long spaceId;
  private Long userId;
  private String userName;
  private String userFullName;
  private String userImage;
  private Boolean isPublic;
  private AnnotationType annotationType;

  private Double startX;
  private Double startY;
  private Double endX;
  private Double endY;

  private Integer startIndex;
  private Integer endIndex;

  private ZonedDateTime creationDate;
  private String color;
  private String audioUrl;
  private String textBody;
  private Integer numberOfLikes;
  private List<CommentViewModel> comments;
  private List<LikeViewModel> likes;
  private Integer pageNumber;
  private List<AnnotationPoint> points;

  public AnnotationModel() {}

  public AnnotationModel(Annotation annotation) {
    this.contentId = annotation.getContentId();
    this.spaceId = annotation.getSpaceId();
    this.userId = annotation.getUserId();
    this.userName = annotation.getUserName();
    this.userFullName = annotation.getUserFullName();
    this.isPublic = annotation.getIsPublic();
    this.annotationType = annotation.getAnnotationType();
    this.startX = annotation.getStartX();
    this.startY = annotation.getStartY();
    this.endX = annotation.getEndX();
    this.endY = annotation.getEndY();
    this.startIndex = annotation.getStartIndex();
    this.endIndex = annotation.getEndIndex();
    this.color = annotation.getColor();
    this.audioUrl = annotation.getAudioUrl();
    this.textBody = annotation.getTextBody();
    this.numberOfLikes = annotation.getNumberOfLikes();
    this.userImage = annotation.getUserImage();
    this.id = annotation.getId();
    this.pageNumber = annotation.getPageNumber();
    this.creationDate = DateConverter.convertDateToZonedDateTime(annotation.getCreationDate());
    this.points = annotation.getFreeHandPointList();
    this.color = annotation.getColor();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getContentId() {
    return contentId;
  }

  public void setContentId(Long contentId) {
    this.contentId = contentId;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Boolean getPublic() {
    return isPublic;
  }

  public void setPublic(Boolean aPublic) {
    isPublic = aPublic;
  }

  public AnnotationType getAnnotationType() {
    return annotationType;
  }

  public void setAnnotationType(AnnotationType annotationType) {
    this.annotationType = annotationType;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getAudioUrl() {
    return audioUrl;
  }

  public void setAudioUrl(String audioUrl) {
    this.audioUrl = audioUrl;
  }

  public String getTextBody() {
    return textBody;
  }

  public void setTextBody(String textBody) {
    this.textBody = textBody;
  }

  public List<CommentViewModel> getComments() {
    return comments;
  }

  public void setComments(List<CommentViewModel> comments) {
    this.comments = comments;
  }

  public List<LikeViewModel> getLikes() {
    return likes;
  }

  public void setLikes(List<LikeViewModel> likes) {
    this.likes = likes;
  }

  public Integer getNumberOfLikes() {
    return numberOfLikes;
  }

  public void setNumberOfLikes(Integer numberOfLikes) {
    this.numberOfLikes = numberOfLikes;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserFullName() {
    return userFullName;
  }

  public void setUserFullName(String userFullName) {
    this.userFullName = userFullName;
  }

  public Double getStartX() {
    return startX;
  }

  public void setStartX(Double startX) {
    this.startX = startX;
  }

  public Double getStartY() {
    return startY;
  }

  public void setStartY(Double startY) {
    this.startY = startY;
  }

  public Double getEndX() {
    return endX;
  }

  public void setEndX(Double endX) {
    this.endX = endX;
  }

  public Double getEndY() {
    return endY;
  }

  public void setEndY(Double endY) {
    this.endY = endY;
  }

  public Integer getStartIndex() {
    return startIndex;
  }

  public void setStartIndex(Integer startIndex) {
    this.startIndex = startIndex;
  }

  public Integer getEndIndex() {
    return endIndex;
  }

  public void setEndIndex(Integer endIndex) {
    this.endIndex = endIndex;
  }

  public String getUserImage() {
    return userImage;
  }

  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer pageNumber) {
    this.pageNumber = pageNumber;
  }

  public List<AnnotationPoint> getPoints() {
    return points;
  }

  public void setPoints(List<AnnotationPoint> points) {
    this.points = points;
  }

  /** @return the creationDate */
  public ZonedDateTime getCreationDate() {
    return creationDate;
  }

  /** @param creationDate the creationDate to set */
  public void setCreationDate(ZonedDateTime creationDate) {
    this.creationDate = creationDate;
  }

  @Override
  public String toString() {
    return "AnnotationModel{"
        + "Id='"
        + id
        + '\''
        + ", contentId="
        + contentId
        + ", spaceId="
        + spaceId
        + ", userId="
        + userId
        + ", userName='"
        + userName
        + '\''
        + ", userFullName='"
        + userFullName
        + '\''
        + ", userImage='"
        + userImage
        + '\''
        + ", isPublic="
        + isPublic
        + ", annotationType="
        + annotationType
        + ", startX="
        + startX
        + ", startY="
        + startY
        + ", endX="
        + endX
        + ", endY="
        + endY
        + ", startIndex="
        + startIndex
        + ", endIndex="
        + endIndex
        + ", color='"
        + color
        + '\''
        + ", audioUrl='"
        + audioUrl
        + '\''
        + ", textBody='"
        + textBody
        + '\''
        + ", numberOfLikes="
        + numberOfLikes
        + ", comments="
        + comments
        + ", likes="
        + likes
        + '}';
  }
}
