package com.eshraqgroup.mint.api.models.Annotation;

import com.eshraqgroup.mint.constants.AnnotationType;

/** Created by ayman on 03/08/16. */
public class VoiceNoteAnnotationModel extends AnnotationBaseModel {

  private String audioUrl;

  public VoiceNoteAnnotationModel() {
    setAnnotationType(AnnotationType.VOICE_NOTE);
  }

  public String getAudioUrl() {
    return audioUrl;
  }

  public void setAudioUrl(String audioUrl) {
    this.audioUrl = audioUrl;
  }
}
