package com.eshraqgroup.mint.api.services;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import com.eshraqgroup.mint.api.models.tag.TagCreateModel;
import com.eshraqgroup.mint.api.models.tag.TagModel;
import com.eshraqgroup.mint.constants.Tags;
import com.eshraqgroup.mint.domain.mongo.Tag;
import com.eshraqgroup.mint.exception.ExistException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.mongo.TagRepository;
import com.eshraqgroup.mint.security.SecurityUtils;

/** Created by ahmad on 5/9/17. */
@Service
public class TagService {
  private final TagRepository tagRepository;
  private final UserRepository userRepository;
  private final MessageSource messageSource;

  @Autowired
  public TagService(
      TagRepository tagRepository, UserRepository userRepository, MessageSource messageSource) {
    this.tagRepository = tagRepository;
    this.userRepository = userRepository;
    this.messageSource = messageSource;
  }

  public ResponseModel create(TagCreateModel tagCreateModel) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              if (user.getFoundation() != null) {
                if (tagRepository.findByUserIdAndName(user.getId(), tagCreateModel.getName())
                    != null) {
                  throw new ExistException();
                }
              }
              Tag tag = new Tag();
              tag.setGroup(tagCreateModel.getGroup());
              tag.setName(tagCreateModel.getName());
              tag.setUserId(user.getId());
              if (user.getFoundation() != null) {
                tag.setFoundationId(user.getFoundation().getId());
                if (user.getOrganization() != null) {
                  tag.setOrganizationId(user.getOrganization().getId());
                }
              }
              tagRepository.save(tag);
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  public ResponseModel create(Set<TagCreateModel> tagCreateModels) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Set<Tag> tags = new HashSet<>();
              tagCreateModels.forEach(
                  tagCreateModel -> {
                    if (tagRepository.findByUserIdAndName(user.getId(), tagCreateModel.getName())
                        != null) {
                      return;
                    }
                    Tag tag = new Tag();
                    tag.setGroup(tagCreateModel.getGroup());
                    tag.setName(tagCreateModel.getName());
                    tag.setUserId(user.getId());
                    if (user.getFoundation() != null) {
                      tag.setFoundationId(user.getFoundation().getId());
                      if (user.getOrganization() != null) {
                        tag.setOrganizationId(user.getOrganization().getId());
                      }
                    }
                    tags.add(tag);
                  });

              tagRepository.save(tags);
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  public ResponseModel delete(String id) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Tag tag = tagRepository.findOne(id);
              if (tag != null) {
                tagRepository.delete(tag);
              }
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  public ResponseModel getAll(Locale currentLocale) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Set<TagModel> tagModels = new HashSet<>();
              if (user.getFoundation() != null) {

                tagModels.addAll(
                    tagRepository
                        .findByFoundationId(user.getFoundation().getId())
                        .map(TagModel::new)
                        .collect(Collectors.toSet()));
              } else {
                tagModels.addAll(
                    tagRepository
                        .findByUserId(user.getId())
                        .map(TagModel::new)
                        .collect(Collectors.toSet()));
              }
              String tagGroupNameDifficulty, tagGroupNameSkill, tagName;

              tagGroupNameDifficulty =
                  messageSource.getMessage(
                      Tags.TAG_GROUP_DIFFICULTY,
                      new Object[0],
                      Tags.TAG_GROUP_DIFFICULTY,
                      currentLocale);

              tagGroupNameSkill =
                  messageSource.getMessage(
                      Tags.TAG_GROUP_SKILL, new Object[0], Tags.TAG_GROUP_SKILL, currentLocale);

              for (String s : Tags.TAG_DIFFICULTY) {
                tagName =
                    messageSource.getMessage(
                        String.format("%s.%s", Tags.TAG_GROUP_DIFFICULTY, s),
                        new Object[0],
                        s,
                        currentLocale);
                tagModels.add(new TagModel(tagName, tagGroupNameDifficulty, null));
              }
              for (String s : Tags.TAG_SKILL) {
                tagName =
                    messageSource.getMessage(
                        String.format("%s.%s", Tags.TAG_GROUP_SKILL, s),
                        new Object[0],
                        s,
                        currentLocale);
                tagModels.add(new TagModel(tagName, tagGroupNameSkill, null));
              }

              return ResponseModel.done(tagModels);
            })
        .orElseThrow(NotPermittedException::new);
  }
}
