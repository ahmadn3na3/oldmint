package com.eshraqgroup.mint.api.models.Annotation;

import com.eshraqgroup.mint.constants.AnnotationType;
import com.eshraqgroup.mint.models.annotation.PositionModel;

/** Created by ayman on 02/08/16. */
public class AnnotationBaseModel extends PositionModel {

  private AnnotationType annotationType;

  private Long contentId;

  private Boolean isPublic;

  public Long getContentId() {
    return contentId;
  }

  public void setContentId(Long contentId) {
    this.contentId = contentId;
  }

  public AnnotationType getAnnotationType() {
    return annotationType;
  }

  public void setAnnotationType(AnnotationType annotationType) {
    this.annotationType = annotationType;
  }

  public Boolean getIsPublic() {
    return isPublic;
  }

  public void setIsPublic(Boolean aPublic) {
    isPublic = aPublic;
  }

  public Boolean getPublic() {
    return isPublic;
  }

  public void setPublic(Boolean aPublic) {
    isPublic = aPublic;
  }

  @Override
  public String toString() {
    return "AnnotationBaseModel{"
        + "annotationType="
        + annotationType
        + ", isPublic="
        + isPublic
        + "}";
  }
}
