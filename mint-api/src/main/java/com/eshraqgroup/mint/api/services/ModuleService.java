package com.eshraqgroup.mint.api.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.api.models.modules.ModuleListModel;
import com.eshraqgroup.mint.api.models.modules.ModuleModel;
import com.eshraqgroup.mint.api.models.modules.PermissionModel;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.Module;
import com.eshraqgroup.mint.domain.jpa.Permission;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.ModuleRepository;
import com.eshraqgroup.mint.repos.jpa.PermissionRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;

@Service
public class ModuleService {
  private final Logger log = LoggerFactory.getLogger(MapperService.class);

  private final ModuleRepository moduleRepository;

  private final PermissionRepository permissionRepository;

  private final UserRepository userRepository;

  @Autowired
  public ModuleService(
      ModuleRepository moduleRepository,
      PermissionRepository permissionRepository,
      UserRepository userRepository) {
    this.moduleRepository = moduleRepository;
    this.permissionRepository = permissionRepository;
    this.userRepository = userRepository;
  }

  @Transactional(readOnly = true)
  public ResponseModel getModules() {
    List<Module> modules = moduleRepository.findAll();

    return ResponseModel.done(
        modules.stream().map(ModuleListModel::new).collect(Collectors.toList()));
  }

  @Transactional(readOnly = true)
  public ResponseModel getModule(Long Id) {
    Module module = moduleRepository.findOne(Id);
    ModuleModel model = new ModuleModel(module);
    module
        .getPermissions()
        .forEach(permission -> model.getPermissions().add(new PermissionModel(permission)));
    return ResponseModel.done(model);
  }

  @Transactional(readOnly = true)
  public ResponseModel getPermissions() {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              final Map<String, Set<Byte>> permissionGroup;
              switch (user.getType()) {
                case SUPER_ADMIN:
                case SYSTEM_ADMIN:
                  permissionGroup =
                      permissionRepository
                          .findByTypeInAndDeletedFalse(
                              Arrays.asList(
                                  UserType.FOUNDATION_ADMIN, UserType.ADMIN, UserType.USER))
                          .stream()
                          .collect(
                              Collectors.groupingBy(
                                  Permission::getKeyCode,
                                  HashMap::new,
                                  Collectors.collectingAndThen(
                                      Collectors.toSet(),
                                      permissions ->
                                          permissions
                                              .stream()
                                              .map(permission -> permission.getCode().byteValue())
                                              .collect(Collectors.toSet()))));
                  break;
                case FOUNDATION_ADMIN:
                  permissionGroup =
                      permissionRepository
                          .findByModuleInAndDeletedFalse(
                              user.getFoundation().getFoundationPackage().getModules())
                          .filter(
                              permission ->
                                  permission.getType() == UserType.ADMIN
                                      || permission.getType() == UserType.USER)
                          .collect(
                              Collectors.groupingBy(
                                  Permission::getKeyCode,
                                  HashMap::new,
                                  Collectors.collectingAndThen(
                                      Collectors.toSet(),
                                      permissions ->
                                          permissions
                                              .stream()
                                              .map(permission -> permission.getCode().byteValue())
                                              .collect(Collectors.toSet()))));
                  break;
                case ADMIN:
                  permissionGroup =
                      permissionRepository
                          .findByModuleInAndDeletedFalse(
                              user.getFoundation().getFoundationPackage().getModules())
                          .filter(permission -> permission.getType() == UserType.USER)
                          .collect(
                              Collectors.groupingBy(
                                  Permission::getKeyCode,
                                  HashMap::new,
                                  Collectors.collectingAndThen(
                                      Collectors.toSet(),
                                      permissions ->
                                          permissions
                                              .stream()
                                              .map(permission -> permission.getCode().byteValue())
                                              .collect(Collectors.toSet()))));
                  break;
                default:
                  throw new NotPermittedException();
              }

              return ResponseModel.done(permissionGroup);
            })
        .orElseThrow(NotPermittedException::new);
  }

  @PostConstruct
  @Transactional
  protected void initializeModulesAndPermission() throws IOException {

    ClassPathResource classPathResource = new ClassPathResource("data/module/module.json");
    ObjectMapper objectMapper = new ObjectMapper();
    InputStream file = classPathResource.getInputStream();
    List<ModuleModel> moduleModels =
        objectMapper.readValue(
            file,
            objectMapper
                .getTypeFactory()
                .constructCollectionType(ArrayList.class, ModuleModel.class));
    log.debug("modules ===> {}", moduleModels);

    moduleModels.forEach(
        moduleModel -> {
          Module module = moduleRepository.findOneByKeyCode(moduleModel.getKey());
          if (module == null) {
            addNewModule(moduleModel);

          } else {
            log.debug("updating Module=>{} ", moduleModel.getName());
            moduleModel
                .getPermissions()
                .forEach(
                    permissionModel -> {
                      Permission permission =
                          new Permission(
                              permissionModel.getName(),
                              permissionModel.getKeyCode(),
                              permissionModel.getCode(),
                              permissionModel.getType(),
                              module);
                      Set<Permission> permissions = module.getPermissions();
                      permissions.size();
                      log.trace(
                          "updating permission=>{} and key =>{}",
                          permissionModel.getName(),
                          permissionModel.getKeyCode());
                      if (!permissions.contains(permission)) {
                        if (permissionRepository.findByName(permission.getName()) == null) {
                          permissionRepository.save(permission);
                        }
                      }
                    });
          }
        });
  }

  private void addNewModule(ModuleModel moduleModel) {
    log.debug("adding new Module=>{} ", moduleModel.getName());
    Module module =
        new Module(moduleModel.getName(), moduleModel.getDescription(), moduleModel.getKey());
    moduleRepository.save(module);
    moduleModel
        .getPermissions()
        .forEach(
            permissionModel -> {
              log.trace(
                  "updating permission=>{} and key =>{}",
                  permissionModel.getName(),
                  permissionModel.getKeyCode());
              if (permissionModel.getName() != null) {
                module
                    .getPermissions()
                    .add(
                        new Permission(
                            permissionModel.getName(),
                            permissionModel.getKeyCode(),
                            permissionModel.getCode(),
                            permissionModel.getType(),
                            module));
              }
            });
    moduleRepository.save(module);
  }
}
