package com.eshraqgroup.mint.api.models.account;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;

/** Created by ahmad on 3/3/16. */
public class ResetPasswordModel {

  @NotNull(message = "error.email.null")
  @Email
  private String mail;

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  @Override
  public String toString() {
    return "ResetPasswordModel{" + "mail='" + mail + '\'' + '}';
  }
}
