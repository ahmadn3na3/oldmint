package com.eshraqgroup.mint.api.models.Annotation;

/** Created by ayman on 03/08/16. */
public class StyleAnnotationModel extends AnnotationBaseModel {

  private String styleType;

  private String color;

  public StyleAnnotationModel() {}

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getStyleType() {
    return styleType;
  }

  public void setStyleType(String styleType) {
    this.styleType = styleType;
  }

  @Override
  public String toString() {
    return "StyleAnnotationModel{"
        + "styleType='"
        + styleType
        + '\''
        + ", color='"
        + color
        + '\''
        + "} "
        + super.toString();
  }
}
