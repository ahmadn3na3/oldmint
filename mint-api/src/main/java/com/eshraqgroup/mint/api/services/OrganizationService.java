package com.eshraqgroup.mint.api.services;

import static com.eshraqgroup.mint.constants.notification.EntityAction.ORGANIZATION_CREATE;
import static com.eshraqgroup.mint.constants.notification.EntityAction.ORGANIZATION_DELETE;
import static com.eshraqgroup.mint.constants.notification.EntityAction.ORGANIZATION_UPDATE;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.api.models.organization.OrganizationCreateModel;
import com.eshraqgroup.mint.api.models.organization.OrganizationModel;
import com.eshraqgroup.mint.api.models.user.UserModel;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.GeneralConstant;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Groups;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Role;
import com.eshraqgroup.mint.domain.jpa.TimeLock;
import com.eshraqgroup.mint.exception.ExistException;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.FoundationRepository;
import com.eshraqgroup.mint.repos.jpa.GroupsRepository;
import com.eshraqgroup.mint.repos.jpa.OrganizationRepository;
import com.eshraqgroup.mint.repos.jpa.TimeLockRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;

/** Created by ahmad on 3/2/16. */
@Service
public class OrganizationService {
  private final Logger log = LoggerFactory.getLogger(OrganizationService.class);

  private final OrganizationRepository organizationRepository;

  private final UserRepository userRepository;

  private final UserAdministrationService userService;

  private final CategoryService categoryService;

  private final GroupsRepository groupsRepository;

  private final GroupService groupService;

  private final SpaceService spaceService;

  private final TimeLockRepository timeLockRepository;

  private final FoundationRepository foundationRepository;
  private final RoleService roleService;
  private final DefaultService defaultService;
  private final Mapper mapper;

  @Autowired
  public OrganizationService(
      GroupService groupService,
      FoundationRepository foundationRepository,
      OrganizationRepository organizationRepository,
      CategoryService categoryService,
      GroupsRepository groupsRepository,
      Mapper mapper,
      UserRepository userRepository,
      UserAdministrationService userService,
      SpaceService spaceService,
      TimeLockRepository timeLockRepository,
      RoleService roleService,
      DefaultService defaultService) {
    this.groupService = groupService;
    this.foundationRepository = foundationRepository;
    this.organizationRepository = organizationRepository;
    this.categoryService = categoryService;
    this.groupsRepository = groupsRepository;
    this.mapper = mapper;
    this.userRepository = userRepository;
    this.userService = userService;
    this.spaceService = spaceService;
    this.timeLockRepository = timeLockRepository;
    this.roleService = roleService;
    this.defaultService = defaultService;
  }

  @Transactional
  @Auditable(ORGANIZATION_CREATE)
  @PreAuthorize(
      "hasAuthority('ORGANIZATION_CREATE') AND hasAnyAuthority('SYSTEM_ADMIN','FOUNDATION_ADMIN')")
  public ResponseModel create(OrganizationCreateModel createModel) {
    log.debug("create organization :", createModel);
    if (organizationRepository.findOneByNameAndDeletedFalse(createModel.getName()).isPresent()
        || foundationRepository.findOneByNameAndDeletedFalse(createModel.getName()).isPresent()) {
      log.warn("organization {} already exits", createModel.getName());
      throw new ExistException("name");
    }

    if (organizationRepository
            .findOneByOrgIdAndDeletedFalse(createModel.getOrganizationCode())
            .isPresent()
        || foundationRepository
            .findOneByCodeAndDeletedFalse(createModel.getOrganizationCode())
            .isPresent()) {
      log.warn("organizatio id {} already exist", createModel.getOrganizationCode());
      throw new ExistException("code");
    }
    Foundation foundation = null;
    if (createModel.getFoundationId() != null) {
      foundation = foundationRepository.findOne(createModel.getFoundationId());
      if (foundation == null) {
        log.warn("foundation {} not found", createModel.getFoundationId());
        throw new NotFoundException("foundation");
      }
    }

    if (organizationRepository.countByFoundationAndDeletedFalse(foundation)
        >= foundation.getFoundationPackage().getNumberOfOrganizations()) {
      throw new MintException(Code.INVALID, "error.foundation.org.limit");
    }

    if (!createModel.getOrganizationCode().matches(GeneralConstant.CODE_PATTERN)) {
      throw new InvalidException("error.code.invaild");
    }

    Organization organization = new Organization();
    organization.setActive(createModel.isActive());
    organization.setName(createModel.getName());
    organization.setEndDate(foundation.getEndDate());
    organization.setStartDate(foundation.getStartDate());
    organization.setOrgId(createModel.getOrganizationCode());
    organization.setFoundation(foundation);
    organization.setMessageEnabled(foundation.getFoundationPackage().getBroadcastMessages());
    organization.setGenderSensitivity(foundation.getGenderSensitivity());
    organization.setOrganizationTimeZone(createModel.getOrganizationTimeZone());
    organizationRepository.save(organization);
    defaultService.addDefaultToOrganization(organization);
    log.debug("organization created");
    return ResponseModel.done(organization.getId());
  }

  @Transactional
  @Auditable(ORGANIZATION_UPDATE)
  @PreAuthorize(
      "hasAuthority('ORGANIZATION_UPDATE') AND hasAnyAuthority('SUPER_ADMIN','SYSTEM_ADMIN','FOUNDATION_ADMIN')")
  public ResponseModel update(Long id, OrganizationCreateModel updateModel) {
    log.debug("update organization {} with model", id, updateModel);
    Organization organization = organizationRepository.findOne(id);
    if (organization == null) {
      log.warn("organization {} not found", id);
      throw new NotFoundException();
    }

    if (!updateModel.getName().equals(organization.getName())) {
      Optional<Organization> tempOrganization =
          organizationRepository.findOneByNameAndDeletedFalse(updateModel.getName());
      if (tempOrganization.isPresent() && !id.equals(tempOrganization.get().getId())) {
        throw new ExistException("organization");
      }
    }

    organization.setName(updateModel.getName());
    organization.setEndDate(organization.getFoundation().getEndDate());
    organization.setStartDate(organization.getFoundation().getStartDate());
    organization.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
    organization.setActive(updateModel.isActive());
    organization.setOrganizationTimeZone(updateModel.getOrganizationTimeZone());
    organization
        .getUsers()
        .forEach(
            user -> {
              user.setStatus(updateModel.isActive());
              user.setStartDate(organization.getStartDate());
              user.setEndDate(organization.getEndDate());
            });
    organizationRepository.save(organization);
    log.debug("organization {} updated", updateModel.getOrganizationCode());
    // userRepository.save(organization.getUsers());

    return ResponseModel.done();
  }

  @Transactional(readOnly = true)
  @PreAuthorize(
      "hasAuthority('ORGANIZATION_READ') AND hasAnyAuthority('SUPER_ADMIN','SYSTEM_ADMIN','FOUNDATION_ADMIN','ADMIN')")
  public ResponseModel getOrganization(Long id) {
    log.debug("get organization {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              if (user.getType() == UserType.ADMIN && !user.getOrganization().getId().equals(id)) {
                throw new NotPermittedException();
              }
              return organizationRepository
                  .findOneByIdAndDeletedFalse(id)
                  .map(organization -> ResponseModel.done(mapOrganizationToModel(organization)))
                  .orElseThrow(NotFoundException::new);
            })
        .orElseThrow(NotPermittedException::new);
  }

  @PreAuthorize(
      "hasAuthority('ORGANIZATION_READ') AND hasAnyAuthority('SUPER_ADMIN','SYSTEM_ADMIN','FOUNDATION_ADMIN')")
  @Transactional(readOnly = true)
  public ResponseModel getAllOrganization(PageRequest page) {
    log.debug("get all organizations");

    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              switch (user.getType()) {
                case SUPER_ADMIN:
                case SYSTEM_ADMIN:
                  Page<Organization> systemAdminPage = organizationRepository.findAll(page);
                  return PageResponseModel.done(
                      systemAdminPage
                          .getContent()
                          .stream()
                          .filter(organization1 -> !organization1.getName().equals("mint"))
                          .map(this::mapOrganizationToModel)
                          .collect(Collectors.toList()),
                      systemAdminPage.getTotalPages(),
                      page.getPageNumber(),
                      systemAdminPage.getContent().size());
                case FOUNDATION_ADMIN:
                  Page<Organization> foundationAdminPage =
                      organizationRepository.findByFoundationIdAndDeletedFalse(
                          user.getFoundation().getId(), page);
                  return PageResponseModel.done(
                      foundationAdminPage
                          .getContent()
                          .stream()
                          .map(this::mapOrganizationToModel)
                          .collect(Collectors.toList()),
                      foundationAdminPage.getTotalPages(),
                      page.getPageNumber(),
                      foundationAdminPage.getTotalElements());
                default:
                  log.warn("user {} not permitted", SecurityUtils.getCurrentUserLogin());
                  throw new NotPermittedException();
              }
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Auditable(ORGANIZATION_DELETE)
  @Transactional
  @PreAuthorize(
      "hasAuthority('ORGANIZATION_DELETE') AND hasAnyAuthority('SUPER_ADMIN','SYSTEM_ADMIN','FOUNDATION_ADMIN')")
  public ResponseModel delete(Long id) {
    log.debug("delete organization {}", id);
    if (null != id) {
      return organizationRepository
          .findOneByIdAndDeletedFalse(id)
          .map(
              organization -> {
                if ("MINT".equals(organization.getOrgId())) {
                  log.warn("not permitted to delete MINT");
                  throw new NotPermittedException();
                }
                log.debug("deleting users in organization {}", organization.getId());
                userService.deleteUserInOrganization(organization.getId());

                log.debug("deleting spaces in organization {}", organization.getId());
                spaceService.deleteSpacesInOrganization(organization);

                log.debug("deleting categories in organization {}", organization.getId());
                categoryService.deleteInOrganization(organization);

                log.debug("deleting time locks in organization {}", organization.getId());
                List<TimeLock> timeLocks =
                    timeLockRepository
                        .findByOrganizationAndDeletedFalse(organization)
                        .collect(Collectors.toList());
                if (!timeLocks.isEmpty()) {
                  timeLockRepository.delete(timeLocks);
                }
                groupService.delete(
                    organization
                        .getGroups()
                        .stream()
                        .filter(groups -> !groups.isDeleted())
                        .map(Groups::getId)
                        .collect(Collectors.toList()));
                roleService.delete(
                    organization
                        .getRoles()
                        .stream()
                        .filter(role -> !role.isDeleted())
                        .map(Role::getId)
                        .collect(Collectors.toList()));
                organizationRepository.delete(organization);
                log.debug("organization {} deleted", organization.getId());
                return ResponseModel.done();
              })
          .orElseThrow(NotFoundException::new);
    }
    throw new MintException(Code.INVALID, "id");
  }

  @Auditable(ORGANIZATION_DELETE)
  @Transactional
  @PreAuthorize(
      "hasAuthority('ORGANIZATION_DELETE') AND hasAnyAuthority('SUPER_ADMIN','SYSTEM_ADMIN','FOUNDATION_ADMIN')")
  public boolean delete(Set<Organization> organizationList) {
    log.debug("delete list of organizations");
    if (null != organizationList && !organizationList.isEmpty()) {
      organizationList.forEach(
          organization -> {
            log.debug("deleting users in organization {}", organization.getId());
            userService.deleteUserInOrganization(organization.getId());

            log.debug("deleting spaces in organization {}", organization.getId());
            spaceService.deleteSpacesInOrganization(organization);

            log.debug("deleting caregories in organization {}", organization.getId());
            categoryService.deleteInOrganization(organization);

            log.debug("deleting question bank in organization {}", organization.getId());

            log.debug("deleting time locks in organization {}", organization.getId());
            List<TimeLock> timeLocks =
                timeLockRepository
                    .findByOrganizationAndDeletedFalse((organization))
                    .collect(Collectors.toList());
            if (!timeLocks.isEmpty()) {
              timeLockRepository.delete(timeLocks);
            }
            groupService.delete(
                organization
                    .getGroups()
                    .stream()
                    .filter(groups -> !groups.isDeleted())
                    .map(Groups::getId)
                    .collect(Collectors.toList()));
            roleService.delete(
                organization
                    .getRoles()
                    .stream()
                    .filter(role -> !role.isDeleted())
                    .map(Role::getId)
                    .collect(Collectors.toList()));

            organizationRepository.delete(organization);
          });
      return true;
    }
    return false;
  }

  @Deprecated
  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('USER_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getUsersInOrganization(Long orgId) {
    log.debug("get users in organization {}", orgId);
    return ResponseModel.done(
        userRepository
            .findByOrganizationIdAndDeletedFalseAndOrganizationDeletedFalse(orgId)
            .map(UserModel::new)
            .collect(Collectors.toList()));
  }

  @Deprecated
  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('GROUP_READ')  AND hasAuthority('ADMIN')")
  public ResponseModel getGroupsInOrganization(Long orgId) {
    log.debug("get Groups in organization {}", orgId);
    return ResponseModel.done(
        groupsRepository
            .findByOrganizationIdAndDeletedFalse(orgId)
            .map(groupService::getGroupModel)
            .collect(Collectors.toList()));
  }

  @Transactional
  @Auditable(ORGANIZATION_UPDATE)
  @PreAuthorize(
      "hasAuthority('ORGANIZATION_UPDATE') AND hasAnyAuthority('SYSTEM_ADMIN','FOUNDATION_ADMIN','SUPER_ADMIN')")
  public ResponseModel changeOrganizationStatus(Long id) {
    log.debug("change organization {} status", id);
    return organizationRepository
        .findOneByIdAndDeletedFalse(id)
        .map(
            organization -> {
              if ("mint".equals(organization.getName())) {
                log.warn("Not permitted to change MINT Status");
                throw new NotPermittedException();
              }
              organization.setActive(!organization.getActive());
              organization.getUsers().forEach(user -> user.setStatus(organization.getActive()));
              organizationRepository.save(organization);
              log.debug("organization {} changed", id);
              return ResponseModel.done();
            })
        .orElseThrow(NotFoundException::new);
  }

  // @Transactional
  // @Auditable("ORGANIZATION_READ")
  // @PreAuthorize("hasAuthority('ADMIN')")
  // public ResponseModel getSettings(Long id) {
  // log.debug("get organization {} settings", id);
  // return organizationRepository.findOneByIdAndDeletedFalse(id).map(organization -> {
  // OrganizationSettingModel organizationSettingModel = new OrganizationSettingModel();
  // organizationSettingModel.setId(organization.getId());
  // organizationSettingModel.setName(organization.getName());
  // organizationSettingModel.setMarketEnabled(organization.getMarketEnabled());
  // organizationSettingModel.setGenderSensitivity(organization.getGenderSensitivity());
  // organizationSettingModel.setMessageEnabled(organization.getMessageEnabled());
  // organizationSettingModel.setTimeLockEnabled(organization.getTimeLockEnabled());
  // return ResponseModel.done(organizationSettingModel);
  // }).orElseGet(() -> {
  // log.warn("organization {} not found", id);
  // return ResponseModel.error(Code.NOT_FOUND, "organization");
  // });
  // }
  //
  // @Transactional
  // @Auditable("ORGANIZATION_UPDATE")
  // @PreAuthorize("hasAuthority('ADMIN')")
  // public ResponseModel setSettings(Long id, OrganizationSettingModel organizationSettingModel) {
  // log.debug("set settings for organization {}, new settings:{}", id,
  // organizationSettingModel);
  // Optional<Organization> org = organizationRepository.findOneByIdAndDeletedFalse(id);
  // Organization organization;
  // if (org.isPresent()) {
  // organization = org.get();
  // organization.setGenderSensitivity(organizationSettingModel.isGenderSensitivity());
  // organization.setTimeLockEnabled(organizationSettingModel.isTimeLockEnabled());
  // organization.setMessageEnabled(organizationSettingModel.isMessageEnabled());
  // organization.setMarketEnabled(organizationSettingModel.isMarketEnabled());
  // organizationRepository.save(organization);
  // log.debug("settings changed for organization {}", id);
  // return ResponseModel.done();
  // } else {
  // log.warn("organization {} not found", id);
  // return ResponseModel.error(Code.NOT_FOUND, "organization");
  // }
  // }

  @Transactional(readOnly = true)
  @PreAuthorize(
      "hasAuthority('ORGANIZATION_READ') AND hasAnyAuthority('SYSTEM_ADMIN','FOUNDATION_ADMIN','SUPER_ADMIN')")
  public ResponseModel getAllOrganizationByFoundation(Long id) {
    log.debug("get all organization in foundation {}", id);
    return foundationRepository
        .findOneByIdAndDeletedFalse(id)
        .map(
            foundation ->
                ResponseModel.done(
                    organizationRepository
                        .findByFoundationIdAndDeletedFalse(id)
                        .map(this::mapOrganizationToModel)
                        .collect(Collectors.toList())))
        .orElseThrow(() -> new NotFoundException("foundation"));
  }

  @PreAuthorize("hasAnyAuthority('ADMIN')")
  @Transactional(readOnly = true)
  public ResponseModel getSpacesByOrganizationId(Long id) {
    return organizationRepository
        .findOneByIdAndDeletedFalse(id)
        .map(spaceService::getSpacesByOrganization)
        .orElseThrow(() -> new NotFoundException("organization"));
  }

  @PreAuthorize("hasAnyAuthority('ADMIN')")
  @Transactional(readOnly = true)
  public ResponseModel getCategoriesByOrganizationId(Long id) {
    return organizationRepository
        .findOneByIdAndDeletedFalse(id)
        .map(categoryService::getCategoriesByOrganization)
        .orElseThrow(() -> new NotFoundException("organization"));
  }

  private OrganizationModel mapOrganizationToModel(Organization organization) {
    OrganizationModel organizationModel = new OrganizationModel();
    mapper.map(organization, organizationModel);
    organizationModel.setCreationDate(
        ZonedDateTime.ofInstant(organization.getCreationDate().toInstant(), ZoneOffset.UTC));
    if (organization.getLastModifiedDate() != null) {
      organizationModel.setLastModifiedDate(
          ZonedDateTime.ofInstant(organization.getLastModifiedDate().toInstant(), ZoneOffset.UTC));
    }
    organizationModel.setGenderSenstivity(
        organization.getFoundation().getGenderSensitivity() == null
            ? Boolean.FALSE
            : organization.getFoundation().getGenderSensitivity());
    organizationModel.setCurrentNumberOfUsers(
        userRepository.countByOrganizationAndDeletedFalse(organization));
    return organizationModel;
  }
}
