package com.eshraqgroup.mint.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.repos.jpa.CategoryRepository;
import com.eshraqgroup.mint.repos.jpa.RoleRepository;

/** Created by ahmad on 9/22/16. */
@Service
public class DefaultService {

  private final RoleRepository roleRepository;

  private final CategoryRepository categoryRepository;

  @Autowired
  public DefaultService(RoleRepository roleRepository, CategoryRepository categoryRepository) {
    this.categoryRepository = categoryRepository;
    this.roleRepository = roleRepository;
  }

  public void addDefaultToOrganization(Organization organization) {
    // addDefaultCategories(organization);
    // addDefaultRoles(organization);
  }
}
