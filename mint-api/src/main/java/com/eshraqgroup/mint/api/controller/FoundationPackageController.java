package com.eshraqgroup.mint.api.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.api.models.foundationpackage.FoundationPackageCreateModel;
import com.eshraqgroup.mint.api.models.foundationpackage.FoundationPackageModel;
import com.eshraqgroup.mint.api.services.FoundationPackageService;
import com.eshraqgroup.mint.controller.abstractcontroller.AbstractController;
import com.eshraqgroup.mint.models.PageRequestModel;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

/** Created by ahmad on 4/18/17. */
@RestController
@RequestMapping("api/package/foundation")
public class FoundationPackageController
    extends AbstractController<FoundationPackageCreateModel, Long> {
  @Autowired FoundationPackageService foundationPackageService;

  @Override
  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(
    value = "Create foundation package",
    response = ResponseModel.class,
    notes = "this method is used to create new foundtion package"
  )
  public ResponseModel create(
      @RequestBody FoundationPackageCreateModel foundationPackageCreateModel) {
    return foundationPackageService.createFoundationPackage(foundationPackageCreateModel);
  }

  @Override
  @RequestMapping(
    path = "/{id}",
    method = RequestMethod.PUT,
    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
  )
  @ApiOperation(
    value = "update foundation package",
    notes = "this method is used to update foundation package using foundation id"
  )
  public ResponseModel update(
      @PathVariable Long id,
      @RequestBody FoundationPackageCreateModel foundationPackageCreateModel) {
    return foundationPackageService.updateFoundationPackage(id, foundationPackageCreateModel);
  }

  @Override
  @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
  @ApiOperation(
    value = "delete foundation package",
    notes = "this method is used to delete foundation package by package id"
  )
  public ResponseModel delete(@PathVariable Long id) {
    return foundationPackageService.deleteFoundationPackage(id);
  }

  @Override
  @RequestMapping(method = RequestMethod.GET)
  @ApiOperation(
    value = "Get Foundation Packages",
    response = FoundationPackageModel.class,
    notes = "this method is used to list all foundation packages"
  )
  public ResponseModel get(
      @RequestHeader(required = false) Integer page,
      @RequestHeader(required = false) Integer size) {
    return foundationPackageService.getAll(PageRequestModel.getPageRequestModel(page, size));
  }

  @Override
  @RequestMapping(path = "/{id}", method = RequestMethod.GET)
  @ApiOperation(
    value = "Get Foundation Package",
    response = FoundationPackageModel.class,
    notes = "this method is used to get foundation package by id"
  )
  public ResponseModel get(@PathVariable Long id) {
    return foundationPackageService.get(id);
  }

  @RequestMapping(path = "/{id}/assign", method = RequestMethod.POST)
  @ApiOperation(
    value = "Assign package",
    response = ResponseModel.class,
    notes = "this method is used to add foundation in package"
  )
  public ResponseModel assign(
      @PathVariable Long id, @RequestBody(required = true) List<Long> foundationIds) {
    return foundationPackageService.assign(id, foundationIds);
  }

  @RequestMapping(path = "/{id}/unassign", method = RequestMethod.POST)
  @ApiOperation(
    value = "Unassign Package ",
    response = ResponseModel.class,
    notes = "this method is used to remove foundation from package"
  )
  public ResponseModel unassign(
      @PathVariable Long id, @RequestBody(required = true) List<Long> foundationIds) {
    return foundationPackageService.unassign(id, foundationIds);
  }
}
