package com.eshraqgroup.mint.api.services;

import static org.springframework.data.jpa.domain.Specifications.where;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.api.models.role.AssignRoleModel;
import com.eshraqgroup.mint.api.models.role.RoleByModel;
import com.eshraqgroup.mint.api.models.role.RoleCreateModel;
import com.eshraqgroup.mint.api.models.role.RoleModel;
import com.eshraqgroup.mint.api.models.user.UserInfoModel;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.configuration.notifications.Message;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Role;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.exception.ExistException;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.FoundationRepository;
import com.eshraqgroup.mint.repos.jpa.OrganizationRepository;
import com.eshraqgroup.mint.repos.jpa.RoleRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.specifications.RoleSpecifications;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.PermissionCheck;

/** Created by ahmad on 3/28/16. */
@Service
public class RoleService {
  private final Logger log = LoggerFactory.getLogger(RoleService.class);

  private final RoleRepository roleRepository;

  private final UserRepository userRepository;

  private final OrganizationRepository organizationRepository;

  private final FoundationRepository foundationRepository;

  private final Mapper mapper;

  public RoleService(
      RoleRepository roleRepository,
      UserRepository userRepository,
      OrganizationRepository organizationRepository,
      FoundationRepository foundationRepository,
      Mapper mapper) {
    this.roleRepository = roleRepository;
    this.userRepository = userRepository;
    this.organizationRepository = organizationRepository;
    this.foundationRepository = foundationRepository;
    this.mapper = mapper;
  }

  @Transactional
  @Auditable(EntityAction.ROLE_CREATE)
  @PreAuthorize("hasAuthority('ROLE_CREATE') AND hasAuthority('ADMIN')")
  // TODO: Separate Methods into 3 methods with permission check
  public ResponseModel createRole(RoleCreateModel roleModel) {
    log.debug("create role : {}", roleModel);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              PermissionCheck.checkUserForFoundationAndOrgOperation(
                  user, roleModel.getOrganizationId(), roleModel.getFoundationId());
              if (roleModel.getOrganizationId() != null) {
                Organization organization =
                    organizationRepository.findOne(roleModel.getOrganizationId());
                if (organization == null) {
                  throw new NotFoundException("organization");
                }
                if (!organization.getActive().booleanValue()) {
                  throw new InvalidException("error.organization.active");
                }
                if (roleRepository
                    .findOneByNameAndOrganizationAndDeletedFalse(roleModel.getName(), organization)
                    .isPresent()) {
                  throw new ExistException("name");
                }
                Role role = new Role();
                role.setName(roleModel.getName());
                role.setType(roleModel.getType());
                role.getPermission().putAll(roleModel.getPermission());
                role.setOrganization(organization);
                role.setFoundation(organization.getFoundation());
                roleRepository.save(role);
                return ResponseModel.done();

              } else if (roleModel.getFoundationId() != null) {
                Foundation foundation = foundationRepository.findOne(roleModel.getFoundationId());
                if (foundation == null) {
                  throw new NotFoundException("foundation");
                }
                if (roleRepository
                    .findOneByNameAndFoundationAndDeletedFalse(roleModel.getName(), foundation)
                    .isPresent()) {
                  throw new ExistException("name");
                }

                Role role = new Role();
                role.setName(roleModel.getName());
                role.setType(roleModel.getType());
                role.getPermission().putAll(roleModel.getPermission());
                role.setFoundation(foundation);
                roleRepository.save(role);
                return ResponseModel.done();
              }
              throw new InvalidException("error.role.create.parent");
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ROLE_UPDATE)
  @PreAuthorize("hasAuthority('ROLE_UPDATE') AND hasAuthority('ADMIN')")
  @Message(entityAction = EntityAction.ROLE_UPDATE, services = Services.NOTIFICATIONS)
  public ResponseModel updateRole(Long id, RoleCreateModel roleModel) {
    log.debug("updating role {} with data {}", id, roleModel);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Role role = roleRepository.findOne(id);
              if (role == null) {
                log.warn("role {} not found", id);
                throw new NotFoundException();
              }
              PermissionCheck.checkUserForFoundationAndOrgOperation(
                  user,
                  role.getOrganization() == null ? null : role.getOrganization().getId(),
                  role.getFoundation().getId());

              if (!Objects.equals(role.getName(), roleModel.getName())
                  && roleRepository
                      .findOneByNameAndFoundationAndDeletedFalse(
                          roleModel.getName(), role.getFoundation())
                      .isPresent()) {
                log.warn("role {} already exist", roleModel.getName());
                throw new ExistException("name");
              }

              role.setName(roleModel.getName());
              role.setType(roleModel.getType());
              role.getPermission().clear();
              role.getPermission().putAll(roleModel.getPermission());
              roleRepository.save(role);

              log.debug("role {} updated", id);
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('ROLE_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getRoles() {
    log.debug("get roles");
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              switch (user.getType()) {
                case SUPER_ADMIN:
                case SYSTEM_ADMIN:
                  if (user.getUserName().equalsIgnoreCase("admin")) {
                    return ResponseModel.done(
                        roleRepository
                            .findAll()
                            .stream()
                            .filter(role -> !role.getName().equalsIgnoreCase("superadminrole"))
                            .map(this::getRoleModel)
                            .collect(Collectors.toSet()));
                  }
                  return ResponseModel.done(
                      roleRepository
                          .findAll()
                          .stream()
                          .filter(
                              role ->
                                  !role.getName().equalsIgnoreCase("superadminrole")
                                      && role.getType() != user.getType())
                          .map(this::getRoleModel)
                          .collect(Collectors.toSet()));
                case FOUNDATION_ADMIN:
                  return ResponseModel.done(
                      roleRepository
                          .findByFoundationAndDeletedFalse(user.getFoundation())
                          .filter(
                              role ->
                                  role.getType() == UserType.ADMIN
                                      || role.getType() == UserType.USER)
                          .map(this::getRoleModel)
                          .collect(Collectors.toSet()));
                case ADMIN:
                  return ResponseModel.done(
                      roleRepository
                          .findByOrganizationAndDeletedFalse((user.getOrganization()))
                          .filter(role -> role.getType() == UserType.USER)
                          .map(this::getRoleModel)
                          .collect(Collectors.toSet()));
                default:
                  return roleRepository
                      .findByUsersIdAndDeletedFalse(user.getId())
                      .map(
                          roles ->
                              ResponseModel.done(
                                  roles
                                      .stream()
                                      .map(this::getRoleModel)
                                      .collect(Collectors.toSet())))
                      .orElse(ResponseModel.done(Collections.emptySet()));
              }
            })
        .orElseThrow(NotPermittedException::new);
  }

  private RoleModel getRoleModel(Role role) {
    log.debug("Map role {} to role model", role.getId());
    RoleModel roleModel = new RoleModel();
    mapper.map(role, roleModel);
    roleModel.setCreationDate(
        ZonedDateTime.ofInstant(role.getCreationDate().toInstant(), ZoneOffset.UTC));
    if (role.getLastModifiedDate() != null) {
      roleModel.setLastModifiedDate(
          ZonedDateTime.ofInstant(role.getLastModifiedDate().toInstant(), ZoneOffset.UTC));
    }
    roleModel.getPermission().putAll(role.getPermission());
    roleModel.setNumberOfUsers(
        userRepository.countByRolesIdInAndDeletedFalse(Collections.singleton(role.getId())));
    return roleModel;
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('ROLE_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getRole(Long Id) {
    log.debug("get role {}", Id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              switch (user.getType()) {
                case SUPER_ADMIN:
                case SYSTEM_ADMIN:
                  return roleRepository
                      .findOneByIdAndDeletedFalse(Id)
                      .map(role -> ResponseModel.done(getRoleModel(role)))
                      .orElseThrow(NotFoundException::new);
                case FOUNDATION_ADMIN:
                  return roleRepository
                      .findOneByFoundationAndIdAndDeletedFalse(user.getFoundation(), Id)
                      .map(role -> ResponseModel.done(getRoleModel(role)))
                      .orElseThrow(NotFoundException::new);
                case ADMIN:
                  return roleRepository
                      .findOneByOrganizationInAndIdAndDeletedFalse(
                          Collections.singletonList(user.getOrganization()), Id)
                      .map(role -> ResponseModel.done(getRoleModel(role)))
                      .orElseThrow(NotFoundException::new);
                default:
                  log.warn("user {} not permitted", SecurityUtils.getCurrentUserLogin());
                  throw new NotPermittedException();
              }
            })
        .orElseThrow(NotFoundException::new);
  }

  @Deprecated
  @Transactional
  @Auditable(EntityAction.ROLE_UPDATE)
  @PreAuthorize("hasAuthority('ROLE_ASSIGN_CREATE') AND hasAuthority('ADMIN')")
  @Message(
    entityAction = EntityAction.ROLE_ASSIGN,
    services = Services.NOTIFICATIONS,
    withModel = true,
    indexOfModel = 0
  )
  public ResponseModel assignRole(AssignRoleModel assignRoleModel) {
    log.debug(
        "assign role {} to user {} ", assignRoleModel.getRoleId(), assignRoleModel.getUserId());
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              User userToAssign = userRepository.findOne(assignRoleModel.getUserId());
              Role role = roleRepository.findOne(assignRoleModel.getRoleId());
              if (userToAssign == null) {
                log.warn("invalid user , NULL");
                throw new NotFoundException("user");
              }
              if (role == null) {
                log.warn("invalid role , NULL");
                throw new NotFoundException("role");
              }

              switch (userToAssign.getType()) {
                case FOUNDATION_ADMIN:
                  if (user.getType() != UserType.SYSTEM_ADMIN
                      && user.getType() != UserType.SUPER_ADMIN) {
                    log.warn("user {} not permitted", user.getId());
                    throw new NotPermittedException();
                  }

                  if (!Objects.equals(
                          userToAssign.getFoundation().getId(), role.getFoundation().getId())
                      || role.getType() != UserType.FOUNDATION_ADMIN) {
                    log.warn(
                        "invalid use organization not the same ot role organization and role type not foundation admin");
                    throw new MintException(Code.INVALID, "role");
                  }
                  break;
                case ADMIN:
                  if (!user.getType().equals(UserType.SUPER_ADMIN)
                      && !user.getType().equals(UserType.SYSTEM_ADMIN)
                      && !user.getType().equals(UserType.FOUNDATION_ADMIN)) {
                    log.warn("user {} not permitted", user.getId());
                    throw new NotPermittedException();
                  }
                  if (!Objects.equals(
                          userToAssign.getOrganization().getId(), role.getOrganization().getId())
                      || role.getType() != UserType.ADMIN) {
                    log.warn(
                        "invalid use organization not the same ot role organization and role type not foundation admin");
                    throw new MintException(Code.INVALID, "role");
                  }
                  break;
              }
              removeRolesFromUser(userToAssign);

              role.getUsers().add(userToAssign);
              roleRepository.save(role);
              log.debug(
                  "role {} assigned to user {} successfully",
                  assignRoleModel.getRoleId(),
                  assignRoleModel.getUserId());
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ROLE_UPDATE)
  @PreAuthorize("hasAuthority('ROLE_ASSIGN_CREATE') AND hasAuthority('ADMIN')")
  @Message(
    entityAction = EntityAction.ROLE_ASSIGN,
    services = Services.NOTIFICATIONS,
    withModel = true
  )
  public ResponseModel assignRole(Long roleId, List<Long> userIds) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Role role = roleRepository.findOne(roleId);

              if (userIds == null || userIds.isEmpty()) {
                log.debug("invalid users , NULL Or Empty");
                throw new MintException(Code.INVALID);
              }
              if (role == null) {
                log.warn("invalid role , NULL");
                throw new MintException(Code.INVALID);
              }
              userRepository
                  .findByIdInAndOrganizationAndTypeAndDeletedFalse(
                      userIds, role.getOrganization(), role.getType())
                  .forEach(
                      user1 -> {
                        removeRolesFromUser(user1);
                        role.getUsers().add(user1);
                      });

              roleRepository.save(role);
              log.debug("role {} assigned to user {} successfully", roleId, userIds);
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ROLE_UPDATE)
  @PreAuthorize("hasAuthority('ROLE_ASSIGN_DELETE') AND hasAuthority('ADMIN')")
  @Message(
    services = Services.NOTIFICATIONS,
    entityAction = EntityAction.ROLE_UNASSIGN,
    withModel = true
  )
  public ResponseModel unassignRole(Long roleId, List<Long> userIds) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Role role = roleRepository.findOne(roleId);

              if (userIds == null || userIds.isEmpty()) {
                log.debug("invalid users , NULL Or Empty");
                throw new MintException(Code.INVALID);
              }
              if (role == null) {
                log.warn("invalid role , NULL");
                throw new MintException(Code.INVALID);
              }
              userRepository
                  .findByIdInAndOrganizationAndTypeAndDeletedFalse(
                      userIds, role.getOrganization(), role.getType())
                  .forEach(user1 -> role.getUsers().remove(user1));

              roleRepository.save(role);
              log.debug("role {} assigned to user {} successfully", roleId, userIds);
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  public void removeRolesFromUser(User user) {
    log.debug("remove roles from user", user.getId());
    for (Role r : user.getRoles()) {
      r.getUsers().remove(user);
      roleRepository.save(r);
      log.debug("role {} removed from user {}", r.getId(), user.getId());
    }
  }

  @Transactional
  @Auditable(EntityAction.ROLE_DELETE)
  @PreAuthorize("hasAuthority('ROLE_DELETE') AND hasAuthority('ADMIN')")
  public ResponseModel delete(Long id) {
    log.debug("deleting role {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              roleRepository
                  .findOneByIdAndDeletedFalse(id)
                  .ifPresent(
                      role -> {
                        PermissionCheck.checkUserForFoundationAndOrgOperation(
                            user,
                            role.getOrganization() == null ? null : role.getOrganization().getId(),
                            role.getFoundation().getId());
                        if (role.getName().equalsIgnoreCase("SuperAdminRole")) {
                          return;
                        }
                        role.getUsers().clear();
                        roleRepository.save(role);
                        log.debug("role {} removed from all users", id);
                        roleRepository.delete(role);
                        log.debug("role {} deleted successfully", id);
                        role.getUsers().clear();
                      });

              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @Auditable(EntityAction.ROLE_DELETE)
  @PreAuthorize("hasAuthority('ROLE_DELETE') AND hasAuthority('ADMIN')")
  public ResponseModel delete(List<Long> ids) {
    if (ids != null && !ids.isEmpty()) {
      for (Long id : ids) {
        delete(id);
      }
    }
    return ResponseModel.done();
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('ROLE_ASSIGN_READ')  AND hasAuthority('ADMIN')")
  public ResponseModel getUserOnRole(Long id) {
    log.debug("get all users on role {}", id);
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user ->
                roleRepository
                    .findOneByIdAndDeletedFalse(id)
                    .map(
                        role -> {
                          switch (user.getType()) {
                            case SUPER_ADMIN:
                            case SYSTEM_ADMIN:
                              return ResponseModel.done(
                                  role.getUsers()
                                      .stream()
                                      .filter(user1 -> !user1.isDeleted())
                                      .map(UserInfoModel::new)
                                      .collect(Collectors.toList()));
                            case FOUNDATION_ADMIN:
                              if (role.getFoundation() != null
                                  && role.getFoundation().equals(user.getFoundation())) {
                                return ResponseModel.done(
                                    role.getUsers()
                                        .stream()
                                        .filter(user1 -> !user1.isDeleted())
                                        .map(UserInfoModel::new)
                                        .collect(Collectors.toList()));
                              }

                            case ADMIN:
                              if (role.getOrganization() != null
                                  && role.getOrganization().equals(user.getOrganization())) {
                                return ResponseModel.done(
                                    role.getUsers()
                                        .stream()
                                        .filter(user1 -> !user1.isDeleted())
                                        .map(UserInfoModel::new)
                                        .collect(Collectors.toList()));
                              }

                            default:
                              log.warn(
                                  "user {} not permitted", SecurityUtils.getCurrentUserLogin());
                              throw new NotPermittedException();
                          }
                        })
                    .orElseThrow(NotFoundException::new))
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional(readOnly = true)
  @PreAuthorize("hasAuthority('ROLE_READ') AND hasAuthority('ADMIN')")
  public ResponseModel getRoleBy(RoleByModel roleByModel) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              log.debug("get role by specifications:{}", roleByModel);
              Specification<Role> byOrganization = null;
              Specification<Role> byType = null;
              Specification<Role> byFoundation = null;

              if (roleByModel.getFoundationId() == null
                  && roleByModel.getOrganizationId() == null
                  && roleByModel.getUserType() == null) {
                log.warn("Missing parameters ,one of  organization id or user type is required");
                throw new MintException(Code.MISSING, "Parameters");
              }

              if (roleByModel.getOrganizationId() == null
                  && roleByModel.getFoundationId() == null) {
                if (user.getType() == UserType.ADMIN) {
                  byOrganization = RoleSpecifications.byOrganization(user.getOrganization());
                }
                if (user.getType() == UserType.FOUNDATION_ADMIN) {
                  byFoundation = RoleSpecifications.byFoundation(user.getFoundation());
                }

              } else {
                Organization org = organizationRepository.findOne(roleByModel.getOrganizationId());
                if (org != null) {
                  byOrganization = RoleSpecifications.byOrganization(org);
                }
                Foundation foundation = foundationRepository.findOne(roleByModel.getFoundationId());
                if (foundation != null) {
                  byFoundation = RoleSpecifications.byFoundation(foundation);
                }
              }
              if (roleByModel.getUserType() != null) {
                byType = RoleSpecifications.hasType(roleByModel.getUserType());
              } else {
                if (user.getType().equals(UserType.FOUNDATION_ADMIN)) {
                  byType = RoleSpecifications.hasType(UserType.ADMIN, UserType.USER);
                } else if (user.getType().equals(UserType.ADMIN)) {
                  byType = RoleSpecifications.hasType(UserType.USER);
                }
              }
              return ResponseModel.done(
                  roleRepository
                      .findAll(
                          where(byOrganization)
                              .and(byFoundation)
                              .and(byType)
                              .and(RoleSpecifications.notDeleted()))
                      .stream()
                      .filter(
                          role ->
                              !role.getName().equalsIgnoreCase("SuperAdminRole")
                                  && !user.getRoles().contains(role))
                      .map(
                          role -> {
                            RoleModel roleModel = new RoleModel();
                            mapper.map(role, roleModel);
                            roleModel.setNumberOfUsers(role.getUsers().size());
                            return roleModel;
                          })
                      .collect(Collectors.toList()));
            })
        .orElseThrow(NotPermittedException::new);
  }
}
