package com.eshraqgroup.mint.search.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.search.document.Space;
import com.eshraqgroup.mint.search.model.SpaceUpdateModel;
import com.eshraqgroup.mint.search.repositories.SpaceSearchRepository;

/** Created by ayman on 24/04/17. */
@Service
public class SpaceSearchService {

  private static final String COLLECTION_NAME = "space";
  private SpaceSearchRepository spaceSearchRepository;
  private SolrTemplate solrTemplate;

  public SpaceSearchService(
      SpaceSearchRepository spaceSearchRepository, SolrTemplate solrTemplate) {
    this.spaceSearchRepository = spaceSearchRepository;
    this.solrTemplate = solrTemplate;
    this.solrTemplate.setSolrCore("space");
  }

  @Transactional
  public PageResponseModel searchInFullText(String query, Pageable pageable) {
    Page<Space> spacesPage =
        spaceSearchRepository.findAllSpaceByFullTextContaining(query, pageable);
    if (spacesPage.getTotalElements() > 0) {
      return PageResponseModel.done(
          spacesPage.getContent(),
          spacesPage.getTotalPages(),
          pageable.getPageNumber(),
          pageable.getPageSize());
    } else {
      return PageResponseModel.done("mo matches for" + query, 0, 0, 0);
    }
  }

  @Transactional
  public ResponseModel update(SpaceUpdateModel spaceUpdateModel) {
    Space spaceDocument = spaceSearchRepository.findById(spaceUpdateModel.getId().toString());
    String response;
    if (null != spaceDocument) {
      solrTemplate.deleteById(COLLECTION_NAME, spaceDocument.getId());
      solrTemplate.commit(COLLECTION_NAME);
    }
    spaceDocument = new Space();
    response = UpdateDocument(spaceDocument, spaceUpdateModel);
    solrTemplate.commit(COLLECTION_NAME);
    return ResponseModel.done(response);
  }

  private String UpdateDocument(Space spaceDocument, SpaceUpdateModel spaceUpdateModel) {
    spaceDocument.setId(String.valueOf(spaceUpdateModel.getId()));
    spaceDocument.setName(spaceUpdateModel.getName());
    spaceDocument.setDescription(spaceUpdateModel.getDescription());
    spaceDocument.setObjective(spaceUpdateModel.getObjective());
    solrTemplate.saveBean(COLLECTION_NAME, spaceDocument);
    return spaceDocument.getId();
  }
}
