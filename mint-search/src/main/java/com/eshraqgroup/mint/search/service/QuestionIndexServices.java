package com.eshraqgroup.mint.search.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.mongodb.DBObject;

/** Created by ahmad on 5/25/17. */
@Component
public class QuestionIndexServices {

  private final MongoTemplate mongoTemplate;

  private final SolrTemplate solrTemplate;

  @Autowired
  public QuestionIndexServices(MongoTemplate mongoTemplate, SolrTemplate solrTemplate) {
    this.mongoTemplate = mongoTemplate;
    this.solrTemplate = solrTemplate;
  }

  @Transactional
  // @PostConstruct
  public void indexQuestionBankFull() {

    solrTemplate.delete("question", new SimpleQuery("*:*"));
    solrTemplate.commit("question");
    Set<SolrInputDocument> questionSolrDomains = new HashSet<>();
    Query query = new Query();
    mongoTemplate.executeQuery(
        query,
        "questionBank",
        dbObject -> {
          questionSolrDomains.add(mapSolrDocumnet(dbObject));
          if (questionSolrDomains.size() == 100) {
            solrTemplate.saveDocuments("question", questionSolrDomains);
            solrTemplate.commit("question");
            questionSolrDomains.clear();
          }
        });
    if (questionSolrDomains.size() > 0) {
      solrTemplate.saveDocuments("question", questionSolrDomains);
      solrTemplate.commit("question");

      questionSolrDomains.clear();
    }
  }

  public void handleCreateQuestion(BaseMessage message) {

    if (message.getEntityId() != null) {
      Query query = new Query();
      query.addCriteria(Criteria.where("_id").is(message.getEntityId()));
      mongoTemplate.executeQuery(
          query,
          "questionBank",
          dbObject -> {
            final SolrInputDocument questionSolrDomain = mapSolrDocumnet(dbObject);
            if (questionSolrDomain == null) {
              return;
            }
            solrTemplate.saveDocument("question", questionSolrDomain);
            solrTemplate.softCommit("question");
          });
    }
  }

  public void handleDeleteQuestion(BaseMessage message) {
    if (message.getEntityId() != null) {
      solrTemplate.deleteById("question", message.getEntityId().toString());
      solrTemplate.softCommit("question");
    }
  }

  public void handleCreateBulkQuestion(BaseMessage message) {
    if (Collection.class.isInstance(message.getEntityId())) {
      if (message.getEntityId() != null && !((Collection<?>) message.getEntityId()).isEmpty()) {
        Set<SolrInputDocument> questionSolrDomains = new HashSet<>();
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").in((Collection<?>) message.getEntityId()));
        mongoTemplate.executeQuery(
            query,
            "questionBank",
            dbObject -> {
              SolrInputDocument questionSolrDomain = mapSolrDocumnet(dbObject);
              if (questionSolrDomain != null) {
                questionSolrDomains.add(questionSolrDomain);
              }

              if (questionSolrDomains.size() == 100) {
                solrTemplate.saveDocuments("question", questionSolrDomains);
                solrTemplate.softCommit("question");
                questionSolrDomains.clear();
              }
            });
        if (!questionSolrDomains.isEmpty()) {
          solrTemplate.saveDocuments("question", questionSolrDomains);
          solrTemplate.softCommit("question");
          questionSolrDomains.clear();
        }
      }
    }
  }

  private SolrInputDocument mapSolrDocumnet(DBObject dbObject) {
    SolrInputDocument questionSolrDomain = new SolrInputDocument();

    questionSolrDomain.addField("id", dbObject.get("_id").toString());
    if (dbObject.containsField("body") && !dbObject.get("body").toString().trim().isEmpty()) {
      questionSolrDomain.addField("body", dbObject.get("body"));
    } else {
      mongoTemplate.remove(dbObject, "questionBank");
      return null;
    }
    questionSolrDomain.addField("type", String.valueOf(dbObject.get("questionType")));
    questionSolrDomain.addField(
        "tags", Arrays.asList(String.valueOf(dbObject.get("tags")).split(",")));
    questionSolrDomain.addField("categoryId", dbObject.get("categoryId"));
    questionSolrDomain.addField("userId", dbObject.get("ownerId"));
    questionSolrDomain.addField("isPublic", dbObject.get("isPublic"));
    if (dbObject.get("spaceId") != null) {
      questionSolrDomain.addField("spaceId", dbObject.get("spaceId"));
    }
    return questionSolrDomain;
  }

  public void handleDeleteBulkQuestion(BaseMessage message) {
    solrTemplate.deleteById("question", (Collection<String>) message.getEntityId());
    solrTemplate.softCommit("question");
  }
}
