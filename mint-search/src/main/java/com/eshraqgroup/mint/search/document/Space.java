package com.eshraqgroup.mint.search.document;

import java.util.ArrayList;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.solr.core.mapping.SolrDocument;

/** Created by ayman on 24/04/17. */
@SolrDocument(solrCoreName = "space")
public class Space {

  @Field private String id;
  @Field private String name;

  @Field private String description;

  @Field private String objective;

  @Field private ArrayList<String> fullText;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getObjective() {
    return objective;
  }

  public void setObjective(String objective) {
    this.objective = objective;
  }

  public ArrayList<String> getFullText() {
    return fullText;
  }

  public void setFullText(ArrayList<String> fullText) {
    this.fullText = fullText;
  }
}
