package com.eshraqgroup.mint.search.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.repository.Boost;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.search.document.Space;

/** Created by ayman on 24/04/17. */
@Repository
public interface SpaceSearchRepository extends SolrCrudRepository<Space, String> {
  Page<Space> findAllSpaceByFullTextContaining(@Boost(2) String term, Pageable pageable);

  Space findByName(@Boost(2) String term);

  Space findById(@Boost(2) String id);

  Page<Space> findAllByDescriptionContaining(@Boost(2) String term, Pageable pageable);

  Page<Space> findAllByObjectiveContaining(@Boost(2) String term, Pageable pageable);
}
