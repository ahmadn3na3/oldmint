package com.eshraqgroup.mint.search.model;

/** Created by ayman on 26/04/17. */
public class SpaceUpdateModel {

  private Long id;
  private String name;
  private String description;
  private String objective;

  public SpaceUpdateModel() {}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getObjective() {
    return objective;
  }

  public void setObjective(String objective) {
    this.objective = objective;
  }
}
