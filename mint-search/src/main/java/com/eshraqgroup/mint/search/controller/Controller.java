package com.eshraqgroup.mint.search.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.search.service.QuestionIndexServices;
import io.swagger.annotations.ApiOperation;

@RestController
public class Controller {
  @Autowired QuestionIndexServices questionIndexServices;

  @RequestMapping("/reindex/{core}")
  @ApiOperation(value = "Reindex", notes = "this method is used to reindex solr cores")
  public void reindex(@PathVariable("core") String core) {
    switch (core.toLowerCase()) {
      case "question":
        questionIndexServices.indexQuestionBankFull();
        break;
      case "space":
        break;
      default:
        throw new InvalidException("corename");
    }
  }
}
