package com.eshraqgroup.mint.search.service;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import com.eshraqgroup.mint.models.messages.BaseMessage;

/** Created by ahmad on 5/30/17. */
@Service
@RabbitListener(queues = "indexqueue")
public class IndexMessageHandler {

  @Autowired QuestionIndexServices questionIndexServices;

  @RabbitHandler
  private void handle(@Payload BaseMessage message) {
    switch (message.getEntityAction()) {
      case QUESTION_CREATE:
        questionIndexServices.handleCreateQuestion(message);
        break;
      case QUESTION_DELETE:
        questionIndexServices.handleDeleteQuestion(message);
        break;
      case QUESTION_UPDATE:
        questionIndexServices.handleDeleteQuestion(message);
        questionIndexServices.handleCreateQuestion(message);
        break;
      case QUESTION_CREATE_BULK:
        questionIndexServices.handleCreateBulkQuestion(message);
        break;
      case QUESTION_DELETE_BULK:
        questionIndexServices.handleDeleteBulkQuestion(message);
        break;
    }
  }
}
