package com.eshraqgroup.mint.batch;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import com.eshraqgroup.mint.batch.services.StatisticsService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MintBatchApplication.class)
@WebAppConfiguration
public class MintBatchApplicationTests {
  @Autowired private StatisticsService statisticsService;

  @Test
  public void runStatistics() {
    LocalDateTime to = LocalDate.now().atTime(23, 59, 59, 999999999);

    LocalDateTime from =
        LocalDate.of(2000, 1, 1).atTime(0, 0, 0, 0).with(ChronoField.DAY_OF_MONTH, 1l);
    //    statisticsService.updateStatistics(from, to);
  }
}
