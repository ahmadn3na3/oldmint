package com.eshraqgroup.mint.batch.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.batch.services.StatisticsService;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.SortDirection;
import com.eshraqgroup.mint.constants.SortField;
import com.eshraqgroup.mint.models.PageRequestModel;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/statistics")
public class StatisticController {

  private CompletableFuture<Date> future;

  @Autowired private StatisticsService statisticsService;

  @GetMapping(path = "/dashboard")
  public ResponseModel getDashBoardStatistics(
      @RequestHeader(name = "foundationId", required = false) Long foundationId,
      @RequestHeader(name = "organizationId", required = false) Long organizationId) {
    return statisticsService.getDashBoardStatistics(foundationId, organizationId);
  }

  @ApiOperation(
    value = "get User Statistics from date1 to date2 with optional filters ",
    notes = "date format = yyyyMMdd"
  )
  @GetMapping(path = "/user/from/{fromDate}/to/{toDate}")
  public ResponseModel getUserStatistics(
      @PathVariable(name = "fromDate") String fromDate,
      @PathVariable(name = "toDate") String toDate,
      @RequestHeader(name = "foundationId", required = false) Long foundationId,
      @RequestHeader(name = "organizationId", required = false) Long organizationId,
      @RequestHeader(name = "groupId", required = false) Long groupId,
      @RequestParam(name = "filter", required = false) String filter,
      @RequestHeader(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestHeader(name = "size", required = false, defaultValue = Integer.MAX_VALUE + "")
          Integer size,
      @RequestHeader(required = false, defaultValue = "NAME") SortField field,
      @RequestHeader(required = false, defaultValue = "ASCENDING") SortDirection sortDirection) {

    LocalDate from = LocalDate.parse(fromDate, DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate to = LocalDate.parse(toDate, DateTimeFormatter.BASIC_ISO_DATE);

    return statisticsService.getUserStatistics(
        foundationId,
        organizationId,
        groupId,
        from,
        to,
        filter,
        PageRequestModel.getPageRequestModel(
            page,
            size,
            new Sort(
                sortDirection.getValue(),
                field == SortField.NAME ? "fullName" : field.getFieldName())));
  }

  @GetMapping(path = "/space/from/{fromDate}/to/{toDate}")
  public ResponseModel getSpaceStatistics(
      @PathVariable(name = "fromDate") String fromDate,
      @PathVariable(name = "toDate") String toDate,
      @RequestHeader(name = "foundationId", required = false) Long foundationId,
      @RequestHeader(name = "organizationId", required = false) Long organizationId,
      @RequestHeader(name = "groupId", required = false) Long groupId,
      @RequestParam(name = "filter", required = false) String filter,
      @RequestHeader(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestHeader(name = "size", required = false, defaultValue = Integer.MAX_VALUE + "")
          Integer size,
      @RequestHeader(required = false, defaultValue = "NAME") SortField field,
      @RequestHeader(required = false, defaultValue = "ASCENDING") SortDirection sortDirection) {

    LocalDate from = LocalDate.parse(fromDate, DateTimeFormatter.BASIC_ISO_DATE);
    LocalDate to = LocalDate.parse(toDate, DateTimeFormatter.BASIC_ISO_DATE);

    return statisticsService.getSpaceStatistics(
        foundationId,
        organizationId,
        groupId,
        from,
        to,
        filter,
        PageRequestModel.getPageRequestModel(
            page,
            size,
            new Sort(
                sortDirection.getValue(),
                field == SortField.NAME ? "spaceName" : field.getFieldName())));
  }

  @GetMapping(path = "/content/{id}")
  public ResponseModel getContentStatistic(
      @PathVariable("id") Long spaceId,
      @RequestHeader(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestHeader(name = "size", required = false, defaultValue = Integer.MAX_VALUE + "")
          Integer size,
      @RequestHeader(required = false, defaultValue = "NAME") SortField field,
      @RequestHeader(required = false, defaultValue = "ASCENDING") SortDirection sortDirection,
      @RequestHeader(required = false) Boolean deleted) {
    return statisticsService.getContentStatistics(
        spaceId,
        PageRequestModel.getPageRequestModel(
            page,
            size,
            new Sort(
                sortDirection.getValue(),
                field == SortField.NAME ? "spaceName" : field.getFieldName())),
        deleted);
  }

  @GetMapping(path = "/assessment/{id}")
  public ResponseModel getAssessmentStatistic(
      @PathVariable("id") Long spaceId,
      @RequestHeader(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestHeader(name = "size", required = false, defaultValue = Integer.MAX_VALUE + "")
          Integer size,
      @RequestHeader(required = false, defaultValue = "NAME") SortField field,
      @RequestHeader(required = false, defaultValue = "ASCENDING") SortDirection sortDirection,
      @RequestHeader(required = false) Boolean deleted,
      @RequestHeader(required = false) AssessmentType type) {
    return statisticsService.getAssessmentStatistics(
        spaceId,
        PageRequestModel.getPageRequestModel(
            page,
            size,
            new Sort(
                sortDirection.getValue(),
                field == SortField.NAME ? "spaceName" : field.getFieldName())),
        deleted,
        type);
  }

  @GetMapping(path = "/discussion/{id}")
  public ResponseModel getDiscussionStatistic(
      @PathVariable("id") Long spaceId,
      @RequestHeader(name = "page", required = false, defaultValue = "0") Integer page,
      @RequestHeader(name = "size", required = false, defaultValue = Integer.MAX_VALUE + "")
          Integer size,
      @RequestHeader(required = false, defaultValue = "NAME") SortField field,
      @RequestHeader(required = false, defaultValue = "ASCENDING") SortDirection sortDirection,
      @RequestHeader(required = false) Boolean deleted) {
    return statisticsService.getDiscussionStatistics(
        spaceId,
        PageRequestModel.getPageRequestModel(
            page,
            size,
            new Sort(
                sortDirection.getValue(),
                field == SortField.NAME ? "title" : field.getFieldName())),
        deleted);
  }

  @GetMapping(path = "/space/{id}")
  public ResponseModel getCurrentSpaceStatistics(@PathVariable(name = "id") long id) {

    return statisticsService.getCurrentSpaceStatistics(id);
  }

  @GetMapping(path = "/run")
  public ResponseModel runStatistics() {
    if (future == null || future.isDone()) {
      future = statisticsService.updateRequest();
      return ResponseModel.done("started");
    } else {
      return ResponseModel.done("still calcuating");
    }
  }

  @GetMapping(path = "/run/status")
  public ResponseModel runStatusStatistics() {
    if (future == null || future.isDone()) {
      return ResponseModel.done("No job running");
    } else {
      return ResponseModel.done("still calcuating");
    }
  }
}
