package com.eshraqgroup.mint.batch.domain;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.EnumMap;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.ContentType;

@Document(collection = "statistics.space")
public class SpaceStatistics {
  @Id private String id;
  @Indexed private Long spaceId;
  @Indexed private Long ownerId;
  @Indexed private String spaceName;
  @Indexed private String categoryName;
  @Indexed private String ownerUserName;
  @Indexed private Date creationDate;
  private Long discussionsCount = 0l;
  private Integer communityCount = 0;
  private EnumMap<ContentType, Long> contentTypesCount = new EnumMap<>(ContentType.class);
  private Long contentCount;
  private EnumMap<AssessmentType, Long> assessmentTypesCount = new EnumMap<>(AssessmentType.class);
  private Long assessmentCount;
  private Long annotationCount;
  private Long numberOfSolvedAssessment;
  @Indexed private Date collectionDate;
  @Indexed private Long organizationId;
  @Indexed private Long foundationId;
  @Indexed private Integer day = LocalDate.now().get(ChronoField.DAY_OF_MONTH);
  @Indexed private Integer month = LocalDate.now().get(ChronoField.MONTH_OF_YEAR);
  @Indexed private Integer year = LocalDate.now().get(ChronoField.YEAR);

  public SpaceStatistics() {
    super();
  }

  public SpaceStatistics(Long spaceId, Long ownerId) {
    super();
    this.spaceId = spaceId;
    this.ownerId = ownerId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  public Long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(Long ownerId) {
    this.ownerId = ownerId;
  }

  public Long getDiscussionsCount() {
    return discussionsCount;
  }

  public void setDiscussionsCount(Long discussionsCount) {
    this.discussionsCount = discussionsCount;
  }

  public Integer getCommunityCount() {
    return communityCount;
  }

  public void setCommunityCount(Integer communityCount) {
    this.communityCount = communityCount;
  }

  public EnumMap<ContentType, Long> getContentTypesCount() {
    return contentTypesCount;
  }

  public void setContentTypesCount(EnumMap<ContentType, Long> contentTypesCount) {
    this.contentTypesCount = contentTypesCount;
  }

  public Long getContentCount() {
    return contentCount;
  }

  public void setContentCount(Long contentCount) {
    this.contentCount = contentCount;
  }

  public EnumMap<AssessmentType, Long> getAssessmentTypesCount() {
    return assessmentTypesCount;
  }

  public void setAssessmentTypesCount(EnumMap<AssessmentType, Long> assessmentTypesCount) {
    this.assessmentTypesCount = assessmentTypesCount;
  }

  public Long getAssessmentCount() {
    return assessmentCount;
  }

  public void setAssessmentCount(Long assessmentCount) {
    this.assessmentCount = assessmentCount;
  }

  public Long getNumberOfSolvedAssessment() {
    return numberOfSolvedAssessment;
  }

  public void setNumberOfSolvedAssessment(Long numberOfSolvedAssessment) {
    this.numberOfSolvedAssessment = numberOfSolvedAssessment;
  }

  public Date getCollectionDate() {
    return collectionDate;
  }

  public void setCollectionDate(Date collectionDate) {
    this.collectionDate = collectionDate;
  }

  public Integer getDay() {
    return day;
  }

  public void setDay(Integer day) {
    this.day = day;
  }

  public Integer getMonth() {
    return month;
  }

  public void setMonth(Integer month) {
    this.month = month;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public Long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(Long organizationId) {
    this.organizationId = organizationId;
  }

  public Long getFoundationId() {
    return foundationId;
  }

  public void setFoundationId(Long foundationId) {
    this.foundationId = foundationId;
  }

  public String getSpaceName() {
    return spaceName;
  }

  public void setSpaceName(String spaceName) {
    this.spaceName = spaceName;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getOwnerUserName() {
    return ownerUserName;
  }

  public void setOwnerUserName(String ownerUserName) {
    this.ownerUserName = ownerUserName;
  }

  public Long getAnnotationCount() {
    return annotationCount;
  }

  public void setAnnotationCount(Long annotationCount) {
    this.annotationCount = annotationCount;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  @Override
  public String toString() {
    return String.format(
        "SpaceStatistics [id=%s, spaceId=%s, ownerId=%s, discussionsCount=%s, communityCount=%s, isPrivate=%s, contentTypesCount=%s, contentCount=%s, assessmentTypesCount=%s, assessmentCount=%s, numberOfSolvedAssessment=%s, collectionDate=%s]",
        id,
        spaceId,
        ownerId,
        discussionsCount,
        communityCount,
        null,
        contentTypesCount,
        contentCount,
        assessmentTypesCount,
        assessmentCount,
        numberOfSolvedAssessment,
        collectionDate);
  }
}
