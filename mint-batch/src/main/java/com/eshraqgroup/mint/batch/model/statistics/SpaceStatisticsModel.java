package com.eshraqgroup.mint.batch.model.statistics;

import java.util.Date;
import java.util.EnumMap;
import com.eshraqgroup.mint.batch.domain.SpaceStatistics;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.ContentType;
import com.eshraqgroup.mint.domain.jpa.Space;

public class SpaceStatisticsModel {

  private Long id;
  private String name;
  private String categoryName;
  private Date creationDate;
  private Long discussionsCount = 0l;
  private Long discussionsCommentCount = 0l;
  private Long discussionsCommentLikeCount = 0l;
  private Integer communityCount = 0;
  private EnumMap<ContentType, Long> contentTypesCount = new EnumMap<>(ContentType.class);
  private EnumMap<ContentType, Long> contentTypesSize = new EnumMap<>(ContentType.class);
  private Long contentCount;
  private Long contentSize;
  private EnumMap<AssessmentType, Long> assessmentTypesCount = new EnumMap<>(AssessmentType.class);
  private Long assessmentCount;
  private Long annotationCount;
  private Long numberOfSolvedAssessment;
  private boolean deleted;
  private Integer month = 0;
  private Integer year = 0;

  public SpaceStatisticsModel(Space space) {
    if (space != null) {
      this.id = space.getId();
      this.name = space.getName();
      this.categoryName = space.getCategory().getName();
      this.creationDate = space.getCreationDate();
      this.deleted = space.isDeleted();
    }
  }

  public SpaceStatisticsModel(Space space, SpaceStatistics spaceStatistics) {
    this(space);
    if (space == null) {
      this.id = spaceStatistics.getSpaceId();
      this.name = spaceStatistics.getSpaceName();
      this.categoryName = spaceStatistics.getCategoryName();
      this.creationDate = spaceStatistics.getCollectionDate();
    }
    this.discussionsCount = spaceStatistics.getDiscussionsCount();
    this.communityCount = spaceStatistics.getCommunityCount();
    this.contentTypesCount = spaceStatistics.getContentTypesCount();
    this.contentCount = spaceStatistics.getContentCount();
    this.assessmentTypesCount = spaceStatistics.getAssessmentTypesCount();
    this.assessmentCount = spaceStatistics.getAssessmentCount();
    this.annotationCount = spaceStatistics.getAnnotationCount();
    this.numberOfSolvedAssessment = spaceStatistics.getNumberOfSolvedAssessment();
    this.month = spaceStatistics.getMonth();
    this.year = spaceStatistics.getYear();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Long getDiscussionsCount() {
    return discussionsCount;
  }

  public void setDiscussionsCount(Long discussionsCount) {
    this.discussionsCount = discussionsCount;
  }

  public Integer getCommunityCount() {
    return communityCount;
  }

  public void setCommunityCount(Integer communityCount) {
    this.communityCount = communityCount;
  }

  public EnumMap<ContentType, Long> getContentTypesCount() {
    return contentTypesCount;
  }

  public void setContentTypesCount(EnumMap<ContentType, Long> contentTypesCount) {
    this.contentTypesCount = contentTypesCount;
  }

  public Long getContentCount() {
    return contentCount;
  }

  public void setContentCount(Long contentCount) {
    this.contentCount = contentCount;
  }

  public EnumMap<AssessmentType, Long> getAssessmentTypesCount() {
    return assessmentTypesCount;
  }

  public void setAssessmentTypesCount(EnumMap<AssessmentType, Long> assessmentTypesCount) {
    this.assessmentTypesCount = assessmentTypesCount;
  }

  public Long getAssessmentCount() {
    return assessmentCount;
  }

  public void setAssessmentCount(Long assessmentCount) {
    this.assessmentCount = assessmentCount;
  }

  public Long getNumberOfSolvedAssessment() {
    return numberOfSolvedAssessment;
  }

  public void setNumberOfSolvedAssessment(Long numberOfSolvedAssessment) {
    this.numberOfSolvedAssessment = numberOfSolvedAssessment;
  }

  public Integer getMonth() {
    return month;
  }

  public void setMonth(Integer month) {
    this.month = month;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public EnumMap<ContentType, Long> getContentTypesSize() {
    return contentTypesSize;
  }

  public Long getContentSize() {
    return contentSize;
  }

  public void setContentSize(Long contentSize) {
    this.contentSize = contentSize;
  }

  public void setContentTypesSize(EnumMap<ContentType, Long> contentTypesSize) {
    this.contentTypesSize = contentTypesSize;
  }

  public Long getAnnotationCount() {
    return annotationCount;
  }

  public void setAnnotationCount(Long annotationCount) {
    this.annotationCount = annotationCount;
  }

  public Long getDiscussionsCommentCount() {
    return discussionsCommentCount;
  }

  public void setDiscussionsCommentCount(Long discussionsCommentCount) {
    this.discussionsCommentCount = discussionsCommentCount;
  }

  public Long getDiscussionsCommentLikeCount() {
    return discussionsCommentLikeCount;
  }

  public void setDiscussionsCommentLikeCount(Long discussionsCommentLikeCount) {
    this.discussionsCommentLikeCount = discussionsCommentLikeCount;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof SpaceStatisticsModel)) {
      return false;
    }

    SpaceStatisticsModel that = (SpaceStatisticsModel) o;

    if (!id.equals(that.id)) {
      return false;
    }
    if (!month.equals(that.month)) {
      return false;
    }
    return year.equals(that.year);
  }

  @Override
  public int hashCode() {
    int result = id.hashCode();
    result = 31 * result + month.hashCode();
    result = 31 * result + year.hashCode();
    return result;
  }
}
