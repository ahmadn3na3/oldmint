package com.eshraqgroup.mint.batch.model.statistics;

import java.util.Date;
import java.util.EnumMap;
import com.eshraqgroup.mint.batch.domain.UserStatistics;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.ContentType;
import com.eshraqgroup.mint.domain.jpa.User;

public class UserStatisticsModel {
  private Long id;
  private String userName;
  private String fullName;
  private Date creationDate;
  private Integer spaceOwned = 0;
  private Long discussionsCount = 0l;
  private EnumMap<ContentType, Long> contentTypesCount = new EnumMap<>(ContentType.class);
  private Long contentCount = 0l;
  private EnumMap<AssessmentType, Long> assessmentTypesCount = new EnumMap<>(AssessmentType.class);
  private Long assessmentCount = 0l;
  private Long numberOfSolvedAssessment = 0l;
  private Long numberOfAnnotation = 0l;
  private Integer month = 0;
  private Integer year = 0;
  private boolean delete;

  public UserStatisticsModel() {}

  public UserStatisticsModel(User user, UserStatistics userStatistics) {
    this.id = userStatistics.getUserId();
    this.userName = userStatistics.getUserName();
    this.fullName = userStatistics.getFullName();
    if (user != null) {
      this.creationDate = user.getCreationDate();
    } else {
      this.delete = true;
    }
    this.spaceOwned = userStatistics.getSpaceOwned();
    this.discussionsCount = userStatistics.getDiscussionsCount();
    this.contentTypesCount = userStatistics.getContentTypesCount();
    this.contentCount = userStatistics.getContentCount();
    this.assessmentTypesCount = userStatistics.getAssessmentTypesCount();
    this.assessmentCount = userStatistics.getAssessmentCount();
    this.numberOfAnnotation = userStatistics.getNumberOfAnnotation();
    this.numberOfSolvedAssessment = userStatistics.getNumberOfSolvedAssessment();
    this.month = userStatistics.getMonth();
    this.year = userStatistics.getYear();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Integer getSpaceOwned() {
    return spaceOwned;
  }

  public void setSpaceOwned(Integer spaceOwned) {
    this.spaceOwned = spaceOwned;
  }

  public Long getDiscussionsCount() {
    return discussionsCount;
  }

  public void setDiscussionsCount(Long discussionsCount) {
    this.discussionsCount = discussionsCount;
  }

  public EnumMap<ContentType, Long> getContentTypesCount() {
    return contentTypesCount;
  }

  public void setContentTypesCount(EnumMap<ContentType, Long> contentTypesCount) {
    this.contentTypesCount = contentTypesCount;
  }

  public Long getContentCount() {
    return contentCount;
  }

  public void setContentCount(Long contentCount) {
    this.contentCount = contentCount;
  }

  public EnumMap<AssessmentType, Long> getAssessmentTypesCount() {
    return assessmentTypesCount;
  }

  public void setAssessmentTypesCount(EnumMap<AssessmentType, Long> assessmentTypesCount) {
    this.assessmentTypesCount = assessmentTypesCount;
  }

  public Long getAssessmentCount() {
    return assessmentCount;
  }

  public void setAssessmentCount(Long assessmentCount) {
    this.assessmentCount = assessmentCount;
  }

  public Long getNumberOfSolvedAssessment() {
    return numberOfSolvedAssessment;
  }

  public void setNumberOfSolvedAssessment(Long numberOfSolvedAssessment) {
    this.numberOfSolvedAssessment = numberOfSolvedAssessment;
  }

  public Long getNumberOfAnnotation() {
    return numberOfAnnotation;
  }

  public void setNumberOfAnnotation(Long numberOfAnnotation) {
    this.numberOfAnnotation = numberOfAnnotation;
  }

  public boolean isDelete() {
    return delete;
  }

  public void setDelete(boolean delete) {
    this.delete = delete;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof UserStatisticsModel)) {
      return false;
    }

    UserStatisticsModel that = (UserStatisticsModel) o;

    if (!id.equals(that.id)) {
      return false;
    }
    if (!month.equals(that.month)) {
      return false;
    }
    return year.equals(that.year);
  }

  @Override
  public int hashCode() {
    int result = id.hashCode();
    result = 31 * result + month.hashCode();
    result = 31 * result + year.hashCode();
    return result;
  }
}
