package com.eshraqgroup.mint.batch.repos;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.batch.domain.UserStatistics;

@Repository
public interface UserStatisticsRepo extends MongoRepository<UserStatistics, String> {
  Optional<UserStatistics> findOneByUserIdAndMonthAndYear(Long userId, Integer month, Integer year);
}
