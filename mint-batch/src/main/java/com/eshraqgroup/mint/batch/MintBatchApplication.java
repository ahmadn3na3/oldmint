package com.eshraqgroup.mint.batch;

import java.util.TimeZone;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = "com.eshraqgroup.mint")
@EnableJpaRepositories(basePackages = {"com.eshraqgroup.mint.repos.jpa"})
@EntityScan(basePackages = {"com.eshraqgroup.mint.domain", "com.eshraqgroup.mint.batch.domain"})
@EnableMongoRepositories(
  basePackages = {"com.eshraqgroup.mint.repos.mongo", "com.eshraqgroup.mint.batch.repos"}
)
public class MintBatchApplication {
  public static void main(String[] args) {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    SpringApplication.run(MintBatchApplication.class, args);
  }
}
