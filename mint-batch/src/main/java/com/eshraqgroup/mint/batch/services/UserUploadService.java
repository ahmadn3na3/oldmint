package com.eshraqgroup.mint.batch.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.eshraqgroup.mint.batch.model.ErrorModel;
import com.eshraqgroup.mint.batch.model.UploadUserResponseModel;
import com.eshraqgroup.mint.batch.model.UserPasswordModel;
import com.eshraqgroup.mint.batch.util.ExcelUtil;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.Gender;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Groups;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Role;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.CategoryRepository;
import com.eshraqgroup.mint.repos.jpa.FoundationRepository;
import com.eshraqgroup.mint.repos.jpa.GroupsRepository;
import com.eshraqgroup.mint.repos.jpa.OrganizationRepository;
import com.eshraqgroup.mint.repos.jpa.RoleRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.util.RandomUtils;

/** Created by ayman on 06/11/16. */
@Service
public class UserUploadService {

  @Autowired FoundationRepository foundationRepository;
  @Autowired OrganizationRepository organizationRepository;

  @Autowired CategoryRepository categoryRepository;

  @Autowired UserRepository userRepository;

  @Autowired GroupsRepository groupsRepository;

  @Autowired PasswordEncoder passwordEncoder;

  @Autowired RoleRepository roleRepository;

  @Transactional
  public ResponseModel uploadUsers(
      MultipartFile dataFile, Long groupId, Long foundationId, Long organizationId, Long roleId) {
    Foundation foundation = null;
    Organization organization = null;

    Groups group = null;
    Role role = null;

    if (organizationId != null) {

      organization =
          organizationRepository
              .findOneByIdAndDeletedFalse(organizationId)
              .orElseThrow(() -> new NotFoundException("organization"));
      foundation = organization.getFoundation();
    }

    if (foundationId != null && foundation == null) {
      foundation =
          foundationRepository
              .findOneByIdAndDeletedFalse(foundationId)
              .orElseThrow(() -> new NotFoundException("foundation"));
    }

    if (groupId != null) {
      group =
          groupsRepository
              .findOneByIdAndDeletedFalse(groupId)
              .orElseThrow(() -> new NotFoundException("group"));
    }

    if (roleId != null) {
      role =
          roleRepository
              .findOneByIdAndDeletedFalse(roleId)
              .orElseThrow(() -> new NotFoundException("role"));
    }

    if (dataFile == null || dataFile.isEmpty()) {
      return ResponseModel.error(Code.INVALID, "file");
    } else {
      try {
        InputStream fileContent = dataFile.getInputStream();
        ByteArrayInputStream is = new ByteArrayInputStream(IOUtils.toByteArray(fileContent));
        Workbook wb = WorkbookFactory.create(is);
        return saveUsers(wb, group, foundation, organization, role);
      } catch (IOException ioExp) {
        return ResponseModel.error(Code.MISSING, ioExp.getMessage());
      } catch (InvalidFormatException ifExp) {
        return ResponseModel.error(Code.MISSING, ifExp.getMessage());
      }
    }
  }

  @Transactional
  protected ResponseModel saveUsers(
      Workbook wb, Groups group, Foundation foundation, Organization organization, Role role)
      throws IOException {
    UploadUserResponseModel uploadUserResponseModel = new UploadUserResponseModel();
    Sheet datasheet = wb.getSheetAt(0);
    datasheet.removeRow(datasheet.getRow(0));
    List<Groups> groupsList = new ArrayList<>();
    Set<Role> roleList = new HashSet<>();
    Set<User> users = new HashSet<>();
    groupsList.add(group);
    roleList.add(role);

    for (Row row : datasheet) {

      User user = new User();

      if (!ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
        continue;
      }

      if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
        if (userRepository
            .findOneByUserNameAndDeletedFalse(
                row.getCell(0).getStringCellValue() + "@" + organization.getOrgId())
            .isPresent()) {
          ErrorModel errorModel =
              new ErrorModel(String.valueOf(row.getRowNum() + 1), "Username already Exist");
          uploadUserResponseModel.addError(errorModel);
          continue;
        } else {
          user.setUserName(row.getCell(0).getStringCellValue() + "@" + organization.getOrgId());
        }
      } else {
        ErrorModel errorModel =
            new ErrorModel(String.valueOf(row.getRowNum() + 1), "Username is null");
        uploadUserResponseModel.addError(errorModel);
        continue;
      }

      if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
        user.setFullName(row.getCell(1).getStringCellValue());
      } else {
        ErrorModel errorModel =
            new ErrorModel(String.valueOf(row.getRowNum() + 1), "Full Name is null");
        uploadUserResponseModel.addError(errorModel);
        continue;
      }

      if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
        user.setMobile(row.getCell(2).getStringCellValue());
      }

      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)) {
        user.setColor(row.getCell(3).getStringCellValue());
      }

      if (ExcelUtil.validate(row.getCell(4), Cell.CELL_TYPE_STRING)) {
        if (userRepository.countByEmailAndDeletedFalse(row.getCell(4).getStringCellValue()) >= 1) {
          ErrorModel errorModel =
              new ErrorModel(String.valueOf(row.getRowNum() + 1), "Email already Exist");
          uploadUserResponseModel.addError(errorModel);
          continue;
        } else {
          user.setEmail(row.getCell(4).getStringCellValue());
        }

      } else {
        ErrorModel errorModel =
            new ErrorModel(String.valueOf(row.getRowNum() + 1), "E-mail is Null");
        uploadUserResponseModel.addError(errorModel);
        continue;
      }

      if (ExcelUtil.validate(row.getCell(5), Cell.CELL_TYPE_BOOLEAN)) {
        user.setStatus(row.getCell(5).getBooleanCellValue());
      }

      if (ExcelUtil.validate(row.getCell(6), Cell.CELL_TYPE_BOOLEAN)) {
        user.setFirstLogin(row.getCell(6).getBooleanCellValue());
      }

      if (ExcelUtil.validate(row.getCell(7), Cell.CELL_TYPE_BOOLEAN)) {
        user.setForceChangePassword(row.getCell(7).getBooleanCellValue());
      }

      if (ExcelUtil.validate(row.getCell(8), Cell.CELL_TYPE_STRING)) {
        user.setGender(Gender.valueOf(row.getCell(8).getStringCellValue()).getValue());
      }

      if (ExcelUtil.validate(row.getCell(9), Cell.CELL_TYPE_NUMERIC)) {
        user.setBirthDate(row.getCell(9).getDateCellValue());
      }

      if (ExcelUtil.validate(row.getCell(10), Cell.CELL_TYPE_STRING)) {
        user.setProfession(row.getCell(10).getStringCellValue());
      }

      if (ExcelUtil.validate(row.getCell(11), Cell.CELL_TYPE_STRING)) {
        user.setCountry(row.getCell(11).getStringCellValue());
      }

      user.setGroups(groupsList);
      user.setOrganization(organization);
      user.setFoundation(foundation);
      user.setRoles(roleList);
      user.setType(UserType.USER);
      user.setStartDate(user.getCreationDate());
      user.setEndDate(organization.getEndDate());
      user.setStatus(organization.getActive());
      String tempPassword = RandomUtils.generatePassword();
      String encryptedPassword = passwordEncoder.encode(tempPassword);
      user.setPassword(encryptedPassword);
      UserPasswordModel userPasswordModel = new UserPasswordModel(user.getUserName(), tempPassword);
      userPasswordModel.setFullName(user.getFullName());
      uploadUserResponseModel.addUserPassword(userPasswordModel);
      if (!users
          .stream()
          .anyMatch(
              u ->
                  u.getUserName().equalsIgnoreCase(user.getUserName())
                      || u.getEmail().equalsIgnoreCase(user.getEmail()))) {
        users.add(user);
      }
    }
    if (users.size()
        > (organization.getFoundation().getFoundationPackage().getNumberOfUsers()
            - userRepository.countByFoundationAndDeletedFalse(organization.getFoundation()))) {
      throw new InvalidException("error.organization.limit");
    }
    userRepository.save(users);
    if (role != null) {
      role.getUsers().addAll(users);
      roleRepository.save(role);
    }
    if (group != null) {
      group.getUsers().addAll(users);
      groupsRepository.save(group);
    }

    return ResponseModel.done(uploadUserResponseModel);
  }
}
