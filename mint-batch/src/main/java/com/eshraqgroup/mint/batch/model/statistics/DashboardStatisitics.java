package com.eshraqgroup.mint.batch.model.statistics;

public class DashboardStatisitics {
  private long spaceCount;
  private long userCount;

  public DashboardStatisitics() {
    // TODO Auto-generated constructor stub
  }

  public DashboardStatisitics(long spaceCount, long userCount) {
    super();
    this.spaceCount = spaceCount;
    this.userCount = userCount;
  }

  public long getSpaceCount() {
    return spaceCount;
  }

  public void setSpaceCount(long spaceCount) {
    this.spaceCount = spaceCount;
  }

  public long getUserCount() {
    return userCount;
  }

  public void setUserCount(long userCount) {
    this.userCount = userCount;
  }
}
