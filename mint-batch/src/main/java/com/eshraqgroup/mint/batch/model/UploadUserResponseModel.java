package com.eshraqgroup.mint.batch.model;

import java.util.ArrayList;
import java.util.List;

/** Created by ayman on 24/08/16. */
public class UploadUserResponseModel {

  private List<UserPasswordModel> userPasswordList;
  private List<ErrorModel> errorList;

  public UploadUserResponseModel() {
    userPasswordList = new ArrayList<>();
    errorList = new ArrayList<>();
  }

  public List<UserPasswordModel> getUserPasswordList() {
    return userPasswordList;
  }

  public void setUserPasswordList(List<UserPasswordModel> userPasswordList) {
    this.userPasswordList = userPasswordList;
  }

  public List<ErrorModel> getErrorList() {
    return errorList;
  }

  public void setErrorList(List<ErrorModel> errorList) {
    this.errorList = errorList;
  }

  public void addError(ErrorModel errorModel) {
    this.errorList.add(errorModel);
  }

  public void addUserPassword(UserPasswordModel userPasswordModel) {
    this.userPasswordList.add(userPasswordModel);
  }
}
