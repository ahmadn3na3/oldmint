package com.eshraqgroup.mint.batch.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.eshraqgroup.mint.batch.services.UserUploadService;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

/** Created by ayman on 04/08/16. */
@RestController
@RequestMapping("/api/batch/upload")
public class UploadController {

  @Autowired UserUploadService userUploadService;

  @RequestMapping(
    path = "/users",
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE,
    method = RequestMethod.POST
  )
  @ApiOperation(
    value = "Upload Users",
    notes = "this method is used to upload bulk of users from file"
  )
  public ResponseModel uploadUsers(
      @RequestParam("dataFile") MultipartFile dataFile,
      @RequestParam(name = "groupId", required = false) Long groupId,
      @RequestParam(value = "organizationId", required = false) Long organizationId,
      @RequestParam(value = "foundationId", required = false) Long foundationId,
      @RequestParam(name = "roleId", required = false) Long roleId,
      HttpServletRequest request) {
    return userUploadService.uploadUsers(dataFile, groupId, foundationId, organizationId, roleId);
  }
}
