package com.eshraqgroup.mint.batch.model.statistics;

public class StatisticDiscussionModel {
  private String id;
  private String title;
  private String body;
  private Long numberOfComments = 0l;
  private String contentTitle;
  private boolean deleted;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String name) {
    this.title = name;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Long getNumberOfComments() {
    return numberOfComments;
  }

  public void setNumberOfComments(Long numberOfComments) {
    this.numberOfComments = numberOfComments;
  }

  public String getContentTitle() {
    return contentTitle;
  }

  public void setContentTitle(String contentTitle) {
    this.contentTitle = contentTitle;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }
}
