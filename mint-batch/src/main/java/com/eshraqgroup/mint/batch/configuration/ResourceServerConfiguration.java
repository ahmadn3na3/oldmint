package com.eshraqgroup.mint.batch.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.security.Http401UnauthorizedEntryPoint;

/** Created by Ayman on 8/4/16. */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

  @Autowired private Http401UnauthorizedEntryPoint authenticationEntryPoint;

  @Value("${mint.security.signingKey}")
  private String signingKey;

  // @Autowired private ApprovalStore approvalStore;
  //
  // @Bean
  // public JwtAccessTokenConverter jwtTokenEnhancer() {
  // final JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
  // jwtAccessTokenConverter.setSigningKey(signingKey);
  // return jwtAccessTokenConverter;
  // }
  //
  // @Bean
  // public TokenStore tokenStore() {
  // JwtTokenStore jwtTokenStore = new JwtTokenStore(jwtTokenEnhancer());
  // jwtTokenStore.setApprovalStore(approvalStore);
  // return jwtTokenStore;
  // }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.exceptionHandling()
        .authenticationEntryPoint(authenticationEntryPoint)
        .and()
        .csrf()
        .disable()
        .headers()
        .frameOptions()
        .disable()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(HttpMethod.OPTIONS, "/**")
        .permitAll()
        .antMatchers("/api/batch/upload")
        .authenticated()
        .antMatchers("/api/**")
        .authenticated()
        .antMatchers("/upload/**")
        .authenticated();
  }

  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    super.configure(resources);
    resources.authenticationEntryPoint(authenticationEntryPoint);
    resources.resourceId(Services.BATCH.getService());
  }
}
