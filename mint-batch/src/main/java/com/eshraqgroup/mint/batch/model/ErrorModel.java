package com.eshraqgroup.mint.batch.model;

/** Created by ayman on 24/08/16. */
public class ErrorModel {
  String rowId;
  String errorMessage;

  public ErrorModel() {}

  public ErrorModel(String rowId, String errorMessage) {
    this.rowId = rowId;
    this.errorMessage = errorMessage;
  }

  public String getRowId() {
    return rowId;
  }

  public void setRowId(String rowId) {
    this.rowId = rowId;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
