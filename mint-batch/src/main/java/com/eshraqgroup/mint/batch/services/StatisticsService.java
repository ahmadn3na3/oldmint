package com.eshraqgroup.mint.batch.services;

import static org.springframework.data.jpa.domain.Specifications.where;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.batch.domain.SpaceStatistics;
import com.eshraqgroup.mint.batch.domain.UserStatistics;
import com.eshraqgroup.mint.batch.model.statistics.DashboardStatisitics;
import com.eshraqgroup.mint.batch.model.statistics.SpaceStatisticsModel;
import com.eshraqgroup.mint.batch.model.statistics.StatisticAssessmentModel;
import com.eshraqgroup.mint.batch.model.statistics.StatisticContentModel;
import com.eshraqgroup.mint.batch.model.statistics.StatisticDiscussionModel;
import com.eshraqgroup.mint.batch.model.statistics.UserStatisticsModel;
import com.eshraqgroup.mint.batch.repos.SpaceStatisticsRepo;
import com.eshraqgroup.mint.batch.repos.UserStatisticsRepo;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.ContentType;
import com.eshraqgroup.mint.domain.jpa.Assessment;
import com.eshraqgroup.mint.domain.jpa.Content;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.domain.mongo.Annotation;
import com.eshraqgroup.mint.domain.mongo.Comment;
import com.eshraqgroup.mint.domain.mongo.ContentUser;
import com.eshraqgroup.mint.domain.mongo.Discussion;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.AssessmentRepository;
import com.eshraqgroup.mint.repos.jpa.ContentRepository;
import com.eshraqgroup.mint.repos.jpa.FoundationRepository;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.OrganizationRepository;
import com.eshraqgroup.mint.repos.jpa.SpaceRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.mongo.AnnotationRepository;
import com.eshraqgroup.mint.repos.mongo.ContentUserRepository;
import com.eshraqgroup.mint.repos.mongo.DiscussionRepository;
import com.eshraqgroup.mint.repos.mongo.UserAssessmentRepository;
import com.eshraqgroup.mint.repos.specifications.UserSpecifications;
import com.eshraqgroup.mint.security.CurrentUserDetail;
import com.eshraqgroup.mint.security.SecurityUtils;

@Service
public class StatisticsService {

  private final Logger log = LoggerFactory.getLogger(StatisticsService.class);

  @Autowired private MongoTemplate mongoTemplate;
  @Autowired private UserRepository userRepository;
  @Autowired private SpaceRepository spaceRepository;
  @Autowired private UserStatisticsRepo userStatisticsRepo;
  @Autowired private SpaceStatisticsRepo spaceStatisticsRepo;
  @Autowired private ContentRepository contentRepository;
  @Autowired private AssessmentRepository assessementRepository;
  @Autowired private JoinedRepository joinedRepository;
  @Autowired private UserAssessmentRepository userAssessmentRepository;
  @Autowired private AnnotationRepository annotationRepository;
  @Autowired private ContentUserRepository contentUserRepository;
  @Autowired private FoundationRepository foundationRepository;
  @Autowired private DiscussionRepository discussionRepository;
  @Autowired private OrganizationRepository organizationRepository;

  @Scheduled(cron = "0 0 0 * * ?")
  // @PostConstruct
  public void updateJob() {
    LocalDateTime to = LocalDate.now().atTime(23, 59, 59, 999999999);
    LocalDateTime from = LocalDate.now().atTime(0, 0, 0, 0).with(ChronoField.DAY_OF_MONTH, 1l);
    updateStatistics(from, to);
  }

  @Async
  public CompletableFuture<Date> updateRequest() {
    LocalDateTime to = LocalDate.now().atTime(23, 59, 59, 999999999);
    LocalDateTime from = LocalDate.now().atTime(0, 0, 0, 0).with(ChronoField.DAY_OF_MONTH, 1l);
    updateStatistics(from, to);
    return CompletableFuture.completedFuture(new Date());
  }

  // TODO: Use LastModifcationDateget
  @Transactional()
  public void updateStatistics(LocalDateTime from, LocalDateTime to) {
    List<SpaceStatistics> spaceStatisticsList = new ArrayList<>();
    spaceRepository
        .findAll()
        .stream()
        .forEach(
            space -> {
              SpaceStatistics spaceStatistics =
                  spaceStatisticsRepo
                      .findOneBySpaceIdAndMonthAndYear(
                          space.getId(), from.getMonthValue(), from.getYear())
                      .orElse(new SpaceStatistics(space.getId(), space.getUser().getId()));
              spaceStatistics.setDay(to.getDayOfMonth());
              if (spaceStatistics.getId() == null) {
                spaceStatistics.setOrganizationId(
                    space.getUser().getOrganization() == null
                        ? null
                        : space.getUser().getOrganization().getId());
                spaceStatistics.setFoundationId(
                    space.getUser().getFoundation() == null
                        ? null
                        : space.getUser().getFoundation().getId());
              }
              spaceStatistics.setSpaceName(space.getName());
              spaceStatistics.setCategoryName(space.getCategory().getName());
              spaceStatistics.setOwnerUserName(space.getUser().getUserName());
              spaceStatistics.setCreationDate(space.getCreationDate());
              contentRepository
                  .contentStaticsByTypeAndCreationDateBetween(
                      space.getCreationDate(),
                      Date.from(to.toInstant(ZoneOffset.UTC)),
                      space.getId())
                  .stream()
                  .forEach(
                      typeCount ->
                          spaceStatistics
                              .getContentTypesCount()
                              .put((ContentType) typeCount[1], (Long) typeCount[0]));
              spaceStatistics.setContentCount(
                  spaceStatistics
                      .getContentTypesCount()
                      .entrySet()
                      .stream()
                      .mapToLong(Entry::getValue)
                      .sum());
              assessementRepository
                  .assessmenttStaticsByTypeAndCreationDateBetween(
                      space.getCreationDate(),
                      Date.from(to.toInstant(ZoneOffset.UTC)),
                      space.getId())
                  .forEach(
                      typeCount -> {
                        spaceStatistics
                            .getAssessmentTypesCount()
                            .put((AssessmentType) typeCount[1], (Long) typeCount[0]);
                      });
              spaceStatistics.setAssessmentCount(
                  spaceStatistics
                      .getAssessmentTypesCount()
                      .entrySet()
                      .stream()
                      .mapToLong(Entry::getValue)
                      .sum());

              Criteria criteria =
                  Criteria.where("creationDate")
                      .gte(space.getCreationDate())
                      .lte(Date.from(to.toInstant(ZoneOffset.UTC)))
                      .and("spaceId")
                      .is(space.getId());
              Query query = new Query().addCriteria(criteria);
              Query annotaionQuery =
                  new Query(
                      Criteria.where("isPublic")
                          .is(Boolean.FALSE)
                          .and("creationDate")
                          .gte(space.getCreationDate())
                          .lte(Date.from(to.toInstant(ZoneOffset.UTC)))
                          .and("spaceId")
                          .is(space.getId()));

              spaceStatistics.setDiscussionsCount(mongoTemplate.count(query, "mint.discussions"));
              spaceStatistics.setAnnotationCount(
                  mongoTemplate.count(annotaionQuery, "mint.annotations"));

              spaceStatistics.setCommunityCount(joinedRepository.joinedStatics(space.getId()));

              spaceStatisticsList.add(spaceStatistics);
              if (spaceStatisticsList.size() >= 1000) {
                spaceStatisticsRepo.save(spaceStatisticsList);
                spaceStatisticsList.clear();
              }
            });
    if (!spaceStatisticsList.isEmpty()) {
      spaceStatisticsRepo.save(spaceStatisticsList);
      spaceStatisticsList.clear();
    }
    List<UserStatistics> userStatisticsList = new ArrayList<>();
    userRepository
        .findAll()
        .forEach(
            user -> {
              UserStatistics userStatistics =
                  userStatisticsRepo
                      .findOneByUserIdAndMonthAndYear(
                          user.getId(), from.getMonthValue(), from.getYear())
                      .orElse(
                          new UserStatistics(
                              user.getId(),
                              user.getOrganization() == null
                                  ? null
                                  : user.getOrganization().getId(),
                              user.getFoundation() == null ? null : user.getFoundation().getId()));
              userStatistics.setUserName(user.getUserName());
              userStatistics.setFullName(user.getFullName());
              userStatistics.setCreationDate(user.getCreationDate());
              userStatistics.setDay(to.getDayOfMonth());
              userStatistics.setSpaceOwned(spaceRepository.countByOwnerId(user.getId()));
              contentRepository
                  .contentStaticsByOwnerIdTypeAndCreationDateBetween(
                      user.getCreationDate(), Date.from(to.toInstant(ZoneOffset.UTC)), user.getId())
                  .forEach(
                      typeCount ->
                          userStatistics
                              .getContentTypesCount()
                              .put((ContentType) typeCount[1], (Long) typeCount[0]));
              userStatistics.setContentCount(
                  userStatistics
                      .getContentTypesCount()
                      .entrySet()
                      .stream()
                      .mapToLong(Entry::getValue)
                      .sum());
              assessementRepository
                  .assessmentStaticsByOwnerIdTypeAndCreationDateBetween(
                      user.getCreationDate(), Date.from(to.toInstant(ZoneOffset.UTC)), user.getId())
                  .forEach(
                      typeCount ->
                          userStatistics
                              .getAssessmentTypesCount()
                              .put((AssessmentType) typeCount[1], (Long) typeCount[0]));
              userStatistics.setAssessmentCount(
                  userStatistics
                      .getAssessmentTypesCount()
                      .entrySet()
                      .stream()
                      .mapToLong(Entry::getValue)
                      .sum());
              Criteria criteria =
                  Criteria.where("creationDate")
                      .gte(user.getCreationDate())
                      .lte(Date.from(to.toInstant(ZoneOffset.UTC)))
                      .and("ownerId")
                      .is(user.getId());
              Query query = new Query().addCriteria(criteria);

              userStatistics.setDiscussionsCount(mongoTemplate.count(query, "mint.discussions"));
              criteria =
                  Criteria.where("creationDate")
                      .gte(user.getCreationDate())
                      .lte(Date.from(to.toInstant(ZoneOffset.UTC)))
                      .and("userId")
                      .is(user.getId());
              userStatistics.setNumberOfSolvedAssessment(
                  mongoTemplate.count(query, "mint.assessment"));
              userStatistics.setNumberOfAnnotation(mongoTemplate.count(query, "mint.annotations"));
              userStatisticsList.add(userStatistics);
              if (userStatisticsList.size() >= 1000) {
                userStatisticsRepo.save(userStatisticsList);
                userStatisticsList.clear();
              }
            });
    if (!userStatisticsList.isEmpty()) {
      userStatisticsRepo.save(userStatisticsList);
      userStatisticsList.clear();
    }
  }

  @Transactional(readOnly = true)
  public ResponseModel getDashBoardStatistics(final Long foundationId, final Long organizationId) {
    CurrentUserDetail currentUserDetail = SecurityUtils.getCurrentUser();
    Specification<User> byOrganization = null;
    Specification<User> byFoundation = null;

    Specification<Space> bySpaceOrganization = null;
    Specification<Space> bySpaceFoundation = null;

    Long foundId =
        currentUserDetail != null && currentUserDetail.getFoundationId() != null
            ? currentUserDetail.getFoundationId()
            : foundationId;
    Long orgId =
        currentUserDetail != null && currentUserDetail.getOrganizationId() != null
            ? currentUserDetail.getOrganizationId()
            : organizationId;

    if (foundId != null) {
      Foundation foundationIns =
          foundationRepository
              .findOneByIdAndDeletedFalse(foundId)
              .orElseThrow(() -> new NotFoundException("foundation"));

      byFoundation = UserSpecifications.inFoundation(foundationIns);
      bySpaceFoundation =
          (root, cq, cb) -> cb.equal(root.get("user").get("foundation"), foundationIns);

    } else {
      byFoundation = (r, q, cb) -> cb.isNull(r.get("foundation"));
      bySpaceFoundation = (root, cq, cb) -> cb.isNull(root.get("user").get("foundation"));
    }

    if (orgId != null) {
      Organization org =
          organizationRepository
              .findOneByIdAndDeletedFalse(orgId)
              .orElseThrow(() -> new NotFoundException("organiztion"));

      byOrganization = UserSpecifications.inOrganization(org);
      bySpaceOrganization = (root, cq, cb) -> cb.equal(root.get("user").get("organization"), org);
    }
    final long userCount =
        userRepository.count(
            where(byFoundation)
                .and(byOrganization)
                .and((r, q, cb) -> cb.equal(r.get("deleted").as(Boolean.class), false)));
    final long spaceCount =
        spaceRepository.count(
            where(bySpaceFoundation)
                .and(bySpaceOrganization)
                .and((r, q, cb) -> cb.equal(r.get("deleted").as(Boolean.class), false)));

    return ResponseModel.done(new DashboardStatisitics(spaceCount, userCount));
  }

  @Transactional(readOnly = true)
  public ResponseModel getUserStatistics(
      final Long foundationId,
      final Long organizationId,
      final Long groupId,
      final LocalDate from,
      final LocalDate to,
      String filter,
      PageRequest pageRequest) {
    CurrentUserDetail currentUserDetail = SecurityUtils.getCurrentUser();
    Long foundId =
        currentUserDetail.getFoundationId() != null
            ? currentUserDetail.getFoundationId()
            : foundationId;
    Long orgId =
        currentUserDetail.getOrganizationId() != null
            ? currentUserDetail.getOrganizationId()
            : organizationId;
    LocalDate fromDate = from != null ? from : LocalDate.now();
    LocalDate toDate = to != null ? to : LocalDate.now();
    Set<Long> usersId = new HashSet<>();
    if (groupId != null) {
      usersId.addAll(
          userRepository
              .findByGroupsIdInAndDeletedFalse(Collections.singletonList(groupId))
              .map(User::getId)
              .collect(Collectors.toSet()));
    }

    Criteria criteria =
        Criteria.where("month")
            .gte(fromDate.getMonthValue())
            .lte(toDate.getMonthValue())
            .and("year")
            .gte(fromDate.getYear())
            .lte(toDate.getYear());
    if (foundId != null) {
      criteria.and("foundationId").is(foundId);
    } else {
      criteria.and("foundationId").exists(false);
    }
    if (orgId != null) {
      criteria.and("organizationId").is(orgId);
    } else {
      criteria.and("organizationId").exists(false);
    }
    if (usersId != null && !usersId.isEmpty()) {
      criteria.and("userId").in(usersId);
    }
    if (filter != null && !filter.isEmpty()) {
      criteria.orOperator(
          Criteria.where("userName").regex(filter, "i"),
          Criteria.where("fullName").regex(filter, "i"));
    }

    Query query = new Query().addCriteria(criteria).with(pageRequest);
    Page<UserStatisticsModel> usPage =
        PageableExecutionUtils.getPage(
                mongoTemplate.find(query, UserStatistics.class),
                pageRequest,
                () -> mongoTemplate.count(query, UserStatistics.class))
            .map(
                userStatistics -> {
                  User user = userRepository.findOne(userStatistics.getUserId());
                  return new UserStatisticsModel(user, userStatistics);
                });

    return PageResponseModel.done(
        usPage.getContent(), usPage.getTotalPages(), usPage.getNumber(), usPage.getTotalElements());
    //
  }

  @Transactional(readOnly = true)
  public ResponseModel getSpaceStatistics(
      final Long foundationId,
      final Long organizationId,
      final Long groupId,
      final LocalDate from,
      final LocalDate to,
      String filter,
      PageRequest pageRequest) {
    CurrentUserDetail currentUserDetail = SecurityUtils.getCurrentUser();
    Long foundId =
        currentUserDetail.getFoundationId() != null
            ? currentUserDetail.getFoundationId()
            : foundationId;
    Long orgId =
        currentUserDetail.getOrganizationId() != null
            ? currentUserDetail.getOrganizationId()
            : organizationId;
    LocalDate fromDate = from != null ? from : LocalDate.now();
    LocalDate toDate = to != null ? to : LocalDate.now();

    Set<Long> spaceIds = new HashSet<>();
    if (groupId != null) {
      spaceIds.addAll(
          joinedRepository
              .findByGroupNameAndDeletedFalse(groupId.toString())
              .map(joined -> joined.getSpace().getId())
              .collect(Collectors.toSet()));
    }

    Criteria criteria =
        Criteria.where("month")
            .gte(fromDate.getMonthValue())
            .lte(toDate.getMonthValue())
            .and("year")
            .gte(fromDate.getYear())
            .lte(toDate.getYear());
    if (foundId != null) {
      criteria.and("foundationId").is(foundId);
    } else {
      criteria.and("foundationId").exists(false);
    }
    if (orgId != null) {
      criteria.and("organizationId").is(orgId);
    } else {
      criteria.and("organizationId").exists(false);
    }
    if (spaceIds != null && !spaceIds.isEmpty()) {
      criteria.and("spaceId").in(spaceIds);
    }
    if (filter != null && !filter.isEmpty()) {
      criteria.orOperator(
          Criteria.where("spaceName").regex(filter, "i"),
          Criteria.where("spaceName").regex(filter, "i"));
    }

    Query query = new Query().addCriteria(criteria).with(pageRequest);

    Page<SpaceStatisticsModel> usPage =
        PageableExecutionUtils.getPage(
                mongoTemplate.find(query, SpaceStatistics.class),
                pageRequest,
                () -> mongoTemplate.count(query, SpaceStatistics.class))
            .map(
                spaceStatistics -> {
                  Space space =
                      spaceRepository
                          .findOneByIdAndDeletedFalse(spaceStatistics.getSpaceId())
                          .orElse(
                              spaceRepository
                                  .findOneByIdAndDeletedTrue(spaceStatistics.getSpaceId())
                                  .orElse(null));
                  return new SpaceStatisticsModel(space, spaceStatistics);
                });

    return PageResponseModel.done(
        usPage.getContent(), usPage.getTotalPages(), usPage.getNumber(), usPage.getTotalElements());
  }

  public ResponseModel getCurrentSpaceStatistics(long id) {
    return Optional.ofNullable(SecurityUtils.getCurrentUser())
        .map(
            currentUserDetail -> {
              Space space =
                  spaceRepository
                      .findOneByIdAndDeletedFalse(id)
                      .orElseThrow(NotFoundException::new);

              SpaceStatisticsModel spaceStatisticsModel = new SpaceStatisticsModel(space);

              LocalDateTime to = LocalDate.now().atTime(23, 59, 59, 999999999);
              contentRepository
                  .contentStaticsByTypeAndCreationDateBetween(
                      space.getCreationDate(),
                      Date.from(to.toInstant(ZoneOffset.UTC)),
                      space.getId())
                  .forEach(
                      typeCount -> {
                        spaceStatisticsModel
                            .getContentTypesCount()
                            .put((ContentType) typeCount[1], (Long) typeCount[0]);
                        spaceStatisticsModel
                            .getContentTypesSize()
                            .put((ContentType) typeCount[1], (Long) typeCount[2]);
                      });
              spaceStatisticsModel.setContentCount(
                  spaceStatisticsModel
                      .getContentTypesCount()
                      .entrySet()
                      .stream()
                      .mapToLong(Entry::getValue)
                      .sum());
              spaceStatisticsModel.setContentSize(
                  spaceStatisticsModel
                      .getContentTypesSize()
                      .entrySet()
                      .stream()
                      .mapToLong(Entry::getValue)
                      .sum());
              assessementRepository
                  .assessmenttStaticsByTypeAndCreationDateBetween(
                      space.getCreationDate(),
                      Date.from(to.toInstant(ZoneOffset.UTC)),
                      space.getId())
                  .forEach(
                      typeCount ->
                          spaceStatisticsModel
                              .getAssessmentTypesCount()
                              .put((AssessmentType) typeCount[1], (Long) typeCount[0]));
              spaceStatisticsModel.setAssessmentCount(
                  spaceStatisticsModel
                      .getAssessmentTypesCount()
                      .entrySet()
                      .stream()
                      .mapToLong(Entry::getValue)
                      .sum());

              Criteria criteria =
                  Criteria.where("creationDate")
                      .gte(space.getCreationDate())
                      .lte(new Date())
                      .and("spaceId")
                      .is(space.getId())
                      .and("deleted")
                      .is(false);
              Query query = new Query().addCriteria(criteria);
              spaceStatisticsModel.setDiscussionsCount(
                  mongoTemplate.count(query, Discussion.class));

              spaceStatisticsModel.setAnnotationCount(mongoTemplate.count(query, Annotation.class));
              mongoTemplate
                  .find(query, Discussion.class)
                  .stream()
                  .forEach(
                      dis -> {
                        spaceStatisticsModel.setDiscussionsCommentCount(
                            spaceStatisticsModel.getDiscussionsCommentCount()
                                + dis.getComments().size());
                        dis.getComments()
                            .forEach(
                                comment -> {
                                  if (comment != null)
                                    spaceStatisticsModel.setDiscussionsCommentLikeCount(
                                        spaceStatisticsModel.getDiscussionsCommentLikeCount()
                                            + comment.getVotes());
                                });
                      });

              spaceStatisticsModel.setCommunityCount(joinedRepository.joinedStatics(space.getId()));
              return ResponseModel.done(spaceStatisticsModel);
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional(readOnly = true)
  @SuppressWarnings("unchecked")
  public ResponseModel getContentStatistics(
      Long spaceId, PageRequest pageRequest, Boolean deleted) {

    Specification<Content> spaceIdSpecification =
        (r, q, cb) -> cb.equal(r.get("space").get("id"), spaceId);
    Specification<Content> deletedSpecification =
        deleted == null ? null : (r, q, cb) -> cb.equal(r.get("deleted"), deleted);

    Page<StatisticContentModel> contentModels =
        contentRepository
            .findAll(where(spaceIdSpecification).and(deletedSpecification), pageRequest)
            .map(
                content -> {
                  StatisticContentModel contentModel = new StatisticContentModel();
                  contentModel.setId(content.getId());
                  contentModel.setName(content.getName());
                  contentModel.setType(content.getType());
                  contentModel.setDeleted(content.isDeleted());
                  Aggregation aggregation =
                      newAggregation(
                          Aggregation.match(Criteria.where("contentId").is(content.getId())),
                          Aggregation.group().sum("views").as("numberOfViews"),
                          Aggregation.project("numberOfViews").andExclude("_id"));
                  log.debug("aggregation ==>  {}", aggregation.toString());

                  Map<String, Integer> dbObject =
                      mongoTemplate
                          .aggregate(aggregation, ContentUser.class, Map.class)
                          .getUniqueMappedResult();
                  if (dbObject != null) {
                    log.debug("result ==>  {}", dbObject.toString());
                    contentModel.setNumberOfViews(dbObject.get("numberOfViews"));
                  }
                  return contentModel;
                });

    return PageResponseModel.done(
        contentModels.getContent(),
        contentModels.getTotalPages(),
        contentModels.getNumber(),
        contentModels.getTotalElements());
  }

  @Transactional(readOnly = true)
  public ResponseModel getAssessmentStatistics(
      Long spaceId, PageRequest pageRequest, Boolean deleted, AssessmentType assessmentType) {
    Specification<Assessment> spaceIdSpecification =
        (r, q, cb) -> cb.equal(r.get("space").get("id"), spaceId);
    Specification<Assessment> deletedSpecification =
        deleted == null ? null : (r, q, cb) -> cb.equal(r.get("deleted"), deleted);
    Specification<Assessment> typeSpecification =
        deleted == null ? null : (r, q, cb) -> cb.equal(r.get("assessmentType"), assessmentType);
    Page<StatisticAssessmentModel> contentModels =
        assessementRepository
            .findAll(
                where(spaceIdSpecification).and(deletedSpecification).and(typeSpecification),
                pageRequest)
            .map(
                assessment -> {
                  StatisticAssessmentModel assessmentModel = new StatisticAssessmentModel();
                  assessmentModel.setId(assessment.getId());
                  assessmentModel.setTitle(assessment.getTitle());
                  assessmentModel.setType(assessment.getAssessmentType());
                  assessmentModel.setDeleted(assessment.isDeleted());
                  assessmentModel.setNumberOfSolved(
                      userAssessmentRepository.countByAssessmentId(assessment.getId()));

                  return assessmentModel;
                });

    return PageResponseModel.done(
        contentModels.getContent(),
        contentModels.getTotalPages(),
        contentModels.getNumber(),
        contentModels.getTotalElements());
  }

  @Transactional(readOnly = true)
  public ResponseModel getDiscussionStatistics(
      Long spaceId, PageRequest pageRequest, Boolean deleted) {
    Criteria criteria = Criteria.where("spaceId").is(spaceId);
    if (deleted != null) {
      criteria.and("deleted").is(deleted);
    }
    Query query = new Query().addCriteria(criteria);
    query.with(pageRequest);
    Page<StatisticDiscussionModel> contentModels =
        PageableExecutionUtils.getPage(
                mongoTemplate.find(query, Discussion.class),
                pageRequest,
                () -> mongoTemplate.count(query, Discussion.class))
            .map(
                disscussion -> {
                  StatisticDiscussionModel discussionModel = new StatisticDiscussionModel();
                  discussionModel.setId(disscussion.getId());
                  discussionModel.setTitle(disscussion.getTitle());
                  discussionModel.setBody(disscussion.getBody());
                  discussionModel.setDeleted(disscussion.isDeleted());
                  discussionModel.setNumberOfComments(
                      mongoTemplate.count(
                          new Query(Criteria.where("parentId").is(disscussion.getId())),
                          Comment.class));
                  if (disscussion.getContentId() != null) {
                    discussionModel.setContentTitle(
                        contentRepository.findNameByID(disscussion.getContentId()));
                  }

                  return discussionModel;
                });

    return PageResponseModel.done(
        contentModels.getContent(),
        contentModels.getTotalPages(),
        contentModels.getNumber(),
        contentModels.getTotalElements());
  }
}
