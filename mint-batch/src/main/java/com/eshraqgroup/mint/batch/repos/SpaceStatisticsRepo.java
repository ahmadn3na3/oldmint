package com.eshraqgroup.mint.batch.repos;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.eshraqgroup.mint.batch.domain.SpaceStatistics;

public interface SpaceStatisticsRepo extends MongoRepository<SpaceStatistics, String> {
  Optional<SpaceStatistics> findOneBySpaceIdAndMonthAndYear(long spaceId, int month, int year);

  Page<SpaceStatistics> findByMonthAndYear(int month, int year, Pageable pageable);
}
