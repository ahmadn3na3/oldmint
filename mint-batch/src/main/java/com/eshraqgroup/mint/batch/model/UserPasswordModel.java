package com.eshraqgroup.mint.batch.model;

public class UserPasswordModel {
  private String fullName;
  private String userName;
  private String password;

  public UserPasswordModel() {}

  public UserPasswordModel(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }

  public UserPasswordModel(String fullName, String userName, String password) {
    this.userName = userName;
    this.password = password;
    this.fullName = fullName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }
}
