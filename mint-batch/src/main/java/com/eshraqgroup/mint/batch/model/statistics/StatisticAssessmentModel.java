package com.eshraqgroup.mint.batch.model.statistics;

import com.eshraqgroup.mint.constants.AssessmentType;

public class StatisticAssessmentModel {
  private Long id;
  private String title;
  private AssessmentType type;
  private int numberOfSolved;
  private boolean deleted;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String name) {
    this.title = name;
  }

  public AssessmentType getType() {
    return type;
  }

  public void setType(AssessmentType type) {
    this.type = type;
  }

  public int getNumberOfSolved() {
    return numberOfSolved;
  }

  public void setNumberOfSolved(int numberOfViews) {
    this.numberOfSolved = numberOfViews;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }
}
