package com.eshraqgroup.mint.batch.domain;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.EnumMap;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.ContentType;

@Document(collection = "statistics.user")
public class UserStatistics {
  @Id private String id;
  @Indexed private Long userId;
  @Indexed private String userName;
  @Indexed private String fullName;
  @Indexed private Date creationDate;
  private Integer spaceOwned = 0;
  private Long discussionsCount = 0l;
  private EnumMap<ContentType, Long> contentTypesCount = new EnumMap<>(ContentType.class);
  private Long contentCount = 0l;
  private EnumMap<AssessmentType, Long> assessmentTypesCount = new EnumMap<>(AssessmentType.class);
  private Long assessmentCount = 0l;
  private Long numberOfSolvedAssessment = 0l;
  private Long numberOfAnnotation = 0l;
  @Indexed private Date collectionDate;
  @Indexed private Long organizationId;
  @Indexed private Long foundationId;
  @Indexed private Integer day = LocalDate.now().get(ChronoField.DAY_OF_MONTH);
  @Indexed private Integer month = LocalDate.now().get(ChronoField.MONTH_OF_YEAR);
  @Indexed private Integer year = LocalDate.now().get(ChronoField.YEAR);

  public UserStatistics() {
    super();
    collectionDate = new Date();
  }

  public UserStatistics(Long userId, Long organizationId, Long foundationId) {
    super();
    this.userId = userId;
    this.organizationId = organizationId;
    this.foundationId = foundationId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Integer getSpaceOwned() {
    return spaceOwned;
  }

  public void setSpaceOwned(Integer spaceOwned) {
    this.spaceOwned = spaceOwned;
  }

  public Long getDiscussionsCount() {
    return discussionsCount;
  }

  public void setDiscussionsCount(Long discussionsCount) {
    this.discussionsCount = discussionsCount;
  }

  public EnumMap<ContentType, Long> getContentTypesCount() {
    return contentTypesCount;
  }

  public void setContentTypesCount(EnumMap<ContentType, Long> contentTypesCount) {
    this.contentTypesCount = contentTypesCount;
  }

  public Long getContentCount() {
    return contentCount;
  }

  public void setContentCount(Long contentCount) {
    this.contentCount = contentCount;
  }

  public EnumMap<AssessmentType, Long> getAssessmentTypesCount() {
    return assessmentTypesCount;
  }

  public void setAssessmentTypesCount(EnumMap<AssessmentType, Long> assessmentTypesCount) {
    this.assessmentTypesCount = assessmentTypesCount;
  }

  public Long getAssessmentCount() {
    return assessmentCount;
  }

  public void setAssessmentCount(Long assessmentCount) {
    this.assessmentCount = assessmentCount;
  }

  public Long getNumberOfSolvedAssessment() {
    return numberOfSolvedAssessment;
  }

  public void setNumberOfSolvedAssessment(Long numberOfSolvedAssessment) {
    this.numberOfSolvedAssessment = numberOfSolvedAssessment;
  }

  public Long getNumberOfAnnotation() {
    return numberOfAnnotation;
  }

  public void setNumberOfAnnotation(Long numberOfAnnotation) {
    this.numberOfAnnotation = numberOfAnnotation;
  }

  public Date getCollectionDate() {
    return collectionDate;
  }

  public void setCollectionDate(Date collectionDate) {
    this.collectionDate = collectionDate;
  }

  public Integer getDay() {
    return day;
  }

  public void setDay(Integer day) {
    this.day = day;
  }

  public Integer getMonth() {
    return month;
  }

  public void setMonth(Integer month) {
    this.month = month;
  }

  public Integer getYear() {
    return year;
  }

  public void setYear(Integer year) {
    this.year = year;
  }

  public Long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(Long organizationId) {
    this.organizationId = organizationId;
  }

  public Long getFoundationId() {
    return foundationId;
  }

  public void setFoundationId(Long foundationId) {
    this.foundationId = foundationId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  @Override
  public String toString() {
    return String.format(
        "UserStatistics [id=%s, userId=%s, spaceOwned=%s, discussionsCount=%s, contentTypesCount=%s, contentCount=%s, assessmentTypesCount=%s, assessmentCount=%s, numberOfSolvedAssessment=%s, numberOfAnnotation=%s, collectionDate=%s, day=%s, month=%s, year=%s]",
        id,
        userId,
        spaceOwned,
        discussionsCount,
        contentTypesCount,
        contentCount,
        assessmentTypesCount,
        assessmentCount,
        numberOfSolvedAssessment,
        numberOfAnnotation,
        collectionDate,
        day,
        month,
        year);
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }
}
