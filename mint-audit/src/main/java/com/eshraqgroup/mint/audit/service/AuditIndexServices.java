package com.eshraqgroup.mint.audit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.audit.domain.Audit;
import com.eshraqgroup.mint.audit.repositories.AuditMongoRepository;
import com.eshraqgroup.mint.models.messages.audit.AuditMessage;

/** Created by ahmad on 5/25/17. */
@Component
public class AuditIndexServices {

  @Autowired AuditMongoRepository auditMongoRepository;

  @Autowired
  @Qualifier("auditSolrTemoplate")
  SolrTemplate solrTemplate;

  public void handleCreateAudit(AuditMessage message) {

    Audit audit = new Audit();
    audit.setUserId(message.getId());
    audit.setUserName(message.getUserName());
    audit.setAuditValue(message.getAuditValue());
    audit.setAuditDate(message.getAuditDate());
    audit.setAuditData(message.getData());
    audit.setOrgId(message.getOrgId());
    audit.setFoundationId(message.getFoundationId());
    audit.setClientId(message.getClientId());
    auditMongoRepository.save(audit);
  }
}
