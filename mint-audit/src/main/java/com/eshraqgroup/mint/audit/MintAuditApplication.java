package com.eshraqgroup.mint.audit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@EnableSolrRepositories(
  value = {"com.eshraqgroup.mint.audit.repositories", "com.eshraqgroup.mint.repos.solrrepo"},
  multicoreSupport = true
)
@SpringBootApplication(scanBasePackages = {"com.eshraqgroup.mint", "com.eshraqgroup.mint.audit"})
@EnableJpaRepositories(basePackages = {"com.eshraqgroup.mint.repos"})
@EntityScan(basePackages = {"com.eshraqgroup.mint.domain", "com.eshraqgroup.mint.audit.domain"})
@EnableMongoRepositories(
  basePackages = {"com.eshraqgroup.mint.audit.repositories", "com.eshraqgroup.mint.repos"}
)
public class MintAuditApplication {
  public static void main(String[] args) {
    SpringApplication.run(MintAuditApplication.class, args);
  }
}
