package com.eshraqgroup.mint.audit.controller;

import java.util.Arrays;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.audit.model.AuditSearchModel;
import com.eshraqgroup.mint.audit.service.AuditSearchService;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.controller.abstractcontroller.AbstractController;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.models.PageRequestModel;
import com.eshraqgroup.mint.models.ResponseModel;
import io.swagger.annotations.ApiOperation;

/** Created by ahmadsalah on 7/12/17. */
@RestController
@RequestMapping("/api/audit")
public class AuditController extends AbstractController {

  @Autowired private AuditSearchService auditSearchService;

  @RequestMapping(path = "/search", method = RequestMethod.POST)
  @ApiOperation(
    value = "search for audit",
    notes = "this method is used to search for audit logs for specific user in specific time period"
  )
  public ResponseModel get(
      @RequestHeader(required = false) Integer page,
      @RequestHeader(required = false) Integer size,
      @RequestBody AuditSearchModel auditSearchModel) {
    Date from = auditSearchModel.getFrom();
    Date to = auditSearchModel.getTo();

    long diffDays = (to.getTime() - from.getTime()) / (24 * 60 * 60 * 1000);
    if (diffDays > 30) {
      throw new MintException();
    }
    if (auditSearchModel.getUserName() != null) {
      return auditSearchService.searchByCriteria(
          auditSearchModel, PageRequestModel.getPageRequestModel(page, size));
    } else {
      return auditSearchService.getAll(
          from,
          to,
          auditSearchModel.getAuditValue(),
          PageRequestModel.getPageRequestModel(page, size));
    }
  }

  @RequestMapping(method = RequestMethod.GET, path = "/lastSuccess/{type}/{id}")
  public ResponseModel getLastSuccessfulOperation(
      @PathVariable String type, @PathVariable Long id) {
    if (!Arrays.asList("user", "organization", "foundation").contains(type)) {
      return ResponseModel.error(Code.INVALID);
    }
    return auditSearchService.getLastSuccessfulOperation(type, id);
  }
}
