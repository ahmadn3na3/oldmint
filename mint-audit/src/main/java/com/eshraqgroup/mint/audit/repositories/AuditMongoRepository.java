package com.eshraqgroup.mint.audit.repositories;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.audit.domain.Audit;

/** Created by ahmadsalah on 7/4/17. */
@Repository
public interface AuditMongoRepository extends MongoRepository<Audit, String> {

  List<Audit> findByIdIn(List<String> ids);

  Stream<Audit> findAllByAuditDateBetween(Date from, Date to);

  Stream<Audit> findAllByAuditValue(String value);

  Stream<Audit> findAllByUserNameIsStartingWith(String userName);

  Optional<Audit> findTopByAuditDataContainsAndOrgIdOrderByAuditDateDesc(String val, Long OrgId);

  Optional<Audit> findTopByAuditDataContainsAndFoundationIdOrderByAuditDateDesc(
      String val, Long foundationId);

  Optional<Audit> findTopByAuditDataIsContainingAndUserIdOrderByAuditDateDesc(
      String val, Long userId);
}
