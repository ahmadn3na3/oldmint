package com.eshraqgroup.mint.audit.model;

import java.util.Date;
import java.util.Map;
import com.eshraqgroup.mint.audit.domain.Audit;
import com.eshraqgroup.mint.constants.notification.EntityAction;

/** Created by ahmadsalah on 7/11/17. */
public class AuditModel {

  private String id;
  private Long userId;

  private String userName;

  private Long orgId;

  private Long foundationId;

  private EntityAction auditValue;

  private Date auditDate;

  private Map<String, String> auditData;

  public AuditModel() {}

  public AuditModel(Audit auditModel) {
    this.id = auditModel.getId();
    this.userId = auditModel.getUserId();
    this.userName = auditModel.getUserName();
    this.auditDate = auditModel.getAuditDate();
    this.auditData = auditModel.getAuditData();
    this.auditValue = auditModel.getAuditValue();
    this.foundationId = auditModel.getFoundationId();
    this.orgId = auditModel.getOrgId();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Date getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public EntityAction getAuditValue() {
    return auditValue;
  }

  public void setAuditValue(EntityAction auditValue) {
    this.auditValue = auditValue;
  }

  public Map<String, String> getAuditData() {
    return auditData;
  }

  public void setAuditData(Map<String, String> auditData) {
    this.auditData = auditData;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getOrgId() {
    return orgId;
  }

  public void setOrgId(Long orgId) {
    this.orgId = orgId;
  }

  public Long getFoundationId() {
    return foundationId;
  }

  public void setFoundationId(Long foundationId) {
    this.foundationId = foundationId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AuditModel that = (AuditModel) o;

    if (id != null ? !id.equals(that.id) : that.id != null) return false;
    if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;
    if (orgId != null ? !orgId.equals(that.orgId) : that.orgId != null) return false;
    if (foundationId != null ? !foundationId.equals(that.foundationId) : that.foundationId != null)
      return false;
    if (auditValue != null ? !auditValue.equals(that.auditValue) : that.auditValue != null)
      return false;
    if (auditDate != null ? !auditDate.equals(that.auditDate) : that.auditDate != null)
      return false;
    return auditData != null ? auditData.equals(that.auditData) : that.auditData == null;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + (userName != null ? userName.hashCode() : 0);
    result = 31 * result + (orgId != null ? orgId.hashCode() : 0);
    result = 31 * result + (foundationId != null ? foundationId.hashCode() : 0);
    result = 31 * result + (auditValue != null ? auditValue.hashCode() : 0);
    result = 31 * result + (auditDate != null ? auditDate.hashCode() : 0);
    result = 31 * result + (auditData != null ? auditData.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AuditModel{"
        + "id='"
        + id
        + '\''
        + ", userName='"
        + userName
        + '\''
        + ", orgId='"
        + orgId
        + '\''
        + ", foundationId='"
        + foundationId
        + '\''
        + ", auditValue='"
        + auditValue
        + '\''
        + ", auditDate="
        + auditDate
        + ", auditData="
        + auditData
        + '}';
  }
}
