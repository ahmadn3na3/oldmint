package com.eshraqgroup.mint.audit.domain;

import java.util.Date;
import java.util.Map;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.eshraqgroup.mint.constants.notification.EntityAction;

/** Created by ahmadsalah on 7/4/17. */
@Document(collection = "audit")
public class Audit {
  @Id private String id;

  @Indexed private Long userId;
  @Indexed private String userName;
  @Indexed private Long orgId;
  @Indexed private Long foundationId;
  @Indexed private EntityAction auditValue;
  @Indexed private Date auditDate;
  @Indexed private String clientId;

  private Map<String, String> auditData;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Date getAuditDate() {
    return auditDate;
  }

  public void setAuditDate(Date auditDate) {
    this.auditDate = auditDate;
  }

  public EntityAction getAuditValue() {
    return auditValue;
  }

  public void setAuditValue(EntityAction auditValue) {
    this.auditValue = auditValue;
  }

  public Map<String, String> getAuditData() {
    return auditData;
  }

  public void setAuditData(Map<String, String> auditData) {
    this.auditData = auditData;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getOrgId() {
    return orgId;
  }

  public void setOrgId(Long orgId) {
    this.orgId = orgId;
  }

  public Long getFoundationId() {
    return foundationId;
  }

  public void setFoundationId(Long foundationId) {
    this.foundationId = foundationId;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }
}
