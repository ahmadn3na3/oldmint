package com.eshraqgroup.mint.audit.model;

import java.text.ParseException;
import java.util.Date;
import org.joda.time.DateTime;

/** Created by ahmadsalah on 7/12/17. */
public class AuditSearchModel {

  private String userName;
  private String auditValue;
  private Date from;
  private Date to;

  public AuditSearchModel() throws ParseException {
    this.from = new DateTime(new Date()).minusDays(30).toDate();
    this.to = new Date();
  }

  public AuditSearchModel(String userName, String auditValue, Date from, Date to) {
    this.userName = userName;
    this.auditValue = auditValue;
    this.from = from;
    this.to = to;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getAuditValue() {
    return auditValue;
  }

  public void setAuditValue(String auditValue) {
    this.auditValue = auditValue;
  }

  public Date getFrom() {
    return from;
  }

  public void setFrom(Date from) throws ParseException {
    this.from = from;
  }

  public Date getTo() {
    return to;
  }

  public void setTo(Date to) throws ParseException {
    this.to = to;
  }

  @Override
  public String toString() {
    return "AuditSearchModel{"
        + "userName='"
        + userName
        + '\''
        + ", auditValue='"
        + auditValue
        + '\''
        + ", from="
        + from
        + ", to="
        + to
        + '}';
  }
}
