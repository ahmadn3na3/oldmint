package com.eshraqgroup.mint.audit.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.eshraqgroup.mint.constants.Services;

/** Created by ahmad on 5/30/17. */
@Configuration
public class AuditMessagingConfiguration {
  @Autowired
  @Qualifier("messageBus")
  Exchange topicExchange;

  @Bean("auditqueue")
  public Queue createQueue() {
    return new Queue(Services.AUDITING.getQueue());
  }

  @Bean("auditBinding")
  Binding createBinding(Queue auditqueue) {
    return BindingBuilder.bind(auditqueue)
        .to(topicExchange)
        .with(Services.AUDITING.getRoutingKey())
        .noargs();
  }
}
