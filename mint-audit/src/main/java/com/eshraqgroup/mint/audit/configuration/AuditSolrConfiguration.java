package com.eshraqgroup.mint.audit.configuration;

import org.apache.solr.client.solrj.SolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrTemplate;

/** Created by ahmad on 5/30/17. */
@Configuration
public class AuditSolrConfiguration {

  @Autowired SolrClient solrClient;

  @Bean("auditSolrTemoplate")
  public SolrTemplate solrTemplate() throws Exception {
    return new SolrTemplate(solrClient);
  }
}
