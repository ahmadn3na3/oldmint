package com.eshraqgroup.mint.audit.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import com.eshraqgroup.mint.models.messages.audit.AuditMessage;

/** Created by ahmad on 5/30/17. */
@Service
public class AuditMessageHandler {

  @Autowired AuditIndexServices auditIndexServices;

  @RabbitListener(queues = "auditqueue")
  private void handle(@Payload AuditMessage message) {
    auditIndexServices.handleCreateAudit(message);
  }
}
