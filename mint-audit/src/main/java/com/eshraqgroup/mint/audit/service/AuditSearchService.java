package com.eshraqgroup.mint.audit.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.audit.domain.Audit;
import com.eshraqgroup.mint.audit.model.AuditModel;
import com.eshraqgroup.mint.audit.model.AuditSearchModel;
import com.eshraqgroup.mint.audit.repositories.AuditMongoRepository;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.models.ResponseModel;

/** Created by ahmadsalah on 7/11/17. */
@Component
public class AuditSearchService {

  private final AuditMongoRepository auditMongoRepository;
  private final MongoTemplate mongoTemplate;

  @Autowired
  public AuditSearchService(
      AuditMongoRepository auditMongoRepository, MongoTemplate mongoTemplate) {
    this.auditMongoRepository = auditMongoRepository;
    this.mongoTemplate = mongoTemplate;
  }

  public ResponseModel searchByUserName(String userName) {
    return ResponseModel.done(
        auditMongoRepository
            .findAllByUserNameIsStartingWith(userName)
            .map(AuditModel::new)
            .collect(Collectors.toList()));
  }

  public ResponseModel searchByAuditValue(String auditValue) {
    return ResponseModel.done(
        auditMongoRepository
            .findAllByAuditValue(auditValue)
            .map(AuditModel::new)
            .collect(Collectors.toList()));
  }

  public ResponseModel searchByAuditDate(Date from, Date to) {
    return ResponseModel.done(
        auditMongoRepository
            .findAllByAuditDateBetween(from, to)
            .map(AuditModel::new)
            .collect(Collectors.toList()));
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel searchByCriteria(
      AuditSearchModel auditSearchModel, PageRequest pageRequest) {

    // query by date first
    Query query =
        new Query(
            Criteria.where("auditDate")
                .gte(auditSearchModel.getFrom())
                .lte(auditSearchModel.getTo()));

    // add filter query with user name

    query.addCriteria(Criteria.where("userName").alike(Example.of(auditSearchModel.getUserName())));
    if (auditSearchModel.getAuditValue() != null) {
      query.addCriteria(Criteria.where("auditValue").is(auditSearchModel.getAuditValue()));
    }

    query.with(pageRequest);

    List<AuditModel> auditList =
        mongoTemplate
            .find(query, Audit.class)
            .stream()
            .map(AuditModel::new)
            .collect(Collectors.toList());
    return ResponseModel.done(auditList);
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel getAll(Date from, Date to, String action, PageRequest pageRequest) {

    Query query = new Query(Criteria.where("auditDate").gte(from).lte(to));

    if (action != null) {
      query.addCriteria(Criteria.where("auditValue").is(action));
    }

    query.with(pageRequest);

    List<AuditModel> auditList =
        mongoTemplate
            .find(query, Audit.class)
            .stream()
            .map(AuditModel::new)
            .collect(Collectors.toList());
    return ResponseModel.done(auditList);
  }

  @Transactional
  @PreAuthorize("hasAuthority('SYSTEM_ADMIN')")
  public ResponseModel getLastSuccessfulOperation(String type, Long id) {
    AuditModel auditModel = null;
    switch (type) {
      case "user":
        auditModel =
            auditMongoRepository
                .findTopByAuditDataIsContainingAndUserIdOrderByAuditDateDesc("afterReturn", id)
                .map(AuditModel::new)
                .orElseThrow(NotFoundException::new);
        break;
      case "organization":
        auditModel =
            auditMongoRepository
                .findTopByAuditDataContainsAndOrgIdOrderByAuditDateDesc("afterReturn", id)
                .map(AuditModel::new)
                .orElseThrow(NotFoundException::new);

        break;
      case "foundation":
        auditModel =
            auditMongoRepository
                .findTopByAuditDataContainsAndFoundationIdOrderByAuditDateDesc("afterReturn", id)
                .map(AuditModel::new)
                .orElseThrow(NotFoundException::new);
        break;
    }
    return ResponseModel.done(auditModel);
  }
}
