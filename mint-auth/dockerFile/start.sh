#!/bin/bash
echo $JAVA_OPTS

RUN_ARG="--spring.profiles.active=${SERVICE_PROFILE:=docker} "

RUN_ARG=$RUN_ARG"--spring.datasource.url=${MYSQL_URL:=jdbc:mysql://${MYSQL_HOST}/mint?useSSL=false&useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull} "
RUN_ARG=$RUN_ARG"--spring.datasource.username=${MYSQL_USER:=root} "
RUN_ARG=$RUN_ARG"--spring.datasource.password=${MYSQL_PASS:=123456} "
RUN_ARG=$RUN_ARG"--spring.jpa.show-sql=${MYSQL_SHOWSQL:=true} "
RUN_ARG=$RUN_ARG"--spring.jpa.hibernate.ddl-auto=${MYSQL_DDLAUTO:=validate} "
RUN_ARG=$RUN_ARG"--spring.data.mongodb.uri=${MONGO_URL:=mongodb://mongo/mint} "
RUN_ARG=$RUN_ARG"--spring.rabbitmq.host=${RMQ_HOST:=rabbitmq} "
RUN_ARG=$RUN_ARG"--spring.rabbitmq.username=${RMQ_USER:=mint} "
RUN_ARG=$RUN_ARG"--spring.rabbitmq.password=${RMQ_PASS:=mint} "
RUN_ARG=$RUN_ARG"--spring.redis.host=${REDIS_HOST:=redis} --spring.redis.port=${REDIS_PORT:=6379} "
RUN_ARG=$RUN_ARG" --mint.lockPass=$LOCK_PASS--mint.weburl=$WEB_URL--mint.apiDomain=$API_URL "

if [ ! -z $REDIS_PASS ]; then
  RUN_ARG=$RUN_ARG"--spring.redis.password=${REDIS_PASS} "
fi

RUN_ARGS=$RUN_ARG"--server.port=${SEVER_PORT:=8080} "
unset RUN_ARG
echo $RUN_ARGS
java $JAVA_OPTS -jar /data/mint-auth.jar $RUN_ARGS