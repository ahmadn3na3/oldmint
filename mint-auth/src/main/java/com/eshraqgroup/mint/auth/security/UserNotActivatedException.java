package com.eshraqgroup.mint.auth.security;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/** This exception is throw in case of a not activated user trying to authenticate. */
public class UserNotActivatedException extends OAuth2Exception {

  public UserNotActivatedException(String message) {
    super(message);
  }

  public UserNotActivatedException(String message, Throwable t) {
    super(message, t);
  }

  @Override
  public String getOAuth2ErrorCode() {
    return "user_not_activated";
  }
}
