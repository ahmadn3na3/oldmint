package com.eshraqgroup.mint.auth.configuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import com.eshraqgroup.mint.configuration.MintOauthExceptionTranslator;
import com.eshraqgroup.mint.configuration.MintProperties;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.domain.jpa.Permission;
import com.eshraqgroup.mint.repos.jpa.PermissionRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;

/** Created by ahmad on 12/1/16. */
@Configuration
@EnableAuthorizationServer
public class OAuth2ServerConfiguration extends AuthorizationServerConfigurerAdapter {

  private final TokenStore tokenStore;
  private final MintOauthExceptionTranslator mintOauthExceptionTranslator;
  private final AuthenticationManager authenticationManager;
  private final MintProperties mintProperties;
  private final UserRepository userRepository;

  @Value("${mint.security.signingKey}")
  private String signingKey;

  private final PermissionRepository permissionRepository;
  private final Set<String> questionBankPermision;

  @Autowired
  public OAuth2ServerConfiguration(
      TokenStore tokenStore,
      MintOauthExceptionTranslator mintOauthExceptionTranslator,
      AuthenticationManager authenticationManager,
      MintProperties mintProperties,
      UserRepository userRepository,
      PermissionRepository permissionRepository) {
    this.tokenStore = tokenStore;
    this.mintOauthExceptionTranslator = mintOauthExceptionTranslator;
    this.authenticationManager = authenticationManager;
    this.mintProperties = mintProperties;
    this.userRepository = userRepository;
    this.permissionRepository = permissionRepository;
    questionBankPermision =
        new HashSet<>(
            Arrays.asList(
                "QUESTION_REVIEW_READ",
                "QUESTION_REVIEW_UPDATE",
                "QUESTION_REVIEW_DELETE",
                "QUESTION_CREATE",
                "QUESTION_UPDATE",
                "QUESTION_DELETE"));
  }

  // @Bean
  // public TokenStore tokenStore() {
  // JdbcTokenStore tokenStore = new JdbcTokenStore(dataSource);
  // return tokenStore;
  // }

  @Bean
  public TokenEnhancer tokenEnhancer() {
    return (oAuth2AccessToken, oAuth2Authentication) -> {
      DefaultOAuth2AccessToken defaultOAuth2AccessToken =
          (DefaultOAuth2AccessToken) oAuth2AccessToken;
      Authentication authentication = oAuth2Authentication.getUserAuthentication();
      String userName = "";
      if (authentication.getPrincipal() instanceof User) {
        UserDetails springSecurityUser = (User) authentication.getPrincipal();
        userName = springSecurityUser.getUsername();
      } else if (authentication.getPrincipal() instanceof String) {
        userName = (String) authentication.getPrincipal();
      }

      if (!mintProperties
              .getSecurity()
              .getAuthentication()
              .getOauthQuestionBank()
              .getClientid()
              .equalsIgnoreCase(userName)
          && mintProperties
              .getSecurity()
              .getAuthentication()
              .getOauthQuestionBank()
              .getClientid()
              .equals(oAuth2Authentication.getOAuth2Request().getClientId())) {
        if (!authentication
            .getAuthorities()
            .stream()
            .anyMatch(o -> questionBankPermision.contains(o.getAuthority()))) {
          throw OAuth2Exception.create(OAuth2Exception.ACCESS_DENIED, "Not Allowed to login ");
        }
      }

      userRepository
          .findOneByUserNameAndDeletedFalse(userName)
          .ifPresent(
              user -> {
                defaultOAuth2AccessToken.setAdditionalInformation(new HashMap<>());
                defaultOAuth2AccessToken
                    .getAdditionalInformation()
                    .put("first_login", user.getFirstLogin());
                defaultOAuth2AccessToken
                    .getAdditionalInformation()
                    .put("force_change_password", user.getForceChangePassword());
                defaultOAuth2AccessToken
                    .getAdditionalInformation()
                    .put("type", user.getType().name());
                defaultOAuth2AccessToken
                    .getAdditionalInformation()
                    .put("b2c", user.getFoundation() == null);

                if (user.getFirstLogin().booleanValue()) {
                  user.setFirstLogin(false);
                  userRepository.save(user);
                }
              });

      return defaultOAuth2AccessToken;
    };
  }

  // @Bean
  // public TokenStore tokenStore() {
  // JwtTokenStore jwtTokenStore = new JwtTokenStore(jwtTokenEnhancer());
  // // jwtTokenStore.setApprovalStore(approvalStore);
  // return jwtTokenStore;
  // }

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {

    endpoints
        .tokenStore(tokenStore)
        .tokenEnhancer(tokenEnhancer())
        .authenticationManager(authenticationManager)
        .exceptionTranslator(mintOauthExceptionTranslator)
        .reuseRefreshTokens(false);
  }

  @Override
  public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
    oauthServer.allowFormAuthenticationForClients();
  }

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    Set<String> permissions =
        permissionRepository
            .findAll()
            .stream()
            .map(Permission::getName)
            .collect(Collectors.toSet());
    clients
        .inMemory()
        .withClient(
            mintProperties.getSecurity().getAuthentication().getOauthAndroid().getClientid())
        .scopes("read", "write")
        .authorities(permissions.toArray(new String[0]))
        .authorizedGrantTypes("password", "refresh_token", "authorization_code", "implicit")
        .secret(mintProperties.getSecurity().getAuthentication().getOauthAndroid().getSecret())
        .accessTokenValiditySeconds(
            mintProperties
                .getSecurity()
                .getAuthentication()
                .getOauthAndroid()
                .getTokenValidityInSeconds())
        .resourceIds(
            Services.API.getService(),
            Services.CONTENT.getService(),
            Services.QUESTION_BANK.getService(),
            Services.NOTIFICATIONS.getService(),
            Services.BATCH.getService(),
            Services.CHAT.getService())
        .refreshTokenValiditySeconds(Integer.MAX_VALUE)
        .and()
        .withClient(mintProperties.getSecurity().getAuthentication().getOauthWeb().getClientid())
        .scopes("read", "write")
        .authorities(permissions.toArray(new String[0]))
        .authorizedGrantTypes("password", "refresh_token", "authorization_code", "implicit")
        .secret(mintProperties.getSecurity().getAuthentication().getOauthWeb().getSecret())
        .accessTokenValiditySeconds(
            mintProperties
                .getSecurity()
                .getAuthentication()
                .getOauthWeb()
                .getTokenValidityInSeconds())
        .resourceIds(
            Services.API.getService(),
            Services.CONTENT.getService(),
            Services.QUESTION_BANK.getService(),
            Services.NOTIFICATIONS.getService(),
            Services.BATCH.getService(),
            Services.CHAT.getService())
        .and()
        .withClient(
            mintProperties.getSecurity().getAuthentication().getOauthControlPanel().getClientid())
        .scopes("read", "write", "admin")
        .authorities(permissions.toArray(new String[0]))
        .authorizedGrantTypes("password", "refresh_token", "authorization_code", "implicit")
        .secret(mintProperties.getSecurity().getAuthentication().getOauthControlPanel().getSecret())
        .accessTokenValiditySeconds(
            mintProperties
                .getSecurity()
                .getAuthentication()
                .getOauthControlPanel()
                .getTokenValidityInSeconds())
        .resourceIds(
            Services.API.getService(),
            Services.CONTENT.getService(),
            Services.BATCH.getService(),
            Services.QUESTION_BANK.getService(),
            Services.NOTIFICATIONS.getService(),
            Services.AUDITING.getService())
        .and()
        .withClient(
            mintProperties.getSecurity().getAuthentication().getOauthQuestionBank().getClientid())
        .scopes("read", "write")
        .authorities(permissions.toArray(new String[0]))
        .authorities("password", "refresh_token", "authorization_code", "implicit")
        .secret(mintProperties.getSecurity().getAuthentication().getOauthQuestionBank().getSecret())
        .resourceIds(
            Services.API.getService(),
            Services.CONTENT.getService(),
            Services.QUESTION_BANK.getService())
        .accessTokenValiditySeconds(
            mintProperties
                .getSecurity()
                .getAuthentication()
                .getOauthQuestionBank()
                .getTokenValidityInSeconds());
  }
}
