package com.eshraqgroup.mint.auth.security;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/** This exception is throw in case of a not activated user trying to authenticate. */
public class UserDisabledException extends OAuth2Exception {

  public UserDisabledException(String message) {
    super(message);
  }

  public UserDisabledException(String message, Throwable t) {
    super(message, t);
  }

  @Override
  public String getOAuth2ErrorCode() {
    return "user_disabled";
  }
}
