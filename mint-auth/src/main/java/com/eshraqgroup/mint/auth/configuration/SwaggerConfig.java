package com.eshraqgroup.mint.auth.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/** Created by ahmad on 1/19/17. */
@Configuration
@EnableSwagger2
@Profile({"dev", "test", "devlocal"})
public class SwaggerConfig {
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.ant("/oauth/token"))
        .paths(PathSelectors.ant("/oauth/check_token"))
        .build()
        .apiInfo(
            new ApiInfo(
                "token issuance ",
                "https://tools.ietf.org/html/rfc6749#page-43",
                "",
                "",
                new Contact("", "", ""),
                "",
                ""));
  }
}
