package com.eshraqgroup.mint.auth.security;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/** This exception is throw in case of a not activated user trying to authenticate. */
public class UserExpiredException extends OAuth2Exception {

  public UserExpiredException(String message) {
    super(message);
  }

  public UserExpiredException(String message, Throwable t) {
    super(message, t);
  }

  @Override
  public String getOAuth2ErrorCode() {
    return "user_expired";
  }
}
