package com.eshraqgroup.mint.auth.security;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.Permission;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.repos.jpa.PermissionRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.CurrentUserDetail;
import com.eshraqgroup.mint.util.PermissionCheck;

/** Authenticate a user from the database. */
@SuppressWarnings("Convert2MethodRef")
@Component("userDetailsService")
public class UserDetailsService
    implements org.springframework.security.core.userdetails.UserDetailsService {

  private final Logger log = LoggerFactory.getLogger(UserDetailsService.class);
  @Autowired PermissionRepository permissionRepository;
  @Autowired private UserRepository userRepository;
  @Autowired private MessageSource messageSource;

  @Override
  @Transactional(readOnly = true)
  public UserDetails loadUserByUsername(final String login) {
    log.debug("Authenticating {}", login);
    HttpServletRequest request =
        ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    String lowercaseLogin = login.toLowerCase();
    Optional<User> userFromDatabase =
        userRepository.findOneByUserNameAndDeletedFalse(lowercaseLogin);
    if (!userFromDatabase.isPresent()) {
      userFromDatabase = userRepository.findOneByEmailAndDeletedFalse(lowercaseLogin);
    }
    if (!userFromDatabase.isPresent()) {
      throw new UsernameNotFoundException(
          "User " + lowercaseLogin + " was not found in the " + "database");
    }
    User user = userFromDatabase.get();
    if (user.getEndDate() != null && user.getEndDate().before(new Date())) {
      throw new UserExpiredException("User " + lowercaseLogin + " is Expired");
    }

    Locale locale =
        Locale.forLanguageTag(
            request != null && request.getHeader("lang") != null
                ? request.getHeader("lang")
                : "en");
    if (!user.getStatus()) {
      if (user.getOrganization() != null || user.getFoundation() != null) {
        String message =
            messageSource.getMessage(
                "error.account.disable.b2b", null, "account not active", locale);
        throw new UserDisabledException(message);
      }
      if (user.getCloudPackage() != null && user.getFirstLogin() == Boolean.FALSE) {
        String message =
            messageSource.getMessage(
                "error.account.disable.b2c", null, "account not active", locale);
        throw new UserDisabledException(message);
      }
      throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
    }
    Set<String> permissions = new HashSet<>();
    List<com.eshraqgroup.mint.domain.jpa.Permission> permissionList =
        permissionRepository.findAll();
    switch (user.getType()) {
      case SUPER_ADMIN:
        permissionList.forEach(permission -> permissions.add(permission.getName()));
        permissions.add(UserType.SUPER_ADMIN.getAuthority());
        permissions.add(UserType.SYSTEM_ADMIN.getAuthority());
        permissions.add(UserType.FOUNDATION_ADMIN.getAuthority());
        permissions.add(UserType.ADMIN.getAuthority());
        break;
      case SYSTEM_ADMIN:
        permissionRepository
            .findByTypeInAndDeletedFalse(
                Arrays.asList(
                    UserType.SYSTEM_ADMIN,
                    UserType.FOUNDATION_ADMIN,
                    UserType.ADMIN,
                    UserType.USER))
            .forEach(permission -> permissions.add(permission.getName()));
        permissions.add(UserType.SYSTEM_ADMIN.getAuthority());
        permissions.add(UserType.FOUNDATION_ADMIN.getAuthority());
        permissions.add(UserType.ADMIN.getAuthority());
        break;
      case USER:
        if (user.getCloudPackage() != null) {
          user.getCloudPackage()
              .getPermission()
              .forEach((s, o) -> permissions.addAll(get(permissionList, s, o)));
        } else {
          user.getRoles()
              .stream()
              .forEach(
                  role ->
                      role.getPermission()
                          .forEach((s, o) -> permissions.addAll(get(permissionList, s, o))));
        }
        break;
      case FOUNDATION_ADMIN:
        permissions.add(UserType.ADMIN.getAuthority());
        permissions.add(UserType.FOUNDATION_ADMIN.getAuthority());
        user.getRoles()
            .stream()
            .forEach(
                role ->
                    role.getPermission()
                        .forEach((s, o) -> permissions.addAll(get(permissionList, s, o))));
        break;
      case ADMIN:
        user.getRoles()
            .stream()
            .forEach(
                role ->
                    role.getPermission()
                        .forEach((s, o) -> permissions.addAll(get(permissionList, s, o))));
        permissions.add(UserType.ADMIN.getAuthority());
        break;
    }
    Long org = user.getOrganization() == null ? null : user.getOrganization().getId();
    Long found = user.getFoundation() == null ? null : user.getFoundation().getId();
    return new CurrentUserDetail(
        user.getId(),
        user.getUserName().toLowerCase(),
        user.getPassword(),
        AuthorityUtils.commaSeparatedStringToAuthorityList(String.join(",", permissions)),
        user.getFullName(),
        user.getThumbnail(),
        user.getEmail(),
        org,
        found,
        user.getType(),
        user.getChatId());
  }

  private Set<String> get(
      List<com.eshraqgroup.mint.domain.jpa.Permission> permissions, String name, byte equation) {
    return permissions
        .stream()
        .filter(
            permission ->
                permission.getKeyCode().equals(name)
                    && PermissionCheck.hasAction(equation, permission.getCode().byteValue()))
        .map(Permission::getName)
        .collect(Collectors.toSet());
  }
}
