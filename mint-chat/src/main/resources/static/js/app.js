var spaceId, token, user, direct, iframe;

$(document).ready(function () {
    window.sessionStorage.clear();
    user = decodeURIComponent(urlParam('user'));
    spaceId = decodeURIComponent(urlParam('space'));
    token = decodeURIComponent(urlParam('token'));

    iframe = document.getElementById('iframRocket');
    iframe.src=iframe.name;
    iframe.name="rocketchatIframe";

    iframe.onload = function () {
        if (user !== null && user !== undefined && user !== "") {
            redirect(user, spaceId, token);
        } else if(spaceId !== null && spaceId !== undefined && spaceId !== "") {
            redirectSpace(spaceId, token);
        }else{
            BackendLogin(token);
        }
    }
});

function redirectSpace(channel, token) {
    ajaxCall("/api/chat/redirect/" + channel + "/", "GET", token, redirectSuccess, failureCallback);
}

function redirect(user, channel, token) {
    ajaxCall("/api/chat/direct/" + user + "/" + channel, "GET", token, directSuccess, failureCallback);
}

function BackendLogin(token){
    ajaxCall("/api/chat/login/","GET",token,redirectToGeneral,failureCallback)
}

function redirectSuccess(response) {
    // logout();
    // if (response === null || response === undefined || response === "") {
    //     console.log(response);
    // } else {
    //     setTimeout(function () {
    //         login(response.authToken);
    //         goSpace(response.group);
    //     }, 1500);

    // }

    if (response.code===10) {
        setTimeout(function () {
            login(response.data.authToken);
            goSpace(response.data.group);
        }, 1500);
        
    } else {
        failureCallback(response);
    }
}
function directSuccess(response) {
    // logout();
    // if (response === null || response === undefined || response === "") {
    //     console.log(response);
    // } else {
    //     setTimeout(function () {
    //         login(response.authToken);
    //         goDirect(response.chatWithUser);
    //     }, 1500);
    // }

    if (response.code===10) {
        setTimeout(function () {
                login(response.data.authToken);
                goDirect(response.data.chatWithUser);
            }, 1500);
        } else {
            failureCallback(response);
        }

}

function redirectToGeneral(response){
    if(response.code===10){
        setTimeout(function () {
            login(response.data.authToken);
            goSpace(response.data.group);
        }, 1500);
    }else{
        failureCallback(response);
    }
}

function login(token) {
    document.getElementById('iframRocket').contentWindow.postMessage({
        externalCommand: 'login-with-token',
        token: token
    }, '*');
}

function goSpace(group) {
    // console.log(group);
    document.getElementById('iframRocket').contentWindow.postMessage({
        externalCommand: 'go',
        path: '/group/' + group + '?layout=embedded'
    }, '*');

}

function goDirect(user) {
    console.log(user);
    document.getElementById('iframRocket').contentWindow.postMessage({
        externalCommand: 'go',
        path: '/direct/' + user + '?layout=embedded'
   }, '*');
}

function logout() {
    document.getElementById('iframRocket').contentWindow.postMessage({
        externalCommand: 'logout'
    }, '*');
}


function urlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    return (results !== null) ? results[1] : "";
}

function ajaxCall(url, type, token, scb, fcb) {
    $.ajax({
        url: url,
        type: type,
        contentType: "application/json",
        accept: "application/json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", token);
        },
        success: scb,
        error: fcb
    });
}

function failureCallback(response) {
    window.sessionStorage.clear();
    console.log(response);
    document.getElementById('iframRocket').hidden="true";
}

// window.addEventListener('message',function(e) {
//     console.log(e); // event name
// });
