package com.eshraqgroup.mint.chat.model;

import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;

/** Created by ahmadsalah on 9/18/17. */
public class UserCreateModel {

  private String name;
  private String email;
  private String password;
  private String username;
  private boolean joinDefaultChannels = false;
  private boolean verified = true;

  public UserCreateModel() {}

  public UserCreateModel(UserInfoMessage infoMessage) {
    this.name = infoMessage.getName();
    this.username = infoMessage.getLogin().replace("@", "_");
    this.email = infoMessage.getEmail();
    this.joinDefaultChannels = true;
    this.verified = true;
  }

  public UserCreateModel(String name, String email, String password, String username) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.username = username.replace("@", "_");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username.replace("@", "_");
  }

  public boolean isJoinDefaultChannels() {
    return joinDefaultChannels;
  }

  public boolean isVerified() {
    return verified;
  }

  public void setJoinDefaultChannels(boolean joinDefaultChannels) {
    this.joinDefaultChannels = joinDefaultChannels;
  }

  public void setVerified(boolean verified) {
    this.verified = verified;
  }
}
