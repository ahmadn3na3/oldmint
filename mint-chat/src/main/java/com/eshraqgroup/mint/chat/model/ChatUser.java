package com.eshraqgroup.mint.chat.model;

import java.util.ArrayList;
import java.util.Date;

/** Created by ahmadsalah on 9/18/17. */
public class ChatUser {

  private Date createdAt;
  private String _id;
  private Services services;
  private String username;
  private ArrayList<Email> emails;
  private String type;
  private String status;
  private boolean active;
  private String name;
  private Date _updatedAt;
  private ArrayList<String> roles;

  public Date getCreatedAt() {
    return this.createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public String getId() {
    return this._id;
  }

  public void setId(String _id) {
    this._id = _id;
  }

  public Services getServices() {
    return this.services;
  }

  public void setServices(Services services) {
    this.services = services;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public ArrayList<Email> getEmails() {
    return this.emails;
  }

  public void setEmails(ArrayList<Email> emails) {
    this.emails = emails;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public boolean getActive() {
    return this.active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getUpdatedAt() {
    return this._updatedAt;
  }

  public void setUpdatedAt(Date _updatedAt) {
    this._updatedAt = _updatedAt;
  }

  public ArrayList<String> getRoles() {
    return this.roles;
  }

  public void setRoles(ArrayList<String> roles) {
    this.roles = roles;
  }

  public String get_id() {
    return _id;
  }

  public void set_id(String _id) {
    this._id = _id;
  }
}
