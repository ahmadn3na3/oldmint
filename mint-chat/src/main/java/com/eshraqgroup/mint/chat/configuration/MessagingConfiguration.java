package com.eshraqgroup.mint.chat.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.eshraqgroup.mint.constants.Services;

/** Created by ahmad on 5/30/17. */
@Configuration
public class MessagingConfiguration {
  @Autowired
  @Qualifier("messageBus")
  Exchange topicExchange;

  @Bean
  public Queue createQueue() {
    return new Queue(Services.CHAT.getQueue());
  }

  @Bean
  Binding createBinding(Queue queue) {
    return BindingBuilder.bind(queue)
        .to(topicExchange)
        .with(Services.CHAT.getRoutingKey())
        .noargs();
  }
}
