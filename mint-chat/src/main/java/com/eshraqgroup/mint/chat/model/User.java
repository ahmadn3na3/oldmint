package com.eshraqgroup.mint.chat.model;

/** Created by ahmadsalah on 9/17/17. */
public class User {

  private String _id;
  private String username;

  public User() {};

  public User(String username) {
    this.username = username;
  }

  public String getId() {
    return this._id;
  }

  public void setId(String _id) {
    this._id = _id;
  }

  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}
