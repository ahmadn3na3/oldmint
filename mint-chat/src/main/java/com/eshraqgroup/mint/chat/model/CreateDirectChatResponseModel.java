package com.eshraqgroup.mint.chat.model;

public class CreateDirectChatResponseModel {
  private Room room;
  private boolean success;

  public Room getRoom() {
    return room;
  }

  public void setRoom(Room room) {
    this.room = room;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }
}
