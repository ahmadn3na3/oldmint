package com.eshraqgroup.mint.chat.constants;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/** Created by ahmadsalah on 9/4/17. */
@ConfigurationProperties(prefix = "chat")
@Component
public class ChatProperties {

  private String domain;
  private String loginUri;
  private String createGroupUri;
  private String adminUserName;
  private String adminPassword;
  private String defaultUserPassword;
  private String groupInfoUri;
  private String createUserUri;
  private String getUserInfoUri;
  private String inviteUserToGroupUri;
  private String userGroupsList;
  private String imUri;

  public String getGroupInfoUri() {
    return groupInfoUri;
  }

  public void setGroupInfoUri(String groupInfoUri) {
    this.groupInfoUri = groupInfoUri;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public String getLoginUri() {
    return loginUri;
  }

  public void setLoginUri(String loginUri) {
    this.loginUri = loginUri;
  }

  public String getCreateGroupUri() {
    return createGroupUri;
  }

  public void setCreateGroupUri(String createGroupUri) {
    this.createGroupUri = createGroupUri;
  }

  public String getAdminUserName() {
    return adminUserName;
  }

  public void setAdminUserName(String adminUserName) {
    this.adminUserName = adminUserName;
  }

  public String getAdminPassword() {
    return adminPassword;
  }

  public void setAdminPassword(String adminPassword) {
    this.adminPassword = adminPassword;
  }

  public String getDefaultUserPassword() {
    return defaultUserPassword;
  }

  public void setDefaultUserPassword(String defaultUserPassword) {
    this.defaultUserPassword = defaultUserPassword;
  }

  public String getCreateUserUri() {
    return createUserUri;
  }

  public void setCreateUserUri(String createUserUri) {
    this.createUserUri = createUserUri;
  }

  public String getGetUserInfoUri() {
    return getUserInfoUri;
  }

  public void setGetUserInfoUri(String getUserInfoUri) {
    this.getUserInfoUri = getUserInfoUri;
  }

  public String getInviteUserToGroupUri() {
    return inviteUserToGroupUri;
  }

  public void setInviteUserToGroupUri(String inviteUserToGroupUri) {
    this.inviteUserToGroupUri = inviteUserToGroupUri;
  }

  public String getImUri() {
    return imUri;
  }

  public void setImUri(String imUri) {
    this.imUri = imUri;
  }

  public String getUserGroupsList() {
    return userGroupsList;
  }

  public void setUserGroupsList(String userGroupsList) {
    this.userGroupsList = userGroupsList;
  }
}
