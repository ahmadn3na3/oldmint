package com.eshraqgroup.mint.chat.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.chat.service.ChatAdminService;
import com.eshraqgroup.mint.models.ResponseModel;

/** Created by ahmadsalah on 9/10/17. */
@RestController
@RequestMapping("/api/chat")
public class ChatController {
  private final Logger log = LoggerFactory.getLogger(ChatController.class);

  @Autowired ChatAdminService chatAdminService;

  @RequestMapping(
    value = "/redirect/{spaceId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseModel redirect(@PathVariable("spaceId") Long spaceId) {
    return chatAdminService.redirect(spaceId);
  }

  @RequestMapping(
    value = "/direct/{userName}/{spaceId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseModel redirect(
      @PathVariable("userName") String userName, @PathVariable("spaceId") Long spaceId) {
    return chatAdminService.directChat(userName, spaceId);
  }

  @RequestMapping(
    value = "/directchat/{userName}/{spaceId}",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseModel createDirectChat(
      @PathVariable("userName") String userName, @PathVariable("spaceId") Long spaceId) {
    return chatAdminService.createDirectChat(userName, spaceId);
  }

  @RequestMapping(
    value = "/login",
    method = RequestMethod.GET,
    produces = MediaType.APPLICATION_JSON_VALUE
  )
  public ResponseModel login() {
    return chatAdminService.login();
  }
}
