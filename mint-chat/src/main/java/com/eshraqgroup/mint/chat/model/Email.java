package com.eshraqgroup.mint.chat.model;

/** Created by ahmadsalah on 9/18/17. */
public class Email {

  private String address;
  private boolean verified;

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public boolean getVerified() {
    return this.verified;
  }

  public void setVerified(boolean verified) {
    this.verified = verified;
  }
}
