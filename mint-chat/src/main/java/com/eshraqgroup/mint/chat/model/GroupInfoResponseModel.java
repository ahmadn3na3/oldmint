package com.eshraqgroup.mint.chat.model;

import org.codehaus.jackson.annotate.JsonProperty;

/** Created by ahmadsalah on 9/17/17. */
public class GroupInfoResponseModel {

  @JsonProperty("group")
  private Group group;

  private boolean success;

  public Group getGroup() {
    return group;
  }

  public void setGroup(Group group) {
    this.group = group;
  }

  public boolean getSuccess() {
    return this.success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }
}
