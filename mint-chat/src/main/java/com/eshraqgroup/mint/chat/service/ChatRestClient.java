package com.eshraqgroup.mint.chat.service;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import com.eshraqgroup.mint.chat.cache.AuthenticationCache;
import com.eshraqgroup.mint.chat.constants.ChatProperties;
import com.eshraqgroup.mint.chat.model.GroupCreateModel;
import com.eshraqgroup.mint.chat.model.InviteUserRequestModel;
import com.eshraqgroup.mint.chat.model.LoginRequestModel;
import com.eshraqgroup.mint.chat.model.LoginResponseModel;
import com.eshraqgroup.mint.chat.model.User;
import com.eshraqgroup.mint.chat.model.UserCreateModel;
import com.eshraqgroup.mint.util.JsonUtils;

@Component
public class ChatRestClient {
  @Autowired ChatProperties chatProperties;

  private final RestTemplate restTemplate;
  @Autowired JsonUtils jsonUtils;

  private static final Logger logger = LoggerFactory.getLogger(ChatRestClient.class);

  public ChatRestClient() {
    this.restTemplate = new RestTemplate();
  }

  public ResponseEntity login(String userName) {
    return login(new LoginRequestModel(userName, chatProperties.getDefaultUserPassword()));
  }

  public ResponseEntity login(LoginRequestModel loginRequestModel) {
    return post(prepareHeaders(), chatProperties.getLoginUri(), loginRequestModel);
  }

  public ResponseEntity getUserInfoByName(String userName) {
    return get(
        prepareHeadersWithAuth(), chatProperties.getGetUserInfoUri() + "?username=" + userName);
  }

  public ResponseEntity getUserInfoById(String Id) {
    return get(prepareHeadersWithAuth(), chatProperties.getGetUserInfoUri() + "?userId=" + Id);
  }

  public ResponseEntity getGroupInfo(String groupName) {
    return get(
        prepareHeadersWithAuth(), chatProperties.getGroupInfoUri() + "?roomName=" + groupName);
  }

  public ResponseEntity getGroupInfoById(String id) {
    return get(prepareHeadersWithAuth(), chatProperties.getGroupInfoUri() + "?roomId=" + id);
  }

  public ResponseEntity createPrivateGroup(GroupCreateModel groupCreateModel) {
    return post(prepareHeadersWithAuth(), chatProperties.getCreateGroupUri(), groupCreateModel);
  }

  public ResponseEntity createUser(UserCreateModel userCreateModel) {
    return post(prepareHeadersWithAuth(), chatProperties.getCreateUserUri(), userCreateModel);
  }

  public ResponseEntity addUserToGroup(InviteUserRequestModel inviteUserRequestModel) {
    return post(
        prepareHeadersWithAuth(), chatProperties.getInviteUserToGroupUri(), inviteUserRequestModel);
  }

  public ResponseEntity createDirectChat(User user) {
    return post(prepareHeadersWithAuth(), chatProperties.getImUri(), user);
  }

  private ResponseEntity<String> get(HttpHeaders headers, String uri) {
    return call(headers, uri, GET, null);
  }

  private ResponseEntity<String> post(String uri, Object payload) {
    return call(new HttpHeaders(), uri, POST, payload);
  }

  private ResponseEntity<String> post(HttpHeaders headers, String uri, Object payload) {
    return call(headers, uri, POST, payload);
  }

  private ResponseEntity<String> call(
      HttpHeaders httpHeaders, String uri, HttpMethod httpMethod, Object payload) {

    httpHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    HttpEntity httpRequest;
    if (payload != null) {
      httpRequest = new HttpEntity(payload, httpHeaders);
    } else {
      httpRequest = new HttpEntity(httpHeaders);
    }

    try {
      return restTemplate.exchange(
          "http:" + chatProperties.getDomain() + uri, httpMethod, httpRequest, String.class);
    } catch (HttpClientErrorException e) {
      logger.error(e.getMessage(), e);
      return new ResponseEntity<>(e.getResponseBodyAsString(), e.getStatusCode());
    }
  }

  private HttpHeaders prepareHeadersWithAuth() {
    HttpHeaders headers = prepareHeaders();
    if (AuthenticationCache.getInstance().getToken() == null) {
      adminLogin();
    }
    headers.add("X-Auth-Token", AuthenticationCache.getInstance().getToken());
    headers.add("X-User-Id", AuthenticationCache.getInstance().getUserId());
    return headers;
  }

  private HttpHeaders prepareHeaders() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    return headers;
  }

  private void adminLogin() {
    ResponseEntity<String> responseEntity =
        login(
            new LoginRequestModel(
                chatProperties.getAdminUserName(), chatProperties.getAdminPassword()));
    if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
      LoginResponseModel loginResponseModel =
          jsonUtils.mapJsonObject(responseEntity.getBody(), LoginResponseModel.class);
      AuthenticationCache authenticationCache = AuthenticationCache.getInstance();
      authenticationCache.setUserId(loginResponseModel.getData().getUserId());
      authenticationCache.setToken(loginResponseModel.getData().getAuthToken());
    } else if (responseEntity.getStatusCode().equals(HttpStatus.UNAUTHORIZED)) {
      logger.error(responseEntity.getBody(), responseEntity);
    }
  }
}
