package com.eshraqgroup.mint.chat.model;

import java.util.ArrayList;
import java.util.Date;
import org.codehaus.jackson.annotate.JsonProperty;

/** Created by ahmadsalah on 9/17/17. */
public class Group {

  private String _id;

  private boolean success;
  private Date ts;
  private String t;
  private String name;
  private ArrayList<String> usernames;
  private User user;
  private int msgs;
  private boolean isDefault;
  private Date _updatedAt;
  private Date lm;

  public String getId() {
    return this._id;
  }

  public void setId(String _id) {
    this._id = _id;
  }

  public Date getTs() {
    return this.ts;
  }

  public void setTs(Date ts) {
    this.ts = ts;
  }

  public String getT() {
    return this.t;
  }

  public void setT(String t) {
    this.t = t;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<String> getUsernames() {
    return this.usernames;
  }

  public void setUsernames(ArrayList<String> usernames) {
    this.usernames = usernames;
  }

  public User getU() {
    return this.user;
  }

  public void setU(User u) {
    this.user = u;
  }

  public int getMsgs() {
    return this.msgs;
  }

  public void setMsgs(int msgs) {
    this.msgs = msgs;
  }

  @JsonProperty("default")
  public boolean getDefault() {
    return this.isDefault;
  }

  @JsonProperty("default")
  public void setDefault(boolean isDefault) {
    this.isDefault = isDefault;
  }

  public Date getUpdatedAt() {
    return this._updatedAt;
  }

  public void setUpdatedAt(Date _updatedAt) {
    this._updatedAt = _updatedAt;
  }

  public Date getLm() {
    return this.lm;
  }

  public void setLm(Date lm) {
    this.lm = lm;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public String get_id() {
    return _id;
  }

  public void set_id(String _id) {
    this._id = _id;
  }
}
