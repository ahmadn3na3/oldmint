package com.eshraqgroup.mint.chat.model;

public class InviteUserResponseModel {

  private boolean success;
  private Group group;
  private User u;

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public Group getGroup() {
    return group;
  }

  public void setGroup(Group group) {
    this.group = group;
  }

  public User getU() {
    return u;
  }

  public void setU(User u) {
    this.u = u;
  }

  @Override
  public String toString() {
    return "InviteUserResponseModel [success=" + success + ", group=" + group + ", u=" + u + "]";
  }
}
