package com.eshraqgroup.mint.chat.model;

import org.codehaus.jackson.annotate.JsonProperty;

/** Created by ahmadsalah on 9/4/17. */
public class LoginResponseModel {

  private String status;

  @JsonProperty("data")
  private AuthenticationResponse data;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public AuthenticationResponse getData() {
    return data;
  }

  public void setData(AuthenticationResponse data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "LoginResponseModel{" + "status='" + status + '\'' + ", data=" + data + '}';
  }
}
