package com.eshraqgroup.mint.chat.model;

/** Created by ahmadsalah on 9/24/17. */
public class InviteUserRequestModel {
  private String userId;
  private String roomId;

  public InviteUserRequestModel() {}

  public InviteUserRequestModel(String userId, String roomId) {
    this.userId = userId;
    this.roomId = roomId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }
}
