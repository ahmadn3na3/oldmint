package com.eshraqgroup.mint.chat.model;

/** Created by ahmadsalah on 9/6/17. */
public class GroupCreateModel {

  private String name;

  public GroupCreateModel() {}

  public GroupCreateModel(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
