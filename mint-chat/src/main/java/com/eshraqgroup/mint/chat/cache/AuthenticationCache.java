package com.eshraqgroup.mint.chat.cache;

/** Created by ahmadsalah on 9/5/17. */
public class AuthenticationCache {

  private static AuthenticationCache instance = new AuthenticationCache();

  private String token;
  private String userId;

  private AuthenticationCache() {
    token = null;
    userId = null;
  }

  public static AuthenticationCache getInstance() {
    return instance;
  }

  public String getToken() {
    return token;
  }

  public synchronized void setToken(String token) {
    this.token = token;
  }

  public String getUserId() {
    return userId;
  }

  public synchronized void setUserId(String userId) {
    this.userId = userId;
  }
}
