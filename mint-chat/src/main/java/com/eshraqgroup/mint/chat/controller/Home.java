package com.eshraqgroup.mint.chat.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** Created by ahmadsalah on 9/11/17. */
@Controller
public class Home {
  @RequestMapping("/chat/home")
  public String getHomePage() {
    return "index";
  }
}
