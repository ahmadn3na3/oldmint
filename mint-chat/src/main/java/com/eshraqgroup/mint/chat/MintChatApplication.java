package com.eshraqgroup.mint.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = {"com.eshraqgroup.mint", "com.eshraqgroup.mint.chat"})
@EnableJpaRepositories(basePackages = {"com.eshraqgroup.mint.repos"})
@EntityScan(basePackages = {"com.eshraqgroup.mint.domain", "com.eshraqgroup.mint.chat.domain"})
@EnableMongoRepositories(basePackages = {"com.eshraqgroup.mint.repos"})
public class MintChatApplication {

  public static void main(String[] args) {
    SpringApplication.run(MintChatApplication.class, args);
  }
}
