package com.eshraqgroup.mint.chat.model;

import javax.validation.constraints.Null;
import org.codehaus.jackson.annotate.JsonProperty;

/** Created by ahmadsalah on 9/24/17. */
public class UserInfoResponseModel {
  @JsonProperty("user")
  @Null
  private ChatUser user;

  private boolean success;

  public UserInfoResponseModel() {}

  public UserInfoResponseModel(ChatUser user) {
    this.user = user;
    this.success = true;
  }

  public UserInfoResponseModel(boolean success) {
    this.success = success;
  }

  public ChatUser getUser() {
    return user;
  }

  public void setUser(ChatUser user) {
    this.user = user;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  @Override
  public String toString() {
    return "UserInfoResponseModel [user=" + user + ", success=" + success + "]";
  }
}
