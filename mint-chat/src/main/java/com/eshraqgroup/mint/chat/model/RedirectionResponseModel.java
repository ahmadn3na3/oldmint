package com.eshraqgroup.mint.chat.model;

public class RedirectionResponseModel {

  private String groupId;
  private String group;
  private String authToken;
  private String userId;
  private String chatWithUser;

  public RedirectionResponseModel() {}

  public RedirectionResponseModel(String group, String authToken, String userId, String roomId) {
    this.group = group;
    this.authToken = authToken;
    this.userId = userId;
    this.groupId = roomId;
  }

  public RedirectionResponseModel(
      String authToken, String userId, String chatWithUser, String group, String roomId) {
    this.authToken = authToken;
    this.chatWithUser = chatWithUser;
    this.userId = userId;
    this.group = group;
    this.groupId = roomId;
  }

  public String getGroup() {
    return group;
  }

  public void setGroup(String group) {
    this.group = group;
  }

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getChatWithUser() {
    return chatWithUser;
  }

  public void setChatWithUser(String chatWithUser) {
    this.chatWithUser = chatWithUser;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }
}
