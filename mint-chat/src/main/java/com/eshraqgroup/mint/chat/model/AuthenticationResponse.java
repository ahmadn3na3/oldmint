package com.eshraqgroup.mint.chat.model;

/** Created by ahmadsalah on 9/4/17. */
public class AuthenticationResponse {

  private String authToken;
  private String userId;

  public String getAuthToken() {
    return authToken;
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }
}
