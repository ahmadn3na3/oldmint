package com.eshraqgroup.mint.chat.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.eshraqgroup.mint.chat.model.CreateDirectChatResponseModel;
import com.eshraqgroup.mint.chat.model.GroupCreateModel;
import com.eshraqgroup.mint.chat.model.GroupInfoResponseModel;
import com.eshraqgroup.mint.chat.model.InviteUserRequestModel;
import com.eshraqgroup.mint.chat.model.InviteUserResponseModel;
import com.eshraqgroup.mint.chat.model.LoginRequestModel;
import com.eshraqgroup.mint.chat.model.LoginResponseModel;
import com.eshraqgroup.mint.chat.model.RedirectionResponseModel;
import com.eshraqgroup.mint.chat.model.User;
import com.eshraqgroup.mint.chat.model.UserCreateModel;
import com.eshraqgroup.mint.chat.model.UserInfoResponseModel;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.SpaceRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.CurrentUserDetail;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.JsonUtils;

/** Created by ahmadsalah on 8/29/17. */
@Component
public class ChatAdminService {

	private static final Logger logger = LoggerFactory.getLogger(ChatAdminService.class);
	@Autowired
	SpaceRepository spaceRepository;
	@Autowired
	JoinedRepository joinedRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	ChatRestClient chatRestClient;
	@Autowired
	JsonUtils jsonUtils;

	@Transactional
	@Deprecated
	public ResponseModel Redirect(Long spaceId) {
		CurrentUserDetail userDetails = SecurityUtils.getCurrentUser();
		String userName = SecurityUtils.getCurrentUserLogin();
		Space space = spaceRepository.findOneByIdAndDeletedFalse(spaceId).get();
		String spaceName;

		// Rocket chat admin login
		if (userDetails.getUsername().equals(chatRestClient.chatProperties.getAdminUserName())) {
			ResponseEntity<String> adminResponseEntity = chatRestClient
					.login(new LoginRequestModel(chatRestClient.chatProperties.getAdminUserName(),
							chatRestClient.chatProperties.getAdminPassword()));
			if (adminResponseEntity.getStatusCode().equals(HttpStatus.OK)) {
				LoginResponseModel loginResponseModel = jsonUtils.mapJsonObject(adminResponseEntity.getBody(),
						LoginResponseModel.class);
				return ResponseModel.done(new RedirectionResponseModel("general",
						loginResponseModel.getData().getAuthToken(), loginResponseModel.getData().getUserId(), null));
			}
		}

		if (space == null)
			throw new NotFoundException();
		else {
			spaceName = space.getName() + "_" + space.getUser().getUserName();
		}
		return getUserInfoByName(userName.replace("@", "_"))
				.map(userInfoResponseModel -> getGroupInfoByName(spaceName, spaceId)
						.map(groupInfoResponseModel -> joinedRepository
								.findOneBySpaceIdAndUserNameAndDeletedFalse(spaceId, userName).map(joined -> {
									ResponseEntity<String> invitationResponse = chatRestClient.addUserToGroup(
											new InviteUserRequestModel(userInfoResponseModel.getUser().getId(),
													groupInfoResponseModel.getGroup().get_id()));

									logger.info(invitationResponse.getBody());

									ResponseEntity<String> loginResponse = chatRestClient
											.login(userName.replace("@", "_"));
									if (loginResponse.getStatusCode().equals(HttpStatus.OK)) {
										LoginResponseModel loginResponseModel = jsonUtils
												.mapJsonObject(loginResponse.getBody(), LoginResponseModel.class);
										return ResponseModel.done(new RedirectionResponseModel(
												groupInfoResponseModel.getGroup().getName(),
												loginResponseModel.getData().getAuthToken(),
												loginResponseModel.getData().getUserId(),
												groupInfoResponseModel.getGroup().getId()));
									} else {
										logger.error(loginResponse.getBody(), loginResponse);
										return ResponseModel.error(Code.UNKNOWN, loginResponse.getBody());
									}
								}).orElse(ResponseModel.error(Code.NOT_FOUND, "JOINED")))
						.orElse(ResponseModel.error(Code.NOT_FOUND, "RC GROUP")))
				.orElse(ResponseModel.error(Code.NOT_FOUND, "RC USER"));
	}

	@Transactional
	public ResponseModel redirect(Long spaceId) {

		return joinedRepository
				.findOneByUserIdAndSpaceIdAndDeletedFalse(SecurityUtils.getCurrentUser().getId(), spaceId)
				.map(joined -> {
					String groupChatId, userChatId, spaceName, userName;
					com.eshraqgroup.mint.domain.jpa.User user = joined.getUser();
					Space space = joined.getSpace();
					userName = user.getUserName().replace("@", "_");
					
					if (userName.equals(chatRestClient.chatProperties.getAdminUserName())) {
						login(userName, chatRestClient.chatProperties.getAdminPassword()).map(loginResponse -> {
							return ResponseModel.done(new RedirectionResponseModel("general",
									loginResponse.getData().getAuthToken(), loginResponse.getData().getUserId(), null));
						});
					}

					if (space == null)
						throw new NotFoundException();
					else {
						spaceName = space.getName() + "_" + space.getUser().getUserName().replace("@", "_");
					}

					if (space.getChatRoomId() == null) {
						// create Chat Room and add all joined users on it
						groupChatId = createGroup(new GroupCreateModel(spaceName), space).map(groupResponse -> {
							return new String(groupResponse.getGroup().getId());
						}).orElse(null);
					} else {
						groupChatId = space.getChatRoomId();
					}
					if (user.getChatId() == null) {
						// create Chat User
						userChatId=createUser(
								new UserCreateModel(user.getFullName(), user.getEmail(),
										chatRestClient.chatProperties.getDefaultUserPassword(), user.getUserName()),user)
						.map(userInfoResponse -> {
									return new String(userInfoResponse.getUser().getId());
								}).orElse(null);
					} else {
						userChatId = user.getChatId();
					}

					if (groupChatId != null && userChatId != null) {
						return addUserToGroup(new InviteUserRequestModel(user.getChatId(), space.getChatRoomId()))
								.map(invitationResponse -> login(userName,
										chatRestClient.chatProperties.getDefaultUserPassword()).map(loginResponse -> {
											return ResponseModel.done(new RedirectionResponseModel(spaceName,
													loginResponse.getData().getAuthToken(),
													loginResponse.getData().getUserId(), space.getChatRoomId()));
										}).orElseThrow(NotPermittedException::new))
								.orElseThrow(NotPermittedException::new);
					} else {
						return ResponseModel.error(Code.UNKNOWN);
					}
				}).orElseThrow(NotPermittedException::new);
	}

	public ResponseModel directChat(String userName, Long spaceId) {
		boolean usersInSameSpace = joinedRepository
				.findOneBySpaceIdAndUserIdAndDeletedFalse(spaceId, SecurityUtils.getCurrentUser().getId())
				.map(joined -> userRepository.findOneByUserNameAndDeletedFalse(userName)
						.map(user -> joinedRepository.findOneBySpaceIdAndUserIdAndDeletedFalse(spaceId, user.getId())
								.map(joined1 -> true).orElse(false))
						.orElse(false))
				.orElse(false);
		if (usersInSameSpace) {
			ResponseEntity<String> loginResponse = chatRestClient.login(new LoginRequestModel(
					SecurityUtils.getCurrentUserLogin(), chatRestClient.chatProperties.getDefaultUserPassword()));
			if (loginResponse.getStatusCode().equals(HttpStatus.OK)) {
				LoginResponseModel loginResponseModel = jsonUtils.mapJsonObject(loginResponse.getBody(),
						LoginResponseModel.class);
				return getUserInfoByName(userName)
						.map(userInfoResponseModel -> ResponseModel.done(new RedirectionResponseModel(
								loginResponseModel.getData().getAuthToken(), loginResponseModel.getData().getUserId(),
								userInfoResponseModel.getUser().getUsername(), null,null)))
						.orElse(ResponseModel.error(Code.UNKNOWN, "RC USER INFO"));
			} else {
				return ResponseModel.error(Code.UNKNOWN, "RC LOGIN EROR");
			}
		} else {
			return ResponseModel.error(Code.UNKNOWN, "USERS ARE NOT IN SAME ROOM");
		}
	}

	public ResponseModel login() {
		String username = SecurityUtils.getCurrentUserLogin().replace("@", "_");
		return getUserInfoByName(username).map(userinfo -> {
			ResponseEntity<String> loginResponse = chatRestClient
					.login(new LoginRequestModel(username, chatRestClient.chatProperties.getDefaultUserPassword()));
			if (loginResponse.getStatusCode().equals(HttpStatus.OK)) {
				LoginResponseModel loginResponseModel = jsonUtils.mapJsonObject(loginResponse.getBody(),
						LoginResponseModel.class);
				return ResponseModel.done(loginResponseModel.getData());
			} else {
				return ResponseModel.error(Code.UNKNOWN, loginResponse);
			}
		}).orElse(ResponseModel.error(Code.NOT_FOUND, "user not found"));
	}

	public ResponseModel createDirectChat(String userName, Long spaceId) {
		return joinedRepository.findOneBySpaceIdAndUserNameAndDeletedFalse(spaceId, userName).map(joined1 -> {
			if (joined1.getUser().getChatId() == null) {
				createUser(new UserCreateModel(joined1.getUser().getFullName(), joined1.getUser().getEmail(),
						chatRestClient.chatProperties.getDefaultUserPassword(), joined1.getUser().getUserName()),
						joined1.getUser()).ifPresent(userInfoResponse -> {
							logger.info(userInfoResponse.toString());
						});
			}
			return joinedRepository
					.findOneBySpaceIdAndUserIdAndDeletedFalse(spaceId, SecurityUtils.getCurrentUser().getId())
					.map(joined -> {
						if (joined.getUser().getChatId() == null) {
							createUser(new UserCreateModel(joined.getUser().getFullName(), joined.getUser().getEmail(),
									chatRestClient.chatProperties.getDefaultUserPassword(),
									joined.getUser().getUserName()), joined.getUser()).ifPresent(userInfoResponse -> {
										logger.info(userInfoResponse.toString());
									});
						}

						return login(joined.getUser().getUserName().replace("@", "_"),
								chatRestClient.chatProperties.getDefaultUserPassword())
										.map(loginResponseModel -> createDirectChat(
												new User(joined1.getUser().getUserName().replace("@", "_")))
														.map(directChatResponse -> {
															return ResponseModel.done(new RedirectionResponseModel(
																	loginResponseModel.getData().getAuthToken(),
																	loginResponseModel.getData().getUserId(),
																	joined1.getUser().getUserName().replace("@", "_"),
																	null, directChatResponse.getRoom().get_id()));
														}).orElseThrow(MintException::new))
										.orElseThrow(NotPermittedException::new);
					}).orElseThrow(NotFoundException::new);
		}).orElseThrow(NotFoundException::new);
	}

	public Optional<LoginResponseModel> login(String username, String password) {
		LoginResponseModel loginResponseModel = null;
		ResponseEntity<String> loginResponse = chatRestClient.login(new LoginRequestModel(username, password));
		if (loginResponse.getStatusCode().equals(HttpStatus.OK)) {
			loginResponseModel = jsonUtils.mapJsonObject(loginResponse.getBody(), LoginResponseModel.class);
		} else {
			logger.error(loginResponse.getBody());
		}
		return Optional.ofNullable(loginResponseModel);
	}

	public Optional<GroupInfoResponseModel> getGroupInfoByName(String groupName, Long spaceId) {
		GroupInfoResponseModel groupInfoResponseModel = null;
		ResponseEntity<String> groupInfoResponse = chatRestClient.getGroupInfo(spaceId.toString());

		if (groupInfoResponse.getStatusCode().equals(HttpStatus.OK)) {
			groupInfoResponseModel = jsonUtils.mapJsonObject(groupInfoResponse.getBody(), GroupInfoResponseModel.class);
		} else if (groupInfoResponse.getStatusCode().equals(HttpStatus.BAD_REQUEST)
				&& groupInfoResponse.getBody().contains("error-room-not-found")) {
			// Create Groups
			ResponseEntity<String> groupCreateResponse = chatRestClient
					.createPrivateGroup(new GroupCreateModel(spaceId.toString()));
			if (groupCreateResponse.getStatusCode().equals(HttpStatus.OK)) {

				groupInfoResponseModel = jsonUtils.mapJsonObject(groupCreateResponse.getBody(),
						GroupInfoResponseModel.class);

				final String groupId = groupInfoResponseModel.getGroup().getId();

				joinedRepository.findBySpaceIdAndDeletedFalse(spaceId).forEach(joined -> {
					ResponseEntity<String> joinedCreationResponseEntity = chatRestClient.createUser(new UserCreateModel(
							joined.getUser().getFullName(), joined.getUser().getEmail(),
							chatRestClient.chatProperties.getDefaultUserPassword(), joined.getUser().getUserName()));
					if (joinedCreationResponseEntity.getStatusCode().equals(HttpStatus.OK)) {
						UserInfoResponseModel userInfoResponseModel = jsonUtils
								.mapJsonObject(joinedCreationResponseEntity.getBody(), UserInfoResponseModel.class);
						chatRestClient.addUserToGroup(
								new InviteUserRequestModel(userInfoResponseModel.getUser().getId(), groupId));
					}
				});
			} // TODO: Handle failure
		}
		return Optional.ofNullable(groupInfoResponseModel);
	}

	public Optional<UserInfoResponseModel> getUserInfoByName(String userName) {
		UserInfoResponseModel userInfoResponseModel = null;
		ResponseEntity<String> userInfoEntityُُResponse = chatRestClient.getUserInfoByName(userName);

		if (userInfoEntityُُResponse.getStatusCode().equals(HttpStatus.OK)) {
			userInfoResponseModel = jsonUtils.mapJsonObject(userInfoEntityُُResponse.getBody(),
					UserInfoResponseModel.class);
		}
		if (userInfoEntityُُResponse.getStatusCode().equals(HttpStatus.BAD_REQUEST)
				&& userInfoEntityُُResponse.getBody().contains("error-invalid-user")) {

			userInfoResponseModel = userRepository.findOneByUserNameAndDeletedFalse(userName.replace("_", "@"))
					.map(user -> {
						ResponseEntity<String> createUserResponse = chatRestClient
								.createUser(new UserCreateModel(user.getFullName(), user.getEmail(),
										chatRestClient.chatProperties.getDefaultUserPassword(), user.getUserName()));
						if (createUserResponse.getStatusCode().equals(HttpStatus.OK)) {
							return jsonUtils.mapJsonObject(createUserResponse.getBody(), UserInfoResponseModel.class);
						} else {
							return null;
						}
					}).orElseThrow(NotFoundException::new);
		}
		return Optional.ofNullable(userInfoResponseModel);
	}

	public Optional<UserInfoResponseModel> getUserInfoById(String id) {
		UserInfoResponseModel userInfoResponseModel = null;
		ResponseEntity<String> userInfoEntityُُResponse = chatRestClient.getUserInfoById(id);

		if (userInfoEntityُُResponse.getStatusCode().equals(HttpStatus.OK)) {
			userInfoResponseModel = jsonUtils.mapJsonObject(userInfoEntityُُResponse.getBody(),
					UserInfoResponseModel.class);
		}
		return Optional.ofNullable(userInfoResponseModel);
	}

	public Optional<GroupInfoResponseModel> getGroupInfoById(String groupId) {
		GroupInfoResponseModel groupInfoResponseModel = null;
		ResponseEntity<String> groupInfoResponse = chatRestClient.getGroupInfoById(groupId);

		if (groupInfoResponse.getStatusCode().equals(HttpStatus.OK)) {
			groupInfoResponseModel = jsonUtils.mapJsonObject(groupInfoResponse.getBody(), GroupInfoResponseModel.class);
		}
		return Optional.ofNullable(groupInfoResponseModel);
	}

	public Optional<UserInfoResponseModel> createUser(UserCreateModel createModel,
			com.eshraqgroup.mint.domain.jpa.User user) {
		UserInfoResponseModel userInfoResponseModel = null;
		ResponseEntity<String> createUserResponse = chatRestClient.createUser(createModel);
		if (createUserResponse.getStatusCode().equals(HttpStatus.OK)) {
			userInfoResponseModel = jsonUtils.mapJsonObject(createUserResponse.getBody(), UserInfoResponseModel.class);
			user.setChatId(userInfoResponseModel.getUser().getId());
			userRepository.saveAndFlush(user);
		} else if (createUserResponse.getBody().contains("already in use")){
			ResponseEntity<String> userInfoResponse=chatRestClient.getUserInfoByName(createModel.getUsername());
			if(userInfoResponse.getStatusCode().is2xxSuccessful()) {
				userInfoResponseModel = jsonUtils.mapJsonObject(createUserResponse.getBody(), UserInfoResponseModel.class);
				user.setChatId(userInfoResponseModel.getUser().getId());
				userRepository.saveAndFlush(user);
			}
		}else {
			logger.info(createUserResponse.getBody());
		}
		return Optional.ofNullable(userInfoResponseModel);
	}

	public Optional<GroupInfoResponseModel> createGroup(GroupCreateModel groupCreateModel, Space space) {
		GroupInfoResponseModel groupInfoResponseModel = null;
		ResponseEntity<String> groupCreateResponseEntity = chatRestClient.createPrivateGroup(groupCreateModel);
		if (groupCreateResponseEntity.getStatusCode() == HttpStatus.OK) {
			groupInfoResponseModel = jsonUtils.mapJsonObject(groupCreateResponseEntity.getBody(),
					GroupInfoResponseModel.class);
			space.setChatRoomId(groupInfoResponseModel.getGroup().getId());
			spaceRepository.saveAndFlush(space);
		} else if(groupCreateResponseEntity.getBody().contains("error-duplicate-channel-name")) {
			ResponseEntity<String> groupInfoResponse=chatRestClient.getGroupInfo(groupCreateModel.getName());
			if(groupInfoResponse.getStatusCode().is2xxSuccessful()) {
				groupInfoResponseModel = jsonUtils.mapJsonObject(groupCreateResponseEntity.getBody(),
						GroupInfoResponseModel.class);
			}
		}else {
			logger.error(groupCreateResponseEntity.getBody());
		}
		return Optional.ofNullable(groupInfoResponseModel);
	}

	public Optional<InviteUserResponseModel> addUserToGroup(InviteUserRequestModel inviteUserRequestModel) {
		InviteUserResponseModel inviteUserResponseModel = null;
		ResponseEntity<String> invitationResponse = chatRestClient.addUserToGroup(inviteUserRequestModel);
		if (invitationResponse.getStatusCode() == HttpStatus.OK) {
			inviteUserResponseModel = jsonUtils.mapJsonObject(invitationResponse.getBody(),
					InviteUserResponseModel.class);
		}
		return Optional.ofNullable(inviteUserResponseModel);
	}

	public Optional<CreateDirectChatResponseModel> createDirectChat(User user) {
		CreateDirectChatResponseModel createDirectChatResponseModel = null;

		ResponseEntity<String> directChatResponse = chatRestClient.createDirectChat(user);

		if (directChatResponse.getStatusCode().equals(HttpStatus.OK)) {
			createDirectChatResponseModel = jsonUtils.mapJsonObject(directChatResponse.getBody(),
					CreateDirectChatResponseModel.class);
		}

		return Optional.ofNullable(createDirectChatResponseModel);
	}
}
