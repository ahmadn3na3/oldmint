package com.eshraqgroup.mint.chat.model;

/** Created by ahmadsalah on 9/18/17. */
public class Services {
  private Password password;

  public Password getPassword() {
    return this.password;
  }

  public void setPassword(Password password) {
    this.password = password;
  }
}
