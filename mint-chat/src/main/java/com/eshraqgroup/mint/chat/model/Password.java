package com.eshraqgroup.mint.chat.model;

/** Created by ahmadsalah on 9/18/17. */
public class Password {

  private String bcrypt;

  public String getBcrypt() {
    return this.bcrypt;
  }

  public void setBcrypt(String bcrypt) {
    this.bcrypt = bcrypt;
  }
}
