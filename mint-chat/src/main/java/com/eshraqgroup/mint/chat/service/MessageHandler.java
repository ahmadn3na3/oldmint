package com.eshraqgroup.mint.chat.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.chat.model.GroupCreateModel;
import com.eshraqgroup.mint.chat.model.InviteUserRequestModel;
import com.eshraqgroup.mint.chat.model.UserCreateModel;
import com.eshraqgroup.mint.constants.JoinedStatus;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.space.SpaceInfoMessage;
import com.eshraqgroup.mint.models.messages.space.SpaceJoinMessage;
import com.eshraqgroup.mint.models.messages.space.SpaceShareInfoMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.repos.jpa.SpaceRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.util.JsonUtils;

/** Created by ahmad on 5/30/17. */
@Service
public class MessageHandler {

  private static final Logger logger = LoggerFactory.getLogger(MessageHandler.class);

  @Autowired ChatAdminService chatAdminService;

  @Autowired ChatRestClient chatRestClient;

  @Autowired UserRepository userRepository;

  @Autowired SpaceRepository spaceRepo;

  @Autowired JsonUtils jsonUtils;

  @RabbitListener(queues = "chatqueue")
  private void handle(@Payload BaseMessage message) {
    try {
      switch (message.getEntityAction()) {
        case SPACE_CREATE:
          onCreateSpace(message);
          break;
        case SPACE_JOIN:
          onJoinSpace(message);
          break;
        case USER_ACTIVATE:
          onUserActivate(message);
          break;
        case SPACE_DELETE:
          onSpaceDelete(message);
          break;
        case SPACE_SHARE:
          onSpaceShare(message);
          break;
        case SPACE_JOIN_ACCEPT:
          onSpaceJoinAccept(message);
          break;
        case SPACE_LEAVE:
          onSpaceLeave(message);
          break;
        case SPACE_UNSHARE:
          onSpaceUnshare(message);
          break;
        case SPACE_UPDATE:
          onSpaceUpdate(message);
          break;
        default:
          logger.info(message.toString());
          break;
      }

    } catch (RuntimeException e) {
      logger.error("error in handle Messsage", e);
    }
  }

  @Transactional
  public void onUserActivate(BaseMessage message) {
    logger.info(message.toString());
    UserInfoMessage userInfoMessage =
        jsonUtils.mapJsonObject(message.getDataModel(), UserInfoMessage.class);

    userRepository
        .findOneByUserNameAndDeletedFalse(userInfoMessage.getLogin())
        .ifPresent(
            user -> {
              UserCreateModel createModel = new UserCreateModel(userInfoMessage);
              chatAdminService
                  .createUser(createModel, user)
                  .ifPresent(
                      userInfo -> {
                        logger.info(userInfo.toString());
                      });
            });
  }

  @Transactional
  public void onSpaceShare(BaseMessage message) {
    logger.info(message.toString());
    SpaceShareInfoMessage spaceShareInfoMessage =
        jsonUtils.mapJsonObject(message.getDataModel(), SpaceShareInfoMessage.class);

    if (spaceShareInfoMessage.getChatId() == null) {
      Space space = spaceRepo.getOne(spaceShareInfoMessage.getId());
      if (space != null) {
        chatAdminService
            .createGroup(
                new GroupCreateModel(space.getName() + "_" + space.getUser().getUserName()), space)
            .ifPresent(
                groupResponse -> {
                  spaceShareInfoMessage.setChatId(groupResponse.getGroup().getId());
                });
      }
    }

    userRepository
        .findByIdInAndDeletedFalse(spaceShareInfoMessage.getUserIds())
        .forEach(
            user -> {
              if (user != null) {
                chatAdminService.createUser(
                    new UserCreateModel(
                        user.getFullName(),
                        user.getEmail(),
                        chatRestClient.chatProperties.getDefaultUserPassword(),
                        user.getUserName()),
                    user);
              }

              if (user.getChatId() != null && spaceShareInfoMessage.getChatId() != null) {
                chatAdminService.addUserToGroup(
                    new InviteUserRequestModel(
                        user.getChatId(), spaceShareInfoMessage.getChatId()));
              }
            });
  }

  // on join Space Add Joined users to space room
  @Transactional
  public void onSpaceJoinAccept(BaseMessage message) {
    logger.info(message.toString());
    onJoinSpace(message);
  }

  // on join Space Add Joined users to space room
  @Transactional
  public void onJoinSpace(BaseMessage message) {
    logger.info(message.toString());
    SpaceJoinMessage spaceJoinMessage =
        jsonUtils.mapJsonObject(message.getDataModel(), SpaceJoinMessage.class);
    if (spaceJoinMessage.getJoinedStatus() == JoinedStatus.JOINED) {

      if (spaceJoinMessage.getChatId() == null) {
        Space space = spaceRepo.getOne(spaceJoinMessage.getId());
        if (space != null) {
          chatAdminService
              .createGroup(
                  new GroupCreateModel(space.getName() + "_" + space.getUser().getUserName()),
                  space)
              .ifPresent(
                  groupInfoResponseModel -> {
                    spaceJoinMessage.setChatId(groupInfoResponseModel.getGroup().getId());
                  });
        }
      }

      if (spaceJoinMessage.getJoinedInfoMessage().getChatId() == null) {
        User user = userRepository.getOne(spaceJoinMessage.getJoinedInfoMessage().getId());
        if (user != null) {
          chatAdminService
              .createUser(
                  new UserCreateModel(
                      user.getFullName(),
                      user.getEmail(),
                      chatRestClient.chatProperties.getDefaultUserPassword(),
                      user.getUserName()),
                  user)
              .ifPresent(
                  userInfoResponseModel -> {
                    spaceJoinMessage
                        .getJoinedInfoMessage()
                        .setChatId(userInfoResponseModel.getUser().getId());
                    ;
                  });
        }
      }

      if (spaceJoinMessage.getChatId() != null
          && spaceJoinMessage.getJoinedInfoMessage().getChatId() != null) {
        InviteUserRequestModel inviteUserRequestModel =
            new InviteUserRequestModel(
                spaceJoinMessage.getJoinedInfoMessage().getChatId(), spaceJoinMessage.getChatId());

        chatAdminService
            .addUserToGroup(inviteUserRequestModel)
            .ifPresent(
                invitationResponse -> {
                  logger.info(invitationResponse.toString());
                });
      }
    }
  }

  /*
   * on create space : check if user exist on chat server , if not create one ,
   * then create room for that space and then add user to that space
   */
  @Transactional
  public void onCreateSpace(BaseMessage message) {
    SpaceInfoMessage spaceInfoMessage =
        jsonUtils.mapJsonObject(message.getDataModel(), SpaceInfoMessage.class);
    userRepository
        .findOneByIdAndDeletedFalse(spaceInfoMessage.getFrom().getId())
        .ifPresent(
            user -> {
              if (user.getChatId() == null) {
                UserCreateModel createModel =
                    new UserCreateModel(
                        user.getFullName(),
                        user.getEmail(),
                        chatRestClient.chatProperties.getDefaultUserPassword(),
                        user.getUserName());
                chatAdminService
                    .createUser(createModel, user)
                    .ifPresent(
                        userInfo -> {
                          logger.info(userInfo.toString());
                        });
              }
              spaceRepo
                  .findOneByIdAndDeletedFalse(spaceInfoMessage.getId())
                  .ifPresent(
                      space -> {
                        chatAdminService
                            .createGroup(
                                new GroupCreateModel(
                                    space.getName() + "_" + space.getUser().getUserName()),
                                space)
                            .ifPresent(
                                groupInfoResponseModel -> {
                                  logger.info(groupInfoResponseModel.toString());
                                });

                        if (user.getChatId() != null && space.getChatRoomId() != null) {
                          InviteUserRequestModel inviteUserRequestModel =
                              new InviteUserRequestModel(user.getChatId(), space.getChatRoomId());

                          chatAdminService
                              .addUserToGroup(inviteUserRequestModel)
                              .ifPresent(
                                  invitationResponse -> {
                                    logger.info(invitationResponse.toString());
                                  });
                        }
                      });
            });
  }

  public void onSpaceDelete(BaseMessage message) {
    logger.info(message.toString());
  }

  public void onSpaceLeave(BaseMessage message) {
    logger.info(message.toString());
  }

  public void onSpaceUnshare(BaseMessage message) {
    logger.info(message.toString());
  }

  public void onSpaceUpdate(BaseMessage message) {
    logger.info(message.toString());
  }
}
