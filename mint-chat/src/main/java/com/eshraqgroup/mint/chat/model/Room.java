package com.eshraqgroup.mint.chat.model;

public class Room {
  private String _id;
  private String t;
  private String msgs;
  private String ts;

  public Room() {}

  public String get_id() {
    return _id;
  }

  public void set_id(String _id) {
    this._id = _id;
  }

  public String getT() {
    return t;
  }

  public void setT(String t) {
    this.t = t;
  }

  public String getMsgs() {
    return msgs;
  }

  public void setMsgs(String msgs) {
    this.msgs = msgs;
  }

  public String getTs() {
    return ts;
  }

  public void setTs(String ts) {
    this.ts = ts;
  }
}
