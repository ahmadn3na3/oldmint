package com.eshraqgroup.mint.file;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import com.eshraqgroup.mint.file.converters.MintInteractiveContentConverter;
import com.eshraqgroup.mint.file.converters.VideoConverterService;
import com.eshraqgroup.mint.file.domain.Task;
import com.eshraqgroup.mint.file.repos.TaskRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MintFileManagementApplication.class)
@WebAppConfiguration
public class MintFileManagementApplicationTests {

  @Autowired VideoConverterService videoConverterService;

  @Autowired MintInteractiveContentConverter interactiveContentConverter;

  @Autowired TaskRepository taskRepository;

  @Value("${user.home}")
  String homeFolder;

  @Test
  @Ignore
  public void convertToMp4() throws IOException {
    Path path = Paths.get(homeFolder, "Documents", "SYDMENG07T01U01_VI02.mp4");
    Assume.assumeTrue(Files.isRegularFile(path));
    videoConverterService.convertToMp4(path, "outMp4");

    path = Paths.get(homeFolder, "Documents", "outMp4.mp4");
    Assume.assumeTrue(Files.isRegularFile(path));
  }

  @Test
  public void convertCycle() throws IOException, ExecutionException, InterruptedException {
    Task task = taskRepository.findOne("59478f5c4c9ea22e3d5e4c45");
    Assume.assumeNotNull(task);

    Path out = videoConverterService.extractAndConvert(task).get();
    Assume.assumeNotNull(out);
    Assume.assumeTrue(Files.exists(out));
  }

  @Test
  public void convertHtml()
      throws IOException, InterruptedException, URISyntaxException, ExecutionException {
    Task task = taskRepository.findOne("595baeeddc181155ce9596de");
    Assume.assumeNotNull(task);
    Path out = interactiveContentConverter.convertInteractiveContentDirectory(task).get();
    Assume.assumeNotNull(out);
    Assume.assumeTrue(Files.exists(Paths.get(out.toString(), "index.html")));
  }
}
