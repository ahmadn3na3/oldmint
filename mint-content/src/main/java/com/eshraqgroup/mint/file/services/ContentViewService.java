package com.eshraqgroup.mint.file.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.ContentStatus;
import com.eshraqgroup.mint.constants.TaskStatus;
import com.eshraqgroup.mint.constants.TaskType;
import com.eshraqgroup.mint.domain.jpa.Content;
import com.eshraqgroup.mint.domain.mongo.ContentUser;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotReadyException;
import com.eshraqgroup.mint.file.converters.VideoConverterService;
import com.eshraqgroup.mint.file.domain.Task;
import com.eshraqgroup.mint.file.models.ContentViewModel;
import com.eshraqgroup.mint.file.repos.TaskRepository;
import com.eshraqgroup.mint.file.util.Base64HexUtil;
import com.eshraqgroup.mint.file.util.FileUtil;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.models.TimeSpentModel;
import com.eshraqgroup.mint.repos.mongo.ContentUserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.nio.util.DirUtils;

@Service
public class ContentViewService {

  private final Logger log = LoggerFactory.getLogger(ContentViewService.class);
  private final ContentService contentService;
  private final TaskRepository taskRepository;
  private final FileUtil fileUtil;
  private final ContentUserRepository contentUserRepository;

  private final VideoConverterService videoConverterService;

  @Value("${mint.viewurl}")
  private String viewUrl;

  @Value("${mint.ffmpeg}")
  private String pathToffmpeg;

  @Value("${mint.ffprobe}")
  private String pathToffprobe;

  @Autowired
  public ContentViewService(
      ContentService contentService,
      TaskRepository taskRepository,
      FileUtil fileUtil,
      VideoConverterService videoConverterService,
      ContentUserRepository contentUserRepository) {
    super();
    this.contentService = contentService;
    this.taskRepository = taskRepository;
    this.fileUtil = fileUtil;
    this.videoConverterService = videoConverterService;
    this.contentUserRepository = contentUserRepository;
  }

  @Transactional
  public ResponseModel getContentServUrl(Long contentId, boolean isAndroid, boolean getOrginal)
      throws IOException {
    // TODO: Second level validation
    Optional.ofNullable(
            taskRepository.findOneByUserNameAndContentIdAndType(
                SecurityUtils.getCurrentUserLogin(), contentId, TaskType.VEIW))
        .ifPresent(this::finishTask);
    Content content = contentService.getContentInformation(contentId);
    if (content == null) {
      throw new NotFoundException();
    }
    if (content.getStatus() != ContentStatus.READY) {
      throw new MintException(Code.INVALID, "error.content.notready");
    }
    Task task =
        new Task(
            content,
            UUID.randomUUID().toString().replace("-", ""),
            UUID.randomUUID().toString().replace("-", ""));

    Path uploadPath = fileUtil.createFilePathFromTask(task);
    log.debug("upload {} is exist {}", uploadPath.toString(), Files.exists(uploadPath));
    log.debug("upload {} is regular {}", uploadPath.toString(), Files.isRegularFile(uploadPath));
    if (!Files.exists(uploadPath) || !Files.isRegularFile(uploadPath)) {
      task.setExt(task.getExt().toUpperCase());
      uploadPath = fileUtil.createFilePathFromTask(task);
      log.debug("upload {} is exist {}", uploadPath.toString(), !Files.exists(uploadPath));
      log.debug("upload {} is regular {}", uploadPath.toString(), !Files.isRegularFile(uploadPath));
      if (!Files.exists(uploadPath) || !Files.isRegularFile(uploadPath)) {
        throw new FileNotFoundException();
      }
    }
    Path viewFolder = fileUtil.createViewParentPathFromTask(task);
    if (!Files.exists(viewFolder) || !Files.isDirectory(viewFolder)) {
      Files.createDirectory(viewFolder);
    }
    ContentViewModel contentViewModel = new ContentViewModel();
    Path viewPath = null;
    switch (content.getType()) {
      case VIDEO:
        if (content.getStatus() != ContentStatus.READY) {
          throw new NotReadyException();
        }
        viewPath = Paths.get(viewFolder.toString(), "view");

        if (getOrginal && content.getAllowUseOrginal().booleanValue()) {
          Path orginalPath = Paths.get(uploadPath.getParent().toString(), "original");
          if (orginalPath.toFile().exists()) {
            DirUtils.copy(orginalPath, viewFolder);
            contentViewModel
                .getUrls()
                .add(
                    String.format(
                        "%s%s/%s.%s",
                        viewUrl, task.getViewFolderName(), task.getFileName(), "mp4"));
            contentViewModel.setSize(task.getContentLength());
          }
          break;
        }
        if (content.getKey() != null && content.getKeyId() != null) {
          contentViewModel.setKeyId(content.getKeyId().substring(0, 16));
          contentViewModel.setKey(content.getKey());
          contentViewModel.setEncodedKeyId(Base64HexUtil.hex2Base64(content.getKeyId()));
          contentViewModel.setEncodedKey(Base64HexUtil.hex2Base64(content.getKey()));
          if (!isAndroid) {
            DirUtils.copyWithPredicate(
                uploadPath.getParent(), viewFolder, path -> !path.endsWith("original"));
            contentViewModel
                .getUrls()
                .add(String.format("%s%s/%s", viewUrl, task.getViewFolderName(), "play.mint.mpd"));
          } else {
            viewPath = fileUtil.createViewFilePathFromTask(task);
            Files.copy(uploadPath, viewPath);
            contentViewModel
                .getUrls()
                .add(
                    String.format(
                        "%s%s/%s.%s",
                        viewUrl, task.getViewFolderName(), task.getViewFileName(), "mp4"));
            contentViewModel.setSize(Files.size(uploadPath));
          }
        } else if (Files.exists(Paths.get(viewPath.toString(), "master.m3u8")) && !isAndroid) {
          final Path uPath = uploadPath;
          DirUtils.copyWithPredicate(uploadPath, viewFolder, path -> !path.equals(uPath));
          contentViewModel
              .getUrls()
              .add(String.format("%s%s/%s", viewUrl, task.getViewFolderName(), "master.m3u8"));
        } else {
          if (!Files.exists(Paths.get(viewPath.toString(), task.getFileName() + ".mp4"))) {
            videoConverterService.convertToMp4(
                fileUtil.createFilePathFromTask(task), task.getFileName());
          }
          contentViewModel
              .getUrls()
              .add(
                  String.format(
                      "%s%s/%s/%s.%s",
                      viewUrl, task.getViewFolderName(), "view", task.getFileName(), "mp4"));
        }

        break;
      case INTERACTIVE:
        if (content.getStatus() != ContentStatus.READY && !isAndroid) {
          throw new NotReadyException();
        }

        if (isAndroid) {
          viewPath = fileUtil.createViewFilePathFromTask(task);
          if (Files.exists(viewPath)) {
            Files.delete(viewPath);
          }
          // Files.createFile(viewPath);
          Files.copy(uploadPath, viewPath);
          contentViewModel
              .getUrls()
              .add(
                  String.format(
                      "%s%s/%s.%s",
                      viewUrl, task.getViewFolderName(), task.getViewFileName(), task.getExt()));
          contentViewModel.setSize(Files.size(uploadPath));
        } else {
          DirUtils.copyWithPredicate(
              uploadPath.getParent(),
              viewFolder,
              path -> {
                return !path.toString().endsWith(".zip");
              });
          contentViewModel
              .getUrls()
              .add(String.format("%s%s/%s", viewUrl, task.getViewFolderName(), "index.html"));
        }

        break;
      default:
        viewPath = fileUtil.createViewFilePathFromTask(task);
        if (Files.exists(viewPath)) {
          Files.delete(viewPath);
        }
        Files.copy(uploadPath, viewPath);
        contentViewModel
            .getUrls()
            .add(
                String.format(
                    "%s%s/%s.%s",
                    viewUrl, task.getViewFolderName(), task.getViewFileName(), task.getExt()));
        contentViewModel.setSize(Files.size(viewPath));
        break;
    }
    log.debug("files url =>", contentViewModel.getUrls());
    taskRepository.save(task);
    contentViewModel.setViewTaskId(task.getId());
    return ResponseModel.done(contentViewModel);
  }

  @Transactional
  public ResponseModel finishView(String id) {
    return Optional.ofNullable(taskRepository.findOne(id))
        .map(
            task -> {
              finishTask(task);
              return ResponseModel.done();
            })
        .orElseThrow(NotFoundException::new);
  }

  @Scheduled(cron = "0 0 0 * * ?")
  public void deleteExpiredTasks() {
    taskRepository
        .findByTypeAndStatusAndExpiryDateAfter(TaskType.VEIW, TaskStatus.OPEN, new Date())
        .forEach(this::finishTask);
  }

  private void finishTask(Task task) {
    if (task != null) {
      Path path = fileUtil.createViewParentPathFromTask(task);
      fileUtil.deleteFile(path);
      taskRepository.delete(task);
      Long userId =
          SecurityUtils.getCurrentUser() == null ? null : SecurityUtils.getCurrentUser().getId();
      if (userId != null) {
        ContentUser contentUser =
            contentUserRepository
                .findByUserIdAndContentId(userId, task.getContentId())
                .map(
                    cu -> {
                      TimeSpentModel model = new TimeSpentModel();
                      model.setStartDateTime(task.getStartDate());
                      model.setEndTime(new Date());
                      model.setTimeSpent(
                          model.getEndTime().getTime() - model.getStartDateTime().getTime());
                      cu.getTimeSpent().add(model);
                      cu.setLastAccessDate(task.getStartDate());
                      cu.setViews(cu.getViews() + 1);
                      return cu;
                    })
                .orElseGet(
                    () -> {
                      log.debug("new contnet user {}", SecurityUtils.getCurrentUserLogin());
                      ContentUser cuContentUser = new ContentUser();
                      cuContentUser.setContentId(task.getContentId());
                      cuContentUser.setUserId(userId);
                      cuContentUser.setUserName(SecurityUtils.getCurrentUser().getUsername());
                      TimeSpentModel model = new TimeSpentModel();
                      model.setStartDateTime(task.getStartDate());
                      model.setEndTime(new Date());
                      model.setTimeSpent(
                          model.getEndTime().getTime() - model.getStartDateTime().getTime());
                      cuContentUser.getTimeSpent().add(model);
                      cuContentUser.setViews(1);
                      cuContentUser.setLastAccessDate(task.getStartDate());
                      return cuContentUser;
                    });
        contentUserRepository.save(contentUser);
      }
    }
  }
}
