package com.eshraqgroup.mint.file.controller.advices;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.models.ResponseModel;

/** Created by ahmad on 7/20/16. */
@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvice {
  @ExceptionHandler({IOException.class, FileNotFoundException.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public ResponseModel handelIOException(IOException ex) {
    return ResponseModel.error(Code.UNKNOWN, ex.toString());
  }
}
