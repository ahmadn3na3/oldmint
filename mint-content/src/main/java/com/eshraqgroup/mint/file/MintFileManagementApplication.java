package com.eshraqgroup.mint.file;

import java.util.TimeZone;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = {"com.eshraqgroup.mint", "com.eshraqgroup.mint.file"})
@EnableJpaRepositories(basePackages = {"com.eshraqgroup.mint.repos"})
@EntityScan(basePackages = {"com.eshraqgroup.mint.domain"})
@EnableMongoRepositories(
  basePackages = {"com.eshraqgroup.mint.repos", "com.eshraqgroup.mint.file.repos"}
)
public class MintFileManagementApplication {

  public static void main(String[] args) {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    SpringApplication.run(MintFileManagementApplication.class, args);
  }
}
