package com.eshraqgroup.mint.file.controller;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.ContentType;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.file.services.ContentBackGroundService;
import io.swagger.annotations.ApiOperation;

@RestController()
@RequestMapping("/contentConvert")
public class ContentCovertController {
  @Autowired ContentBackGroundService contentBackGroundService;

  private Map<ContentType, CompletableFuture<List<Path>>> futureMap = new ConcurrentHashMap<>();

  @GetMapping(path = "/{type}")
  @ApiOperation(
    value = "Content convert",
    notes = "this method is used to convert content to specific type"
  )
  public void contentConvert(@PathVariable(name = "type") ContentType contentType) {
    CompletableFuture<List<Path>> pathCompletableFuture = futureMap.get(contentType);
    if (pathCompletableFuture != null && !pathCompletableFuture.isDone()) {
      throw new MintException(Code.INVALID, "Not Available");
    }
    futureMap.put(contentType, contentBackGroundService.convertType(contentType));
  }

  @GetMapping(path = "/{type}/status")
  @ApiOperation(value = "check status", notes = "this method is used to check status of conversion")
  public String checkStatus(@PathVariable(name = "type") ContentType contentType)
      throws ExecutionException, InterruptedException {
    CompletableFuture<List<Path>> pathCompletableFuture = futureMap.get(contentType);
    if (pathCompletableFuture == null) {
      return "no running jobs for " + contentType.name();
    }
    if (pathCompletableFuture.isDone()) {
      String message = "done" + pathCompletableFuture.get().size();
      futureMap.remove(contentType);
      return message;
    }
    if (pathCompletableFuture.isCompletedExceptionally()) {
      futureMap.remove(contentType);
      return "error";
    }
    return "running";
  }
}
