package com.eshraqgroup.mint.file.services;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.constants.ContentStatus;
import com.eshraqgroup.mint.constants.ContentType;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.Content;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.repos.jpa.ContentRepository;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.PermissionCheck;

/** Created by ahmad on 7/18/16. */
@Service
public class ContentService {

  private final ContentRepository contentRepository;

  private final JoinedRepository joinedRepository;

  private final UserRepository userRepository;

  @Autowired
  public ContentService(
      ContentRepository contentRepository,
      JoinedRepository joinedRepository,
      UserRepository userRepository) {
    this.contentRepository = contentRepository;
    this.joinedRepository = joinedRepository;
    this.userRepository = userRepository;
  }

  @Transactional(readOnly = true)
  public Content getContentInformation(Long id) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Objects.requireNonNull(id);

              return Optional.ofNullable(contentRepository.findOne(id))
                  .map(
                      content -> {
                        if (user.getType() != UserType.USER) {
                          Long orgId =
                              content.getSpace().getCategory().getOrganization() == null
                                  ? null
                                  : content.getSpace().getCategory().getOrganization().getId();
                          Long foundId =
                              content.getSpace().getCategory().getFoundation() == null
                                  ? null
                                  : content.getSpace().getCategory().getFoundation().getId();
                          PermissionCheck.checkUserForFoundationAndOrgOperation(
                              user, orgId, foundId);
                          return content;
                        }
                        return joinedRepository
                            .findOneBySpaceIdAndUserIdAndDeletedFalse(
                                content.getSpace().getId(), user.getId())
                            .map(joined -> content)
                            .orElseThrow(NotPermittedException::new);
                      })
                  .orElseThrow(NotFoundException::new);
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  public void updateContentStatus(Long id, ContentStatus status) {
    updateContentStatus(id, status, null, null, null, null, null);
  }

  @Transactional(readOnly = true)
  public List<Content> getContentListByType(ContentType contentType) {
    return contentRepository.findByTypeAndDeletedFalseAndStatusIn(
        contentType, ContentStatus.READY, ContentStatus.UPLOADED);
  }

  @Transactional
  public void updateContentStatus(
      Long id,
      ContentStatus status,
      String key,
      String keyId,
      String ext,
      ContentType type,
      String originalPath) {
    Objects.requireNonNull(id);
    Objects.requireNonNull(status);

    contentRepository
        .findOneByIdAndDeletedFalse(id)
        .ifPresent(
            content -> {
              content.setStatus(status);
              if (key != null && keyId != null) {
                content.setKey(key);
                content.setKeyId(keyId);
              }
              if (ext != null && !content.getExt().equalsIgnoreCase(ext)) {
                content.setExt(ext);
              }
              if (type != null && content.getType() != type) {
                content.setType(type);
              }

              if (originalPath != null) {
                content.setOriginalPath(originalPath);
              }

              contentRepository.save(content);
            });
  }

  @Transactional
  public void deleteContent(Long id) {
    Objects.requireNonNull(id);
    contentRepository.delete(id);
  }
}
