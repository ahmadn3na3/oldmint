package com.eshraqgroup.mint.file.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

public class EncryptionKeysGenerator {

  @SuppressWarnings("resource")
  public static String getVideoKeyId(final File in) {

    try (FileInputStream is = new FileInputStream(in)) {
      final String hex = Hex.encodeHexString(DigestUtils.md5(is));
      return hex;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static String getVideoKey(final String keyId) {
    final String in = "Mint." + keyId + "@Eshraq";
    final String hex = Hex.encodeHexString(DigestUtils.md5(in));
    return hex;
  }

  public static String getVideoKey(final File in) {
    final String in5 = "Mint." + getVideoKeyId(in) + "@Eshraq";
    final String hex = Hex.encodeHexString(DigestUtils.md5(in5));
    return hex;
  }
}
