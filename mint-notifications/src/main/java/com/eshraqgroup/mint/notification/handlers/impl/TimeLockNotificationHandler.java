package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.UPDATE;
import static com.eshraqgroup.mint.constants.notification.EntityType.TIME_LOCK;
import static com.eshraqgroup.mint.constants.notification.MessageCategory.APP;
import java.time.ZonedDateTime;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.From;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
//TODO: TetFawar
@Component
public class TimeLockNotificationHandler extends AbstractHandler {

  public TimeLockNotificationHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper) {
    super(userRepository, amqNotifier, mailService, objectMapper);
    // TODO Auto-generated constructor stub
  }

  @Override
  protected void onUpdate(BaseMessage notificationMessage) {
    userRepository
        .findOneByUserNameAndDeletedFalse(notificationMessage.getUserName())
        .ifPresent(
            user -> {
              Long timelockId = new Long((Integer) notificationMessage.getEntityId());
              userRepository
                  .findByTimeLockIdAndDeletedFalse(timelockId)
                  .forEach(
                      user1 -> {
                        BaseNotificationMessage baseNotificationMessage =
                            new BaseNotificationMessage(
                                ZonedDateTime.now(),
                                APP,
                                new From(user.getId(), user.getUserName()),
                                new Target(TIME_LOCK, timelockId.toString(), UPDATE));
                        amqNotifier.send(amqNotifier.saveMessage(user1, baseNotificationMessage));
                      });
            });
  }
}
