package com.eshraqgroup.mint.notification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages = "com.eshraqgroup.mint")
@EntityScan(
  basePackages = {"com.eshraqgroup.mint.domain", "com.eshraqgroup.mint.notification.domain"}
)
@EnableJpaRepositories(basePackages = {"com.eshraqgroup.mint.repos"})
@EnableMongoRepositories(
  basePackages = {"com.eshraqgroup.mint.repos", "com.eshraqgroup.mint.notification.repo"}
)
public class MintBusApplication {
  public static void main(String[] args) {
    SpringApplication.run(MintBusApplication.class, args);
  }
}
