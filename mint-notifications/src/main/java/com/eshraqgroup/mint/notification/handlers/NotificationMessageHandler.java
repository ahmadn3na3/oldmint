package com.eshraqgroup.mint.notification.handlers;

import static com.eshraqgroup.mint.constants.notification.EntityType.ANNOTATIONS;
import static com.eshraqgroup.mint.constants.notification.EntityType.ANNOTATION_REPLY;
import static com.eshraqgroup.mint.constants.notification.EntityType.ASSESSMENTS;
import static com.eshraqgroup.mint.constants.notification.EntityType.CATEGORY;
import static com.eshraqgroup.mint.constants.notification.EntityType.CONTENT;
import static com.eshraqgroup.mint.constants.notification.EntityType.DISCUSSION;
import static com.eshraqgroup.mint.constants.notification.EntityType.DISCUSSION_REPLY;
import static com.eshraqgroup.mint.constants.notification.EntityType.PERMISSION;
import static com.eshraqgroup.mint.constants.notification.EntityType.QUESTION;
import static com.eshraqgroup.mint.constants.notification.EntityType.ROLE;
import static com.eshraqgroup.mint.constants.notification.EntityType.SPACE;
import static com.eshraqgroup.mint.constants.notification.EntityType.TIME_LOCK;
import static com.eshraqgroup.mint.constants.notification.EntityType.USER;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.notification.handlers.impl.AnnotationHandler;
import com.eshraqgroup.mint.notification.handlers.impl.AnnotationRelplyHandler;
import com.eshraqgroup.mint.notification.handlers.impl.AssessmentNotificationHandler;
import com.eshraqgroup.mint.notification.handlers.impl.CategoryNotificationHandler;
import com.eshraqgroup.mint.notification.handlers.impl.ContentHandler;
import com.eshraqgroup.mint.notification.handlers.impl.DiscussionHadler;
import com.eshraqgroup.mint.notification.handlers.impl.DiscussionReplyHandler;
import com.eshraqgroup.mint.notification.handlers.impl.PermissionHandler;
import com.eshraqgroup.mint.notification.handlers.impl.QuestionHandler;
import com.eshraqgroup.mint.notification.handlers.impl.RoleNotificationHandler;
import com.eshraqgroup.mint.notification.handlers.impl.SpaceNotificationHandler;
import com.eshraqgroup.mint.notification.handlers.impl.TimeLockNotificationHandler;
import com.eshraqgroup.mint.notification.handlers.impl.UserHandler;

/** Created by ayman on 01/03/17. */
@Component
@RabbitListener(queues = "notificationqueue")
public class NotificationMessageHandler {

  @Autowired SpaceNotificationHandler spaceNotificationHandler;

  @Autowired UserHandler userHandler;

  @Autowired AnnotationHandler annotationHandler;

  @Autowired AnnotationRelplyHandler annotationRelplyHandler;

  @Autowired DiscussionHadler discussionHadler;

  @Autowired PermissionHandler permissionHandler;

  @Autowired QuestionHandler questionHandler;

  @Autowired RoleNotificationHandler roleNotificationHandler;

  @Autowired TimeLockNotificationHandler timeLockNotificationHandler;

  @Autowired DiscussionReplyHandler discussionReplyHandler;

  @Autowired ContentHandler contentHandler;

  @Autowired CategoryNotificationHandler categoryNotificationHandler;

  @Autowired AssessmentNotificationHandler assessmentNotificationHandler;

  @RabbitHandler
  private void handle(@Payload BaseMessage notificationMessage) {
    int entityType = -1;
    if (null != notificationMessage) {
      entityType = notificationMessage.getEntityAction().getEntity();
    }
    switch (entityType) {
      case SPACE:
        spaceNotificationHandler.handle(notificationMessage);
        break;
      case USER:
        userHandler.handle(notificationMessage);
        break;
      case CONTENT:
        contentHandler.handle(notificationMessage);
        break;
      case ANNOTATIONS:
        annotationHandler.handle(notificationMessage);
        break;
      case DISCUSSION:
        discussionHadler.handle(notificationMessage);
        break;
      case ANNOTATION_REPLY:
        annotationRelplyHandler.handle(notificationMessage);
        break;
      case PERMISSION:
        permissionHandler.handle(notificationMessage);
        break;
      case QUESTION:
        questionHandler.handle(notificationMessage);
        break;
      case ROLE:
        roleNotificationHandler.handle(notificationMessage);
        break;
      case TIME_LOCK:
        timeLockNotificationHandler.handle(notificationMessage);
        break;
      case ASSESSMENTS:
        assessmentNotificationHandler.handle(notificationMessage);
        break;
      case DISCUSSION_REPLY:
        discussionReplyHandler.handle(notificationMessage);
        break;
      case CATEGORY:
        categoryNotificationHandler.handle(notificationMessage);
        break;
      default:
        throw new InvalidException();
    }
  }
}
