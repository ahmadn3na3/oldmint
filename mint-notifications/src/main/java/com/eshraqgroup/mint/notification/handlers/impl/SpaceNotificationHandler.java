package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.ACCEPTED;
import static com.eshraqgroup.mint.constants.notification.Actions.CREATE;
import static com.eshraqgroup.mint.constants.notification.Actions.DELETE;
import static com.eshraqgroup.mint.constants.notification.Actions.JOIN;
import static com.eshraqgroup.mint.constants.notification.Actions.RATE;
import static com.eshraqgroup.mint.constants.notification.Actions.SHARED;
import static com.eshraqgroup.mint.constants.notification.Actions.UPDATE;
import static com.eshraqgroup.mint.constants.notification.EntityType.SPACE;
import static com.eshraqgroup.mint.constants.notification.MessageCategory.USER;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.models.messages.space.SpaceInfoMessage;
import com.eshraqgroup.mint.models.messages.space.SpaceJoinMessage;
import com.eshraqgroup.mint.models.messages.space.SpaceShareInfoMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.notification.util.Utilities;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class SpaceNotificationHandler extends AbstractHandler {

  private final Utilities utilities;

  @Autowired
  public SpaceNotificationHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper,
      Utilities utilities) {
    super(userRepository, amqNotifier, mailService, objectMapper);
    this.utilities = utilities;
  }

  @Override
  protected void handleNonCRUDAction(BaseMessage notificationMessage) {
    switch (notificationMessage.getEntityAction().getAction()) {
      case RATE:
        onRate(notificationMessage);
        break;

      case SHARED:
        onShare(notificationMessage);
        break;
      case JOIN:
        onJoin(notificationMessage);
        break;
      case ACCEPTED:
        onAcceptJoin(notificationMessage);
        break;

      default:
        break;
    }
  }

  private void onAcceptJoin(BaseMessage baseMessage) {
    SpaceJoinMessage spaceJoinMessage = mapJsonObject(baseMessage, SpaceJoinMessage.class);
    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            spaceJoinMessage.getFrom(),
            new Target(
                SPACE, spaceJoinMessage.getId().toString(), JOIN, spaceJoinMessage.getImage()));
    User user = userRepository.findOne(spaceJoinMessage.getJoinedInfoMessage().getId());
    if (user != null) {
      UserInfoMessage userInfoMessage = new UserInfoMessage(user);
      NotificationMessage notificationMessage =
          amqNotifier.saveMessage(
              userInfoMessage,
              baseNotificationMessage,
              createMessage(baseMessage),
              spaceJoinMessage.getName(),
              spaceJoinMessage.getCategoryName());
      if (user.getNotification() && !user.isDeleted()) {
        amqNotifier.send(notificationMessage);
        if (user.hasMailNotification())
          mailService.sendNotificationMail(notificationMessage, userInfoMessage, true, false);
      }
    }
  }

  private void onJoin(BaseMessage baseMessage) {
    SpaceJoinMessage spaceJoinMessage = mapJsonObject(baseMessage, SpaceJoinMessage.class);
    String message = "mint.notification.message.space.joined";
    if (spaceJoinMessage.getIsPrivate() != null && spaceJoinMessage.getIsPrivate().booleanValue()) {
      message = "mint.notification.message.space.join";
    }
    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            spaceJoinMessage.getFrom(),
            new Target(
                SPACE, spaceJoinMessage.getId().toString(), JOIN, spaceJoinMessage.getImage()));

    User user = utilities.getSpaceOwner(spaceJoinMessage.getId());
    if (user != null) {
      UserInfoMessage userInfoMessage = new UserInfoMessage(user);
      NotificationMessage notificationMessage =
          amqNotifier.saveMessage(
              userInfoMessage,
              baseNotificationMessage,
              message,
              spaceJoinMessage.getName(),
              spaceJoinMessage.getCategoryName());
      if (user.getNotification() && !user.isDeleted()) {
        amqNotifier.send(notificationMessage);
        if (user.hasMailNotification())
          mailService.sendNotificationMail(notificationMessage, userInfoMessage, true, false);
      }
    }
  }

  @Override
  protected void onCreate(BaseMessage baseMessage) {
    SpaceInfoMessage spaceInfoMessage = mapJsonObject(baseMessage, SpaceInfoMessage.class);
    if (spaceInfoMessage.getIsPrivate().booleanValue()) {
      return;
    }

    List<User> followers = utilities.getFollowerList(spaceInfoMessage.getFrom().getId());

    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            spaceInfoMessage.getFrom(),
            new Target(
                SPACE, spaceInfoMessage.getId().toString(), CREATE, spaceInfoMessage.getImage()));

    if (null != followers && !followers.isEmpty()) {
      followers.forEach(
          user -> {
            UserInfoMessage userInfoMessage = new UserInfoMessage(user);
            NotificationMessage notificationMessage =
                amqNotifier.saveMessage(
                    userInfoMessage,
                    baseNotificationMessage,
                    createMessage(baseMessage),
                    spaceInfoMessage.getName(),
                    spaceInfoMessage.getCategoryName());
            if (user.getNotification() && !user.isDeleted()) {
              amqNotifier.send(notificationMessage);
              if (user.hasMailNotification())
                mailService.sendNotificationMail(notificationMessage, userInfoMessage, true, false);
            }
          });
    }
  }

  @Override
  protected void onUpdate(BaseMessage baseMessage) {
    SpaceInfoMessage spaceInfoMessage = mapJsonObject(baseMessage, SpaceInfoMessage.class);

    List<Joined> community =
        utilities.getCommunityUserList(
            spaceInfoMessage.getId(), spaceInfoMessage.getFrom().getId());

    if (null != community && !community.isEmpty()) {
      community
          .stream()
          .forEach(
              joined -> {
                UserInfoMessage userInfoMessage = new UserInfoMessage(joined.getUser());
                NotificationMessage notificationMessage =
                    amqNotifier.saveMessage(
                        userInfoMessage,
                        new BaseNotificationMessage(
                            ZonedDateTime.now(),
                            USER,
                            spaceInfoMessage.getFrom(),
                            new Target(
                                SPACE,
                                spaceInfoMessage.getId().toString(),
                                UPDATE,
                                spaceInfoMessage.getImage())),
                        createMessage(baseMessage),
                        spaceInfoMessage.getName(),
                        spaceInfoMessage.getCategoryName());

                if (joined.getNotification()
                    && joined.getUser().getNotification()
                    && !joined.getUser().isDeleted()) {
                  amqNotifier.send(notificationMessage);
                  if (joined.getUser().hasMailNotification())
                    mailService.sendNotificationMail(
                        notificationMessage, userInfoMessage, true, true);
                }
              });
    }
  }

  @Override
  protected void onDelete(BaseMessage baseMessage) {
    SpaceShareInfoMessage spaceInfoMessage =
        mapJsonObject(baseMessage, SpaceShareInfoMessage.class);
    if (spaceInfoMessage.getUserIds() != null && !spaceInfoMessage.getUserIds().isEmpty()) {
      List<User> community = userRepository.findAll(spaceInfoMessage.getUserIds());
      BaseNotificationMessage baseNotificationMessage =
          new BaseNotificationMessage(
              ZonedDateTime.now(),
              USER,
              spaceInfoMessage.getFrom(),
              new Target(SPACE, spaceInfoMessage.getId().toString(), DELETE));

      if (null != community && !community.isEmpty()) {
        community
            .stream()
            .forEach(
                user -> {
                  UserInfoMessage userInfoMessage = new UserInfoMessage(user);
                  NotificationMessage notificationMessage1 =
                      amqNotifier.saveMessage(
                          userInfoMessage,
                          baseNotificationMessage,
                          createMessage(baseMessage),
                          spaceInfoMessage.getName(),
                          spaceInfoMessage.getCategoryName());
                  if (user.getNotification() && !user.isDeleted()) {
                    amqNotifier.send(notificationMessage1);
                    if (user.hasMailNotification())
                      mailService.sendNotificationMail(
                          notificationMessage1, userInfoMessage, true, false);
                  }
                });
      }
    }
  }

  private void onRate(BaseMessage baseMessage) {
    SpaceInfoMessage spaceInfoMessage = mapJsonObject(baseMessage, SpaceInfoMessage.class);

    List<Joined> community =
        utilities.getCommunityUserList(
            spaceInfoMessage.getId(), spaceInfoMessage.getFrom().getId());
    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            spaceInfoMessage.getFrom(),
            new Target(
                SPACE, spaceInfoMessage.getId().toString(), UPDATE, spaceInfoMessage.getImage()));

    if (null != community && !community.isEmpty()) {
      community
          .stream()
          .filter(joined -> joined.getSpaceRole() == SpaceRole.OWNER)
          .forEach(
              joined -> {
                UserInfoMessage userInfoMessage = new UserInfoMessage(joined.getUser());
                NotificationMessage notificationMessage =
                    amqNotifier.saveMessage(
                        userInfoMessage,
                        baseNotificationMessage,
                        createMessage(baseMessage),
                        spaceInfoMessage.getName(),
                        spaceInfoMessage.getCategoryName());

                if (joined.getNotification()
                    && joined.getUser().getNotification()
                    && !joined.getUser().isDeleted()) {
                  amqNotifier.send(notificationMessage);
                  if (joined.getUser().hasMailNotification())
                    mailService.sendNotificationMail(
                        notificationMessage, userInfoMessage, true, true);
                }
              });
    }
  }

  private void onShare(BaseMessage baseMessage) {
    SpaceShareInfoMessage spaceShareInfoMessage =
        mapJsonObject(baseMessage, SpaceShareInfoMessage.class);

    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            spaceShareInfoMessage.getFrom(),
            new Target(SPACE, spaceShareInfoMessage.getId().toString(), SHARED));
    userRepository
        .findByIdInAndDeletedFalse(spaceShareInfoMessage.getUserIds())
        .forEach(
            user -> {
              UserInfoMessage infoMessage = new UserInfoMessage(user);
              NotificationMessage notificationMessage1 =
                  amqNotifier.saveMessage(
                      infoMessage,
                      baseNotificationMessage,
                      createMessage(baseMessage),
                      spaceShareInfoMessage.getName(),
                      spaceShareInfoMessage.getCategoryName());
              if (user.getNotification()) {
                amqNotifier.send(notificationMessage1);
                if (user.hasMailNotification())
                  mailService.sendNotificationMail(notificationMessage1, infoMessage, true, false);
              }
            });
  }
}
