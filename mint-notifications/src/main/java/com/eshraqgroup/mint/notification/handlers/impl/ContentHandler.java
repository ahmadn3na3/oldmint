package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.CREATE;
import static com.eshraqgroup.mint.constants.notification.Actions.UPDATE;
import static com.eshraqgroup.mint.constants.notification.EntityType.CONTENT;
import static com.eshraqgroup.mint.constants.notification.MessageCategory.USER;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.constants.ContentType;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.models.messages.content.ContentInfoMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.notification.util.Utilities;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class ContentHandler extends AbstractHandler {

  private final Utilities utilities;

  @Autowired
  public ContentHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper,
      Utilities utilities) {
    super(userRepository, amqNotifier, mailService, objectMapper);
    this.utilities = utilities;
  }

  @Override
  protected void onCreate(BaseMessage baseMessage) {

    notify(baseMessage, CREATE, true, true);
  }

  @Override
  protected void onUpdate(BaseMessage baseMessage) {
    notify(baseMessage, UPDATE, true, true);
  }

  @Override
  protected void onDelete(BaseMessage notificationMessage) {
    super.onDelete(notificationMessage);
  }

  private void notify(BaseMessage baseMessage, int action, boolean withImage, boolean useTagImage) {
    ContentInfoMessage contentInfoMessage = mapJsonObject(baseMessage, ContentInfoMessage.class);
    if (!contentInfoMessage.getContentType().equals(ContentType.WORKSHEET)) {
      List<Joined> joinedList =
          utilities.getCommunityUserList(
              contentInfoMessage.getSpaceId(), contentInfoMessage.getFrom().getId());
      joinedList.forEach(
          joined -> {
            UserInfoMessage userInfoMessage = new UserInfoMessage(joined.getUser());
            NotificationMessage notificationMessage =
                amqNotifier.saveMessage(
                    userInfoMessage,
                    new BaseNotificationMessage(
                        ZonedDateTime.now(),
                        USER,
                        contentInfoMessage.getFrom(),
                        new Target(CONTENT, contentInfoMessage.getId().toString(), action)),
                    createMessage(baseMessage),
                    contentInfoMessage.getName(),
                    contentInfoMessage.getSpaceName(),
                    contentInfoMessage.getCategoryName());

            if (joined.getNotification()
                && joined.getUser().getNotification()
                && !joined.getUser().isDeleted()) {
              amqNotifier.send(notificationMessage);
              if (joined.getUser().hasMailNotification())
                mailService.sendNotificationMail(
                    notificationMessage, userInfoMessage, withImage, useTagImage);
            }
          });
    }
  }
}
