package com.eshraqgroup.mint.notification.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.constants.notification.MessageCategory;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.notification.domian.Notification;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.eshraqgroup.mint.notification.repo.NotificationRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;

/** Created by ayman on 15/08/17. */
@Service
public class NotificationService {

  @Autowired NotificationRepository notificationRepository;

  @Autowired UserRepository userRepository;

  @Transactional
  public ResponseModel acknowledge(String[] id) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              notificationRepository
                  .findByIdInAndUserIdAndReceivedFalseAndDeletedFalse(
                      Arrays.asList(id), user.getId())
                  .forEach(
                      notification -> {
                        notification.setReceived(true);
                        notificationRepository.save(notification);
                      });
              return ResponseModel.done();
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  public ResponseModel getMyNotifications() {
    List<Notification> notificationList = new ArrayList<>();
    List<NotificationMessage> notificationMessages =
        notificationRepository
            .findByUserIdAndReceivedFalseAndDeletedFalse(SecurityUtils.getCurrentUser().getId())
            .map(
                notification -> {
                  notification.setReceived(true);
                  notificationList.add(notification);
                  NotificationMessage notificationMessage = new NotificationMessage(notification);
                  return notificationMessage;
                })
            .collect(Collectors.toList());
    if (!notificationList.isEmpty()) {
      notificationRepository.save(notificationList);
    }
    return ResponseModel.done(notificationMessages);
  }

  @Transactional
  public PageResponseModel viewAll(PageRequest pageRequestModel) {

    Page<Notification> notificationPage =
        notificationRepository.findByUserIdAndDeletedFalseAndNotificationCategoryEquals(
            SecurityUtils.getCurrentUser().getId(), pageRequestModel, MessageCategory.USER);

    return PageResponseModel.done(
        notificationPage
            .getContent()
            .stream()
            .map(NotificationMessage::new)
            .collect(Collectors.toList()),
        notificationPage.getTotalPages(),
        pageRequestModel.getPageNumber(),
        notificationPage.getTotalElements());
  }

  @Transactional
  public ResponseModel seen(String[] id) {
    return ResponseModel.done(
        notificationRepository
            .findByIdInAndUserIdAndDeletedFalse(
                Arrays.asList(id), SecurityUtils.getCurrentUser().getId())
            .map(
                notification -> {
                  notification.setSeen(true);
                  notificationRepository.save(notification);
                  return notification.getId();
                })
            .collect(Collectors.toList()));
  }
}
