package com.eshraqgroup.mint.notification.handlers;

import static com.eshraqgroup.mint.constants.notification.Actions.CREATE;
import static com.eshraqgroup.mint.constants.notification.Actions.DELETE;
import static com.eshraqgroup.mint.constants.notification.Actions.UPDATE;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 01/03/17. */
public abstract class AbstractHandler {

  protected static final String MESSAGE_PREFIX = "mint.notification.message";
  protected static Logger logger = LoggerFactory.getLogger(AbstractHandler.class);
  protected final UserRepository userRepository;

  protected final AmqNotifier amqNotifier;

  protected final MailService mailService;

  protected final ObjectMapper objectMapper;

  public AbstractHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper) {
    super();
    this.userRepository = userRepository;
    this.amqNotifier = amqNotifier;
    this.mailService = mailService;
    this.objectMapper = objectMapper;
  }

  @Transactional
  public void handle(BaseMessage notificationMessage) {
    logger.debug("message recived");
    try {
      switch (notificationMessage.getEntityAction().getAction()) {
        case UPDATE:
          onUpdate(notificationMessage);
          break;
        case CREATE:
          onCreate(notificationMessage);
          break;
        case DELETE:
          onDelete(notificationMessage);
          break;
        default:
          handleNonCRUDAction(notificationMessage);
      }
    } catch (Exception e) {
      logger.error("error in handle", e);
    }
  }

  protected void handleNonCRUDAction(BaseMessage notificationMessage) {}

  protected void onCreate(BaseMessage notificationMessage) {}

  protected void onUpdate(BaseMessage notificationMessage) {}

  protected void onDelete(BaseMessage notificationMessage) {}

  protected <T> T mapJsonObject(BaseMessage notificationMessage, Class<T> objectClass) {
    try {
      return this.objectMapper.readValue(notificationMessage.getDataModel(), objectClass);
    } catch (IOException e) {
      logger.error("error in parse", e);
      throw new MintException(e, Code.UNKNOWN, "parse error in messagedata");
    }
  }

  protected String createMessage(BaseMessage baseMessage) {
    return String.format("%s.%s", MESSAGE_PREFIX, baseMessage.getEntityAction().getMessageSuffix());
  }
}
