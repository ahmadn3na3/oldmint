package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.FOLLOW;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.constants.notification.MessageCategory;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.From;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.models.messages.UserFollowMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class UserHandler extends AbstractHandler {
  private static Logger logger = LoggerFactory.getLogger(UserHandler.class);

  @Autowired
  public UserHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper) {
    super(userRepository, amqNotifier, mailService, objectMapper);
  }

  @Override
  protected void handleNonCRUDAction(BaseMessage notificationMessage) {
    switch (notificationMessage.getEntityAction().getAction()) {
      case FOLLOW:
        onFollow(notificationMessage);
        break;
      default:
        break;
    }
  }

  private void onFollow(BaseMessage message) {
    UserFollowMessage userFollowMessage = mapJsonObject(message, UserFollowMessage.class);
    // TODO: sunday
    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            MessageCategory.USER,
            new From(userFollowMessage.getFollowerInfoMessage()),
            new Target(
                message.getEntityAction().getEntity(),
                userFollowMessage.getFollowerInfoMessage().getId().toString(),
                message.getEntityAction().getAction(),
                userFollowMessage.getFollowerInfoMessage().getImage()));

    NotificationMessage notificationMessage =
        amqNotifier.saveMessage(
            userFollowMessage.getUserInfoMessage(),
            baseNotificationMessage,
            createMessage(message));
    Optional.ofNullable(userRepository.findOne(userFollowMessage.getUserInfoMessage().getId()))
        .ifPresent(
            user -> {
              if (user.getNotification()) {
                if (user.hasMailNotification())
                  mailService.sendNotificationMail(
                      notificationMessage, userFollowMessage.getUserInfoMessage(), true, true);
              }
            });
  }

  @Override
  protected void onCreate(BaseMessage notificationMessage) {
    UserInfoMessage userInfoMessage = mapJsonObject(notificationMessage, UserInfoMessage.class);
    switch (notificationMessage.getEntityAction()) {
      case USER_REGISTER:
        mailService.sendActivationEmail(userInfoMessage);
        break;
      case USER_CREATE:
        mailService.sendCreationEmail(userInfoMessage);
        break;

      default:
        break;
    }
  }

  @Override
  protected void onUpdate(BaseMessage notificationMessage) {
    UserInfoMessage userInfoMessage = mapJsonObject(notificationMessage, UserInfoMessage.class);
    switch (notificationMessage.getEntityAction()) {
      case USER_REACTIVATE:
        mailService.sendActivationEmail(userInfoMessage);
        break;
      case USER_FORGETPASSOWORD:
        mailService.sendPasswordResetMail(userInfoMessage);
        break;

      default:
        break;
    }
  }
}
