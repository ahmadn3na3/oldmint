package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.CREATE;
import static com.eshraqgroup.mint.constants.notification.EntityType.DISCUSSION;
import static com.eshraqgroup.mint.constants.notification.MessageCategory.USER;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.models.messages.discussion.DiscussionMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.notification.util.Utilities;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class DiscussionHadler extends AbstractHandler {
  private final Utilities utilities;

  @Autowired
  public DiscussionHadler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper,
      Utilities utilities) {
    super(userRepository, amqNotifier, mailService, objectMapper);
    // TODO Auto-generated constructor stub
    this.utilities = utilities;
  }

  @Override
  protected void onCreate(BaseMessage baseMessage) {
    DiscussionMessage discussionMessage = mapJsonObject(baseMessage, DiscussionMessage.class);
    logger.debug("message recived in descussion  message {} ", baseMessage.getDataModel());
    List<Joined> joinedList =
        utilities.getCommunityUserList(
            discussionMessage.getSpaceId(), discussionMessage.getFrom().getId());
    logger.debug("joined list {}", joinedList.size());
    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            discussionMessage.getFrom(),
            new Target(DISCUSSION, discussionMessage.getId(), CREATE));
    joinedList
        .stream()
        .filter(
            joined ->
                joined.getSpaceRole() != SpaceRole.VIEWER
                    && !joined.getUser().getId().equals(discussionMessage.getFrom().getId()))
        .forEach(
            joined -> {
              UserInfoMessage userInfoMessage = new UserInfoMessage(joined.getUser());
              logger.trace("save notification {}", userInfoMessage.getLogin());
              NotificationMessage notificationMessage =
                  amqNotifier.saveMessage(
                      userInfoMessage,
                      baseNotificationMessage,
                      createMessage(baseMessage),
                      discussionMessage.getTitle(),
                      discussionMessage.getSpaceName(),
                      discussionMessage.getCategoryName());

              if (joined.getNotification()
                  && joined.getUser().getNotification()
                  && !joined.getUser().isDeleted()) {
                logger.trace("sending notification allowed {} ", userInfoMessage.getLogin());
                amqNotifier.send(notificationMessage);
                if (joined.getUser().hasMailNotification())
                  mailService.sendNotificationMail(
                      notificationMessage, userInfoMessage, true, false);
              }
            });
  }
}
