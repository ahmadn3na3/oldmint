package com.eshraqgroup.mint.notification.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.eshraqgroup.mint.controller.abstractcontroller.AbstractController;
import com.eshraqgroup.mint.models.PageRequestModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.notification.service.NotificationService;
import io.swagger.annotations.ApiOperation;

/** Created by ayman on 15/08/17. */
@RestController
@RequestMapping("/api/notification")
public class NotificationController extends AbstractController {

  @Autowired NotificationService notificationService;

  @RequestMapping(method = RequestMethod.POST)
  @ApiOperation(
    value = "Acknowledge",
    notes = "this method is used to acknowledge back end that notification is recived by its id"
  )
  public ResponseModel acknowledge(@RequestParam String[] id) {
    return notificationService.acknowledge(id);
  }

  @RequestMapping(method = RequestMethod.GET)
  @ApiOperation(
    value = "Get Notifications",
    notes = "this method is used to list  unrecived user notifications"
  )
  public ResponseModel getMyNotifications() {
    return notificationService.getMyNotifications();
  }

  @RequestMapping(method = RequestMethod.GET, path = "/view")
  @ApiOperation(
    value = "View Notifications",
    notes = "this method is used to list  all user notifications"
  )
  public ResponseModel viewNotifications(
      @RequestHeader(required = false) Integer page,
      @RequestHeader(required = false) Integer size) {
    return notificationService.viewAll(PageRequestModel.getPageRequestModel(page, size));
  }

  @RequestMapping(method = RequestMethod.POST, path = "/seen")
  @ApiOperation(value = "Seen", notes = "user  see notifications")
  public ResponseModel seen(@RequestParam String[] id) {
    return notificationService.seen(id);
  }
}
