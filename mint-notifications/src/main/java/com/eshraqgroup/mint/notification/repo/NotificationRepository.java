package com.eshraqgroup.mint.notification.repo;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.notification.domian.Notification;

/** Created by ayman on 02/03/17. */
@Repository
public interface NotificationRepository extends MongoRepository<Notification, String> {
  Stream<Notification> findByUserIdAndReceivedFalseAndDeletedFalse(Long userid);

  Page<Notification> findByUserIdAndDeletedFalseAndNotificationCategoryEquals(
      Long userid, Pageable pageable, int i);

  Stream<Notification> findByIdInAndUserIdAndReceivedFalseAndDeletedFalse(
      List<String> id, Long userId);

  Stream<Notification> findByIdInAndUserIdAndDeletedFalse(List<String> id, Long userId);

  Optional<Long> countByUserIdAndReceivedFalseAndDeletedFalse(Long userId);
}
