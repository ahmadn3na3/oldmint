package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.CREATE;
import static com.eshraqgroup.mint.constants.notification.EntityType.ASSESSMENTS;
import static com.eshraqgroup.mint.constants.notification.MessageCategory.USER;
import java.time.ZonedDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.constants.notification.Actions;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.models.messages.assessment.AssessementsInfoMessage;
import com.eshraqgroup.mint.models.messages.assessment.AssessmentSubmitMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.notification.util.Utilities;
import com.eshraqgroup.mint.repos.jpa.AssessmentRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class AssessmentNotificationHandler extends AbstractHandler {

  private final AssessmentRepository assessmentRepository;

  private final Utilities utilities;

  @Autowired
  public AssessmentNotificationHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper,
      AssessmentRepository assessmentRepository,
      Utilities utilities) {
    super(userRepository, amqNotifier, mailService, objectMapper);
    this.assessmentRepository = assessmentRepository;
    this.utilities = utilities;
  }

  @Override
  protected void handleNonCRUDAction(BaseMessage notificationMessage) {
    switch (notificationMessage.getEntityAction().getAction()) {
      case Actions.SUBMITTED:
        onSubmit(notificationMessage);
        break;

      default:
        break;
    }
  }

  private void onSubmit(BaseMessage baseMessage) {
    AssessmentSubmitMessage assessmentSubmitMessage =
        mapJsonObject(baseMessage, AssessmentSubmitMessage.class);
    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            assessmentSubmitMessage.getFrom(),
            new Target(ASSESSMENTS, assessmentSubmitMessage.getId().toString(), Actions.SUBMITTED));
    final String message;
    switch (assessmentSubmitMessage.getAssessmentType()) {
      case QUIZ:
        message =
            assessmentSubmitMessage.getIsOwner().booleanValue()
                ? "mint.notification.message.assessment.quiz.graded"
                : "mint.notification.message.assessment.quiz.solved";
        break;
      case ASSIGNMENT:
        message =
            assessmentSubmitMessage.getIsOwner().booleanValue()
                ? "mint.notification.message.assessment.assignment.graded"
                : "mint.notification.message.assessment.assignment.solved";
        break;
      case PRACTICE:
        return;
      case WORKSHEET:
        message =
            assessmentSubmitMessage.getIsOwner().booleanValue()
                ? "mint.notification.message.assessment.worksheet.graded"
                : "mint.notification.message.assessment.worksheet.solved";
        break;
      default:
        message = null;
        break;
    }

    UserInfoMessage userInfoMessage =
        !assessmentSubmitMessage.getIsOwner().booleanValue()
            ? assessmentSubmitMessage.getOwner()
            : assessmentSubmitMessage.getUserSolved();
    Joined joined =
        utilities.getJoinedUser(userInfoMessage.getId(), assessmentSubmitMessage.getSpaceId());
    NotificationMessage notificationMessage =
        amqNotifier.saveMessage(
            userInfoMessage,
            baseNotificationMessage,
            message,
            assessmentSubmitMessage.getName(),
            assessmentSubmitMessage.getSpaceName(),
            assessmentSubmitMessage.getCategoryName());

    if (joined.getNotification() && joined.getUser().getNotification()) {
      amqNotifier.send(notificationMessage);
      if (joined.getUser().hasMailNotification()) {
        mailService.sendNotificationMail(notificationMessage, userInfoMessage, true, false);
      }
    }
  }

  @Override
  protected void onCreate(BaseMessage baseMessage) {
    AssessementsInfoMessage assessementsInfoMessage =
        mapJsonObject(baseMessage, AssessementsInfoMessage.class);

    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            USER,
            assessementsInfoMessage.getFrom(),
            new Target(ASSESSMENTS, assessementsInfoMessage.getId().toString(), CREATE));
    final String message;
    switch (assessementsInfoMessage.getAssessmentType()) {
      case QUIZ:
        message = "mint.notification.message.assessment.quiz.create";
        break;
      case ASSIGNMENT:
        message = "mint.notification.message.assessment.assignment.create";
        break;
      case PRACTICE:
        return;
      case WORKSHEET:
        message = "mint.notification.message.assessment.worksheet.create";
        break;
      default:
        message = null;
        break;
    }

    utilities
        .getCommunityUserList(
            assessementsInfoMessage.getSpaceId(), assessementsInfoMessage.getFrom().getId())
        .stream()
        .filter(
            joined ->
                joined.getSpaceRole() != SpaceRole.VIEWER
                    && !joined.getUser().getId().equals(assessementsInfoMessage.getFrom().getId()))
        .forEach(
            joined -> {
              UserInfoMessage infoMessage = new UserInfoMessage(joined.getUser());
              NotificationMessage notificationMessage =
                  amqNotifier.saveMessage(
                      infoMessage,
                      baseNotificationMessage,
                      message,
                      assessementsInfoMessage.getName(),
                      assessementsInfoMessage.getSpaceName(),
                      assessementsInfoMessage.getCategoryName(),
                      assessementsInfoMessage.getAssessmentType() == AssessmentType.QUIZ
                          ? String.format(
                              "%1$td/%1$tm/%1$tY %1$tr %1$tZ",
                              assessementsInfoMessage.getStartDateTime())
                          : String.format(
                              "%1$td/%1$tm/%1$tY %1$tr %1$tZ",
                              assessementsInfoMessage.getDueDateTime()));
              if (joined.getUser().getNotification() && joined.getNotification()) {
                amqNotifier.send(notificationMessage);
                if (joined.getUser().hasMailNotification())
                  mailService.sendNotificationMail(notificationMessage, infoMessage, true, false);
              }
            });
  }
}
