package com.eshraqgroup.mint.notification.handlers.impl;

import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class PermissionHandler extends AbstractHandler {

  public PermissionHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper) {
    super(userRepository, amqNotifier, mailService, objectMapper);
    // TODO Auto-generated constructor stub
  }
}
