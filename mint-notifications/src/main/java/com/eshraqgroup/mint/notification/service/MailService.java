package com.eshraqgroup.mint.notification.service;

import java.io.IOException;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

/**
 * Service for sending e-mails.
 *
 * <p>
 *
 * <p>We use the @Async annotation to send e-mails asynchronously.
 */
@Service
public class MailService {

  private final Logger log = LoggerFactory.getLogger(MailService.class);

  private final JavaMailSenderImpl javaMailSender;

  private final MessageSource messageSource;

  private final SpringTemplateEngine templateEngine;

  private final SendGrid sendGrid;

  @Value("${mint.enableMailNotification}")
  private boolean enableMailNotification;

  @Value("${mint.weburl}")
  private String weburl;

  @Autowired
  public MailService(
      JavaMailSenderImpl javaMailSender,
      MessageSource messageSource,
      SpringTemplateEngine templateEngine,
      SendGrid sendGrid) {
    this.javaMailSender = javaMailSender;
    this.messageSource = messageSource;
    this.templateEngine = templateEngine;
    this.sendGrid = sendGrid;
  }

  public void sendEmail(
      String to, String subject, String content, boolean isMultipart, boolean isHtml) {
    log.debug(
        "Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
        isMultipart,
        isHtml,
        to,
        subject,
        content);

    Email from = new Email("info@mintplatform.net");
    Email toEmail = new Email(to);
    Content mailContent = new Content(MediaType.TEXT_HTML_VALUE, content);
    Mail mail = new Mail(from, subject, toEmail, mailContent);

    Request request = new Request();
    try {
      request.setMethod(Method.POST);
      request.setEndpoint("mail/send");
      request.setBody(mail.build());
      Response response = sendGrid.api(request);
      log.debug("mail statusCode {}", response.getStatusCode());
      log.debug("mail body {}", response.getBody());
      log.debug("mail header {}", response.getHeaders());
    } catch (IOException ex) {
      log.error("E-mail could not be sent to user '{}', exception is: {}", to, ex.getMessage());
    }
  }

  @Async
  public void sendActivationEmail(UserInfoMessage user) {
    log.debug("Sending activation e-mail to '{}'", user.getEmail());
    Locale locale = Locale.forLanguageTag(user.getLang());
    Context context = new Context(locale);
    context.setVariable("user", user);
    context.setVariable("weburl", weburl);
    String content;
    if (user.getLang() != null && user.getLang().toLowerCase().contains("ar")) {
      content = templateEngine.process("mails/activate_mail_ar", context);
    } else {
      content = templateEngine.process("mails/activate_mail_en", context);
    }
    String subject =
        messageSource.getMessage("email.creation.title", null, "user creation", locale);
    sendEmail(user.getEmail(), subject, content, false, true);
  }

  @Async
  public void sendCreationEmail(UserInfoMessage user) {
    log.debug("Sending creation e-mail to '{}'", user.getEmail());
    Locale locale = Locale.forLanguageTag(user.getLang());
    Context context = new Context(locale);
    context.setVariable("user", user);
    context.setVariable("password", user.getPassword());
    context.setVariable("weburl", weburl);
    String content;
    if (user.getLang() != null && user.getLang().toLowerCase().contains("ar")) {
      content = templateEngine.process("mails/creation_mail_ar", context);
    } else {
      content = templateEngine.process("mails/creation_mail_en", context);
    }
    String subject = messageSource.getMessage("email.activation.title", null, "", locale);
    log.info("Mail Content Is {}", content);
    sendEmail(user.getEmail(), subject, content, false, true);
  }

  @Async
  public void sendPasswordResetMail(UserInfoMessage user) {
    log.debug("Sending password reset e-mail to '{}'", user.getEmail());
    Locale locale = Locale.forLanguageTag(user.getLang());
    Context context = new Context(locale);
    context.setVariable("user", user);
    context.setVariable("weburl", weburl);
    String content;
    if (user.getLang() != null && user.getLang().toLowerCase().contains("ar")) {
      content = templateEngine.process("mails/reset_mail_ar", context);
    } else {
      content = templateEngine.process("mails/reset_mail_en", context);
    }

    String subject = messageSource.getMessage("email.reset.title", null, locale);
    sendEmail(user.getEmail(), subject, content, false, true);
  }

  @Async
  public void sendNotificationMail(
      NotificationMessage notificationMessage,
      UserInfoMessage user,
      boolean withImage,
      boolean useTargeImage) {

    if (!enableMailNotification) {
      return;
    }
    log.debug("Sending Notification e-mail to '{}'", user.getEmail());

    Locale locale = Locale.forLanguageTag(user.getLang());
    Context context = new Context(locale);
    context.setVariable("user", user);
    context.setVariable("withImage", withImage);
    context.setVariable("weburl", weburl);
    if (withImage && useTargeImage) {
      context.setVariable("image", notificationMessage.getTarget().getImage());
    } else if (withImage) {
      context.setVariable("image", notificationMessage.getFrom().getImage());
    }
    context.setVariable("from", notificationMessage.getFrom().getName());
    context.setVariable("actionMessage", notificationMessage.getMessage());
    context.setVariable("date", String.format("%1$td/%1$tm/%1$tY", notificationMessage.getDate()));
    String content;
    if (user.getLang() != null && user.getLang().toLowerCase().contains("ar")) {
      content = templateEngine.process("mails/notification_email_ar", context);
    } else {
      content = templateEngine.process("mails/notification_email_en", context);
    }

    String subject =
        messageSource.getMessage("email.notification.title", null, "notification mail", locale);
    sendEmail(user.getEmail(), subject, content, false, true);
  }
}
