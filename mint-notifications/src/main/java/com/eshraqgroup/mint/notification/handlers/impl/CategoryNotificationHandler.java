package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.CREATE;
import static com.eshraqgroup.mint.constants.notification.Actions.DELETE;
import static com.eshraqgroup.mint.constants.notification.Actions.UPDATE;
import static com.eshraqgroup.mint.constants.notification.EntityType.CATEGORY;
import static com.eshraqgroup.mint.constants.notification.MessageCategory.APP;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.models.messages.category.CategoryMessageInfo;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class CategoryNotificationHandler extends AbstractHandler {

  @Autowired
  public CategoryNotificationHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper) {
    super(userRepository, amqNotifier, mailService, objectMapper);
  }

  @Override
  @Transactional
  protected void onCreate(BaseMessage notificationMessage) {
    logger.debug("Category create");
    CategoryMessageInfo categoryMessageInfo =
        mapJsonObject(notificationMessage, CategoryMessageInfo.class);
    categoryNotify(categoryMessageInfo, CREATE);
  }

  @Override
  @Transactional
  protected void onDelete(BaseMessage notificationMessage) {
    logger.debug("Category delete");
    CategoryMessageInfo categoryMessageInfo =
        mapJsonObject(notificationMessage, CategoryMessageInfo.class);
    categoryNotify(categoryMessageInfo, DELETE);
  }

  @Override
  @Transactional
  protected void onUpdate(BaseMessage notificationMessage) {
    logger.debug("Category update");
    CategoryMessageInfo categoryMessageInfo =
        mapJsonObject(notificationMessage, CategoryMessageInfo.class);
    categoryNotify(categoryMessageInfo, UPDATE);
  }

  @Transactional()
  protected void categoryNotify(CategoryMessageInfo categoryMessageInfo, int action) {

    BaseNotificationMessage baseNotificationMessage =
        new BaseNotificationMessage(
            ZonedDateTime.now(),
            APP,
            categoryMessageInfo.getFrom(),
            new Target(CATEGORY, categoryMessageInfo.getId().toString(), action));

    List<UserInfoMessage> userInfoMessages;
    if (null != categoryMessageInfo.getOrganizationId()) {
      logger.debug("handle organization category");
      userInfoMessages =
          userRepository
              .findByOrganizationIdAndDeletedFalse(categoryMessageInfo.getOrganizationId())
              .filter(user -> user.getType().equals(UserType.USER))
              .map(UserInfoMessage::new)
              .collect(Collectors.toList());

    } else if (null != categoryMessageInfo.getFoundationId()) {
      userInfoMessages =
          userRepository
              .findByFoundationIdAndDeletedFalse(categoryMessageInfo.getFoundationId())
              .filter(user -> user.getType().equals(UserType.USER))
              .map(UserInfoMessage::new)
              .collect(Collectors.toList());

    } else {
      userInfoMessages =
          userRepository
              .findByOrganizationIsNullAndFoundationIsNullAndDeletedFalse()
              .filter(user -> user.getType().equals(UserType.USER))
              .map(UserInfoMessage::new)
              .collect(Collectors.toList());
    }

    amqNotifier.sendAll(amqNotifier.saveAll(userInfoMessages, baseNotificationMessage, null, null));
  }
}
