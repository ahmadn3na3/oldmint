package com.eshraqgroup.mint.notification.handlers.impl;

import static com.eshraqgroup.mint.constants.notification.Actions.CREATE;
import static com.eshraqgroup.mint.constants.notification.Actions.UPDATE;
import static com.eshraqgroup.mint.constants.notification.EntityType.ANNOTATIONS;
import static com.eshraqgroup.mint.constants.notification.MessageCategory.USER;
import java.time.ZonedDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.constants.SpaceRole;
import com.eshraqgroup.mint.domain.jpa.Joined;
import com.eshraqgroup.mint.models.messages.BaseMessage;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.Target;
import com.eshraqgroup.mint.models.messages.annotation.AnnotationMessage;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;
import com.eshraqgroup.mint.notification.components.AmqNotifier;
import com.eshraqgroup.mint.notification.handlers.AbstractHandler;
import com.eshraqgroup.mint.notification.models.NotificationMessage;
import com.eshraqgroup.mint.notification.service.MailService;
import com.eshraqgroup.mint.notification.util.Utilities;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.repos.mongo.AnnotationRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Created by ayman on 04/07/17. */
@Component
public class AnnotationHandler extends AbstractHandler {

  private final Utilities utilities;

  private final AnnotationRepository annotationRepository;

  @Autowired
  public AnnotationHandler(
      UserRepository userRepository,
      AmqNotifier amqNotifier,
      MailService mailService,
      ObjectMapper objectMapper,
      Utilities utilities,
      AnnotationRepository annotationRepository) {
    super(userRepository, amqNotifier, mailService, objectMapper);
    this.utilities = utilities;
    this.annotationRepository = annotationRepository;
  }

  @Override
  protected void onCreate(BaseMessage baseMessage) {
    notify(baseMessage, CREATE);
  }

  @Override
  protected void onUpdate(BaseMessage baseMessage) {
    notify(baseMessage, UPDATE);
  }
  //TODO: send public flag in message
  private void notify(BaseMessage baseMessage, int action) {
    AnnotationMessage annotationMessage = mapJsonObject(baseMessage, AnnotationMessage.class);

    annotationRepository
        .findOneByIdAndDeletedFalse(annotationMessage.getId())
        .ifPresent(
            annotation -> {
              if (annotation.getIsPublic()) {
                List<Joined> joinedList =
                    utilities.getCommunityUserList(
                        annotationMessage.getSpaceId(), annotationMessage.getFrom().getId());
                BaseNotificationMessage baseNotificationMessage =
                    new BaseNotificationMessage(
                        ZonedDateTime.now(),
                        USER,
                        annotationMessage.getFrom(),
                        new Target(ANNOTATIONS, annotationMessage.getId(), action));
                joinedList
                    .stream()
                    .filter(
                        joined ->
                            joined.getSpaceRole() != SpaceRole.VIEWER
                                && !joined
                                    .getUser()
                                    .getId()
                                    .equals(annotationMessage.getFrom().getId()))
                    .forEach(
                        joined -> {
                          UserInfoMessage userInfoMessage = new UserInfoMessage(joined.getUser());
                          NotificationMessage notificationMessage =
                              amqNotifier.saveMessage(
                                  userInfoMessage,
                                  baseNotificationMessage,
                                  createMessage(baseMessage),
                                  annotationMessage.getSpaceName(),
                                  annotationMessage.getCategoryName());

                          if (joined.getNotification()
                              && joined.getUser().getNotification()
                              && !joined.getUser().isDeleted()) {
                            amqNotifier.send(notificationMessage);
                            if (joined.getUser().hasMailNotification())
                              mailService.sendNotificationMail(
                                  notificationMessage, userInfoMessage, true, true);
                          }
                        });
              }
            });
  }
}
