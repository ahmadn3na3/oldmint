package com.eshraqgroup.mint.question.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import com.eshraqgroup.mint.constants.QuestionType;

/** Created by ayman on 06/11/16. */
public class ExcelUtil {

  public static boolean validate(Cell cell, int cellType) {
    if (cell != null && cellType != cell.getCellType()) {
      return false;
    }
    switch (cellType) {
      case Cell.CELL_TYPE_NUMERIC:
        return cell != null;
      case Cell.CELL_TYPE_STRING:
        return cell != null
            && !cell.getStringCellValue().isEmpty()
            && !cell.getStringCellValue().equals("#");
      case Cell.CELL_TYPE_FORMULA:
      case Cell.CELL_TYPE_BOOLEAN:
      case Cell.CELL_TYPE_ERROR:
        return cell != null;
      case Cell.CELL_TYPE_BLANK:
      default:
        return false;
    }
  }

  public static QuestionType validateStructure(Sheet sheet) {
    String fileType = sheet.getSheetName();

    switch (fileType) {
      case "YesNo":
        return QuestionType.TRUE_FALSE;
      case "Sequence":
        return QuestionType.SEQUENCE;
      case "MultiChoose":
        return QuestionType.MULTIPLE_CHOICES;
      case "SingleChoose":
        return QuestionType.SINGLE_CHOICE;
      case "Match":
        return QuestionType.MATCHING;
      case "Complete":
        return QuestionType.COMPLETE;
      case "Essay":
        return QuestionType.ESSAY;
      default:
        return null;
    }
  }
}
