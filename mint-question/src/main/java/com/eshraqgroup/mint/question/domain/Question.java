package com.eshraqgroup.mint.question.domain;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.eshraqgroup.mint.constants.QuestionType;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;
import com.eshraqgroup.mint.domain.mongo.Choices;

/** Created by ayman on 07/06/16. */
@Document(collection = "questionBank")
public class Question extends AbstractEntity {
  @Id private String id;

  @Indexed private String body;

  private Boolean isPublic = Boolean.TRUE;

  private QuestionType questionType;

  private String correctAnswer;

  private String bodyResourceUrl;

  @Indexed private Long categoryId;
  @Indexed private Long ownerId;
  private Long spaceId;
  private String tags;

  @Indexed private Long foundationId;

  @Indexed private Long organizationId;

  private List<Choices> choicesList;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Boolean getPublic() {
    return isPublic;
  }

  public void setPublic(Boolean aPublic) {
    isPublic = aPublic;
  }

  public String getTags() {
    return tags;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  public QuestionType getQuestionType() {
    return questionType;
  }

  public void setQuestionType(QuestionType questionType) {
    this.questionType = questionType;
  }

  public String getCorrectAnswer() {
    return correctAnswer;
  }

  public void setCorrectAnswer(String correctAnswer) {
    this.correctAnswer = correctAnswer;
  }

  public String getBodyResourceUrl() {
    return bodyResourceUrl;
  }

  public void setBodyResourceUrl(String bodyResourceUrl) {
    this.bodyResourceUrl = bodyResourceUrl;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public Long getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(Long ownerId) {
    this.ownerId = ownerId;
  }

  public List<Choices> getChoicesList() {
    return choicesList;
  }

  public void setChoicesList(List<Choices> choicesList) {
    this.choicesList = choicesList;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  public Long getFoundationId() {
    return foundationId;
  }

  public void setFoundationId(Long foundationId) {
    this.foundationId = foundationId;
  }

  public Long getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(Long organizationId) {
    this.organizationId = organizationId;
  }
}
