package com.eshraqgroup.mint.question.services;

import static com.eshraqgroup.mint.constants.notification.EntityAction.QUESTION_CREATE;
import static com.eshraqgroup.mint.constants.notification.EntityAction.QUESTION_DELETE;
import static com.eshraqgroup.mint.constants.notification.EntityAction.QUESTION_DELETE_BULK;
import static com.eshraqgroup.mint.constants.notification.EntityAction.QUESTION_UPDATE;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.FilterQuery;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleFilterQuery;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.eshraqgroup.mint.configuration.auditing.Auditable;
import com.eshraqgroup.mint.configuration.notifications.Message;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.Category;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.domain.mongo.Choices;
import com.eshraqgroup.mint.domain.solrdomains.QuestionSolrDomain;
import com.eshraqgroup.mint.exception.InvalidException;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.exception.NotFoundException;
import com.eshraqgroup.mint.exception.NotPermittedException;
import com.eshraqgroup.mint.models.PageRequestModel;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.models.SimpleModel;
import com.eshraqgroup.mint.question.domain.Question;
import com.eshraqgroup.mint.question.models.question.QuestionCreateModel;
import com.eshraqgroup.mint.question.models.question.QuestionModel;
import com.eshraqgroup.mint.question.models.question.QuestionSearchModel;
import com.eshraqgroup.mint.question.models.question.choices.ChoicesModel;
import com.eshraqgroup.mint.question.repository.QuestionRepository;
import com.eshraqgroup.mint.repos.jpa.CategoryRepository;
import com.eshraqgroup.mint.repos.jpa.JoinedRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;

/** Created by ayman on 13/06/16. */
@Service
public class QuestionService {

  private final Logger log = LoggerFactory.getLogger(QuestionService.class);

  private final QuestionRepository questionRepository;

  private final CategoryRepository categoryRepository;

  private final UserRepository userRepository;

  private final SolrTemplate solrTemplate;

  private final JoinedRepository joinedRepository;

  private final MongoTemplate mongoTemplate;

  @Autowired
  public QuestionService(
      QuestionRepository questionRepository,
      CategoryRepository categoryRepository,
      UserRepository userRepository,
      SolrTemplate solrTemplate,
      JoinedRepository joinedRepository,
      MongoTemplate mongoTemplate) {
    this.questionRepository = questionRepository;
    this.categoryRepository = categoryRepository;
    this.userRepository = userRepository;
    this.solrTemplate = solrTemplate;
    this.joinedRepository = joinedRepository;
    this.mongoTemplate = mongoTemplate;
  }

  @Transactional
  @Auditable(QUESTION_CREATE)
  @PreAuthorize("hasAuthority('QUESTION_CREATE')")
  @Message(services = Services.INDEX, entityAction = QUESTION_CREATE)
  public ResponseModel createQuestion(QuestionCreateModel createModel) {
    return categoryRepository
        .findOneByIdAndDeletedFalse(createModel.getCategoryId())
        .map(
            category ->
                userRepository
                    .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
                    .map(
                        user -> {
                          if (category.getFoundation() != null) {
                            if ((category.getOrganization() != null
                                    && !category.getOrganization().equals(user.getOrganization()))
                                || !category.getFoundation().equals(user.getFoundation())) {
                              throw new NotPermittedException();
                            }
                          } else {
                            if (category.getFoundation() != null) {
                              throw new NotPermittedException();
                            }
                          }
                          Question question = new Question();
                          question.setBody(createModel.getBody());
                          question.setBodyResourceUrl(createModel.getBodyResourceUrl());
                          question.setCorrectAnswer(createModel.getCorrectAnswer());
                          question.setQuestionType(createModel.getQuestionType());
                          question.setPublic(createModel.getPublic());
                          question.setCategoryId(category.getId());
                          if (user.getFoundation() == null) {
                            question.setSpaceId(createModel.getSpaceId());
                          } else {
                            question.setFoundationId(user.getFoundation().getId());
                            if (user.getOrganization() != null) {
                              question.setOrganizationId(user.getOrganization().getId());
                            }
                          }
                          question.setOwnerId(user.getId());
                          if (!createModel.getTags().isEmpty()) {
                            question.setTags(String.join(",", createModel.getTags()));
                          }
                          if (null != createModel.getChoicesList()
                              && !createModel.getChoicesList().isEmpty()) {
                            question.setChoicesList(mapChoicesModelToDomain(createModel, question));
                          }
                          questionRepository.save(question);
                          return ResponseModel.done((Object) question.getId());
                        })
                    .orElseThrow(NotPermittedException::new))
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @Auditable(QUESTION_DELETE)
  @PreAuthorize("hasAnyAuthority('QUESTION_DELETE','QUESTION_REVIEW_DELETE')")
  @Message(services = Services.INDEX, entityAction = EntityAction.QUESTION_DELETE)
  public ResponseModel deleteQuestion(String id) {
    if (null != id) {
      return Optional.ofNullable(questionRepository.findOne(id))
          .map(
              question -> {
                questionRepository.delete(question);
                log.debug("question {} deleted", id);
                return ResponseModel.done((Object) id);
              })
          .orElseThrow(NotFoundException::new);
    }
    log.warn("id parameter can't be null");
    throw new MintException(Code.INVALID, "id");
  }

  @Transactional
  @Auditable(QUESTION_DELETE_BULK)
  @PreAuthorize("hasAnyAuthority('QUESTION_DELETE','QUESTION_REVIEW_DELETE')")
  @Message(services = Services.INDEX, entityAction = EntityAction.QUESTION_DELETE_BULK)
  public ResponseModel deleteQuestion(List<String> id) {
    if (null != id && !id.isEmpty()) {
      Iterable<Question> question = questionRepository.findAll(id);
      questionRepository.delete(question);
      log.debug("question {} deleted", id);
      return ResponseModel.done(id);
    }
    log.warn("id parameter can't be null");
    throw new MintException(Code.INVALID, "id");
  }

  @Transactional
  @PreAuthorize("hasAnyAuthority('QUESTION_READ','QUESTION_REVIEW_READ')")
  public ResponseModel getQuestion(String id, String lang) {
    return Optional.ofNullable(questionRepository.findOne(id))
        .map(question -> ResponseModel.done(this.maptoModel(question, lang)))
        .orElseThrow(NotFoundException::new);
  }

  @Transactional
  @PreAuthorize("hasAnyAuthority('QUESTION_READ','QUESTION_REVIEW_READ')")
  public PageResponseModel getAllQuestion(
      PageRequest pageRequest,
      String[] tag,
      String filter,
      Long categoryId,
      String lang,
      Sort.Direction direction) {
    User user =
        userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).get();
    final Page<QuestionModel> questionPage;
    org.springframework.data.mongodb.core.query.Query query =
        new org.springframework.data.mongodb.core.query.Query();

    if (categoryId != null) {
      query.addCriteria(
          org.springframework.data.mongodb.core.query.Criteria.where("categoryId").is(categoryId));
    }
    if (filter != null) {
      query.addCriteria(
          org.springframework.data.mongodb.core.query.Criteria.where("body").regex(filter, "i"));
    }
    if (tag != null && tag.length != 0) {
      String tagJoined =
          Arrays.stream(tag).map(tags -> tags.replace("\\s", "_")).collect(Collectors.joining(","));
      query.addCriteria(
          org.springframework.data.mongodb.core.query.Criteria.where("tags").regex(tagJoined));
    }

    if (user.getFoundation() == null
        || (user.getFoundation() != null
            && !SecurityUtils.isCurrentUserInRole("QUESTION_REVIEW_READ"))) {

      query.addCriteria(
          org.springframework.data.mongodb.core.query.Criteria.where("ownerId").is(user.getId()));
    } else {

      query.addCriteria(
          org.springframework.data.mongodb.core.query.Criteria.where("foundationId")
              .is(user.getFoundation().getId()));
      if (user.getOrganization() != null) {
        query.addCriteria(
            org.springframework.data.mongodb.core.query.Criteria.where("organizationId")
                .is(user.getOrganization().getId()));
      }
    }
    query.with(pageRequest);
    log.debug("mongo query");
    List<Question> questions = mongoTemplate.find(query, Question.class);
    questionPage =
        PageableExecutionUtils.getPage(
                questions, pageRequest, () -> mongoTemplate.count(query, Question.class))
            .map(question -> this.maptoModel(question, lang));

    return PageResponseModel.done(
        questionPage.getContent(),
        questionPage.getTotalPages(),
        questionPage.getNumber(),
        questionPage.getTotalElements());
  }

  @Transactional
  @Auditable(QUESTION_UPDATE)
  @PreAuthorize("hasAnyAuthority('QUESTION_UPDATE','QUESTION_REVIEW_UPDATE')")
  @Message(services = Services.INDEX, entityAction = EntityAction.QUESTION_UPDATE)
  public ResponseModel updateQuestion(String id, QuestionCreateModel updateModel) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Question question = questionRepository.findOne(id);
              if (question == null) {
                log.warn("question {} not found", id);
                throw new NotFoundException();
              }
              if (!question.getOwnerId().equals(user.getId())
                  && !SecurityUtils.isCurrentUserInRole("QUESTION_REVIEW_UPDATE")) {
                throw new NotPermittedException();
              }
              question.setBody(updateModel.getBody());
              question.setCorrectAnswer(updateModel.getCorrectAnswer());
              question.setBodyResourceUrl(updateModel.getBodyResourceUrl());
              question.setPublic(updateModel.getPublic());
              question.setCategoryId(updateModel.getCategoryId());
              question.setTags(String.join(",", updateModel.getTags()));

              if (user.getFoundation() == null) {
                question.setSpaceId(updateModel.getSpaceId());
              }

              if (null != updateModel.getChoicesList() && !updateModel.getChoicesList().isEmpty()) {
                question.setChoicesList(mapChoicesModelToDomain(updateModel, question));
              }

              questionRepository.save(question);

              return ResponseModel.done((Object) id);
            })
        .orElseThrow(NotPermittedException::new);
  }

  @Transactional
  @PreAuthorize("hasAnyAuthority('QUESTION_READ','QUESTION_REVIEW_READ')")
  public PageResponseModel searchByCriteria(
      QuestionSearchModel questionSearchModel, PageRequest pageRequestModel, String lang) {
    return userRepository
        .findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin())
        .map(
            user -> {
              Query query =
                  new SimpleQuery(Criteria.where("body").contains(questionSearchModel.getBody()));

              Criteria criteria = Criteria.where("type").in(questionSearchModel.getQuestionType());
              FilterQuery filterQuery = new SimpleFilterQuery(criteria);
              if (questionSearchModel.getCategoryId() != null
                  && questionSearchModel.getCategoryId() != 0) {
                filterQuery.addCriteria(
                    Criteria.where("categoryId").is(questionSearchModel.getCategoryId()));

                if (user.getFoundation() == null) {

                  if (questionSearchModel.getSpaceId() != null
                      && !questionSearchModel.getSpaceId().equals(0L)) {
                    filterQuery.addCriteria(
                        Criteria.where("spaceId").is(questionSearchModel.getSpaceId()));
                  } else {
                    List<Long> spaceIds =
                        joinedRepository
                            .findByUserAndSpaceCategoryIdAndDeletedFalse(
                                user,
                                questionSearchModel.getCategoryId(),
                                PageRequestModel.getPageRequestModel(null, null))
                            .map(joined -> joined.getSpace().getId())
                            .getContent();
                    if (!spaceIds.isEmpty()) {
                      filterQuery.addCriteria(Criteria.where("spaceId").in(spaceIds));
                    }
                  }
                } else {
                  if (SecurityUtils.isCurrentUserInRole("QUESTION_REVIEW_READ")) {
                    Set<Long> ids =
                        user.getFoundation()
                            .getUsers()
                            .stream()
                            .filter(user1 -> !user1.isDeleted())
                            .map(User::getId)
                            .collect(Collectors.toSet());
                    filterQuery.addCriteria(Criteria.where("userId").in(ids));
                  } else {
                    filterQuery.addCriteria(Criteria.where("userId").is(user.getId()));
                  }
                }
              } else {
                throw new InvalidException("error.question.categoryId");
              }

              if (questionSearchModel.getTags() != null) {
                Arrays.stream(questionSearchModel.getTags())
                    .filter(s -> !s.isEmpty())
                    .forEach(
                        tag ->
                            filterQuery.addCriteria(
                                Criteria.where("tags").is(tag.replaceAll("\\s", "_"))));
              }

              if (questionSearchModel.getExclude() != null
                  && questionSearchModel.getExclude().length != 0) {
                filterQuery.addCriteria(
                    Criteria.where("id").not().in(questionSearchModel.getExclude()));
              }
              query.addFilterQuery(filterQuery);
              query.setPageRequest(pageRequestModel);
              Set<QuestionModel> questionModels = new HashSet<>();

              Page<String> ids =
                  solrTemplate
                      .query("question", query, QuestionSolrDomain.class)
                      .map(QuestionSolrDomain::getId);
              questionRepository
                  .findAll(ids.getContent())
                  .forEach(question -> questionModels.add(this.maptoModel(question, lang)));
              return PageResponseModel.done(
                  questionModels, ids.getTotalPages(), ids.getNumber(), ids.getTotalElements());
            })
        .orElseThrow(NotPermittedException::new);
  }

  public List<Choices> mapChoicesModelToDomain(
      QuestionCreateModel questionCreateModel, Question question) {
    List<Choices> choicesList = new ArrayList<>();
    if (questionCreateModel.getChoicesList() != null) {
      for (ChoicesModel c : questionCreateModel.getChoicesList()) {
        Choices choices = new Choices();
        choices.setId(c.getId());
        choices.setLabel(c.getLabel());
        choices.setCorrectAnswer(c.getCorrectAnswer());
        choices.setCorrectAnswerDescription(c.getCorrectAnswerDescription());
        choices.setCorrectOrder(c.getCorrectOrder());
        choices.setPairCol(c.getPairColumn());
        choices.setCorrectAnswerResourceUrl(c.getCorrectAnswerResourceUrl());
        choicesList.add(choices);
      }
    }
    return choicesList;
  }

  private QuestionModel maptoModel(Question question, String lang) {
    QuestionModel questionModel = new QuestionModel(question);
    Category category = categoryRepository.findOne(question.getCategoryId());
    User user = userRepository.findOne(question.getOwnerId());
    questionModel.setCategory(
        new SimpleModel(
            category.getId(),
            lang.equalsIgnoreCase("ar") ? category.getNameAr() : category.getName()));
    questionModel.setOwner(new SimpleModel(user.getId(), user.getFullName()));
    return questionModel;
  }

  @PostConstruct
  @Transactional()
  public void updateQuestionThatHaveNotFoundation() {
    org.springframework.data.mongodb.core.query.Query query =
        new org.springframework.data.mongodb.core.query.Query(
            org.springframework.data.mongodb.core.query.Criteria.where("foundationId")
                .exists(false)
                .and("spaceId")
                .exists(false));
    mongoTemplate
        .find(query, Question.class)
        .forEach(
            question -> {
              User user = userRepository.findOne(question.getOrganizationId());
              if (user != null && user.getFoundation() != null) {
                question.setFoundationId(user.getFoundation().getId());
                if (user.getOrganization() != null) {
                  question.setFoundationId(user.getOrganization().getId());
                }
                questionRepository.save(question);
              }
            });
  }
}
