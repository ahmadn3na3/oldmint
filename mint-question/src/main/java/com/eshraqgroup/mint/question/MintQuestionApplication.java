package com.eshraqgroup.mint.question;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@SpringBootApplication(scanBasePackages = {"com.eshraqgroup.mint", "com.eshraqgroup.mint.question"})
@EnableJpaRepositories(basePackages = {"com.eshraqgroup.mint.repos"})
@EntityScan(basePackages = {"com.eshraqgroup.mint.domain", "com.eshraqgroup.mint.question.domain"})
@EnableMongoRepositories(
  basePackages = {"com.eshraqgroup.mint.repos", "com.eshraqgroup.mint.question.repository"}
)
@EnableSolrRepositories(basePackages = {"com.eshraqgroup.mint.repos.solrrepo"})
public class MintQuestionApplication {

  public static void main(String[] args) {
    SpringApplication.run(MintQuestionApplication.class, args);
  }
}
