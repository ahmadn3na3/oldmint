package com.eshraqgroup.mint.question.configuration;

import org.apache.solr.client.solrj.SolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.RequestMethod;
import org.springframework.data.solr.core.SolrTemplate;

/** Created by ahmad on 5/30/17. */
@Configuration
public class SolrConfiguration {

  @Autowired SolrClient solrClient;

  @Bean
  public SolrTemplate solrTemplate() throws Exception {
    return new SolrTemplate(solrClient, null, RequestMethod.POST);
  }
}
