package com.eshraqgroup.mint.question.repository;

import java.util.stream.Stream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.question.domain.Question;

/** Created by ayman on 13/06/16. */
@Repository
public interface QuestionRepository extends MongoRepository<Question, String> {
  Stream<Question> findByOwnerIdAndDeletedFalse(Long id);

  Page<Question> findByOwnerIdAndDeletedFalse(Long id, Pageable pageable);

  Page<Question> findByOwnerIdInAndDeletedFalse(Iterable<Long> id, Pageable pageable);

  Page<Question> findByCategoryId(Long id, Pageable pageable);

  Page<Question> findByCategoryIdIn(Iterable<Long> ids, Pageable pageable);
}
