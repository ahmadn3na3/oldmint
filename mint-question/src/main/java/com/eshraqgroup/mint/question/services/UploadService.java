package com.eshraqgroup.mint.question.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import com.eshraqgroup.mint.configuration.notifications.Message;
import com.eshraqgroup.mint.constants.Code;
import com.eshraqgroup.mint.constants.PairColumn;
import com.eshraqgroup.mint.constants.QuestionType;
import com.eshraqgroup.mint.constants.Services;
import com.eshraqgroup.mint.constants.notification.EntityAction;
import com.eshraqgroup.mint.domain.jpa.Category;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.domain.mongo.Choices;
import com.eshraqgroup.mint.exception.MintException;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.question.domain.Question;
import com.eshraqgroup.mint.question.repository.QuestionRepository;
import com.eshraqgroup.mint.question.util.ExcelUtil;
import com.eshraqgroup.mint.repos.jpa.CategoryRepository;
import com.eshraqgroup.mint.repos.jpa.UserRepository;
import com.eshraqgroup.mint.security.SecurityUtils;

/** Created by ayman on 04/08/16. */
@Service
public class UploadService {

  private final CategoryRepository categoryRepository;

  private final QuestionRepository questionRepository;

  private final UserRepository userRepository;

  @Autowired
  public UploadService(
      CategoryRepository categoryRepository,
      QuestionRepository questionRepository,
      UserRepository userRepository) {
    this.categoryRepository = categoryRepository;
    this.questionRepository = questionRepository;
    this.userRepository = userRepository;
  }

  @Transactional
  @Message(entityAction = EntityAction.QUESTION_CREATE_BULK, services = Services.INDEX)
  public ResponseModel uploadDataFile(
      MultipartFile dataFile,
      QuestionType questionType,
      Long categoryId,
      String tags,
      Long spaceId) {
    List<Question> questionList = null;
    User user =
        userRepository.findOneByUserNameAndDeletedFalse(SecurityUtils.getCurrentUserLogin()).get();
    Category category = categoryRepository.findOne(categoryId);

    if (category == null) {
      return ResponseModel.error(Code.NOT_FOUND, "category");
    }

    if (questionType == null) {
      return ResponseModel.error(Code.NOT_FOUND, "type");
    }

    if (dataFile == null || dataFile.isEmpty()) {
      return ResponseModel.error(Code.INVALID, "file");
    } else {
      try {
        InputStream fileContent = dataFile.getInputStream();
        ByteArrayInputStream is = new ByteArrayInputStream(IOUtils.toByteArray(fileContent));
        Workbook wb = WorkbookFactory.create(is);

        QuestionType fileType = ExcelUtil.validateStructure(wb.getSheetAt(0));
        if (fileType != questionType) {
          return ResponseModel.error(Code.INVALID, "File Format");
        }

        switch (questionType) {
          case TRUE_FALSE:
            questionList = loadTrueFalseQuestion(wb, category, tags, user, spaceId);
            break;
          case MATCHING:
            questionList = loadMatchingQuestion(wb, category, tags, user, spaceId);
            break;
          case COMPLETE:
            questionList = loadCompleteQuestion(wb, category, tags, user, spaceId);
            break;
          case SEQUENCE:
            questionList = loadSequenceQuestion(wb, category, tags, user, spaceId);
            break;
          case SINGLE_CHOICE:
            questionList = loadSingleChoiceQuestion(wb, category, tags, user, spaceId);
            break;
          case MULTIPLE_CHOICES:
            questionList = loadMultipleChoiceQuestion(wb, category, tags, user, spaceId);
            break;
          case ESSAY:
            questionList = loadEssayQuestion(wb, category, tags, user, spaceId);
            break;
        }
      } catch (IOException e) {
        throw new MintException(Code.UPLOAD_DATA, e.getMessage());
      } catch (InvalidFormatException e) {
        throw new MintException(Code.UPLOAD_DATA, e.getMessage());
      }
      if (questionList != null && !questionList.isEmpty()) {
        questionRepository.save(questionList);
        questionList.stream().map(Question::getBody).collect(Collectors.toSet());
        Set<String> questionStrings =
            questionList.stream().map(Question::getBody).collect(Collectors.toSet());
        Set<String> questionIds =
            questionList.stream().map(Question::getId).collect(Collectors.toSet());
        return ResponseModel.done(questionStrings, questionIds);
      } else {
        throw new MintException(Code.UPLOAD_DATA, "No valid question to add");
      }
    }
  }

  private List<Question> loadEssayQuestion(
      Workbook wb, Category category, String chapterName, User user, Long spaceId) {
    Sheet sheet = wb.getSheetAt(0);
    sheet.removeRow(sheet.getRow(0));
    List<Question> questionList = new ArrayList<>();

    for (Row row : sheet) {

      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)
          && row.getCell(3).getStringCellValue().trim().length() < 250) {
        if (ExcelUtil.validate(row.getCell(4), row.getCell(4).getCellType())) {

          Question question = new Question();
          Set<String> tags = new HashSet<>();
          question.setCategoryId(category.getId());
          question.setOwnerId(user.getId());
          question.setQuestionType(QuestionType.ESSAY);
          question.setSpaceId(spaceId);
          question.setFoundationId(
              user.getFoundation() != null ? user.getFoundation().getId() : null);
          question.setOrganizationId(
              user.getOrganization() != null ? user.getOrganization().getId() : null);
          question.setBody(row.getCell(3).getStringCellValue().trim());
          if (chapterName != null && !chapterName.isEmpty()) {
            tags.add(chapterName.replace(" ", "_"));
          }

          if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
            tags.add(row.getCell(0).getStringCellValue().replace(" ", "_"));
          }

          if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
            tags.add(row.getCell(2).getStringCellValue().replace(" ", "_"));
          }

          if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
            tags.add(row.getCell(1).getStringCellValue().replace(" ", "_"));
          }

          question.setTags(String.join(",", tags));

          question.setCorrectAnswer(mapValue(row.getCell(4).getCellType(), row, 4));

          questionList.add(question);
        }
      }
    }
    return questionList;
  }

  private List<Question> loadSequenceQuestion(
      Workbook wb, Category category, String chapterName, User user, Long spaceId) {
    Sheet sheet = wb.getSheetAt(0);
    sheet.removeRow(sheet.getRow(0));
    List<Question> questionList = new ArrayList<>();

    for (Row row : sheet) {
      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)
          && row.getCell(3).getStringCellValue().trim().length() < 250) {

        Question question = new Question();
        Set<String> tags = new HashSet<>();
        question.setCategoryId(category.getId());
        question.setOwnerId(user.getId());
        question.setQuestionType(QuestionType.SEQUENCE);
        question.setSpaceId(spaceId);
        question.setFoundationId(
            user.getFoundation() != null ? user.getFoundation().getId() : null);
        question.setOrganizationId(
            user.getOrganization() != null ? user.getOrganization().getId() : null);
        question.setBody(row.getCell(3).getStringCellValue());
        if (chapterName != null && !chapterName.isEmpty()) {
          tags.add(chapterName.replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(0).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(2).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(1).getStringCellValue().replace(" ", "_"));
        }

        question.setTags(String.join(",", tags));

        List<Choices> choicesList = new ArrayList<>();
        for (int i = 4; i < 16; i = i + 2) {
          int cellType = row.getCell(i).getCellType();
          if (ExcelUtil.validate(row.getCell(i), cellType)) {
            Choices choice = mapChoiceLabel(cellType, row, i);
            choice.setCorrectOrder(new Double(row.getCell(i + 1).getNumericCellValue()).intValue());
            choicesList.add(choice);
          }
        }
        if (choicesList.isEmpty()) {
          continue;
        }
        question.setChoicesList(choicesList);

        questionList.add(question);
      }
    }
    return questionList;
  }

  private List<Question> loadMultipleChoiceQuestion(
      Workbook wb, Category category, String chapterName, User user, Long spaceId) {
    Sheet sheet = wb.getSheetAt(0);
    sheet.removeRow(sheet.getRow(0));
    FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

    List<Question> questionList = new ArrayList<>();

    for (Row row : sheet) {

      int base = 18;
      int correctAnswerCounter = 0;
      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)
          && row.getCell(3).getStringCellValue().trim().length() < 250) {

        Question question = new Question();
        Set<String> tags = new HashSet<>();
        question.setCategoryId(category.getId());
        question.setSpaceId(spaceId);
        question.setOwnerId(user.getId());
        question.setQuestionType(QuestionType.MULTIPLE_CHOICES);
        question.setFoundationId(
            user.getFoundation() != null ? user.getFoundation().getId() : null);
        question.setOrganizationId(
            user.getOrganization() != null ? user.getOrganization().getId() : null);
        question.setBody(row.getCell(3).getStringCellValue());
        if (chapterName != null && !chapterName.isEmpty()) {
          tags.add(chapterName.replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(0).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(2).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(1).getStringCellValue().replace(" ", "_"));
        }

        question.setTags(String.join(",", tags));

        List<Choices> choicesList = new ArrayList<>();
        for (int i = 4; i < 16; i = i + 2) {
          int cellType = row.getCell(i).getCellType();

          if (ExcelUtil.validate(row.getCell(i), cellType)) {

            Choices choice = mapChoiceLabel(cellType, row, i);

            if (ExcelUtil.validate(row.getCell(base + 1), Cell.CELL_TYPE_FORMULA)) {
              choice.setCorrectAnswer(evaluator.evaluate(row.getCell(base + 1)).getBooleanValue());
              if (choice.getCorrectAnswer()) {
                correctAnswerCounter++;
              }
              base++;
            }

            choicesList.add(choice);
          }
        }
        if (choicesList.isEmpty() || (correctAnswerCounter == 0)) {
          continue;
        }
        question.setChoicesList(choicesList);

        questionList.add(question);
      }
    }
    return questionList;
  }

  private List<Question> loadSingleChoiceQuestion(
      Workbook wb, Category category, String chapterName, User user, Long spaceId) {
    Sheet sheet = wb.getSheetAt(0);
    sheet.removeRow(sheet.getRow(0));
    List<Question> questionList = new ArrayList<>();

    Integer correctAnswerIndex = 0;
    for (Row row : sheet) {
      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)
          && row.getCell(3).getStringCellValue().trim().length() < 250) {

        if (ExcelUtil.validate(row.getCell(10), Cell.CELL_TYPE_NUMERIC)) {
          correctAnswerIndex = new Double(row.getCell(10).getNumericCellValue()).intValue();
          if (ExcelUtil.validate(row.getCell(4), row.getCell(4).getCellType())) {

            Question question = new Question();
            Set<String> tags = new HashSet<>();
            question.setCategoryId(category.getId());
            question.setOwnerId(user.getId());
            question.setSpaceId(spaceId);
            question.setQuestionType(QuestionType.SINGLE_CHOICE);
            question.setBody(row.getCell(3).getStringCellValue());
            question.setFoundationId(
                user.getFoundation() != null ? user.getFoundation().getId() : null);
            question.setOrganizationId(
                user.getOrganization() != null ? user.getOrganization().getId() : null);
            if (chapterName != null && !chapterName.isEmpty()) {
              tags.add(chapterName.replace(" ", "_"));
            }

            if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
              tags.add(row.getCell(0).getStringCellValue().replace(" ", "_"));
            }

            if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
              tags.add(row.getCell(2).getStringCellValue().replace(" ", "_"));
            }

            if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
              tags.add(row.getCell(1).getStringCellValue().replace(" ", "_"));
            }

            question.setTags(String.join(",", tags));

            List<Choices> choicesList = new ArrayList<>();

            for (int i = 0; i < 6; i++) {
              int cellType = row.getCell(i + 4).getCellType();

              if (ExcelUtil.validate(row.getCell(i + 4), cellType)) {

                Choices choice = mapChoiceLabel(cellType, row, i + 4);

                if (i == correctAnswerIndex - 1) {
                  choice.setCorrectAnswer(Boolean.TRUE);
                } else {
                  choice.setCorrectAnswer(Boolean.FALSE);
                }
                choicesList.add(choice);
              }
            }
            if (choicesList.size() < 2) {
              continue;
            }
            question.setChoicesList(choicesList);

            questionList.add(question);
          }
        }
      }
    }
    return questionList;
  }

  private List<Question> loadCompleteQuestion(
      Workbook wb, Category category, String chapterName, User user, Long spaceId) {
    Sheet sheet = wb.getSheetAt(0);

    sheet.removeRow(sheet.getRow(0));
    List<Question> questionList = new ArrayList<>();

    for (Row row : sheet) {

      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)
          && row.getCell(3).getStringCellValue().trim().length() < 250) {
        QuestionBody questionBody = addSpaceToken(row.getCell(3).getStringCellValue());
        if ((questionBody.getSpaces() > 3) || (questionBody.getSpaces() == 0)) {
          continue;
        }
        Question question = new Question();
        Set<String> tags = new HashSet<>();
        question.setCategoryId(category.getId());
        question.setOwnerId(user.getId());
        question.setSpaceId(spaceId);
        question.setQuestionType(QuestionType.COMPLETE);
        question.setFoundationId(
            user.getFoundation() != null ? user.getFoundation().getId() : null);
        question.setOrganizationId(
            user.getOrganization() != null ? user.getOrganization().getId() : null);
        question.setBody(questionBody.getBody());
        if (chapterName != null && !chapterName.isEmpty()) {
          tags.add(chapterName.replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(0).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(2).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(1).getStringCellValue().replace(" ", "_"));
        }

        question.setTags(String.join(",", tags));

        List<Choices> choicesList = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
          int cellType = row.getCell(i + 4).getCellType();
          if (ExcelUtil.validate(row.getCell(i + 4), cellType)) {
            Choices choice = mapChoiceLabel(cellType, row, i + 4);
            choice.setCorrectOrder(i + 1);
            choicesList.add(choice);
          }
        }

        if (choicesList.isEmpty() || choicesList.size() != questionBody.getSpaces()) {
          continue;
        }

        question.setChoicesList(choicesList);
        questionList.add(question);
      }
    }
    return questionList;
  }

  private List<Question> loadMatchingQuestion(
      Workbook wb, Category category, String chapterName, User user, Long spaceId) {
    Sheet sheet = wb.getSheetAt(0);
    sheet.removeRow(sheet.getRow(0));
    List<Question> questionList = new ArrayList<>();
    for (Row row : sheet) {
      Integer order = 0;
      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)
          && row.getCell(3).getStringCellValue().trim().length() < 250) {
        Question question = new Question();

        Set<String> tags = new HashSet<>();
        question.setCategoryId(category.getId());
        question.setOwnerId(user.getId());
        question.setSpaceId(spaceId);
        question.setQuestionType(QuestionType.MATCHING);
        question.setFoundationId(
            user.getFoundation() != null ? user.getFoundation().getId() : null);
        question.setOrganizationId(
            user.getOrganization() != null ? user.getOrganization().getId() : null);
        question.setBody(row.getCell(3).getStringCellValue());
        if (chapterName != null && !chapterName.isEmpty()) {
          tags.add(chapterName.replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(0).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(2).getStringCellValue().replace(" ", "_"));
        }

        if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
          tags.add(row.getCell(1).getStringCellValue().replace(" ", "_"));
        }

        question.setTags(String.join(",", tags));

        List<Choices> choicesList = new ArrayList<>();
        for (int i = 4; i < 16; i = i + 2) {

          int iCellType = row.getCell(i).getCellType();
          int i1CellType = row.getCell(i + 1).getCellType();

          if (ExcelUtil.validate(row.getCell(i), iCellType)
              && ExcelUtil.validate(row.getCell(i + 1), i1CellType)) {

            order++;
            Choices choiceL = mapChoiceLabel(iCellType, row, i);
            Choices choiceR = mapChoiceLabel(i1CellType, row, i + 1);

            choiceL.setCorrectOrder(order);
            choiceR.setCorrectOrder(order);

            choiceL.setPairCol(PairColumn.LEFT);
            choiceR.setPairCol(PairColumn.RIGHT);

            choicesList.add(choiceL);
            choicesList.add(choiceR);
          }
        }
        if (choicesList.isEmpty()) {
          continue;
        }

        question.setChoicesList(choicesList);
        questionList.add(question);
      }
    }
    return questionList;
  }

  private List<Question> loadTrueFalseQuestion(
      Workbook wb, Category category, String chapterName, User user, Long spaceId) {
    Sheet sheet = wb.getSheetAt(0);

    sheet.removeRow(sheet.getRow(0));
    FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
    List<Question> questionList = new ArrayList<>();

    for (Row row : sheet) {
      if (ExcelUtil.validate(row.getCell(3), Cell.CELL_TYPE_STRING)
          && row.getCell(3).getStringCellValue().trim().length() < 250) {
        if (ExcelUtil.validate(row.getCell(7), Cell.CELL_TYPE_FORMULA)) {
          Question question = new Question();
          Set<String> tags = new HashSet<>();
          question.setSpaceId(spaceId);
          question.setCategoryId(category.getId());
          question.setOwnerId(user.getId());
          question.setQuestionType(QuestionType.TRUE_FALSE);
          question.setFoundationId(
              user.getFoundation() != null ? user.getFoundation().getId() : null);
          question.setOrganizationId(
              user.getOrganization() != null ? user.getOrganization().getId() : null);
          question.setBody(row.getCell(3).getStringCellValue());
          if (chapterName != null && !chapterName.isEmpty()) {
            tags.add(chapterName.replace(" ", "_"));
          }

          if (ExcelUtil.validate(row.getCell(0), Cell.CELL_TYPE_STRING)) {
            tags.add(row.getCell(0).getStringCellValue().replace(" ", "_"));
          }

          if (ExcelUtil.validate(row.getCell(2), Cell.CELL_TYPE_STRING)) {
            tags.add(row.getCell(2).getStringCellValue().replace(" ", "_"));
          }

          if (ExcelUtil.validate(row.getCell(1), Cell.CELL_TYPE_STRING)) {
            tags.add(row.getCell(1).getStringCellValue().replace(" ", "_"));
          }
          question.setTags(String.join(",", tags));

          Boolean answer = evaluator.evaluate(row.getCell(7)).getBooleanValue();
          question.setCorrectAnswer(answer.toString());

          questionList.add(question);
        }
      }
    }
    return questionList;
  }

  public Choices mapChoiceLabel(int cellType, Row row, int cellIndex) {
    Choices choice = new Choices();

    switch (cellType) {
      case Cell.CELL_TYPE_STRING:
        choice.setLabel(row.getCell(cellIndex).getStringCellValue());
        break;
      case Cell.CELL_TYPE_NUMERIC:
        choice.setLabel(Double.toString(row.getCell(cellIndex).getNumericCellValue()));
        break;
    }
    return choice;
  }

  public String mapValue(int cellType, Row row, int cellIndex) {
    String value = "";
    switch (cellType) {
      case Cell.CELL_TYPE_STRING:
        value = row.getCell(cellIndex).getStringCellValue();
        break;
      case Cell.CELL_TYPE_NUMERIC:
        value = Double.toString(row.getCell(cellIndex).getNumericCellValue());
        break;
    }
    return value;
  }

  public QuestionBody addSpaceToken(String questionBody) {
    String[] temp = questionBody.split("_");
    return new QuestionBody(String.join("        ", temp), temp.length - 1);
  }

  class QuestionBody {
    private String body;
    private int spaces = 0;

    public QuestionBody(String body, int spaces) {
      this.body = body;
      this.spaces = spaces;
    }

    public String getBody() {
      return body;
    }

    public int getSpaces() {
      return spaces;
    }
  }
}
