package com.eshraqgroup.mint.question.models.question;

import java.util.Arrays;
import com.eshraqgroup.mint.constants.QuestionType;

/** Created by ayman on 14/07/16. */
public class QuestionSearchModel {

  private QuestionType[] questionType = QuestionType.values();
  private String body = "";
  private Long categoryId;
  private Long spaceId;
  private String[] exclude;
  private Integer limit = 10;
  private String[] tags;

  public QuestionType[] getQuestionType() {
    return questionType;
  }

  public void setQuestionType(QuestionType[] questionType) {
    this.questionType = questionType;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public String[] getExclude() {
    return exclude;
  }

  public void setExclude(String[] exclude) {
    this.exclude = exclude;
  }

  public String[] getTags() {
    return tags;
  }

  public void setTags(String[] tags) {
    this.tags = tags;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  @Override
  public String toString() {
    return String.format(
        "QuestionSearchModel [questionType=%s, body=%s, categoryId=%s, spaceId=%s, exclude=%s, limit=%s, tags=%s]",
        Arrays.toString(questionType),
        body,
        categoryId,
        spaceId,
        Arrays.toString(exclude),
        limit,
        Arrays.toString(tags));
  }
}
