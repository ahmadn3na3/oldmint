package com.eshraqgroup.mint.question.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.eshraqgroup.mint.constants.QuestionType;
import com.eshraqgroup.mint.constants.SortField;
import com.eshraqgroup.mint.controller.abstractcontroller.AbstractController;
import com.eshraqgroup.mint.models.PageRequestModel;
import com.eshraqgroup.mint.models.PageResponseModel;
import com.eshraqgroup.mint.models.ResponseModel;
import com.eshraqgroup.mint.question.models.question.QuestionCreateModel;
import com.eshraqgroup.mint.question.models.question.QuestionSearchModel;
import com.eshraqgroup.mint.question.services.QuestionService;
import com.eshraqgroup.mint.question.services.UploadService;
import io.swagger.annotations.ApiOperation;

/** Created by ayman on 13/06/16. */
@RestController
@RequestMapping("/api/question")
public class QuestionController extends AbstractController<QuestionCreateModel, String> {

  @Autowired QuestionService questionService;

  @Autowired UploadService uploadService;

  @RequestMapping(method = RequestMethod.POST)
  @Override
  @ApiOperation(value = "Create Question", notes = "this method is used to create new question")
  public ResponseModel create(@RequestBody @Validated QuestionCreateModel createModel) {
    return questionService.createQuestion(createModel);
  }

  @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
  @Override
  @ApiOperation(
    value = "Update Question",
    notes = "this method is used to update question by question id"
  )
  public ResponseModel update(
      @PathVariable String id, @RequestBody @Validated QuestionCreateModel updateModel) {
    return questionService.updateQuestion(id, updateModel);
  }

  @RequestMapping(path = "/{id}", method = RequestMethod.GET)
  @ApiOperation(value = "Get Question", notes = "this method is used to get question by id")
  public ResponseModel get(
      @PathVariable String id, @RequestHeader(required = false, defaultValue = "en") String lang) {
    return questionService.getQuestion(id, lang);
  }

  @RequestMapping(method = RequestMethod.GET)
  @ApiOperation(
    value = "Get All Questions",
    notes = "this method is used to list all questions of user"
  )
  public PageResponseModel get(
      @RequestHeader(required = false) Integer page,
      @RequestHeader(required = false) Integer size,
      @RequestHeader(required = false, defaultValue = "CREATION_DATE") SortField field,
      @RequestHeader(required = false, defaultValue = "ASC") Sort.Direction direction,
      @RequestParam(required = false) String filter,
      @RequestParam(required = false) String[] tag,
      @RequestParam(required = false) Long categoryId,
      @RequestHeader(required = false, defaultValue = "en") String lang) {

    return questionService.getAllQuestion(
        PageRequestModel.getPageRequestModel(page, size, new Sort(direction, field.getFieldName())),
        tag,
        filter,
        categoryId,
        lang,
        direction);
  }

  @RequestMapping(method = RequestMethod.DELETE)
  @ApiOperation(
    value = "Delete list of Question",
    notes = "this method is used to delete question by list of ids "
  )
  public ResponseModel delete(@RequestBody List<String> id) {
    return questionService.deleteQuestion(id);
  }

  @Override
  @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
  @ApiOperation(value = "Delete Question", notes = "this method is used to delete question by id ")
  public ResponseModel delete(@PathVariable String id) {
    return questionService.deleteQuestion(id);
  }

  @RequestMapping(path = "/search", method = RequestMethod.POST)
  @ApiOperation(
    value = "Search for a Question",
    notes = "this method is used to search question by question search model"
  )
  public PageResponseModel search(
      @RequestBody @Validated QuestionSearchModel questionSearchModel,
      @RequestHeader(required = false) Integer page,
      @RequestHeader(required = false) Integer size,
      @RequestHeader(required = false, defaultValue = "en") String lang) {
    return questionService.searchByCriteria(
        questionSearchModel, PageRequestModel.getPageRequestModel(page, size), lang);
  }

  @RequestMapping(
    path = "/upload",
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE,
    method = RequestMethod.POST
  )
  @ApiOperation(
    value = "Upload Questions",
    notes = "this method is used to upload questions using data files "
  )
  public ResponseModel uploadDataFile(
      @RequestParam("dataFile") MultipartFile dataFile,
      @RequestParam("questiontype") QuestionType questionType,
      @RequestParam("categoryId") Long categoryId,
      @RequestParam(name = "spaceId", required = false) Long spaceId,
      @RequestParam(name = "tags", required = false) String tags,
      HttpServletRequest request) {

    return uploadService.uploadDataFile(dataFile, questionType, categoryId, tags, spaceId);
  }
}
