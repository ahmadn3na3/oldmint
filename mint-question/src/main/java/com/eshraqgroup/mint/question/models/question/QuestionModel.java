package com.eshraqgroup.mint.question.models.question;

import java.time.ZonedDateTime;
import java.util.Arrays;
import com.eshraqgroup.mint.domain.mongo.Choices;
import com.eshraqgroup.mint.models.SimpleModel;
import com.eshraqgroup.mint.question.domain.Question;
import com.eshraqgroup.mint.question.models.question.choices.ChoicesModel;
import com.eshraqgroup.mint.util.DateConverter;

/** Created by ayman on 13/06/16. */
public class QuestionModel extends QuestionCreateModel {
  private String id;

  private ZonedDateTime creationDate;
  private ZonedDateTime lastModifiedDate;
  private String lastModifiedBy;
  private SimpleModel category;
  private SimpleModel owner;

  public QuestionModel() {}

  public QuestionModel(Question q) {
    this.setId(q.getId());
    this.setBody(q.getBody());
    this.setPublic(q.getPublic());
    this.setQuestionType(q.getQuestionType());
    this.setCorrectAnswer(q.getCorrectAnswer());
    this.setBodyResourceUrl(q.getBodyResourceUrl());
    this.setCategoryId(q.getCategoryId());
    this.setOwnerId(q.getOwnerId());
    this.setSpaceId(q.getSpaceId());
    this.setCreationDate(DateConverter.convertDateToZonedDateTime(q.getCreationDate()));
    this.setLastModifiedDate(DateConverter.convertDateToZonedDateTime(q.getLastModifiedDate()));
    this.setLastModifiedBy(q.getLastModifiedBy());
    if (q.getTags() != null) {
      this.getTags().addAll(Arrays.asList(q.getTags().split(",")));
    }

    if (q.getChoicesList() != null) {

      for (Choices choice : q.getChoicesList()) {
        ChoicesModel choicesModel = new ChoicesModel();
        choicesModel.setId(choice.getId());
        choicesModel.setCorrectAnswer(choice.getCorrectAnswer());
        choicesModel.setCorrectAnswerResourceUrl(choice.getCorrectAnswerResourceUrl());
        choicesModel.setCorrectOrder(choice.getCorrectOrder());
        choicesModel.setLabel(choice.getLabel());
        choicesModel.setCorrectAnswerDescription(choice.getCorrectAnswerDescription());
        choicesModel.setPairColumn(choice.getPairCol());
        this.getChoicesList().add(choicesModel);
      }
    }
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ZonedDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(ZonedDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public ZonedDateTime getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }

  public String getLastModifiedBy() {
    return lastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    this.lastModifiedBy = lastModifiedBy;
  }

  public SimpleModel getCategory() {
    return category;
  }

  public void setCategory(SimpleModel category) {
    this.category = category;
  }

  public SimpleModel getOwner() {
    return owner;
  }

  public void setOwner(SimpleModel owner) {
    this.owner = owner;
  }

  @Override
  public String toString() {
    return "QuestionModel{"
        + "id="
        + id
        + ", creationDate="
        + creationDate
        + ", lastModifiedDate="
        + lastModifiedDate
        + ", lastModifiedBy='"
        + lastModifiedBy
        + '\''
        + "} "
        + super.toString();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    QuestionModel that = (QuestionModel) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }
}
