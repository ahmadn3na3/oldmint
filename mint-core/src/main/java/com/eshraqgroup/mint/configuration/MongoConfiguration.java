package com.eshraqgroup.mint.configuration;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import com.eshraqgroup.mint.configuration.oauth2.OAuth2AuthenticationReadConverter;
import com.eshraqgroup.mint.util.JSR310DateConverters;

/** Created by ahmad on 3/8/17. */
@Configuration
public class MongoConfiguration {

  @Bean(name = "customConversionsMongo")
  public CustomConversions customConversions() {
    List<Converter<?, ?>> converters = new ArrayList<>();
    converters.add(new OAuth2AuthenticationReadConverter());
    converters.add(JSR310DateConverters.DateToZonedDateTimeConverter.INSTANCE);
    converters.add(JSR310DateConverters.ZonedDateTimeToDateConverter.INSTANCE);
    converters.add(JSR310DateConverters.DateToLocalDateConverter.INSTANCE);
    converters.add(JSR310DateConverters.LocalDateToDateConverter.INSTANCE);
    converters.add(JSR310DateConverters.DateToLocalDateTimeConverter.INSTANCE);
    converters.add(JSR310DateConverters.LocalDateTimeToDateConverter.INSTANCE);
    return new CustomConversions(converters);
  }
}
