package com.eshraqgroup.mint.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.stereotype.Component;

/** Created by ahmad on 4/7/16. */
@Component
public class MintOauthExceptionTranslator implements WebResponseExceptionTranslator {
  private final Logger log = LoggerFactory.getLogger(MintOauthExceptionTranslator.class);

  @Override
  public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
    log.error("error in OAUTH ", e);
    Exception exception2 = e;
    if (e instanceof OAuth2Exception || e.getCause() instanceof OAuth2Exception) {
      OAuth2Exception oAuth2Exception = null;
      if (e.getCause() instanceof OAuth2Exception) {
        oAuth2Exception = (OAuth2Exception) e.getCause();
      } else {
        oAuth2Exception = (OAuth2Exception) e;
      }

      switch (oAuth2Exception.getOAuth2ErrorCode()) {
        case OAuth2Exception.INVALID_CLIENT:
          oAuth2Exception.addAdditionalInformation("code", "32");
          break;
        case OAuth2Exception.INVALID_GRANT:
          oAuth2Exception.addAdditionalInformation("code", "31");
          break;
        case OAuth2Exception.INVALID_TOKEN:
          oAuth2Exception.addAdditionalInformation("code", "33");
          break;
        case OAuth2Exception.INVALID_SCOPE:
          oAuth2Exception.addAdditionalInformation("code", "34");
          break;
        case OAuth2Exception.INSUFFICIENT_SCOPE:
          oAuth2Exception.addAdditionalInformation("code", "34");
          break;
        case "user_not_activated":
          oAuth2Exception.addAdditionalInformation("code", "37");
          break;
        case "user_expired":
          oAuth2Exception.addAdditionalInformation("code", "36");
          break;
        case "user_disabled":
          oAuth2Exception.addAdditionalInformation("code", "38");
          break;
      }

      return ResponseEntity.status(oAuth2Exception.getHttpErrorCode()).body(oAuth2Exception);
    }
    return ResponseEntity.status(500).body(new OAuth2Exception(e.getMessage(), e));
  }
}
