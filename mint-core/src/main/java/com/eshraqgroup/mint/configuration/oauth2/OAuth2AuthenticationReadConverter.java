package com.eshraqgroup.mint.configuration.oauth2;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.security.CurrentUserDetail;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Converter to deserialize back into an OAuth2Authentication Object made necessary because Spring
 * Mongo can't map clientAuthentication to authorizationRequest.
 */
@ReadingConverter
@Component
public class OAuth2AuthenticationReadConverter
    implements Converter<DBObject, OAuth2Authentication> {
  private final Logger log = LoggerFactory.getLogger(OAuth2AuthenticationReadConverter.class);

  @Override
  public OAuth2Authentication convert(DBObject source) {
    DBObject storedRequest = (DBObject) source.get("storedRequest");
    OAuth2Request oAuth2Request =
        new OAuth2Request(
            (Map<String, String>) storedRequest.get("requestParameters"),
            (String) storedRequest.get("clientId"),
            null,
            true,
            new HashSet((List) storedRequest.get("scope")),
            null,
            null,
            null,
            null);

    DBObject userAuthorization = (DBObject) source.get("userAuthentication");
    Object principal =
        getPrincipalObject(userAuthorization.get("principal"), oAuth2Request.getClientId());
    Authentication userAuthentication =
        new UsernamePasswordAuthenticationToken(
            principal,
            userAuthorization.get("credentials"),
            getAuthorities((List) userAuthorization.get("authorities")));

    return new OAuth2Authentication(oAuth2Request, userAuthentication);
  }

  private Object getPrincipalObject(Object principal, String clientId) {
    if (principal instanceof DBObject) {
      DBObject principalDBObject = (DBObject) principal;

      Long id = (Long) principalDBObject.get("_id");
      Long orgId = (Long) principalDBObject.get("organizationId");
      Long foundId = (Long) principalDBObject.get("foundationId");
      UserType type = UserType.valueOf(principalDBObject.get("type").toString());
      String userName = (String) principalDBObject.get("username");
      String password = "";
      String fullName = (String) principalDBObject.get("fullName");
      String email = (String) principalDBObject.get("email");
      String image = (String) principalDBObject.get("image");
      String chatId = (String) principalDBObject.get("chatId");
      boolean enabled = (boolean) principalDBObject.get("enabled");
      boolean accountNonExpired = (boolean) principalDBObject.get("accountNonExpired");
      boolean credentialsNonExpired = (boolean) principalDBObject.get("credentialsNonExpired");
      boolean accountNonLocked = (boolean) principalDBObject.get("accountNonLocked");
      CurrentUserDetail currentUserDetail =
          new CurrentUserDetail(
              id,
              userName,
              password,
              enabled,
              accountNonExpired,
              credentialsNonExpired,
              accountNonLocked,
              Collections.EMPTY_LIST,
              fullName,
              email,
              image,
              orgId,
              foundId,
              type,
              chatId);
      currentUserDetail.setCurrentClientId(clientId);
      return currentUserDetail;
    } else {
      return principal;
    }
  }

  private Collection<GrantedAuthority> getAuthorities(List authorities) {
    Set<GrantedAuthority> grantedAuthorities = new HashSet<>(authorities.size());
    for (Object authority : authorities) {
      log.trace("authority ==> {}", authority);
      if (authority.getClass().equals(BasicDBObject.class)) {
        BasicDBObject dbObject = (BasicDBObject) authority;
        grantedAuthorities.add(new SimpleGrantedAuthority((String) dbObject.get("role")));
      } else {
        grantedAuthorities.add(new SimpleGrantedAuthority(authority.toString()));
      }
    }
    return grantedAuthorities;
  }
}
