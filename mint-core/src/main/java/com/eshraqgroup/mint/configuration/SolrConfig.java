package com.eshraqgroup.mint.configuration;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.convert.CustomConversions;

/** Created by ahmad on 5/4/17. */
@Configuration
public class SolrConfig {
  @Bean(name = "customConversions")
  public CustomConversions solrCustomConversions() {
    return new CustomConversions(Collections.emptyList());
  }
}
