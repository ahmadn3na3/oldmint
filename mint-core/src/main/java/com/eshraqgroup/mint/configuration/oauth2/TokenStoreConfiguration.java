package com.eshraqgroup.mint.configuration.oauth2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
import org.springframework.security.oauth2.provider.token.store.redis.StandardStringSerializationStrategy;
import com.eshraqgroup.mint.repos.mongo.OAuth2AccessTokenRepository;
import com.eshraqgroup.mint.repos.mongo.OAuth2RefreshTokenRepository;

/** Created by ahmad on 7/13/16. */
@Configuration
public class TokenStoreConfiguration {
  @Autowired private OAuth2AccessTokenRepository oAuth2AccessTokenRepository;

  @Autowired private OAuth2RefreshTokenRepository oAuth2RefreshTokenRepository;

  @Bean
  public TokenStore tokenStore(RedisConnectionFactory connectionFactory) {
    RedisTokenStore tokenStore =
        new RedisTokenStore(connectionFactory) {
          @Override
          public void storeAccessToken(
              OAuth2AccessToken token, OAuth2Authentication authentication) {
            findTokensByClientIdAndUserName(
                    authentication.getOAuth2Request().getClientId(), authentication.getName())
                .forEach(
                    oAuth2AccessToken -> {
                      if (oAuth2AccessToken.getRefreshToken() != null) {
                        removeRefreshToken(oAuth2AccessToken.getRefreshToken());
                      }
                      removeAccessToken(oAuth2AccessToken);
                    });
            if (readAccessToken(token.getValue()) != null) {
              removeAccessToken(token);
            }
            super.storeAccessToken(token, authentication);
          }
        };

    tokenStore.setAuthenticationKeyGenerator(new UniqueAuthenticationKeyGenerator());
    tokenStore.setSerializationStrategy(
        new StandardStringSerializationStrategy() {

          private final JdkSerializationRedisSerializer OBJECT_SERIALIZER =
              new JdkSerializationRedisSerializer(TokenStoreConfiguration.class.getClassLoader());

          @Override
          @SuppressWarnings("unchecked")
          protected <T> T deserializeInternal(byte[] bytes, Class<T> clazz) {
            return (T) OBJECT_SERIALIZER.deserialize(bytes);
          }

          @Override
          protected byte[] serializeInternal(Object object) {
            return OBJECT_SERIALIZER.serialize(object);
          }
        });
    return tokenStore;
  }
}
