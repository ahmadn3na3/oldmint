package com.eshraqgroup.mint.domain.solrdomains;

import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.solr.core.mapping.SolrDocument;

/** Created by ahmad on 5/25/17. */
@SolrDocument(solrCoreName = "question")
public class QuestionSolrDomain {
  @Id private String id;

  @Field("body")
  private String body;

  @Field("tags")
  private Set<String> tags = new HashSet<>();

  @Field("userId")
  private Long userId;

  @Field("categoryId")
  private Long categoryId;

  @Field("type")
  private String type;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Set<String> getTags() {
    return tags;
  }

  public void setTags(Set<String> tags) {
    this.tags = tags;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    QuestionSolrDomain that = (QuestionSolrDomain) o;

    return id.equals(that.id);
  }

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public String toString() {
    return "QuestionSolrDomain{"
        + "id='"
        + id
        + '\''
        + ", body='"
        + body
        + '\''
        + ", tags="
        + tags
        + ", userId="
        + userId
        + ", categoryId="
        + categoryId
        + ", type='"
        + type
        + '\''
        + '}';
  }
}
