package com.eshraqgroup.mint.domain.mongo;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.oauth2.provider.approval.Approval;

/** Created by ahmad on 7/3/17. */
@Document(collection = "OAUTH_APPROVAL")
public class OauthApproval {
  @Id private String id;
  private String userId;
  private String clientId;
  private String scope;
  private Approval.ApprovalStatus status;
  private Date expiresAt;
  private Date lastUpdatedAt;

  public OauthApproval() {}

  public OauthApproval(Approval approval) {
    this.userId = approval.getUserId();
    this.clientId = approval.getClientId();
    this.scope = approval.getScope();
    this.status = approval.getStatus();
    this.expiresAt = approval.getExpiresAt();
    this.lastUpdatedAt = approval.getLastUpdatedAt();
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public Approval.ApprovalStatus getStatus() {
    return status;
  }

  public void setStatus(Approval.ApprovalStatus status) {
    this.status = status;
  }

  public Date getExpiresAt() {
    return expiresAt;
  }

  public void setExpiresAt(Date expiresAt) {
    this.expiresAt = expiresAt;
  }

  public Date getLastUpdatedAt() {
    return lastUpdatedAt;
  }

  public void setLastUpdatedAt(Date lastUpdatedAt) {
    this.lastUpdatedAt = lastUpdatedAt;
  }
}
