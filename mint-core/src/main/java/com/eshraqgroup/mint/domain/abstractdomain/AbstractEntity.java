package com.eshraqgroup.mint.domain.abstractdomain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.eshraqgroup.mint.security.SecurityUtils;
import com.eshraqgroup.mint.util.DateConverter;

/** Created by ahmad on 2/24/16. */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate = new Date();

  @Column
  private String createBy =
      SecurityUtils.getCurrentUserLogin() == null ? "System" : SecurityUtils.getCurrentUserLogin();

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date lastModifiedDate = null;

  @Column private String lastModifiedBy;
  @Column private boolean deleted;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date deletedDate;

  @Column private String deletedBy;

  // TODO:make (unique = true, nullable = true, length = 40)
  @Column private String uid = UUID.randomUUID().toString();

  protected AbstractEntity() {}

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }

  public Date getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(Date lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }

  public String getLastModifiedBy() {
    return lastModifiedBy;
  }

  public void setLastModifiedBy(String lastModifiedBy) {
    this.lastModifiedBy = lastModifiedBy;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  public Date getDeletedDate() {
    return deletedDate;
  }

  public void setDeletedDate(Date deletedDate) {
    this.deletedDate = deletedDate;
  }

  public String getDeletedBy() {
    return deletedBy;
  }

  public void setDeletedBy(String deletedBy) {
    this.deletedBy = deletedBy;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  @PrePersist
  public void prePersist() {
    setCreateBy(
        SecurityUtils.getCurrentUserLogin() == null
            ? "System"
            : SecurityUtils.getCurrentUserLogin());
    setCreationDate(DateConverter.convertZonedDateTimeToDate(ZonedDateTime.now()));
  }

  @PreUpdate
  public void preUpdate() {
    setLastModifiedBy(
        SecurityUtils.getCurrentUserLogin() == null
            ? "System"
            : SecurityUtils.getCurrentUserLogin());
    setLastModifiedDate(DateConverter.convertZonedDateTimeToDate(ZonedDateTime.now()));
  }

  @PreRemove
  public void preRemove() {
    setDeletedBy(
        SecurityUtils.getCurrentUserLogin() == null
            ? "System"
            : SecurityUtils.getCurrentUserLogin());
    setDeletedDate(DateConverter.convertZonedDateTimeToDate(ZonedDateTime.now()));
  }
}
