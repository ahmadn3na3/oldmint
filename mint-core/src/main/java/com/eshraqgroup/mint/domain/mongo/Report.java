package com.eshraqgroup.mint.domain.mongo;

import java.util.Date;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import com.eshraqgroup.mint.constants.ReportReason;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;

/** Created by ahmad on 5/26/16. */
@Document(collection = "mint.report")
public class Report extends AbstractEntity {
  @Id private String id;
  @Indexed private String reporterUserName;
  @Indexed private String reportedUserName;
  @Indexed private Long spaceId;
  @Indexed private ReportReason reportReason;
  @Indexed private Date reportDate;
  private String reportNotes;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getReporterUserName() {
    return reporterUserName;
  }

  public void setReporterUserName(String reporterUserName) {
    this.reporterUserName = reporterUserName;
  }

  public String getReportedUserName() {
    return reportedUserName;
  }

  public void setReportedUserName(String reportedUserName) {
    this.reportedUserName = reportedUserName;
  }

  public ReportReason getReportReason() {
    return reportReason;
  }

  public void setReportReason(ReportReason reportReason) {
    this.reportReason = reportReason;
  }

  public Date getReportDate() {
    return reportDate;
  }

  public void setReportDate(Date reportDate) {
    this.reportDate = reportDate;
  }

  public String getReportNotes() {
    return reportNotes;
  }

  public void setReportNotes(String reportNotes) {
    this.reportNotes = reportNotes;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }
}
