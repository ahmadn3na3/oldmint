package com.eshraqgroup.mint.domain.jpa;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import com.eshraqgroup.mint.constants.PairColumn;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;

/** Created by ayman on 29/06/16. */
@Entity
@Table(name = "assessment_question_choice")
@DynamicInsert
@DynamicUpdate
public class AssessmentQuestionChoice extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(
    name = "assessment_question",
    nullable = false,
    foreignKey = @ForeignKey(name = "FK_ASSESSMENT_QUESTION")
  )
  private AssessmentQuestion assessmentQuestion;

  @Column private String label;

  @Column private Boolean correctAnswer;

  @Column private Integer correctOrder;

  @Column private String correctAnswerResourceUrl;

  @Column private String correctAnswerDescription;

  @Enumerated private PairColumn pairCol;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public AssessmentQuestion getAssessmentQuestion() {
    return assessmentQuestion;
  }

  public void setAssessmentQuestion(AssessmentQuestion assessmentQuestion) {
    this.assessmentQuestion = assessmentQuestion;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Boolean getCorrectAnswer() {
    return correctAnswer;
  }

  public void setCorrectAnswer(Boolean correctAnswer) {
    this.correctAnswer = correctAnswer;
  }

  public Integer getCorrectOrder() {
    return correctOrder;
  }

  public void setCorrectOrder(Integer correctOrder) {
    this.correctOrder = correctOrder;
  }

  public String getCorrectAnswerResourceUrl() {
    return correctAnswerResourceUrl;
  }

  public void setCorrectAnswerResourceUrl(String correctAnswerResourceUrl) {
    this.correctAnswerResourceUrl = correctAnswerResourceUrl;
  }

  public PairColumn getPairCol() {
    return pairCol;
  }

  public void setPairCol(PairColumn pairCol) {
    this.pairCol = pairCol;
  }

  public String getCorrectAnswerDescription() {
    return correctAnswerDescription;
  }

  public void setCorrectAnswerDescription(String correctAnswerDescription) {
    this.correctAnswerDescription = correctAnswerDescription;
  }

  @Override
  public String toString() {
    return "AssessmentQuestionChoice{"
        + "id="
        + id
        + ", assessmentQuestion="
        + assessmentQuestion
        + ", label='"
        + label
        + '\''
        + ", correctAnswer="
        + correctAnswer
        + ", correctOrder="
        + correctOrder
        + ", correctAnswerResourceUrl='"
        + correctAnswerResourceUrl
        + '\''
        + ", pairCol="
        + pairCol
        + "} "
        + super.toString();
  }
}
