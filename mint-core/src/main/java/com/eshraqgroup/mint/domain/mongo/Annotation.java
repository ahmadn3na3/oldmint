package com.eshraqgroup.mint.domain.mongo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import com.eshraqgroup.mint.constants.AnnotationType;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;
import com.eshraqgroup.mint.models.annotation.AnnotationPoint;

/** Created by ayman on 0v1/08/16. */
@Document(collection = "mint.annotations")
public class Annotation extends AbstractEntity {
  @Id private String id;

  @Indexed private Long contentId;

  @Indexed private Long spaceId;

  @Indexed private Long userId;

  private String userName;

  private String userFullName;

  private Boolean isPublic = Boolean.FALSE;

  private String userImage;

  @Indexed private AnnotationType annotationType;

  // private PositionModel position;

  private Double startX;
  private Double startY;
  private Double endX;
  private Double endY;
  private Integer startIndex;
  private Integer endIndex;
  private Float pageScale;

  private LinkedList<AnnotationPoint> freeHandPointList;

  @Indexed private Integer pageNumber;

  private String color;

  private String audioUrl;

  private String textBody;

  private Integer numberOfLikes = 0;

  @DBRef private List<Comment> comments = new ArrayList<>();

  @DBRef private List<Like> likes = new ArrayList<>();

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Long getContentId() {
    return contentId;
  }

  public void setContentId(Long contentId) {
    this.contentId = contentId;
  }

  public Boolean getIsPublic() {
    return isPublic;
  }

  public void setIsPublic(Boolean aPublic) {
    isPublic = aPublic;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public AnnotationType getAnnotationType() {
    return annotationType;
  }

  public void setAnnotationType(AnnotationType annotationType) {
    this.annotationType = annotationType;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getAudioUrl() {
    return audioUrl;
  }

  public void setAudioUrl(String audioUrl) {
    this.audioUrl = audioUrl;
  }

  public String getTextBody() {
    return textBody;
  }

  public void setTextBody(String textBody) {
    this.textBody = textBody;
  }

  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer pageNumber) {
    this.pageNumber = pageNumber;
  }

  public List<Comment> getComments() {
    return comments;
  }

  public void setComments(List<Comment> comments) {
    this.comments = comments;
  }

  public List<Like> getLikes() {
    return likes;
  }

  public void setLikes(List<Like> likes) {
    this.likes = likes;
  }

  public Integer getNumberOfLikes() {
    return numberOfLikes;
  }

  public void setNumberOfLikes(Integer numberOfLikes) {
    this.numberOfLikes = numberOfLikes;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Double getStartX() {
    return startX;
  }

  public void setStartX(Double startX) {
    this.startX = startX;
  }

  public Boolean getPublic() {
    return isPublic;
  }

  public void setPublic(Boolean aPublic) {
    isPublic = aPublic;
  }

  public Double getStartY() {
    return startY;
  }

  public void setStartY(Double startY) {
    this.startY = startY;
  }

  public Double getEndX() {
    return endX;
  }

  public void setEndX(Double endX) {
    this.endX = endX;
  }

  public Double getEndY() {
    return endY;
  }

  public void setEndY(Double endY) {
    this.endY = endY;
  }

  public Integer getStartIndex() {
    return startIndex;
  }

  public void setStartIndex(Integer startIndex) {
    this.startIndex = startIndex;
  }

  public Integer getEndIndex() {
    return endIndex;
  }

  public void setEndIndex(Integer endIndex) {
    this.endIndex = endIndex;
  }

  public String getUserFullName() {
    return userFullName;
  }

  public void setUserFullName(String userFullName) {
    this.userFullName = userFullName;
  }

  public LinkedList<AnnotationPoint> getFreeHandPointList() {
    return freeHandPointList;
  }

  public void setFreeHandPointList(LinkedList<AnnotationPoint> freeHandPointList) {
    this.freeHandPointList = freeHandPointList;
  }

  public String getUserImage() {
    return userImage;
  }

  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  public Float getPageScale() {
    return pageScale;
  }

  public void setPageScale(Float pageScale) {
    this.pageScale = pageScale;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Annotation that = (Annotation) o;

    if (id != null ? !id.equals(that.id) : that.id != null) return false;
    if (!contentId.equals(that.contentId)) return false;
    if (!spaceId.equals(that.spaceId)) return false;
    if (!userId.equals(that.userId)) return false;
    return annotationType != that.annotationType;
  }

  @Override
  public int hashCode() {
    int result = id != null ? id.hashCode() : 0;
    result = 31 * result + contentId.hashCode();
    result = 31 * result + spaceId.hashCode();
    result = 31 * result + userId.hashCode();
    result = 31 * result + annotationType.hashCode();
    return result;
  }
}
