package com.eshraqgroup.mint.domain.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;

/** Created by ahmad on 2/17/16. */
@Entity
// @Table(indexes = {
// @Index(columnList = "user_name", name = "user_name_idx",unique = false),
// @Index(columnList = "email", name = "email_idx")
// })
@DynamicInsert
@DynamicUpdate
public class User extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(nullable = false, unique = false)
  private String userName;

  @Column private String thumbnail;

  @Column(nullable = false)
  private String password;

  @Column(nullable = false)
  private String fullName;

  @Column private String mobile;
  @Column private String color;

  @Column(nullable = false)
  private String email;

  @Column private Boolean status = Boolean.FALSE;
  @Column private String langKey;
  @Column private String activationKey;
  @Column private String resetKey;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date resetDate;

  @Column private Boolean firstLogin = Boolean.TRUE;
  @Column private Boolean forceChangePassword = Boolean.FALSE;
  @Column private Boolean gender = null;

  @Column
  @Temporal(TemporalType.DATE)
  private Date birthDate;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date activationDate;

  @Column private String profession;
  @Column private String country;

  @Column private String userStatus;

  @Column(length = 500)
  private String interests;

  @Column @Enumerated private UserType type;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date startDate;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date endDate;

  @ManyToOne
  @JoinColumn(name = "organization", foreignKey = @ForeignKey(name = "FK_USER_ORGANIZATIO"))
  private Organization organization;

  @ManyToOne
  @JoinColumn(name = "foundation_id", foreignKey = @ForeignKey(name = "FK_USER_FOUNDATION"))
  private Foundation foundation;

  @ManyToOne
  @JoinColumn(name = "time_lock", foreignKey = @ForeignKey(name = "FK_USER_TIME_LOCAK"))
  private TimeLock timeLock;

  @ManyToMany(mappedBy = "users")
  private List<Groups> groups = new ArrayList<>();

  @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
  private List<Space> spaces = new ArrayList<>();

  @OneToMany(mappedBy = "owner")
  private List<Content> contents = new ArrayList<>();

  @ManyToMany(mappedBy = "users")
  private Set<Role> roles = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "cloud_package_id", foreignKey = @ForeignKey(name = "FK_USER_PACkAGE"))
  private CloudPackage cloudPackage;

  @Column private Boolean notification = true;

  @Column private Boolean mailNotification = true;

  @Column private String chatId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Boolean getStatus() {
    return status;
  }

  public void setStatus(Boolean status) {
    this.status = status;
  }

  public String getLangKey() {
    return langKey;
  }

  public void setLangKey(String langKey) {
    this.langKey = langKey;
  }

  public String getActivationKey() {
    return activationKey;
  }

  public void setActivationKey(String activationKey) {
    this.activationKey = activationKey;
  }

  public String getResetKey() {
    return resetKey;
  }

  public void setResetKey(String resetKey) {
    this.resetKey = resetKey;
  }

  public Date getResetDate() {
    return resetDate;
  }

  public void setResetDate(Date resetDate) {
    this.resetDate = resetDate;
  }

  public Boolean getFirstLogin() {
    return firstLogin;
  }

  public void setFirstLogin(Boolean firstLogin) {
    this.firstLogin = firstLogin;
  }

  public Boolean getForceChangePassword() {
    return forceChangePassword;
  }

  public void setForceChangePassword(Boolean forceChangePassword) {
    this.forceChangePassword = forceChangePassword;
  }

  public Boolean getGender() {
    return gender;
  }

  public void setGender(Boolean gender) {
    this.gender = gender;
  }

  public Date getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(Date birthDate) {
    this.birthDate = birthDate;
  }

  public Date getActivationDate() {
    return activationDate;
  }

  public void setActivationDate(Date activationDate) {
    this.activationDate = activationDate;
  }

  public String getProfession() {
    return profession;
  }

  public void setProfession(String profession) {
    this.profession = profession;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public UserType getType() {
    return type;
  }

  public void setType(UserType type) {
    this.type = type;
  }

  public Organization getOrganization() {
    return organization;
  }

  public void setOrganization(Organization organization) {
    this.organization = organization;
  }

  public List<Space> getSpaces() {
    return spaces;
  }

  public void setSpaces(List<Space> spaces) {
    this.spaces = spaces;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  public List<Groups> getGroups() {
    return groups;
  }

  public void setGroups(List<Groups> groups) {
    this.groups = groups;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public List<Content> getContents() {
    return contents;
  }

  public void setContents(List<Content> contents) {
    this.contents = contents;
  }

  public TimeLock getTimeLock() {
    return timeLock;
  }

  public void setTimeLock(TimeLock timeLock) {
    this.timeLock = timeLock;
  }

  public String getUserStatus() {
    return userStatus;
  }

  public void setUserStatus(String userStatus) {
    this.userStatus = userStatus;
  }

  public String getInterests() {
    return interests;
  }

  public void setInterests(String interests) {
    this.interests = interests;
  }

  public Foundation getFoundation() {
    return foundation;
  }

  public void setFoundation(Foundation foundation) {
    this.foundation = foundation;
  }

  public CloudPackage getCloudPackage() {
    return cloudPackage;
  }

  public void setCloudPackage(CloudPackage cloudPackage) {
    this.cloudPackage = cloudPackage;
  }

  public Boolean getNotification() {
    if (notification == null) {
      return Boolean.FALSE;
    }
    return notification;
  }

  public void setNotification(Boolean notification) {
    if (notification != null) {
      this.notification = notification;
    }
  }

  public Boolean hasMailNotification() {
    return mailNotification;
  }

  public void setMailNotification(Boolean mailNotification) {
    if (mailNotification != null) {
      this.mailNotification = mailNotification;
    }
  }

  public String getChatId() {
    return chatId;
  }

  public void setChatId(String chatId) {
    this.chatId = chatId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if (getId() != null ? !getId().equals(user.getId()) : user.getId() != null) return false;
    if (!getUserName().equals(user.getUserName())) return false;
    return getEmail().equals(user.getEmail());
  }

  @Override
  public int hashCode() {
    int result = getId() != null ? getId().hashCode() : 0;
    result = 31 * result + getUserName().hashCode();
    result = 31 * result + getEmail().hashCode();
    return result;
  }
}
