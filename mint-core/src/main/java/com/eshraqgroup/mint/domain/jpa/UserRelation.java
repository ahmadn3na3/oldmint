package com.eshraqgroup.mint.domain.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
/** Created by ahmad on 5/23/16. */
import com.eshraqgroup.mint.constants.UserRelationType;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;

@Entity
@DynamicInsert
@DynamicUpdate
public class UserRelation extends AbstractEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_USER"))
  private User user;

  @ManyToOne
  @JoinColumn(name = "follow_id", foreignKey = @ForeignKey(name = "FK_FOLLOWER"))
  private User follow;

  @Enumerated private UserRelationType relationType = UserRelationType.FOLLOWER;

  @Column private String groupName;

  @Column private String reason;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public User getFollow() {
    return follow;
  }

  public void setFollow(User follow) {
    this.follow = follow;
  }

  public UserRelationType getRelationType() {
    return relationType;
  }

  public void setRelationType(UserRelationType relationType) {
    this.relationType = relationType;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }
}
