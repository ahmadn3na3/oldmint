package com.eshraqgroup.mint.domain.mongo;

import com.eshraqgroup.mint.constants.PairColumn;

/** Created by ayman on 13/06/16. */
public class Choices {

  private Long id;
  private String label;
  private Boolean correctAnswer = false;
  private Integer correctOrder;
  private String correctAnswerResourceUrl;
  private PairColumn pairCol;
  private String correctAnswerDescription;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public Integer getCorrectOrder() {
    return correctOrder;
  }

  public void setCorrectOrder(Integer correctOrder) {
    this.correctOrder = correctOrder;
  }

  public String getCorrectAnswerResourceUrl() {
    return correctAnswerResourceUrl;
  }

  public void setCorrectAnswerResourceUrl(String correctAnswerResourceUrl) {
    this.correctAnswerResourceUrl = correctAnswerResourceUrl;
  }

  public PairColumn getPairCol() {
    return pairCol;
  }

  public void setPairCol(PairColumn pairCol) {
    this.pairCol = pairCol;
  }

  public Boolean getCorrectAnswer() {
    return correctAnswer;
  }

  public void setCorrectAnswer(Boolean correctAnswer) {
    if (correctAnswer == null) this.correctAnswer = false;
    else this.correctAnswer = correctAnswer;
  }

  public String getCorrectAnswerDescription() {
    return correctAnswerDescription;
  }

  public void setCorrectAnswerDescription(String correctAnswerDescription) {
    this.correctAnswerDescription = correctAnswerDescription;
  }
}
