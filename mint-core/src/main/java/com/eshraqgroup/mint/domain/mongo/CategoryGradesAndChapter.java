package com.eshraqgroup.mint.domain.mongo;

import java.util.HashSet;
import java.util.Set;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;
import com.eshraqgroup.mint.models.category.Chapter;
import com.eshraqgroup.mint.models.category.Grade;

@Document(collection = "CategoryGradesAndChapter")
public class CategoryGradesAndChapter extends AbstractEntity {
  @Id private String id;

  @Field
  @Indexed(name = "categoryId", collection = "CategoryGradesAndChapter")
  private Long categoryId;

  private Set<Grade> grades = new HashSet<>();
  private Set<Chapter> chapters = new HashSet<>();
  private Long userId;

  public CategoryGradesAndChapter() {}

  public CategoryGradesAndChapter(Long categoryId) {
    this.categoryId = categoryId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Set<Grade> getGrades() {
    return grades;
  }

  public void setGrades(Set<Grade> grades) {
    this.grades = grades;
  }

  public Set<Chapter> getChapters() {
    return chapters;
  }

  public void setChapters(Set<Chapter> chapters) {
    this.chapters = chapters;
  }

  public Long getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Long categoryId) {
    this.categoryId = categoryId;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }
}
