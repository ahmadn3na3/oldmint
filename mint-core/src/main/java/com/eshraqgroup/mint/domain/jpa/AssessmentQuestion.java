package com.eshraqgroup.mint.domain.jpa;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import com.eshraqgroup.mint.constants.QuestionType;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;

/** Created by ayman on 29/06/16. */
@Entity
@DynamicInsert
@DynamicUpdate
public class AssessmentQuestion extends AbstractEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "assessment", foreignKey = @ForeignKey(name = "FK_ASSESSMENT"))
  private Assessment assessment;

  @Deprecated @Column
  // TODO: remove after migration
  private Long question;

  @Column private String body;

  @Column private Boolean isPublic = Boolean.TRUE;

  @Enumerated private QuestionType questionType;

  @Column(length = 4000)
  private String correctAnswer;

  @Column private String bodyResourceUrl;

  @OneToMany(mappedBy = "assessmentQuestion", cascade = CascadeType.ALL)
  private Set<AssessmentQuestionChoice> assessmentQuestionChoices = new HashSet<>();

  private Integer questionWeight;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Assessment getAssessment() {
    return assessment;
  }

  public void setAssessment(Assessment assessment) {
    this.assessment = assessment;
  }

  public Integer getQuestionWeight() {
    return questionWeight;
  }

  public void setQuestionWeight(Integer questionWeight) {
    this.questionWeight = questionWeight;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public Boolean getPublic() {
    return isPublic;
  }

  public void setPublic(Boolean aPublic) {
    isPublic = aPublic;
  }

  public QuestionType getQuestionType() {
    return questionType;
  }

  public void setQuestionType(QuestionType questionType) {
    this.questionType = questionType;
  }

  public String getCorrectAnswer() {
    return correctAnswer;
  }

  public void setCorrectAnswer(String correctAnswer) {
    this.correctAnswer = correctAnswer;
  }

  public String getBodyResourceUrl() {
    return bodyResourceUrl;
  }

  public void setBodyResourceUrl(String bodyResourceUrl) {
    this.bodyResourceUrl = bodyResourceUrl;
  }

  public Set<AssessmentQuestionChoice> getAssessmentQuestionChoices() {
    return assessmentQuestionChoices;
  }

  public void setAssessmentQuestionChoices(
      Set<AssessmentQuestionChoice> assessmentQuestionChoices) {
    this.assessmentQuestionChoices = assessmentQuestionChoices;
  }

  public Long getQuestion() {
    return question;
  }

  public void setQuestion(Long question) {
    this.question = question;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AssessmentQuestion that = (AssessmentQuestion) o;

    if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
    return getAssessment().equals(that.getAssessment());
  }

  @Override
  public int hashCode() {
    int result = getId() != null ? getId().hashCode() : 0;
    result = 31 * result + getAssessment().hashCode();
    return result;
  }
}
