package com.eshraqgroup.mint.repos.jpa;

import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.AssessmentQuestionChoice;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ayman on 29/06/16. */
@Repository
public interface AssessmentQuestionChoicesRepository
    extends AbstractRepository<AssessmentQuestionChoice, Long> {}
