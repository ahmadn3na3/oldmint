package com.eshraqgroup.mint.repos.mongo;

import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.mongo.Report;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ahmad on 5/26/16. */
@Repository
public interface ReportRepository extends AbstractMongoRepository<Report, String> {

  Integer countByReportedUserName(String userName);

  Integer countByReporterUserName(String userName);
}
