package com.eshraqgroup.mint.repos.jpa;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.Groups;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ahmad on 3/7/16. */
@Repository
public interface GroupsRepository extends AbstractRepository<Groups, Long> {

  Optional<Groups> findOneByNameAndDeletedFalse(String name);

  Optional<Groups> findOneByIdAndDeletedFalse(Long id);

  Stream<Groups> findByOrganizationIdAndDeletedFalse(Long id);

  Stream<Groups> findByNameInAndDeletedFalse(List<String> name);

  Stream<Groups> findByIdInAndOrganizationInAndDeletedFalse(
      Iterable<Long> idss, Iterable<Organization> organization);
}
