package com.eshraqgroup.mint.repos.jpa;

import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.Category;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ahmad on 3/13/16. */
@Repository
public interface CategoryRepository extends AbstractRepository<Category, Long> {

  Optional<Category> findOneByNameAndOrganizationAndDeletedFalse(
      String name, Organization organization);

  Optional<Category> findOneByNameAndFoundationAndOrganizationIsNullAndDeletedFalse(
      String name, Foundation foundation);

  Optional<Category> findOneByNameAndOrganizationIsNullAndFoundationIsNullAndDeletedFalse(
      String name);

  Stream<Category> findByOrganizationIsNullAndFoundationIsNullAndDeletedFalse();

  Stream<Category> findByFoundationAndDeletedFalse(Foundation foundation);

  Stream<Category> findByOrganizationAndDeletedFalse(Organization organization);

  Optional<Category> findOneByIdAndDeletedFalse(Long id);
}
