package com.eshraqgroup.mint.repos.mongo;

import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.mongo.ContentUser;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ahmad on 7/20/16. */
@Repository
public interface ContentUserRepository extends AbstractMongoRepository<ContentUser, String> {

  Optional<ContentUser> findByUserIdAndContentId(Long userId, Long contentId);
}
