package com.eshraqgroup.mint.repos.jpa;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.constants.AssessmentType;
import com.eshraqgroup.mint.domain.jpa.Assessment;
import com.eshraqgroup.mint.domain.jpa.Space;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ayman on 13/06/16. */
@Repository
public interface AssessmentRepository extends AbstractRepository<Assessment, Long> {

  Integer countByOwnerIdAndSpaceIdAndDeletedFalse(Long ownerId, Long spaceId);

  Optional<Assessment> findOneByIdAndDeletedFalse(Long id);

  Optional<Assessment> findOneByIdAndDeletedFalseAndOwnerAndAssessmentType(
      Long id, User user, AssessmentType assessmentType);

  Stream<Assessment> findBySpaceInAndDeletedFalse(Iterable<Space> spaces);

  Page<Assessment> findBySpaceIdAndDeletedFalseAndOwnerOrPublishTrue(
      Long id, User user, Pageable pageable);

  Stream<Assessment> findBySpaceIdAndDeletedDateAfterAndDeletedTrue(Long id, Date date);

  Stream<Assessment> findBySpaceIdAndLastModifiedDateAfterAndDeletedFalse(
      Long id, Date currentDate);

  Stream<Assessment> findBySpaceIdAndLastModifiedDateIsNullAndCreationDateAfterAndDeletedFalse(
      Long id, Date date);

  @Override
  Page<Assessment> findAll(Pageable page);

  @Query(
      "select a from Assessment a where a.assessmentType=?1 and a.space.id = ?2 and a.deleted=false and (a.publish=true or a.owner.id=?3 )")
  Page<Assessment> findAllByAssessmentType(
      AssessmentType type, Long spaceId, Long userId, Pageable page);

  Stream<Assessment>
      findBySpaceIdAndPublishTrueAndPublishDateNotNullAndDeletedFalseOrderByPublishDateDesc(
          Long spaceId);

  @Query(
      "select count(s), s.assessmentType from Assessment s where s.creationDate between ?1 and ?2 and s.deleted=false and s.space.id=?3 group by s.assessmentType")
  List<Object[]> assessmenttStaticsByTypeAndCreationDateBetween(Date from, Date to, Long spaceId);

  @Query(
      "select count(s), s.assessmentType from Assessment s where s.creationDate between ?1 and ?2 and s.deleted=false and s.owner.id=?3 group by s.assessmentType")
  List<Object[]> assessmentStaticsByOwnerIdTypeAndCreationDateBetween(
      Date from, Date to, Long ownerId);
}
