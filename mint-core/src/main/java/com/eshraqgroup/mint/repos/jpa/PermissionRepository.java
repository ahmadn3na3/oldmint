package com.eshraqgroup.mint.repos.jpa;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.Module;
import com.eshraqgroup.mint.domain.jpa.Permission;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

@Repository
public interface PermissionRepository extends AbstractRepository<Permission, Long> {
  List<Permission> findByTypeInAndDeletedFalse(Collection<UserType> types);

  Stream<Permission> findByModuleInAndDeletedFalse(Collection<Module> modules);

  Permission findByName(String name);
}
