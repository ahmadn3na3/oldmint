package com.eshraqgroup.mint.repos.jpa;

import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.constants.UserRelationType;
import com.eshraqgroup.mint.domain.jpa.UserRelation;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ahmad on 5/23/16. */
@Repository
public interface UserRelationRepository extends AbstractRepository<UserRelation, Long> {
  Optional<UserRelation> findOneByUserUserNameAndFollowIdAndDeletedFalse(
      String userName, Long userId);

  Integer countByUserUserNameAndFollowIdAndDeletedFalse(String userName, Long userId);

  Stream<UserRelation> findByUserIdAndRelationTypeAndDeletedFalse(
      Long userId, UserRelationType relationType);
}
