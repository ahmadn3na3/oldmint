package com.eshraqgroup.mint.repos.jpa;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.CloudPackage;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.User;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;
import springfox.documentation.annotations.Cacheable;

/** Created by ahmad on 2/17/16. Spring Data JPA repository for the User entity. */
@Repository
public interface UserRepository extends AbstractRepository<User, Long> {

  Optional<User> findOneByActivationKeyAndDeletedFalse(String activationKey);

  Optional<User> findOneByResetKeyAndDeletedFalse(String resetKey);

  Optional<User> findOneByEmailAndDeletedFalse(String email);

  Optional<User> findOneByEmailAndDeletedFalseAndOrganizationIsNullAndFoundationIsNull(
      String email);

  Optional<User> findOneByEmailAndDeletedFalseAndOrganizationAndFoundation(
      String email, Organization organization, Foundation foundation);

  Optional<User> findOneByIdAndDeletedFalse(Long Id);

  @Cacheable("users")
  Optional<User> findOneByUserNameAndDeletedFalse(String login);

  Stream<User> findByFoundationAndDeletedFalse(Foundation foundation);

  Stream<User> findByFoundationIdAndDeletedFalse(Long foundationId);

  Stream<User> findByIdInAndOrganizationAndDeletedFalse(
      List<Long> users, Organization organization);

  Integer countByOrganizationAndDeletedFalse(Organization organization);

  Stream<User> findByOrganizationIdAndDeletedFalseAndOrganizationDeletedFalse(Long orgId);

  Stream<User> findByOrganizationIdAndDeletedFalse(Long orgid);

  Stream<User> findByOrganizationIsNullAndFoundationIsNullAndDeletedFalse();

  Page<User> findByOrganizationIsNullAndFoundationIsNullAndDeletedFalse(Pageable pageable);

  Stream<User> findByGroupsIdInAndOrganizationAndDeletedFalse(
      Iterable<Long> ids, Organization organization);

  Stream<User> findByRolesIdInAndOrganizationAndDeletedFalse(
      Iterable<Long> ids, Organization organization);

  Integer countByRolesIdInAndDeletedFalse(Iterable<Long> ids);

  Stream<User> findByIdInAndOrganizationAndTypeAndDeletedFalse(
      Iterable<Long> id, Organization organization, UserType userType);

  Integer countByEmailAndDeletedFalse(String email);

  Integer countByFoundationAndDeletedFalse(Foundation foundation);

  Integer countByCloudPackageAndDeletedFalse(CloudPackage cloudPackage);

  Page<User> findByGroupsIdInAndFoundationAndEmailAndDeletedFalse(
      Iterable<Long> accessedGroups, Foundation organization, String email, Pageable pageable);

  Page<User> findAllByTypeAndDeletedFalse(UserType userType, Pageable pageable);

  Stream<User> findByRolesIdInAndDeletedFalse(Long id);

  Stream<User> findByTimeLockIdAndDeletedFalse(Long timeLockId);

  Stream<User> findByIdInAndDeletedFalse(Iterable<Long> userIdList);

  Stream<User> findByGroupsIdInAndDeletedFalse(Iterable<Long> groupIdList);
}
