package com.eshraqgroup.mint.repos.mongo;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.constants.AnnotationType;
import com.eshraqgroup.mint.domain.mongo.Annotation;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ayman on 02/08/16. */
@Repository
public interface AnnotationRepository extends AbstractMongoRepository<Annotation, String> {
  List<Annotation> findByContentIdAndDeletedFalse(Long contentId);

  List<Annotation> findByContentIdAndPageNumberAndDeletedFalse(Long contentId, Integer PageNumber);

  List<Annotation> findByContentIdAndAnnotationTypeAndDeletedFalse(
      Long contentId, AnnotationType annotationType);

  List<Annotation> findByContentIdAndPageNumberAndAnnotationTypeAndDeletedFalse(
      Long contentId, Integer PageNumber, AnnotationType annotationType);

  Optional<Annotation> findOneByIdAndDeletedFalse(String id);

  Integer countBySpaceIdAndUserId(Long spaceId, Long userId);

  Integer countByContentIdAndDeletedFalse(Long contentId);

  List<Annotation> deleteByContentId(Long contentId);
}
