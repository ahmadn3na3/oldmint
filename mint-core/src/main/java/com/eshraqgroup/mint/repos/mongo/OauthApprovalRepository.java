package com.eshraqgroup.mint.repos.mongo;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.eshraqgroup.mint.domain.mongo.OauthApproval;

/** Created by ahmad on 7/3/17. */
public interface OauthApprovalRepository extends MongoRepository<OauthApproval, String> {
  OauthApproval findByUserIdAndAndClientIdAndScope(String u, String c, String s);

  List<OauthApproval> findByUserIdAndClientId(String u, String c);
}
