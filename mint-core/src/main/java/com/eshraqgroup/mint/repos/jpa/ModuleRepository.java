package com.eshraqgroup.mint.repos.jpa;

import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.Module;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

@Repository
public interface ModuleRepository extends AbstractRepository<Module, Long> {
  Module findOneByKeyCode(String keyCode);
}
