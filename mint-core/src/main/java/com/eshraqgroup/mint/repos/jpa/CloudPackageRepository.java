package com.eshraqgroup.mint.repos.jpa;

import com.eshraqgroup.mint.constants.PackageType;
import com.eshraqgroup.mint.domain.jpa.CloudPackage;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ahmad on 4/19/17. */
public interface CloudPackageRepository extends AbstractRepository<CloudPackage, Long> {
  CloudPackage findByPackageTypeAndNameAndDeletedFalse(PackageType packageType, String name);
}
