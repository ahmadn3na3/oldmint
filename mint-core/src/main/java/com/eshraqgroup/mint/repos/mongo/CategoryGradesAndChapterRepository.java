package com.eshraqgroup.mint.repos.mongo;

import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.mongo.CategoryGradesAndChapter;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ahmad on 5/15/17. */
@Repository
public interface CategoryGradesAndChapterRepository
    extends AbstractMongoRepository<CategoryGradesAndChapter, String> {
  CategoryGradesAndChapter findByCategoryId(Long category);
}
