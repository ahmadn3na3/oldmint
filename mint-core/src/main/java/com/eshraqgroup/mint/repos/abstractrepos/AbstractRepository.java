package com.eshraqgroup.mint.repos.abstractrepos;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;

/** Created by ahmad on 5/3/16. */
@NoRepositoryBean
public interface AbstractRepository<T extends AbstractEntity, ID extends Serializable>
    extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

  @Override
  @Query(
      "update #{#entityName} e set e.deleted=true , e.deletedDate = UTC_TIMESTAMP, e.deletedBy = ?#{principal.username} where e.id=?1")
  @Modifying
  void delete(ID id);

  @Override
  @Query(
      "update #{#entityName} e set e.deleted=true , e.deletedDate = UTC_TIMESTAMP, e.deletedBy = ?#{principal.username}  where e =?1")
  @Modifying
  void delete(T t);

  @Override
  @Query(
      "update #{#entityName} e set e.deleted=true , e.deletedDate = UTC_TIMESTAMP, e.deletedBy = ?#{principal.username} where e in ?1")
  @Modifying
  void delete(Iterable<? extends T> iterable);

  @Override
  @Query("from #{#entityName} e  where e.id = ?1 and e.deleted=false")
  T findOne(ID id);

  @Override
  @Query("from #{#entityName} e  where e.deleted=false")
  List<T> findAll();

  @Override
  @Query("from #{#entityName} e  where e.id in ?1 and e.deleted=false ")
  List<T> findAll(Iterable<ID> iterable);

  @Override
  @Query("from #{#entityName} e  where e.deleted=false")
  Page<T> findAll(Pageable pageable);
}
