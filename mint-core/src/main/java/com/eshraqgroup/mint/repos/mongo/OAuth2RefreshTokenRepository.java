package com.eshraqgroup.mint.repos.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.mongo.OAuth2AuthenticationRefreshToken;

/** Spring Data MongoDB repository for the OAuth2AuthenticationRefreshToken entity. */
@Repository
public interface OAuth2RefreshTokenRepository
    extends MongoRepository<OAuth2AuthenticationRefreshToken, String> {
  OAuth2AuthenticationRefreshToken findByTokenId(String tokenId);
}
