package com.eshraqgroup.mint.repos.jpa;

import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.FoundationPackage;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ahmad on 4/16/17. */
@Repository
public interface FoundationPackageRepository extends AbstractRepository<FoundationPackage, Long> {
  FoundationPackage findByNameAndDeletedFalse(String name);
}
