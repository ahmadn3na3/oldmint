package com.eshraqgroup.mint.repos.mongo;

import java.util.stream.Stream;
import com.eshraqgroup.mint.domain.mongo.UserResources;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ahmad on 3/1/17. */
public interface UserResourcesRepository extends AbstractMongoRepository<UserResources, String> {
  Stream<UserResources> findByUserId(Long UserId);
}
