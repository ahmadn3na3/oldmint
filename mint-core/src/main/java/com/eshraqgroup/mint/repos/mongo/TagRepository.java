package com.eshraqgroup.mint.repos.mongo;

import java.util.stream.Stream;
import com.eshraqgroup.mint.domain.mongo.Tag;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ahmad on 5/9/17. */
public interface TagRepository extends AbstractMongoRepository<Tag, String> {
  Integer countByUserId(Long userId);

  Stream<Tag> findByUserId(Long userId);

  Tag findByUserIdAndName(Long userId, String name);

  Tag findByFoundationIdAndName(Long foundationId, String name);

  Stream<Tag> findByFoundationId(Long foundationId);
}
