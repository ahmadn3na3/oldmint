package com.eshraqgroup.mint.repos.jpa;

import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ayman on 02/06/16. */
@Repository
public interface FoundationRepository extends AbstractRepository<Foundation, Long> {
  Optional<Foundation> findOneByNameAndDeletedFalse(String instituteName);

  Optional<Foundation> findOneByIdAndDeletedFalse(Long Id);

  Optional<Foundation> findOneByCodeAndDeletedFalse(String code);

  Foundation findByNameAndDeletedFalse(String foundationName);
}
