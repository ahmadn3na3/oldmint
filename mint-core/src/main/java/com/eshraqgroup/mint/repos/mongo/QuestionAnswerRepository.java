package com.eshraqgroup.mint.repos.mongo;

import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.mongo.QuestionAnswer;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ayman on 04/07/16. */
@Repository
public interface QuestionAnswerRepository extends AbstractMongoRepository<QuestionAnswer, String> {

  Optional<QuestionAnswer> findOneByUserIdAndQuestionIdAndDeletedFalse(
      Long userId, Long questionId);
}
