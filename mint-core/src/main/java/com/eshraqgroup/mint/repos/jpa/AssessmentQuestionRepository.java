package com.eshraqgroup.mint.repos.jpa;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.Assessment;
import com.eshraqgroup.mint.domain.jpa.AssessmentQuestion;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

/** Created by ayman on 29/06/16. */
@Repository
public interface AssessmentQuestionRepository extends AbstractRepository<AssessmentQuestion, Long> {
  List<AssessmentQuestion> findByAssessmentAndDeletedFalse(Assessment assessment);

  Long countByAssessmentIdAndDeletedFalse(Long assessmentId);
}
