package com.eshraqgroup.mint.repos.mongo;

import java.util.List;
import java.util.Optional;
import com.eshraqgroup.mint.constants.CommentType;
import com.eshraqgroup.mint.domain.mongo.Comment;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ayman on 25/08/16. */
public interface CommentRepository extends AbstractMongoRepository<Comment, String> {
  Optional<Comment> findOneByIdAndDeletedFalse(String id);

  List<Comment> findOneByParentIdInAndDeletedFalse(Iterable<String> id);

  Integer countByParentIdAndDeletedFalse(String parentId);

  Integer countByUserIdAndSpaceIdAndTypeAndDeletedFalse(
      Long userId, Long spaceId, CommentType type);

  List<Comment> deleteByParentId(String parentId);

  List<Comment> deleteByParentIdIn(Iterable<String> parentIds);
}
