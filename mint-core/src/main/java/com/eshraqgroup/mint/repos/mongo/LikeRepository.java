package com.eshraqgroup.mint.repos.mongo;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import com.eshraqgroup.mint.domain.mongo.Like;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ayman on 25/08/16. */
public interface LikeRepository extends AbstractMongoRepository<Like, String> {

	Optional<Like> findOneByUserIdAndParentIdAndDeletedFalse(Long userId, String parentId);

	List<Like> findByParentIdInAndDeletedFalse(Iterable<String> parentIds);

	default void deleteByParentId(String parentId) {
		deleteByParentIdIn(Collections.singleton(parentId));
	}

	default void deleteByParentIdIn(Iterable<String> parentIds) {
		List<Like> list = this.findByParentIdInAndDeletedFalse(parentIds);
		if (list != null && !list.isEmpty()) {
			delete(list);
		}
	}
}
