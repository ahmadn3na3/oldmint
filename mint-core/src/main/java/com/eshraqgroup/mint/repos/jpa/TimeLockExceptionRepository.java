package com.eshraqgroup.mint.repos.jpa;

import java.util.Optional;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.TimeLockException;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

@Repository
public interface TimeLockExceptionRepository extends AbstractRepository<TimeLockException, Long> {
  Optional<TimeLockException> findOneByIdAndTimeLockIdAndDeletedFalse(Long id, Long timeLockId);
}
