package com.eshraqgroup.mint.repos.abstractrepos;

import java.io.Serializable;
import java.util.Date;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.eshraqgroup.mint.domain.abstractdomain.AbstractEntity;
import com.eshraqgroup.mint.security.SecurityUtils;

public interface AbstractMongoRepository<T extends AbstractEntity, ID extends Serializable>
    extends MongoRepository<T, ID> {
  @Override
  default void delete(T arg0) {
    arg0.setDeleted(true);
    arg0.setDeletedDate(new Date());
    arg0.setDeletedBy(SecurityUtils.getCurrentUserLogin());
    this.save(arg0);
  }

  @Override
  default void delete(ID arg0) {
    T entity = this.findOne(arg0);
    entity.setDeleted(true);
    entity.setDeletedDate(new Date());
    entity.setDeletedBy(SecurityUtils.getCurrentUserLogin());
    this.save(entity);
  }

  @Override
  default void delete(Iterable<? extends T> arg0) {
    arg0.forEach(
        entity -> {
          entity.setDeleted(true);
          entity.setDeletedDate(new Date());
          entity.setDeletedBy(SecurityUtils.getCurrentUserLogin());
        });
    this.save(arg0);
  }
}
