package com.eshraqgroup.mint.repos.mongo;

import java.util.Optional;
import java.util.stream.Stream;
import com.eshraqgroup.mint.domain.mongo.UserAssessment;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ayman on 04/07/16. */
public interface UserAssessmentRepository extends AbstractMongoRepository<UserAssessment, String> {
  Stream<UserAssessment> findByAssessmentIdAndDeletedFalse(Long id);

  Stream<UserAssessment> findByAssessmentIdAndDeletedFalseOrderByTotalGradeAsc(Long id);

  Optional<UserAssessment> findOneByUserIdAndAssessmentIdAndDeletedFalse(
      Long userId, Long assessmentId);

  Integer countByAssessmentId(Long id);
}
