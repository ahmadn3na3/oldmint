package com.eshraqgroup.mint.repos.solrrepo;

import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.solrdomains.QuestionSolrDomain;

/** Created by ahmad on 5/25/17. */
@Repository
public interface QuestionSolrRepository extends SolrCrudRepository<QuestionSolrDomain, String> {}
