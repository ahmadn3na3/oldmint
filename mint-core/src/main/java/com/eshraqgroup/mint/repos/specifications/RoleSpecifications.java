package com.eshraqgroup.mint.repos.specifications;

import org.springframework.data.jpa.domain.Specification;
import com.eshraqgroup.mint.constants.UserType;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.Role;

/** Created by ayman on 21/06/16. */
public final class RoleSpecifications {
  private RoleSpecifications() {}

  public static Specification<Role> notDeleted() {
    return (root, user, cb) -> cb.equal(root.get("deleted"), false);
  }

  public static Specification<Role> hasType(UserType usertype) {
    return (root, role, cb) -> cb.equal(root.<UserType>get("type"), usertype);
  }

  public static Specification<Role> hasType(UserType... usertype) {
    return (root, role, cb) -> root.get("type").in(usertype);
  }

  public static Specification<Role> byOrganization(Organization organization) {
    return (root, role, cb) -> cb.equal(root.get("organization"), organization);
  }

  public static Specification<Role> byFoundation(Foundation foundation) {
    return (root, role, cb) -> cb.equal(root.get("foundation"), foundation);
  }
}
