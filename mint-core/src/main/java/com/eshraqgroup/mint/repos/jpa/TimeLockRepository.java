package com.eshraqgroup.mint.repos.jpa;

import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.jpa.Foundation;
import com.eshraqgroup.mint.domain.jpa.Organization;
import com.eshraqgroup.mint.domain.jpa.TimeLock;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractRepository;

@Repository
public interface TimeLockRepository extends AbstractRepository<TimeLock, Long> {
  Page<TimeLock> findByFoundationAndDeletedFalse(Foundation foundation, Pageable pageable);

  Page<TimeLock> findByOrganizationAndDeletedFalse(Organization organization, Pageable pageable);

  Stream<TimeLock> findByOrganizationAndDeletedFalse(Organization organization);

  Optional<TimeLock> findOneByNameAndOrganizationAndDeletedFalse(
      String name, Organization organization);
}
