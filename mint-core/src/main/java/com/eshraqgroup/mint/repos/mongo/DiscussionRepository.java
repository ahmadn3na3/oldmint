package com.eshraqgroup.mint.repos.mongo;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import com.eshraqgroup.mint.domain.mongo.Discussion;
import com.eshraqgroup.mint.repos.abstractrepos.AbstractMongoRepository;

/** Created by ayman on 25/08/16. */
@Repository
public interface DiscussionRepository extends AbstractMongoRepository<Discussion, String> {
  List<Discussion> findBySpaceIdAndDeletedFalseOrderByCreationDateDesc(Long spaceId);

  Page<Discussion> findBySpaceIdAndDeletedFalseOrderByCreationDateDesc(
      Long spaceId, Pageable pageRequest);

  Page<Discussion> findBySpaceId(Long spaceId, Pageable pageRequest);

  List<Discussion> findBySpaceIdAndDeletedTrueAndDeletedDateAfter(Long spaceId, Date since);

  List<Discussion> findBySpaceIdAndDeletedFalseAndLastModifiedDateAfter(Long spaceId, Date since);

  List<Discussion> findBySpaceIdAndLastModifiedDateIsNullAndDeletedFalseAndCreationDateAfter(
      Long spaceId, Date since);

  Optional<Discussion> findOneByIdAndDeletedFalse(String id);

  Integer countBySpaceIdAndOwnerIdAndDeletedFalse(Long SpaceId, Long ownerId);
}
