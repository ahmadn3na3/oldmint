package com.eshraqgroup.mint.constants;

/** Created by ahmad on 5/23/16. */
public enum UserRelationType {
  FOLLOWER,
  SUPERVISOR,
  BLOCKED
}
