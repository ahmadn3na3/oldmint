package com.eshraqgroup.mint.constants;

/** Created by ayman on 13/06/16. */
public enum AssessmentType {
  QUIZ,
  ASSIGNMENT,
  WORKSHEET,
  PRACTICE
}
