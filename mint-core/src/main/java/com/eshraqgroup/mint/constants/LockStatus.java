package com.eshraqgroup.mint.constants;

public enum LockStatus {
  LOCK,
  UNLOCK
}
