package com.eshraqgroup.mint.constants;

import org.springframework.security.core.GrantedAuthority;

/** Created by ahmad on 4/25/16. */
public enum UserType implements GrantedAuthority {
  SYSTEM_ADMIN,
  USER,
  FOUNDATION_ADMIN,
  ADMIN,
  SUPER_ADMIN;

  @Override
  public String getAuthority() {
    return this.name();
  }
}
