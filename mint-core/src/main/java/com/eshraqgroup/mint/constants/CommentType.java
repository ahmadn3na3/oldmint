package com.eshraqgroup.mint.constants;

public enum CommentType {
  DISCUSSION,
  ANNOTATION
}
