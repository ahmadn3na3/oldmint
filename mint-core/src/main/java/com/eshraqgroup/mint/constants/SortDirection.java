package com.eshraqgroup.mint.constants;

import org.springframework.data.domain.Sort;

/** Created by ahmad on 4/17/16. */
public enum SortDirection {
  ASCENDING(Sort.Direction.ASC),
  DESCENDING(Sort.Direction.DESC);

  private Sort.Direction value;

  SortDirection(Sort.Direction value) {
    this.value = value;
  }

  public Sort.Direction getValue() {
    return value;
  }
}
