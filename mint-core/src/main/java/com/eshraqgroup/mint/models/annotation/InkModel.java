package com.eshraqgroup.mint.models.annotation;

import java.util.ArrayList;
import java.util.List;

/** Created by ahmad on 11/27/16. */
public class InkModel {
  private List<Point> points = new ArrayList<>();
  private Integer pageNumber;
  private Double viewx;
  private Double viewy;

  public List<Point> getPoints() {
    return points;
  }

  public void setPoints(List<Point> points) {
    this.points = points;
  }

  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer pageNumber) {
    this.pageNumber = pageNumber;
  }

  public Double getViewx() {
    return viewx;
  }

  public void setViewx(Double viewx) {
    this.viewx = viewx;
  }

  public Double getViewy() {
    return viewy;
  }

  public void setViewy(Double viewy) {
    this.viewy = viewy;
  }
}
