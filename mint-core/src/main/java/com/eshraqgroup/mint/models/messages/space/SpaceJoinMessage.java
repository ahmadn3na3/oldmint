package com.eshraqgroup.mint.models.messages.space;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.eshraqgroup.mint.constants.JoinedStatus;
import com.eshraqgroup.mint.models.messages.From;
import com.eshraqgroup.mint.models.messages.user.UserInfoMessage;

public class SpaceJoinMessage extends SpaceInfoMessage {
  private UserInfoMessage joinedInfoMessage;
  private JoinedStatus joinedStatus;

  public SpaceJoinMessage() {}

  public SpaceJoinMessage(
      Long id,
      String name,
      String image,
      From from,
      String categoryName,
      Boolean isPrivate,
      UserInfoMessage joinedInfoMessage,
      JoinedStatus joinedStatus,
      String chatId) {
    super(id, name, image, from, categoryName, isPrivate, chatId);
    this.joinedInfoMessage = joinedInfoMessage;
    this.joinedStatus = joinedStatus;
  }

  public UserInfoMessage getJoinedInfoMessage() {
    return joinedInfoMessage;
  }

  public void setJoinedInfoMessage(UserInfoMessage joinedInfoMessage) {
    this.joinedInfoMessage = joinedInfoMessage;
  }

  public JoinedStatus getJoinedStatus() {
    return joinedStatus;
  }

  public void setJoinedStatus(JoinedStatus joinedStatus) {
    this.joinedStatus = joinedStatus;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("joinedInfoMessage", joinedInfoMessage)
        .append("joinedStatus", joinedStatus)
        .toString();
  }
}
