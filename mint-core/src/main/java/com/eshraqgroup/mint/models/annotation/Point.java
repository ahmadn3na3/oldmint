package com.eshraqgroup.mint.models.annotation;

/** Created by ayman on 03/08/16. */
public class Point {
  private Double x;
  private Double y;
  private Double density;
  private Integer type;

  public Point() {}

  public Point(Double x, Double y) {
    setX(x);
    setY(y);
  }

  public Double getX() {
    return x;
  }

  public void setX(Double x) {
    this.x = x;
  }

  public Double getY() {
    return y;
  }

  public void setY(Double y) {
    this.y = y;
  }

  public Double getDensity() {
    return density;
  }

  public void setDensity(Double density) {
    this.density = density;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
}
