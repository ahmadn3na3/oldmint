package com.eshraqgroup.mint.models.messages.discussion;

import org.apache.commons.lang3.builder.ToStringBuilder;
import com.eshraqgroup.mint.models.messages.From;

public class DiscussionMessage {
  private String id;
  private String title;
  private Long spaceId;
  private String spaceName;
  private String categoryName;
  private From from;

  public DiscussionMessage() {}

  public DiscussionMessage(
      String id, String title, Long spaceId, String spaceName, String categoryName, From from) {
    this.id = id;
    this.title = title;
    this.spaceId = spaceId;
    this.spaceName = spaceName;
    this.categoryName = categoryName;
    this.from = from;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Long getSpaceId() {
    return spaceId;
  }

  public void setSpaceId(Long spaceId) {
    this.spaceId = spaceId;
  }

  public String getSpaceName() {
    return spaceName;
  }

  public void setSpaceName(String spaceName) {
    this.spaceName = spaceName;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public From getFrom() {
    return from;
  }

  public void setFrom(From from) {
    this.from = from;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
        .append("id", id)
        .append("title", title)
        .append("spaceId", spaceId)
        .append("spaceName", spaceName)
        .append("categoryName", categoryName)
        .append("from", from)
        .toString();
  }
}
