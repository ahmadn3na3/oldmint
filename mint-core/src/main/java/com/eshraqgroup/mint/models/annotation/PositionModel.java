package com.eshraqgroup.mint.models.annotation;

/** Created by ayman on 02/08/16. */
public class PositionModel {

  private Integer pageNumber;

  private Double startX;
  private Double startY;
  private Double endX;
  private Double endY;
  private Double density;
  private Integer startIndex;
  private Integer endIndex;

  public PositionModel() {}

  public Integer getPageNumber() {
    return pageNumber;
  }

  public void setPageNumber(Integer pageNumber) {
    this.pageNumber = pageNumber;
  }

  public Double getStartX() {
    return startX;
  }

  public void setStartX(Double startX) {
    this.startX = startX;
  }

  public Double getStartY() {
    return startY;
  }

  public void setStartY(Double startY) {
    this.startY = startY;
  }

  public Double getEndX() {
    return endX;
  }

  public void setEndX(Double endX) {
    this.endX = endX;
  }

  public Double getEndY() {
    return endY;
  }

  public void setEndY(Double endY) {
    this.endY = endY;
  }

  public Integer getStartIndex() {
    return startIndex;
  }

  public void setStartIndex(Integer startIndex) {
    this.startIndex = startIndex;
  }

  public Integer getEndIndex() {
    return endIndex;
  }

  public void setEndIndex(Integer endIndex) {
    this.endIndex = endIndex;
  }

  public Double getDensity() {
    return density;
  }

  public void setDensity(Double density) {
    this.density = density;
  }
}
