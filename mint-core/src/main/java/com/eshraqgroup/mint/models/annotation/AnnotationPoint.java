package com.eshraqgroup.mint.models.annotation;

/** Created by ahmad on 2/23/17. */
public class AnnotationPoint extends Point {
  private Double x2;
  private Double y2;
  private Double x3;
  private Double y3;

  public Double getX2() {
    return x2;
  }

  public void setX2(Double x2) {
    this.x2 = x2;
  }

  public Double getY2() {
    return y2;
  }

  public void setY2(Double y2) {
    this.y2 = y2;
  }

  public Double getX3() {
    return x3;
  }

  public void setX3(Double x3) {
    this.x3 = x3;
  }

  public Double getY3() {
    return y3;
  }

  public void setY3(Double y3) {
    this.y3 = y3;
  }
}
