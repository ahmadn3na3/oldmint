package com.eshraqgroup.mint.models;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/** Created by ayman on 14/03/17. */
public class PageRequestModel extends PageRequest {

  private PageRequestModel(int page, int size) {
    super(page, size);
  }

  public static PageRequest getPageRequestModel(Integer page, Integer size) {
    return getPageRequestModel(page, size, null);
  }

  public static PageRequest getPageRequestModel(Integer page, Integer size, Sort sort) {
    PageRequest pageRequest = new PageRequest(0, Integer.MAX_VALUE);
    if (null != page && null != size && sort != null) {
      pageRequest = new PageRequest((page > 0) ? page : 0, size, sort);

    } else if (null != page && null != size) {
      pageRequest = new PageRequest((page > 0) ? page : 0, size);
    } else if (null == page && null == size && sort != null) {
      pageRequest = new PageRequest(0, Integer.MAX_VALUE, sort);
    }
    return pageRequest;
  }
}
