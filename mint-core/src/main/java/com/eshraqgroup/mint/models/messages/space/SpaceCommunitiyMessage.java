package com.eshraqgroup.mint.models.messages.space;

import java.util.List;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;

/** Created by ahmad on 2/7/17. */
public class SpaceCommunitiyMessage extends BaseNotificationMessage {

  private List<String> userNames;

  public List<String> getUserNames() {
    return userNames;
  }

  public void setUserNames(List<String> userNames) {
    this.userNames = userNames;
  }
}
