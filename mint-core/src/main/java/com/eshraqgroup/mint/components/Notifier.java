package com.eshraqgroup.mint.components;

import java.time.ZonedDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.models.messages.BaseNotificationMessage;
import com.eshraqgroup.mint.models.messages.From;
import com.eshraqgroup.mint.models.messages.Target;

/** Created by ayman on 28/02/17. */
@Component
public class Notifier {
  public static Logger logger = LoggerFactory.getLogger(Notifier.class);

  @Autowired RabbitTemplate rabbitTemplate;

  public Notifier() {}

  @Async
  public void send(String exchange, String routingKey, Object message) {
    rabbitTemplate.setRoutingKey(routingKey);
    rabbitTemplate.setExchange(exchange);
    rabbitTemplate.convertAndSend(message);
  }

  @Async
  public void buildMessageAndSend(
      String exchnage,
      String routingKey,
      long senderId,
      String senderName,
      int notificationCategory,
      int action,
      int type,
      String targetId) {

    BaseNotificationMessage message = new BaseNotificationMessage();

    From from = new From();
    Target target = new Target();

    from.setId(senderId);
    from.setName(senderName);

    target.setAction(action);
    target.setType(type);
    target.setId(targetId);

    message.setDate(ZonedDateTime.now());
    message.setNotificationCategory(notificationCategory);
    message.setFrom(from);
    message.setTarget(target);

    this.send(exchnage, routingKey, message);
  }
}
