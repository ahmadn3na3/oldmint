package com.eshraqgroup.mint.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.AbstractAuthenticationTargetUrlRequestHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import com.eshraqgroup.mint.models.ResponseModel;
import com.fasterxml.jackson.databind.ObjectMapper;

/** Spring Security logout handler, specialized for Ajax requests. */
@Component
public class AjaxLogoutSuccessHandler extends AbstractAuthenticationTargetUrlRequestHandler
    implements LogoutSuccessHandler {

  public static final String BEARER_AUTHENTICATION = "Bearer ";
  private static final String HEADER_AUTHORIZATION = "Authorization";

  @Autowired private TokenStore tokenStore;

  @Override
  public void onLogoutSuccess(
      HttpServletRequest request, HttpServletResponse response, Authentication authentication)
      throws IOException, ServletException {

    // Request the token
    String token = request.getHeader(HEADER_AUTHORIZATION);

    if (token != null && token.startsWith(BEARER_AUTHENTICATION)) {
      String accessTokenValue = token.split(" ")[1];

      OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(accessTokenValue);
      if (oAuth2AccessToken != null) {
        OAuth2RefreshToken oAuth2RefreshToken = oAuth2AccessToken.getRefreshToken();
        if (oAuth2RefreshToken != null) tokenStore.removeRefreshToken(oAuth2RefreshToken);

        tokenStore.removeAccessToken(oAuth2AccessToken);
      }
    }

    response.setStatus(HttpServletResponse.SC_OK);
    response.setStatus(HttpServletResponse.SC_OK);
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    new ObjectMapper().writeValue(response.getOutputStream(), ResponseModel.done());
  }
}
